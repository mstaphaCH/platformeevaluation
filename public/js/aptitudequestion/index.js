/* Add button question aptitude */
$('.question-button-add-apt').on('click', function() {
	var form = $('.question-form-add-apt');
	form.find('.contact-error').fadeOut();
	form.find('.contact-success').fadeOut();
	form.find('.contact-loading').fadeOut();
	if($('.question-form-add-apt').valid()) {
		var action = form.attr('action');
		$.ajax({
			url : action,
			type: "POST",
			data: {
				qst_num: form.find('.qst_num').val(),
				qst_desc: form.find('.qst_desc').val(),
				qst_imp: form.find('.qst_imp').val(),

				qst_op1: form.find('.qst_op1').val(),
				qst_op2: form.find('.qst_op2').val(),
				qst_op3: form.find('.qst_op3').val(),
				qst_op4: form.find('.qst_op4').val(),
				qst_val_op1: form.find('.qst_val_op1').val(),
				qst_val_op2: form.find('.qst_val_op2').val(),
				qst_val_op3: form.find('.qst_val_op3').val(),
				qst_val_op4: form.find('.qst_val_op4').val(),

				code_option_a: form.find('[name="code_option_a"]').val(),
				code_option_b: form.find('[name="code_option_b"]').val(),
				code_option_c: form.find('[name="code_option_c"]').val(),
				code_option_d: form.find('[name="code_option_d"]').val(),

				qst_expl: form.find('.qst_expl').val(),
				qst_tpe: form.find('#qst_tpe').val(),
			},
			success: function(result) {
				console.log(result);
				window.location = URL+'AptitudeQuestion';
			},
			error: function(result) {
				form.find('.contact-loading').fadeOut();
				form.find('.contact-error').find('.message').html('Désolé, une erreur est survenue');
				form.find('.contact-error').fadeIn();
			}
		});
	}else{
		form.find('.contact-error').fadeOut();
		form.find('.contact-success').fadeOut();
		form.find('.contact-loading').fadeOut();
		form.find('.contact-error').find('.message').html('Veuillez à remplir tous les champs');
		form.find('.contact-error').fadeIn();
	}
	return false;
});
