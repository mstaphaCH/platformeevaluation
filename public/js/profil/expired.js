(function ($) {
	"use strict";

	$(document).ready(function(){

		/* Show/Hide sections */
		$(".user-list-profil").hide();
		$(".modprofform").hide();
		$(".chgpaswd").show();
		$(".chgpaswd_2").hide();


		$(".modprof").on('click', function(){
			$(".user-list-profil").hide();
			$(".chgpaswd").hide();
			$(".chgpaswd_2").hide();
			$(".modprofform").show();
		});
		// $(".paswd").on('click', function(){
		// });

		$(".paswd_2").on('click', function(){
			$(".user-list-profil").hide();
			$(".modprofform").hide();
			$(".chgpaswd_2").show();
			$(".chgpaswd").hide();
		});

		$(".return.add.1st-passwd").on('click', function(){
			$(".chgpaswd").hide();
			$(".chgpaswd_2").hide();
			$(".modprofform").hide();
			$(".user-list-profil").show();
		});
		$(".return.add.2nd-passwd").on('click', function(){
			$(".user-list-profil").show();
			$(".modprofform").hide();
			$(".chgpaswd").hide();
			$(".chgpaswd_2").hide();
		});
		
		$(".label_better").label_better({
	        position: "top",
		    easing: "bounce",
		    offset: 0
	    });

		/* Form validation */
		$( ".passwd" ).validate({
			rules: {
				'new_passwd': {required:true, noOnlyWhiteSpaces:true, noWhiteSpaces:true, rangelength:[9,60]},
				'new_repasswd': {required:true, noOnlyWhiteSpaces:true, noWhiteSpaces:true, rangelength:[9,60], passwordMuch:'.new_passwd'}
			},
			messages: {
				'new_passwd': 
					{required: "La saisie d'un mot de passe est obligatoire",
					noWhiteSpaces: noWhiteSpaces_check,
					noOnlyWhiteSpaces: noOnlyWhiteSpaces_check,
					rangelength: "Le mot de passe doit avoir au minimum 9 caractères"},
				'new_repasswd': 
					{required: "La re-saisie d'un mot de passe est obligatoire",
					noWhiteSpaces: noWhiteSpaces_check,
					rangelength: "Le mot de passe doit avoir au minimum 9 caractères",
					passwordMuch: "Mots de passe non identiques"}
			}
		});

		/* Add form button */
		$('.add.1st-passwd').on('click', function() {
			var form = $('.passwd');
			form.find('.contact-error').fadeOut();
			form.find('.contact-success').fadeOut();
			form.find('.contact-loading').fadeOut();
			if(form.valid()) {
				var action = form.attr('action');
				$.ajax({
					url : action,
					type: "POST",
					data: {
						user_id: form.find('.thisID').val(),
						new_passwd: form.find('.new_passwd').val(),
						new_repasswd: form.find('.new_repasswd').val()
					},
					success: function(result) {
						console.log(result);
						refresh(URL + 'Account');
					},
					error: function(result) {
						form.find('.contact-loading').fadeOut();
						form.find('.contact-error').find('.message').html('Désolé, une erreur est survenue');
						form.find('.contact-error').fadeIn();
					}
				});
			}else{
				form.find('.contact-error').fadeOut();
				form.find('.contact-success').fadeOut();
				form.find('.contact-loading').fadeOut();
				form.find('.contact-error').find('.message').html('Veuillez à remplir tous les champs');
				form.find('.contact-error').fadeIn();
			}
			return false;
		});

		$('.image-upload').on('change', function(e) {
			e.preventDefault();
			var files = e.target.files;
			if (files.length > 0) {
				if (window.FormData !== undefined) {
					var data = new FormData();
                    data.append("file", files[0]);
                    console.log(data);

              		$.ajax({
						url: URL+'Profil/ImageUpload',
						type: 'post',
						data: data,
						contentType: false,
						cache: false,
						processData: false,
						success: function(data) {
							console.log(data);
						}
					});
				}
			}

			// $('#message').empty();
			// $('#loading').show();

		});

		$('.add.2nd-passwd').on('click', function() {
			var form = $('.passwd_2');
			form.find('.contact-error').fadeOut();
			form.find('.contact-success').fadeOut();
			form.find('.contact-loading').fadeOut();
			console.log(form.find('.thisID').val());
			console.log(form.find('.new_passwd_2').val());
			console.log(form.find('.new_repasswd_2').val());
			if(form.valid()) {
				var action = form.attr('action');
				$.ajax({
					url : action,
					type: "POST",
					data: {
						user_id: form.find('.thisID').val(),
						new_passwd_2: form.find('.new_passwd_2').val(),
						new_repasswd_2: form.find('.new_repasswd_2').val()
					},
					success: function(result) {
						console.log(result);
						refresh(URL + 'Profil');
					},
					error: function(result) {
						form.find('.contact-loading').fadeOut();
						form.find('.contact-error').find('.message').html('Désolé, une erreur est survenue');
						form.find('.contact-error').fadeIn();
					}
				});
			}else{
				form.find('.contact-error').fadeOut();
				form.find('.contact-success').fadeOut();
				form.find('.contact-loading').fadeOut();
				form.find('.contact-error').find('.message').html('Veuillez à remplir tous les champs');
				form.find('.contact-error').fadeIn();
			}
			return false;
		});

	});

})(jQuery);
