(function ($) {
	"use strict";

$(document).ready(function(){
	/* Info button redirection */
	$(document).delegate('info-button.question.competence','click',function(e) {
		var x = $(this).attr('id');
		window.location = URL+'CompetenceQuestion/Details/'+x;
	});

	$(document).delegate('info-button.question.aptitude','click',function(e) {
		var x = $(this).attr('id');
		window.location = URL+'AptitudeQuestion/Details/'+x;
	});

	$(document).delegate('.question-button.competence.details', 'click', function(e) {
		//window.location = URL+'CompetenceQuestion';
	});

	$(document).delegate('.question-button.aptitude.details', 'click', function(e) {
		window.location = URL+'AptitudeQuestion';
	});

	/* competence question form */
	$('.question-form').validate({
		rules: {
			'qst_num': {number:true, min:1, max:999999, required:true, remote:{url: URL+'CompetenceQuestion/checkQuestionId', type:'post'}},
			'qst_desc': {required:true,	noOnlyWhiteSpaces:true, rangelength:[10,2000]},
			'qst_thm': {required:true},
			'qst_imp': {maxlength:1000},
			'qst_comp': {required:true},
			'question_typologie': {required:true},
			'qst_tpe': {required:true},
			'qst_obj': {maxlength:1000},
			'qst_op1': {required:true, noOnlyWhiteSpaces:true, rangelength:[1,2000]},
			'qst_op2': {required:true, noOnlyWhiteSpaces:true, rangelength:[1,2000]},
			'qst_op3': {required:true, noOnlyWhiteSpaces:true, rangelength:[1,2000]},
			'qst_op4': {required:true, noOnlyWhiteSpaces:true, rangelength:[1,2000]},
			'qst_val_op1': {number:true, required:true, min:0, max:9999},
			'qst_val_op2': {number:true, required:true, min:0, max:9999},
			'qst_val_op3': {number:true, required:true, min:0, max:9999},
			'qst_val_op4': {number:true, required:true, min:0, max:9999},
			'qst_rep_cor': {required:true},
			'qst_expl': {maxlength:1000}
		},
		messages: {
			'qst_num': {
				number: "Veuillez à entrer un entier positive",
				min: "Veuillez à entrer un numéro entre 1 et 999999",
				max: "Veuillez à entrer un numéro entre 1 et 999999",
				required: "Veuillez à entrer un numéro entre 1 et 999999",
				remote: "Numéro utilisé"
			},
			'qst_desc': 
			{
				required: "Veuillez à saisir une question",
				noOnlyWhiteSpaces: noOnlyWhiteSpaces_check,
				rangelength: "Veuillez à entrer une question entre 10 et 2000 caractères"
			},
			'qst_thm': { required: "Veuillez à choisir un thème" },
			'qst_imp': { maxlength: "Veuillez à ne pas dépasser 1000 caractères" },
			'qst_comp': { required: "Veuillez à choisir la complexité" },
			'question_typologie': {required: "Veuillez à choisir la typologie"},
			'qst_tpe': {required: "Veuillez à choisir le type de la question"},
			'qst_obj': {maxlength: "Veuillez à ne pas dépasser 1000 caractères"},
			'qst_op1': 
			{
				required: "Veuillez à saisir l'option",
				noOnlyWhiteSpaces: noOnlyWhiteSpaces_check,
				rangelength: "Veuillez à saisir une option entre 1 et 2000 caractères"
			},
			'qst_op2': 
			{
				required: "Veuillez à saisir l'option",
				noOnlyWhiteSpaces: noOnlyWhiteSpaces_check,
				rangelength: "Veuillez à saisir une option entre 1 et 2000 caractères"
			},
			'qst_op3': 
			{
				required: "Veuillez à saisir l'option",
				noOnlyWhiteSpaces: noOnlyWhiteSpaces_check,
				rangelength: "Veuillez à saisir une option entre 1 et 2000 caractères"
			},
			'qst_op4': 
			{
				required: "Veuillez à saisir l'option",
				noOnlyWhiteSpaces: noOnlyWhiteSpaces_check,
				rangelength: "Veuillez à saisir une option entre 1 et 2000 caractères"
			},
			'qst_val_op1': 
			{
				number: "Veuillez à saisir un nombre valide",
				required: "Veuillez à saisir un nombre entre 0 et 9999",
				min: "Veuillez à saisir un nombre entre 0 et 9999",
				max: "Veuillez à saisir un nombre entre 0 et 9999"
			},
			'qst_val_op2': 
			{
				number: "Veuillez à saisir un nombre valide",
				required: "Veuillez à saisir un nombre entre 0 et 9999",
				min: "Veuillez à saisir un nombre entre 0 et 9999",
				max: "Veuillez à saisir un nombre entre 0 et 9999"
			},
			'qst_val_op3': 
			{
				number: "Veuillez à saisir un nombre valide",
				required: "Veuillez à saisir un nombre entre 0 et 9999",
				min: "Veuillez à saisir un nombre entre 0 et 9999",
				max: "Veuillez à saisir un nombre entre 0 et 9999"
			},
			'qst_val_op4': 
			{
				number: "Veuillez à saisir un nombre valide",
				required: "Veuillez à saisir un nombre entre 0 et 9999",
				min: "Veuillez à saisir un nombre entre 0 et 9999",
				max: "Veuillez à saisir un nombre entre 0 et 9999"
			},
			'qst_rep_cor': {required: "Veuillez à choisir la réponse correcte"},
			'qst_expl': {maxlength: "Veuillez à ne pas dépasser 1000 caractères"}
		}
	});

	/* aptitude question form */
	$('.question-form-apt').validate({
		rules: {
			'qst_num': {number:true, min:1, max:999999, required:true, remote:{url: URL+'CompetenceQuestion/checkQuestionId', type:'post'}},
			'qst_desc': {required:true,	noOnlyWhiteSpaces:true, rangelength:[10,2000]},
			'qst_imp': {maxlength:1000},
			'qst_tpe': {required:true},
			'qst_op1': {required:true, noOnlyWhiteSpaces:true, rangelength:[1,2000]},
			'qst_op2': {required:true, noOnlyWhiteSpaces:true, rangelength:[1,2000]},
			'qst_op3': {required:true, noOnlyWhiteSpaces:true, rangelength:[1,2000]},
			'qst_op4': {required:true, noOnlyWhiteSpaces:true, rangelength:[1,2000]},
			'qst_val_op1': {number:true, required:true, min:0, max:9999},
			'qst_val_op2': {number:true, required:true, min:0, max:9999},
			'qst_val_op3': {number:true, required:true, min:0, max:9999},
			'qst_val_op4': {number:true, required:true, min:0, max:9999},

			'code_option_a': {required:true},
			'code_option_b': {required:true},
			'code_option_c': {required:true},
			'code_option_d': {required:true},

			'qst_expl': {maxlength:1000}
		},
		messages: {
			'qst_num': 
			{
				number: "Veuillez à entrer un entier positive",
				min: "Veuillez à entrer un numéro entre 1 et 999999",
				max: "Veuillez à entrer un numéro entre 1 et 999999",
				required: "Veuillez à entrer un numéro entre 1 et 999999",
				remote: "Numéro utilisé"
			},
			'qst_desc': 
			{
				required: "Veuillez à saisir une question",
				noOnlyWhiteSpaces: noOnlyWhiteSpaces_check,
				rangelength: "Veuillez à entrer une question entre 10 et 2000 caractères"
			},
			'qst_imp': { maxlength: "Veuillez à ne pas dépasser 1000 caractères" },
			'qst_tpe': {required: "Veuillez à choisir le type de la question"},
			'qst_op1': 
			{
				required: "Veuillez à saisir l'option",
				noOnlyWhiteSpaces: noOnlyWhiteSpaces_check,
				rangelength: "Veuillez à saisir une option entre 1 et 2000 caractères"
			},
			'qst_op2': 
			{
				required: "Veuillez à saisir l'option",
				noOnlyWhiteSpaces: noOnlyWhiteSpaces_check,
				rangelength: "Veuillez à saisir une option entre 1 et 2000 caractères"
			},
			'qst_op3': 
			{
				required: "Veuillez à saisir l'option",
				noOnlyWhiteSpaces: noOnlyWhiteSpaces_check,
				rangelength: "Veuillez à saisir une option entre 1 et 2000 caractères"
			},
			'qst_op4': 
			{
				required: "Veuillez à saisir l'option",
				noOnlyWhiteSpaces: noOnlyWhiteSpaces_check,
				rangelength: "Veuillez à saisir une option entre 1 et 2000 caractères"
			},
			'qst_val_op1': 
			{
				number: "Veuillez à saisir un nombre valide",
				required: "Veuillez à saisir un nombre entre 0 et 9999",
				min: "Veuillez à saisir un nombre entre 0 et 9999",
				max: "Veuillez à saisir un nombre entre 0 et 9999"
			},
			'qst_val_op2': 
			{
				number: "Veuillez à saisir un nombre valide",
				required: "Veuillez à saisir un nombre entre 0 et 9999",
				min: "Veuillez à saisir un nombre entre 0 et 9999",
				max: "Veuillez à saisir un nombre entre 0 et 9999"
			},
			'qst_val_op3': 
			{
				number: "Veuillez à saisir un nombre valide",
				required: "Veuillez à saisir un nombre entre 0 et 9999",
				min: "Veuillez à saisir un nombre entre 0 et 9999",
				max: "Veuillez à saisir un nombre entre 0 et 9999"
			},
			'qst_val_op4': 
			{
				number: "Veuillez à saisir un nombre valide",
				required: "Veuillez à saisir un nombre entre 0 et 9999",
				min: "Veuillez à saisir un nombre entre 0 et 9999",
				max: "Veuillez à saisir un nombre entre 0 et 9999"
			},

			'code_option_a': {required: "Veuillez à selectionner une option"},
			'code_option_b': {required: "Veuillez à selectionner une option"},
			'code_option_c': {required: "Veuillez à selectionner une option"},
			'code_option_d': {required: "Veuillez à selectionner une option"},

			'qst_expl': {maxlength: "Veuillez à ne pas dépasser 1000 caractères"}
		}
	});

	/* Add button question competence */
	// $('.question-form-add').on('submit', function(e) {
	// 	e.preventDefault();
	// 	var formData = new FormData($(this)[0]);

	// 	if($('.question-form-add').valid()) {
	// 		var action = URL+'CompetenceQuestion/Add';
	// 		$.ajax({
	// 			url : action,
	// 			type: "POST",
	// 			contentType: false,
	// 			cache: false,
	// 			processData: false,
	// 			data: formData,
	// 			success: function(result) {
	// 				console.log(result);
	// 				window.location = URL+'CompetenceQuestion';
	// 			},
	// 			error: function(result) {
	// 				console.log("error");
	// 			}
	// 		});
	// 	}else{
	// 		console.log("There is empty fields");
	// 	}
	// 	return false;
	// });


	/* Add button question competence */
	$('.question-form.edit').on('submit', function(e) {
		e.preventDefault();
		var formData = new FormData($(this)[0]);
		
		if($('.question-form.edit').valid()) {
			var action = URL+'CompetenceQuestion/Update';
			$.ajax({
				url : action,
				type: "POST",
				contentType: false,
				cache: false,
				processData: false,
				data: formData,
				success: function(result) {
					console.log(result);
					window.location = URL+'CompetenceQuestion';
				},
				error: function(result) {
					console.log("error");
				}
			});
		}else{
			console.log("There is empty fields");
		}
	return false;
	});

	$('.question-form.competence').on('submit', function(e) {
		e.preventDefault();
		var formData = new FormData($(this)[0]);

		var form = $('.question-form-add.competence');
		form.find('.contact-error').fadeOut();
		form.find('.contact-success').fadeOut();
		form.find('.contact-loading').fadeOut();
		
		if($('.question-form').valid()) {
			var action = URL+'CompetenceQuestion/Add';
			$.ajax({
				url : action,
				type: "POST",
				contentType: false,
				cache: false,
				processData: false,
				data: formData,
				success: function(result) {
					console.log(result);
					window.location = URL+'CompetenceQuestion';
				},
				error: function(result) {
					form.find('.contact-loading').fadeOut();
					form.find('.contact-error').find('.message').html('Désolé, une erreur est survenue');
					form.find('.contact-error').fadeIn();
				}
			});
		}else{
			form.find('.contact-error').fadeOut();
			form.find('.contact-success').fadeOut();
			form.find('.contact-loading').fadeOut();
			form.find('.contact-error').find('.message').html('Veuillez à remplir tous les champs');
			form.find('.contact-error').fadeIn();
		}
	return false;
	});

	/* Add button question aptitude */
	$('.question-button-add.aptitude').on('click', function() {
		var form = $('.question-form-add.aptitude');
		form.find('.contact-error').fadeOut();
		form.find('.contact-success').fadeOut();
		form.find('.contact-loading').fadeOut();
		if($('.question-form-add.aptitude').valid()) {
			var action = form.attr('action');
			$.ajax({
				url : action,
				type: "POST",
				data: {
					qst_num: form.find('.qst_num').val(),
					qst_desc: form.find('.qst_desc').val(),
					qst_imp: form.find('.qst_imp').val(),

					qst_op1: form.find('.qst_op1').val(),
					qst_op2: form.find('.qst_op2').val(),
					qst_op3: form.find('.qst_op3').val(),
					qst_op4: form.find('.qst_op4').val(),
					qst_val_op1: form.find('.qst_val_op1').val(),
					qst_val_op2: form.find('.qst_val_op2').val(),
					qst_val_op3: form.find('.qst_val_op3').val(),
					qst_val_op4: form.find('.qst_val_op4').val(),

					code_option_a: form.find('[name="code_option_a"]').val(),
					code_option_b: form.find('[name="code_option_b"]').val(),
					code_option_c: form.find('[name="code_option_c"]').val(),
					code_option_d: form.find('[name="code_option_d"]').val(),

					qst_expl: form.find('.qst_expl').val(),
					qst_tpe: form.find('#qst_tpe').val(),
				},
				success: function(result) {
					console.log(result);
					window.location = URL+'AptitudeQuestion';
				},
				error: function(result) {
					form.find('.contact-loading').fadeOut();
					form.find('.contact-error').find('.message').html('Désolé, une erreur est survenue');
					form.find('.contact-error').fadeIn();
				}
			});
	}else{
		form.find('.contact-error').fadeOut();
		form.find('.contact-success').fadeOut();
		form.find('.contact-loading').fadeOut();
		form.find('.contact-error').find('.message').html('Veuillez à remplir tous les champs');
		form.find('.contact-error').fadeIn();
	}
	return false;
	});

	/* Add button question competence */
	/*var formElt = document.getElementById("form-add-competence");
	formElt.addEventListener("submit", function (e){

		alert('hi');
		
		if($('.question-form-add.competence').valid()) {
			
		}else{
			e.preventDefault();
		}
	});*/


	$(".save").on('click', function () {
		var checkbox_value = "";
		var idd = $(this).attr('id');
		$("input[type='checkbox'][name='"+idd+"']").each(function () {
			var ischecked = $(this).is(":checked");
			if (ischecked) {
				checkbox_value += "|" + $(this).val();
			}
			console.log(checkbox_value);
		});
		console.log("idd => "+idd);
		$.ajax({
			type: 'POST',
			url: URL+'AptitudeQuestion/UpdateEvaluations',
			data: { 
				'qst_id': idd,
				'checkbox': checkbox_value
			},
			success: function(result){
				console.log(result);
				$("#"+idd).fadeOut("slow");
				setTimeout(function(){
					$("#"+idd).text('succès');
					$("#"+idd).css('background-color','#0add08');
					$("#"+idd).fadeIn("slow");
				},1000);
				setTimeout(function(){
					$("#"+idd).fadeOut("slow");
				},3000);
				setTimeout(function(){
					$("#"+idd).text('enregistrer');
					$("#"+idd).css('background-color','');
					$("#"+idd).fadeIn("slow");
				},4000);
			},
			error: function(error){
				$("#"+idd).fadeOut("slow");
				setTimeout(function(){
					$("#"+idd).text("erreur");
					$("#"+idd).css('background-color','red');
					$("#"+idd).fadeIn("slow");
				},1000);
				setTimeout(function(){
					$("#"+idd).fadeOut("slow");
				},3000);
				setTimeout(function(){
					$("#"+idd).text('enregistrer');
					$("#"+idd).css('background-color','');
					$("#"+idd).fadeIn("slow");
				},4000);
			}
		});
	});

	/* Code multiple select option hide when selected */
	$('.code-option').on('change', function(){
		var j = 0, v = 0, b = 0, r = 0;
		var tab = [];
		$('.code-option :selected').each(function(i, sel){
			tab[i] = $(sel).val();
			if(tab[i] == 'j') { $(".code-option option[value='j']").addClass('hidden') }
			if(tab[i] == 'v') {	$(".code-option option[value='v']").addClass('hidden') }
			if(tab[i] == 'b') {	$(".code-option option[value='b']").addClass('hidden') }
			if(tab[i] == 'r') {	$(".code-option option[value='r']").addClass('hidden') }
		});
		for (var i = tab.length - 1; i >= 0; i--) {
			if (tab[i] == 'j') { j = 1; };
			if (tab[i] == 'v') { v = 1; };
			if (tab[i] == 'b') { b = 1; };
			if (tab[i] == 'r') { r = 1; };
		};
		if (j == 0) { $(".code-option option[value='j']").removeClass('hidden') };
		if (v == 0) { $(".code-option option[value='v']").removeClass('hidden') };
		if (b == 0) { $(".code-option option[value='b']").removeClass('hidden')	};
		if (r == 0) { $(".code-option option[value='r']").removeClass('hidden')	};
	});
	/* end */

});
})(jQuery);
