// $(document).ready(function(){
// 	$('.delete').on('click',function(dialog,op,id){
// 		var winW = window.innerWidth;
// 	    var winH = window.innerHeight;
// 		$('#dialogoverlay').attr('style','display:block');
// 		$('#dialogoverlay').attr('style','height:'+winH+'px');
// 		$('#dialogbox').attr('style','left:'+(winW/2)-(550*.5)+'px');
// 		$('#dialogbox').attr('style','top:100px');
// 		$('#dialogbox').attr('style','display:block');
		
// 		$('#dialogboxhead').html("Confirm that action");
// 	    $('#dialogboxbody').html(dialog);
// 		$('#dialogboxfoot').html('<button onclick="Confirm.yes(\''+op+'\',\''+id+'\')">Yes</button> <button onclick="Confirm.no()">No</button>');
// 	});
// });

function CustomAlert(){
	this.render = function(dialog){
		var winW = window.innerWidth;
	    var winH = window.innerHeight;
		var dialogoverlay = document.getElementById('dialogoverlay');
	    var dialogbox = document.getElementById('dialogbox');
		dialogoverlay.style.display = "block";
	    dialogoverlay.style.height = winH+"px";
		dialogbox.style.left = (winW/2) - (550 * .5)+"px";
	    dialogbox.style.top = "100px";
	    dialogbox.style.display = "block";
		document.getElementById('dialogboxhead').innerHTML = "Acknowledge This Message";
	    document.getElementById('dialogboxbody').innerHTML = dialog;
		document.getElementById('dialogboxfoot').innerHTML = '<button onclick="Alert.ok()">OK</button>';
	}
	this.ok = function(){
		document.getElementById('dialogbox').style.display = "none";
		document.getElementById('dialogoverlay').style.display = "none";
	}
}
var Alert = new CustomAlert();
function CustomConfirm(){
	this.render = function(dialog,op,object,id){
		var winW = window.innerWidth;
	    var winH = window.innerHeight;
		var dialogoverlay = document.getElementById('dialogoverlay');
	    var dialogbox = document.getElementById('dialogbox');
		dialogoverlay.style.display = "block";
	    dialogoverlay.style.height = winH+"px";
		dialogbox.style.left = (winW/2) - (550 * .5)+"px";
	    dialogbox.style.top = "100px";
	    dialogbox.style.display = "block";
		
		document.getElementById('dialogboxhead').innerHTML = "Confirm that action";
	    document.getElementById('dialogboxbody').innerHTML = dialog;
		document.getElementById('dialogboxfoot').innerHTML = '<button onclick="Confirm.yes(\''+op+'\',\''+object+'\',\''+id+'\')">Yes</button> <button onclick="Confirm.no()">No</button>';
	}
	this.no = function(){
		document.getElementById('dialogbox').style.display = "none";
		document.getElementById('dialogoverlay').style.display = "none";
	}
	this.yes = function(op,object,id){
		if(op === "delete"){
			if(object==="structure") {
				$.ajax({
					type: 'POST',
					url: URL + 'Structure/Delete',
					data: {
						'structure_id' : id
					},
					success: function(result) {
						Alert.render('Structure deleted successfully !');
						setTimeout(function() {
							window.location = 'Structure';
						}, 2000);
					},
					error: function(result) {
						console.log("failed");
					}
				});
			}else if(object==="user") {
				$.ajax({
					type: 'POST',
					url: URL + 'User/Delete',
					data: {
						'user_id' : id
					},
					success: function(result) {
						Alert.render('User deleted successfully !');
						setTimeout(function() {
							window.location = 'User';
						}, 2000);
					},
					error: function(result) {
						console.log("failed");
					}
				});
			}else if(object==="session") {
				$.ajax({
					type: 'POST',
					url: URL + 'Session/Delete',
					data: {
						'session_id' : id
					},
					success: function(result) {
						Alert.render('Session deleted successfully !');
						setTimeout(function() {
							window.location = 'Session';
						}, 2000);
					},
					error: function(result) {
						console.log("failed");
					}
				});
			}else if(object==="message") {
				$.ajax({
					type: 'POST',
					url: URL + 'Message/Delete',
					data: {
						'message_id' : id
					},
					success: function(result) {
						Alert.render('Message deleted successfully !');
						setTimeout(function() {
							window.location = 'Message';
						}, 2000);
					},
					error: function(result) {
						console.log("failed");
					}
				});
			}else if(object==="evaluation") {
				$.ajax({
					type: 'POST',
					url: URL + 'Evaluation/Delete',
					data: {
						'evaluation_id' : id
					},
					success: function(result) {
						Alert.render('Evaluation deleted successfully !');
						setTimeout(function() {
							window.location = 'Evaluation';
						}, 2000);
					},
					error: function(result) {
						console.log("failed");
					}
				});
			}else if(object==="theme") {
				$.ajax({
					type: 'POST',
					url: URL + 'Theme/Delete',
					data: {
						'theme_id' : id
					},
					success: function(result) {
						Alert.render('Theme deleted successfully !');
						setTimeout(function() {
							window.location = 'Theme';
						}, 2000);
					},
					error: function(result) {
						console.log("failed");
					}
				});
			}else if(object==="candidate") {
				$.ajax({
					type: 'POST',
					url: URL + 'Candidate/Delete',
					data: {
						'candidate_id' : id
					},
					success: function(result) {
						Alert.render('Candidate deleted successfully !');
						setTimeout(function() {
							window.location = 'Candidate';
						}, 2000);
					},
					error: function(result) {
						console.log("failed");
					}
				});
			}
		}
		document.getElementById('dialogbox').style.display = "none";
		document.getElementById('dialogoverlay').style.display = "none";
	}
}
var Confirm = new CustomConfirm();
