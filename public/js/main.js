var noOnlyWhiteSpaces_check = "Vous ne pouvez pas saisir que des espaces";
var noWhiteSpaces_check = "Les espaces ne sont pas permis";
var ville_required_lenght = "Veuillez saisir une ville entre 4 et 50 caractères";
var pays_required_lenght = "Veuillez saisir un pays entre 4 et 50 caractères";
var phone_required = "Veuillez saisir un numéro de téléphone";

(function ($) {
	"use strict";

	$('.user-code').hide();
	$('.login-code').hide();

	/* evaluation */
	$(".make-switch input").bootstrapSwitch('onText', 'Candidat libres');
	$(".make-switch input").bootstrapSwitch('offText', 'Entretien');
	// var stateValue = $(".make-switch input").is(":checked") ? $(".make-switch input").val() : "off";
	
	/* Show dropdown menu on hover */
	$('ul.nav li.dropdown').hover(function() {
		$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
	}, function() {
		$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
	});

$(document).ready(function () {//Function to prevent Default Events
	function pde(e){
		if(e.preventDefault)
			e.preventDefault();
		else
			e.returnValue = false;
}//$('.cell-block').css('height', $('.first-td').height());
/* fix the search bar on top */
$(function(){
	var shrinkHeader = 130;
	$(window).scroll(function() {
		var scroll = getCurrentScroll();
		if ( scroll >= shrinkHeader && $('.btn-plus').find('.ion-minus-round').length > 0) {
			$('.fixed-top').addClass('shrink');
			$('.fixed-horizontal').show();
			$('.fixed-horizontal').find('.th1').css('width', '30px');
			$('.fixed-horizontal').find('.th2').css('width', $('.thn2').width() + 10);
			$('.fixed-horizontal').find('.th3').css('width', $('.thn3').width() + 10);
			$('.fixed-horizontal').find('.th4').css('width', $('.thn4').width() + 10);
			$('.fixed-horizontal').find('.th5').css('width', $('.thn5').width() + 10);
		}else {
			$('.fixed-top').removeClass("shrink");
			$('.fixed-horizontal').hide();
			$('.shrink').css('padding-left', '0');
			$('.shrink').css('padding-right', '0');
		}
	});
	function getCurrentScroll() {
		return window.pageYOffset || document.documentElement.scrollTop;
	}
});
/* Hide/Show table content */
/* With fixed horizontal bar */
$('.thn1').find('.btn-plus').on('click', function(evt) {
	if ($('tbody').is(':visible')) {
		if ($('.fixed-horizontal').is(':visible')) {
			$('html, body').animate({scrollTop: '0'}, 1200, 'easeInOutCubic');
			setTimeout(function(evt){
				$('.ion-minus-round').attr('class', 'ion-plus-round');
				$('tbody').fadeOut();
			}, 1000);
		}else{
			$('tbody').fadeOut();
			$('.ion-minus-round').attr('class', 'ion-plus-round');
		}
	}else{
		$('tbody').fadeIn();
		$('.ion-plus-round').attr('class', 'ion-minus-round');
	}
});
/* Generique */
$('.table-hide').find('.btn-plus').on('click', function(evt) {
	if ($('.table-hide').find('tbody').is(':visible')) {
		$('.table-hide').find('tbody').fadeOut();
		$('.table-hide').find('.ion-minus-round').attr('class', 'ion-plus-round');
	}else{
		$('.table-hide').find('tbody').fadeIn();
		$('.table-hide').find('.ion-plus-round').attr('class', 'ion-minus-round');
	}
});
/* If there is two table */
$('.table-hide-1').find('.btn-plus').on('click', function(evt) {
	if ($('.table-hide-1').find('tbody').is(':visible')) {
		$('.table-hide-1').find('tbody').fadeOut();
		$('.table-hide-1').find('.ion-minus-round').attr('class', 'ion-plus-round');
	}else{
		$('.table-hide-1').find('tbody').fadeIn();
		$('.table-hide-1').find('.ion-plus-round').attr('class', 'ion-minus-round');
	}
});
$('.table-hide-2').find('.btn-plus').on('click', function(evt) {
	if ($('.table-hide-2').find('tbody').is(':visible')) {
		$('.table-hide-2').find('tbody').fadeOut();
		$('.table-hide-2').find('.ion-minus-round').attr('class', 'ion-plus-round');
	}else{
		$('.table-hide-2').find('tbody').fadeIn();
		$('.table-hide-2').find('.ion-plus-round').attr('class', 'ion-minus-round');
	}
});
/**/

/* evaluation test */
var pos = 0, test, test_status, question, choice, choices, chA, chB, chC, correct = 0;
var questions = [
[ "What is 10 + 4?", "12", "14", "16", "B" ],
[ "What is 20 - 9?", "7", "13", "11", "C" ],
[ "What is 7 x 3?", "21", "24", "25", "A" ],
[ "What is 8 / 2?", "10", "2", "4", "C" ]
];
function renderQuestion() {
	if(pos >= questions.length) {
		$("#test").html("<h2>You got "+correct+" of "+questions.length+" questions correct</h2>");
		$("#test_status").html("Test Completed");
		pos = 0;
		correct = 0;
		return false;
	}
	$("#test_status").html("Question "+(pos+1)+" of "+questions.length);
	question = questions[pos][0];
	chA = questions[pos][1];
	chB = questions[pos][2];
	chC = questions[pos][3];
	var htmll = ''
	htmll += "<h3>"+question+"</h3>";
	htmll += "<input type='radio' name='choices' value='A'> "+chA+"<br>";
	htmll += "<input type='radio' name='choices' value='B'> "+chB+"<br>";
	htmll += "<input type='radio' name='choices' value='C'> "+chC+"<br><br>";
	htmll += "<button id='answerBtn'>Submit Answer</button>";
	$("#test").html(htmll);
}
renderQuestion();
$('#answerBtn').on('click', function(){
	choices = $('[Name="choices"]');
	for(var i=0; i<choices.length; i++){
		if(choices[i].checked){
			choice = choices[i].value;
		}
	}
	if(choice == questions[pos][4]){
		correct++;
	}
	pos++;
	renderQuestion();
});
/* end */
$('.eval_status').on('click', function(){
	var evalName = $(this).attr('name');
	var evalId = '#' + $(this).attr('id');

	$.ajax({
		type: 'POST',
		url: 'Evaluation/changeEvaluationStatus',
		data: {
			'eval_id' : evalName
		},
		success: function(result) {
			if($(evalId).find('.ion-ios-checkmark-outline').length) {
				$(evalId).find('i').addClass('ion-ios-circle-outline');
				$(evalId).find('i').removeClass('ion-ios-checkmark-outline');
			}else if($(evalId).find('.ion-ios-circle-outline').length) {
				$(evalId).find('i').addClass('ion-ios-checkmark-outline');
				$(evalId).find('i').removeClass('ion-ios-circle-outline');
			}
		}
	});
});

/* $('.horizontal').perfectScrollbar();	*/
/* Forms validations */
$.validator.addMethod(
	'telnbrvalid',
	function(value, element, params){
		return $("#str_tel").intlTelInput("isValidNumber");},
		'Téléphone number is not valid'
		);
$.validator.addMethod(
	'telnbrtooshort',
	function(value, element, params){
		var error = $("#str_tel").intlTelInput("getValidationError");
		if(error == intlTelInputUtils.validationError.TOO_SHORT) {return false;}
		else{return true;}},
		'Téléphone number is too short'
		);
$.validator.addMethod(
	'noOnlyWhiteSpaces',
	function(value, element, params){
		var value = this.elementValue(element).replace(/\s+/g, ' ').trim();
		if(value===""){return false;}else{return true;}},
		'White spaces only not permitted'
		);
$.validator.addMethod(
	'noWhiteSpaces',
	function(value, element, params){
		var value = this.elementValue(element);
		return !/\s/g.test(value);
	},
	'White spaces not permitted'
	);
$.validator.addMethod(
	'passwordMuch',
	function(value, element, params){
		var value = this.elementValue(element);
		if($(params).val()===value){return true;}else{return false;};
	},
	'Passwords doesn\'t much'
	);
$.validator.addMethod(
	'codeMuch',
	function(value, element, params){
		var value = this.elementValue(element);
		if($(params).val()===value){return true;}else{return false;};
	},
	'Codes doesn\'t much'
	);

$.validator.addMethod(
	'twoAlpha',
	function(value, element, params){
		var value = this.elementValue(element);
		var i=0;
		var b=0;
		var element =value;

		for (i=0;i<element.length;i++) {
			if (isNaN(element[i])) {
				if ((element[i].toUpperCase()>='A') && (element[i].toUpperCase()<='Z'))
					b++;
				else return false;
			}
		}
		if(b>=2) return true;
		return false;
	},
	'Two alphabetic required'
	);

$('.evaluation-form-add').validate({
	rules: {
		'eval_titre': {required:true, noOnlyWhiteSpaces:true, rangelength:[4,50], remote:{url:'Evaluation/checkEvalName', type:'post'}},
		'eval_tpe': {required:true},
		'eval_tmp_h': {required:true, number:true, min:0, max:24},
		'eval_tmp_m': {required:true, number:true, min:0, max:59},
		'eval_seuil': {required:true, number:true, min:1, max:100},
		'eval_desc': {maxlength:1000}},
		messages: {
			'eval_titre': 
			{required: "Veuillez saisir un titre d'évaluation",
			noOnlyWhiteSpaces: noOnlyWhiteSpaces_check,
			rangelength: "Veuillez saisir un titre entre 4 et 50 caractères",
			remote: "Le titre de l'évaluation est utilisé"},
			'eval_tpe': 
			{required: "Veuillez choisir le type de l'évaluation"},
			'eval_tmp_h': {required: "[0, 24]", number: "[0, 24]", min: "[0, 24]", max: "[0, 24]"},
			'eval_tmp_m': {required: "[0, 59]", number: "[0, 59]", min: "[0, 59]", max: "[0, 59]"},
			'eval_seuil': {required: "[1, 100]", number: "[1, 100]", min: "[1, 100]", max: "[1, 100]"},
			'eval_desc': {maxlength: "Veuillez à ne pas dépasser 1000 caractères"}
		}
	});
$('.evaluation-form-edit').validate({
	rules: {
		'eval_titre': {required:true, noOnlyWhiteSpaces:true, rangelength:[4,50]},
		'eval_tpe': {required:true},
		'eval_tmp_h': {required:true, number:true, min:0, max:24},
		'eval_tmp_m': {required:true, number:true, min:0, max:59},
		'eval_seuil': {required:true, number:true, min:1, max:100},
		'eval_desc': {maxlength:1000}},
		messages: {
			'eval_titre': 
			{required: "Veuillez saisir un titre d'évaluation",
			noOnlyWhiteSpaces: noOnlyWhiteSpaces_check,
			rangelength: "Veuillez saisir un titre entre 4 et 50 caractères"},
			'eval_tpe': 
			{required: "Veuillez choisir le type de l'évaluation"},
			'eval_tmp_h': 
			{required: "[0, 24]",
			number: "[0, 24]",
			min: "[0, 24]",
			max: "[0, 24]"},
			'eval_tmp_m': 
			{required: "[0, 59]",
			number: "[0, 59]",
			min: "[0, 59]",
			max: "[0, 59]"},
			'eval_seuil': 
			{required: "[1, 100]",
			number: "[1, 100]",
			min: "[1, 100]",
			max: "[1, 100]"},
			'eval_desc': 
			{maxlength: "Veuillez à ne pas dépasser 1000 caractères"}
		}
	});

$(".theme-form").validate({
	rules: {
		'theme_title': {required:true, noOnlyWhiteSpaces:true, rangelength:[3,50]},
		'theme_description': {maxlength:1000},
		'theme_weighting': {number:true, min:0, max:1000}},
		messages: {
			'theme_title': {
				required: "Veuillez saisir un nom de thème",
				noOnlyWhiteSpaces: noOnlyWhiteSpaces_check,
				rangelength: "Veuillez saisir un nom de thème entre 3 et 50 caractères"
			},
				'theme_description': {
					maxlength: "Veuillez à ne pas dépasser 1000 caractères"
				},
					'theme_weighting': {
						number: "Veuillez saisir un entier",
						min: "Veuillez saisir un entier entre 0 et 1000",
						max: "Veuillez saisir un entier entre 0 et 1000"
					}
				}
					});
/* live filter
$('.user_address_number').on('keyup', clean($('.user_address_number')[0],'/(?:[^0-9]|^(^0))$/gi'));
$('.user_address_number').on('keydown', clean($('.user_address_number')[0],'/(?:[^0-9]|^(^0))$/gi'));
*/

// Minify the Nav Bar
$(document).scroll(function () {
	var position = $(document).scrollTop();
	var headerHeight = $('.navbar').outerHeight();
// Show "Back to Top" button
if ($(this).scrollTop() > 1){
	$('.scrolltotop').addClass('show-to-top');
} else {
	$('.scrolltotop').removeClass('show-to-top');
}
});
$("#op1").hide();
$('.controls').on('change', function(){
	if($('input[name="ut"]:checked').val()=="global"){
		$("#structure").val("excellium consulting");
		$(".op2").hide();
		$("#none").hide();
		$("#op1").show();
	}
	if($('input[name="ut"]:checked').val()=="local"){
		$("#structure").val("");
		$("#op1").hide();
		$(".op2").show();
	}
});
// Scroll on Top
$('.scrolltotop, .navbar-brand').on('click', function(evt) {
	$('html, body').animate({scrollTop: '0'}, 1200, 'easeInOutCubic');
	pde(evt);
});
/*==========  Dynamic Text  ==========*/
/*setTimeout('frameLooper()',3000);*/
/*==========  Alerts  ==========*/
$('.alert').on('inview', function (event, isInView) {
	if (isInView) {
		$(this).addClass('in');
	}
});
$(function() {
	$('[data-hide]').on('click', function() {
		$(this).closest('.' + $(this).attr('data-hide')).fadeOut();
	});
});
$('.form-control').on('change', function(){
	var selected = $(this).find("option:selected").val();
	$(this).innerHTML = selected;
});/*==========  Validate Email  ==========*/
function validateEmail($validate_email) {
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	if( !emailReg.test( $validate_email ) ) {
		return false;
	} else {
		return true;
	}
}/*==========  Validate Input  ==========*/
function validateInput($validate_input) {
	var nameReg = /^[\w-\._ ]{2,20}$/;
	if( !nameReg.test( $validate_input ) ) {
		return false;
	} else {
		return true;
	}
}/*==========  Validate Input  ==========*/
function validateNumber($validate_input) {
	var nameReg = /^[0-9]{1,20}$/;
	if( !nameReg.test( $validate_input ) ) {
		return false;
	} else {
		return true;
	}
}/*==========  Contact Form  ==========*/
$('.sign_up_form').on('submit', function() {
	var contactForm = $(this);
	contactForm.find('.sign-error').fadeOut();
	contactForm.find('.sign-success').fadeOut();
	contactForm.find('.sign-loading').fadeOut();
	contactForm.find('.sign-loading').fadeIn();
	if (validateEmail(contactForm.find('.sign-email').val()) && validateInput(contactForm.find('.sign-name').val()) &&
		validateInput(contactForm.find('.sign-object').val()) && contactForm.find('.sign-email').val().length !== 0 &&
		contactForm.find('.sign-name').val().length !== 0 && contactForm.find('.sign-object').val().length !== 0 &&
		contactForm.find('.sign-message').val().length !== 0) {
		var action = contactForm.attr('action');
	$.ajax({
		url : action,
		type: "POST",
		data: {
			cf: contactForm.find('.candidat_firstname').val(),
			cln: contactForm.find('.candidat_lastname').val(),
			cl: contactForm.find('.candidat_login').val(),
			ce: contactForm.find('.candidat_email').val(),
			ct: contactForm.find('.candidat_tel').val(),
			cp2: contactForm.find('.candidat_repasswd').val(),
			cp1: contactForm.find('.candidat_passwd').val(),
			cn: contactForm.find('.candidat_num').val(),
			cr: contactForm.find('.candidat_rue').val(),
			cv: contactForm.find('.candidat_ville').val(),
			cp: contactForm.find('.candidat_pays').val()
		},
		success: function(result) {
			contactForm.find('.sign_up_form-loading').fadeOut();
			contactForm.find('.sign_up_form-success').find('.message').html('Votre message à été envoyer&nbsp;! <br/>Merci de nous avoir contacté&nbsp;!');
			contactForm.find('.sign_up_form-success').fadeIn();
		},
		error: function(result) {
			contactForm.find('.sign_up_form-loading').fadeOut();
			contactForm.find('.sign_up_form-error').find('.message').html('Désolé, une erreur est survenue.');
			contactForm.find('.sign_up_form-error').fadeIn();
		}
	}
	);
} else if(!validateEmail(contactForm.find('.sign_up_form-email').val()) && contactForm.find('.sign_up_form-email').val().length !== 0 && contactForm.find('.sign_up_form-name').val().length !== 0 && contactForm.find('.sign_up_form-message').val().length !== 0) {
	contactForm.find('.sign_up_form-error').fadeOut();
	contactForm.find('.sign_up_form-success').fadeOut();
	contactForm.find('.sign_up_form-loading').fadeOut();
	contactForm.find('.sign_up_form-error').find('.message').html('Merci d\'entrer une adresse Email valide.');
	contactForm.find('.sign_up_form-error').fadeIn();
} else if(!validateInput(contactForm.find('.sign_up_form-name').val()) && contactForm.find('.sign_up_form-name').val().length !== 0 && contactForm.find('.sign-name').val().length !== 0 && contactForm.find('.sign-message').val().length !== 0) {
	contactForm.find('.sign_up_form-error').fadeOut();
	contactForm.find('.sign_up_form-success').fadeOut();
	contactForm.find('.sign_up_form-loading').fadeOut();
	contactForm.find('.sign_up_form-error').find('.message').html('Le Nom doit être entre 2 et 20 caractères');
	contactForm.find('.sign_up_form-error').fadeIn();
} else if (!validateInput(contactForm.find('.sign_up_form-object').val()) && contactForm.find('.sign_up_form-object').val().length !== 0 && contactForm.find('.sign-object').val().length !== 0 && contactForm.find('.sign-message').val().length !== 0) {
	contactForm.find('.sign_up_form-error').fadeOut();
	contactForm.find('.sign_up_form-success').fadeOut();
	contactForm.find('.sign_up_form-loading').fadeOut();
	contactForm.find('.sign_up_form-error').find('.message').html('Le champs Objet doit être entre 2 et 20 caractères');
	contactForm.find('.sign_up_form-error').fadeIn();
} else {
	contactForm.find('.sign_up_form-error').fadeOut();
	contactForm.find('.sign_up_form-success').fadeOut();
	contactForm.find('.sign_up_form-loading').fadeOut();
	contactForm.find('.sign_up_form-error').find('.message').html('Veuillez à remplir tous les champs.');
	contactForm.find('.sign_up_form-error').fadeIn();
}
$('.sign_up_form')[0].reset();
return false;
});

});
})(jQuery);

function refresh(link) {
	window.location.href = link;
}
function smoothform() {
	var place = $('#form');
	$('html, body').animate({scrollTop: $(place).offset().top - 70}, 1200, 'easeInOutCubic');
	pde(evt);
	var place = $('#form');
	$('html, body').animate({scrollTop: $(place).offset().top - 50}, 1200, 'easeInOutCubic');
	pde(evt);
}
function toggleNavPanel(x,y) {
	var panel = document.getElementById('h'+x), navarrow = document.getElementById('n'+y);
	if(panel.style.height != "0px"){
		navarrow.innerHTML = "&#9662;";
		panel.style.padding = "0%";
		panel.style.height = "0px";
		panel.style.maxHeight = "0px";
		panel.style.minHeight = "0px";
	}else{
		panel.style.height = "auto";
		panel.style.maxHeight = "200px";
		panel.style.padding = "0px 10px 0px 10px";
		panel.style.minHeight = "5px";
		navarrow.innerHTML = "&#9652;";
	}
}
function request_page(pn){
	var eval = $("#eval_nom").val();
	var results_box = document.getElementById("results_box");
	var pagination_controls = $("#pagination_controls");
	$('#eval_nom').on('change',function() {
		$('#results_box').html('<tr><td style="width:9%;text-align:center;" colspan="9">Chargement de données ...</td></tr>');
	});
	var hr = new XMLHttpRequest();
	hr.open("POST", "scripts/pagination_parser.php", true);
	hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	hr.onreadystatechange = function() {
		if(hr.readyState == 4 && hr.status == 200) {
			var dataArray = hr.responseText.split("||");
			var html_output = "";
			for(i = 0; i < dataArray.length - 1; i++){
				html_output += dataArray[i];
			}
			setTimeout(function(){
				$('#results_box').fadeOut();
			},2200);
			setTimeout(function(){
				$('#results_box').html(html_output);
				$('#results_box').fadeIn("slow");
			},2500);
		}
	}
	hr.send("rpp="+rpp+"&last="+last+"&pn="+pn+"&eval="+eval);
// Change the pagination controls
var paginationCtrls = "";
// Only if there is more than 1 page worth of results give the user pagination controls
if(last != 1){
	if (pn > 1) {
		paginationCtrls += '<button onclick="request_page('+(pn-1)+')">&lt;</button>';
	}
	paginationCtrls += ' &nbsp; &nbsp; <b>Page '+pn+' of '+last+'</b> &nbsp; &nbsp; ';
	if (pn != last) {
		paginationCtrls += '<button onclick="request_page('+(pn+1)+')">&gt;</button>';
	}
}
pagination_controls.innerHTML = paginationCtrls;
}

// function for cookies
function updateCookiesData(key, value, evalId)
{ 	
	_data = {};
	_data[evalId] = {};
	if(Cookies.getJSON('data') == undefined){
		_data[evalId][key] = value;
		Cookies.set('data', _data);
	}else{
		_data = Cookies.getJSON('data');
		if(_data[evalId] == undefined)
			_data[evalId] = {};
		_data[evalId][key] = value;
		Cookies.set('data', _data);
	}
}

function resetEvaluation(evalId){
	_data = {};
	_data[evalId] = {};
	if(Cookies.getJSON('data') == undefined){
		_data[evalId][key] = value;
		Cookies.set('data', _data);
	}else{
		_data = Cookies.getJSON('data');
		if(_data[evalId] == undefined)
			_data[evalId] = {};
		_data[evalId] = {}
		Cookies.set('data', _data);
	}	
}


// function for cookies
function getOrSetCookiesData(key, value, evalId) {
	_data = {};
	_data[evalId] = {};
	if(Cookies.getJSON('data') == undefined){
		_data[evalId][key] = value;
		Cookies.set('data', _data);
		return value;
	}else if(Cookies.getJSON('data')[evalId] == undefined) {
		_data = Cookies.getJSON('data');
		_data[evalId] = {};
		_data[evalId][key] = value;
		Cookies.set('data', _data);
		return value;
	}else if(Cookies.getJSON('data')[evalId][key] == undefined) {
		_data = Cookies.getJSON('data');
		_data[evalId][key] = value;
		Cookies.set('data', _data);
		return value;
	}else{
		return Cookies.getJSON('data')[evalId][key];
	}			
}
