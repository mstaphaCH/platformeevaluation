(function ($) {
	"use strict";

	$(document).ready(function(){

		$( ".login-form" ).validate({
			rules: {
				'login-email': {required:true, email:true},
				'login-passwd': {required:true, noWhiteSpaces:true, rangelength:[9,50]}},
				messages: {
					'login-email': 
					{required: "Veuillez saisir votre E-mail",
					email: "E-mail non valide"},
					'login-passwd': 
					{required: "Veuillez saisir votre mot de passe",
					noWhiteSpaces: noWhiteSpaces_check,
					rangelength: "Longueur minimal est de 9 caractères et/ou chiffres"}
				}
			});

		/* login form button */
		$('.login-btn').on('click', function() {
			var form = $('.login-form');
			form.find('.contact-error').fadeOut();
			form.find('.contact-success').fadeOut();
			form.find('.contact-loading').fadeOut();
			if(form.valid()) {
				var action = form.attr('action');
				$.ajax({
					url : action,
					type: "POST",
					data: {
						login_email: form.find('#login-email').val(),
						login_passwd: form.find('#login-passwd').val(),
						login_code: form.find('#login-code').val()
					},
					success: function(result) {
						var result = result.split(',');
						if(result[0] === 'code') {
							$('.login-code').show("slow");
						}else if(result[0] === 'success') {
							if (result[1]==='passwd') {
								window.location.href = 'zdb-users/profile.php?passwd=expired';
							}else{
								window.location.href = URL;
							}
						}else if(result[0] === 'code_error') {
							$('.login-code').hide("fast");
							if (result[1] == 0) {
								$('.login-error').find('.message').html('Nombre de tentative atteint !<br>Veuillez contacter votre administrateur.');
							}else if(result[1] == -1){
								$('.login-error').find('.message').html('Désolé, code incorrect !');
							}else{
								$('.login-error').find('.message').html('Désolé, code incorrect !<br>Vous avez utilisé '+result[1]+' tentative(s) sur 3 !');
							}
							$('#login-code').val("");
							$('.login-error').show("slow");
							setTimeout("$('.login-error').hide('slow');",2000);
						}else if(result[0] === 'structure_inactive') {
							$('.login-code').hide("fast");
							$('.login-error').find('.message').html('Désolé, votre structure a été désactivé !');
							$('#login-code').val("");
							$('.login-error').show("slow");
							setTimeout("$('.login-error').hide('slow');",2000);
						}else{
							$('.login-code').hide("fast");
							$('.login-error').find('.message').html('Désolé, login et/ou mot de passe incorrect !');
							$('#login-code').val("");
							$('.login-error').show("slow");
							setTimeout("$('.login-error').hide('slow');",2000);
						}
					},
					error: function(result) {
						form.find('.login-code').hide();	
						form.find('.contact-loading').fadeOut();
						form.find('.contact-error').find('.message').html('Désolé, une erreur est survenue');
						form.find('.contact-error').fadeIn();
					}
				});
			}else{
				form.find('.contact-error').fadeOut();
				form.find('.contact-success').fadeOut();
				form.find('.contact-loading').fadeOut();
				form.find('.contact-error').find('.message').html('Veuillez à remplir tous les champs');
				form.find('.contact-error').fadeIn();
			}
			return false;
			});
});

})(jQuery);
