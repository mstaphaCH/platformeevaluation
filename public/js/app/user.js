(function ($) {
	"use strict";

	$(document).ready(function() {
		var nom_required_lenght = "Veuillez saisir un nom entre 4 et 50 caractères";
		var prenom_required_lenght = "Veuillez saisir un prenom entre 4 et 50 caractères";
		var profile_required_select = "Veuillez saisir un profil";

		$('#user_profile').on('change', function(){
			var usr_prf = $('#user_profile').val();
			
			$.ajax({
				type: 'POST',
				url: 'User/requireUserCode',
				data: {
					'user_profile' : usr_prf
				},
				success: function(result) {
					if(result === 'true')
						$('.user-code').show("slow");
					else
						$('.user-code').hide("fast");
				},
				error: function(result) {
					$('.user-code').hide();	
				}
			});
		});

		$('.active-user.list.filter').on('submit', function(e){
			e.preventDefault();
			$.ajax({
				type: 'post',
				url: URL+'User/Index',
				processData: false,
				data: $(this).serialize(),
				success: function(result) {
					$('#activeTable tbody').html(result);
				},
				error: function(result) {
					console.log("error");
				}
			});
		});

		$('.inactive-user.list.filter').on('submit', function(e){
			e.preventDefault();
			$.ajax({
				type: 'post',
				url: URL+'User/Index',
				processData: false,
				data: $(this).serialize(),
				success: function(result) {
					$('#inactiveTable tbody').html(result);
				},
				error: function(result) {
					console.log("error");
				}
			});
		});

/*		$('.structure-button-edit').on('click', function(){
			
			var thisID = $('#thisID').val();
			var user_firstname = $('#user_firstname').val();
			var user_lastname = $('#user_lastname').val();
			var user_login = $('#user_login').val();
			var user_email = $('#user_email').val();

			var user_address_number = $('#user_address_number').val();
			var user_street = $('#user_street').val();
			var user_city = $('#user_city').val();
			var user_country = $('#user_country').val();
			var user_website = $('#user_website').val();

			$.ajax({
				type: 'POST',
				url: '../../User/Update',
				data: {
					'thisID' : thisID,
					'user_firstname' : user_firstname,
					'user_lastname' : user_lastname,
					'user_login' : user_login,
					'user_email' : user_email,

					'user_address_number' : user_address_number,
					'user_street' : user_street,
					'user_city' : user_city,
					'user_country' : user_country,
					'user_website' : user_website
				},
				success: function(result) {
					console.log(result);
				},
				error: function(result) {
				}
			});
		});*/

		$('.user-list-form').validate({
			rules: {
				'user_firstname': {required:true, rangelength:[4,50]},
				'user_lastname': {required:true, rangelength:[4,50]},
				'user_profile': {required:true},
				'user_login': {required:true, noOnlyWhiteSpaces:true, rangelength:[4,50]},
				'user_password': {required:true, noWhiteSpaces:true, rangelength:[9,50]},
				'user_repassword': {required:true, noWhiteSpaces:true, rangelength:[9,50], passwordMuch:'.user_password'},
				'user_code': {required:true, noWhiteSpaces:true, rangelength:[6,6], twoAlpha:true},
				'user_recode': {required:true, noWhiteSpaces:true, rangelength:[6,6], twoAlpha:true, codeMuch:'.user_code'},
				'user_email': {required:true, email:true, rangelength:[6,50]},
				'user_address_number': {required:true, number:true, min:1, max:1000000},
				'user_street': {required:true, noOnlyWhiteSpaces:true, rangelength:[6,100]},
				'user_city': {required:true, noOnlyWhiteSpaces:true, rangelength:[4,50]},
				'user_country': {required:true, noOnlyWhiteSpaces:true, rangelength:[3,30]},
				'user_website': {rangelength:[4,100]},
				'user_structure': {required:true}
			},
			messages: {
				'user_firstname': {
					required: nom_required_lenght,
					rangelength: nom_required_lenght
				},
				'user_lastname': {
					required: prenom_required_lenght,
					rangelength: prenom_required_lenght
				},
				'user_profile': {
					required: profile_required_select
				},
				'user_login': {
					required: "Veuillez saisir un login",
					noOnlyWhiteSpaces: noOnlyWhiteSpaces_check,
					rangelength: "Veuillez saisir un login entre 4 et 50 caractères"
				},
				'user_password': {
					required: "Veuillez saisir un mot de passe",
					noWhiteSpaces: noWhiteSpaces_check,
					rangelength: "Veuillez saisir un mot de passe d'au moins 9 caractères"
				},
				'user_repassword': {
					required: "Veuillez resaisir le mot de passe",
					noWhiteSpaces: noWhiteSpaces_check,
					rangelength: "Veuillez saisir un mot de passe d'au moins 9 caractères",
					passwordMuch: "Mots de passe non identiques"
				},
				'user_code': {
					required: "Veuillez saisir un code",
					noWhiteSpaces: noWhiteSpaces_check,
					rangelength: "Veuillez saisir un code de 6 caractères",
					twoAlpha: "Veuillez saisir un code qui contient au moins deux lettres"
				},
				'user_recode': {
					required: "Veuillez resaisir le code",
					noWhiteSpaces: noWhiteSpaces_check,
					rangelength: "Veuillez saisir un code de 6 caractères",
					codeMuch: "Codes non identiques",
					twoAlpha: "Veuillez saisir un code qui contient au moins deux lettres"
				},
				'user_email': {
					required: "Veuillez saisir une adresse email",
					email: "Veuillez saisir une adresse email valide",
					rangelength: "Veuillez saisir une adresse email entre 6 et 50 caractères"
				},
				'user_address_number': {
					required: "Entier svp !",
					number: "Entier svp !",
					min: "Nombre entre 1 et 1000000",
					max: "Nombre entre 1 et 1000000"
				},
				'user_street': {
					required: "Veuillez saisir une adresse",
					noOnlyWhiteSpaces: noOnlyWhiteSpaces_check,
					rangelength: "L'adresse doit être entre 6 et 100 caractères"
				},
				'user_city': {
					required: "Veuillez saisir une ville",
					noOnlyWhiteSpaces: noOnlyWhiteSpaces_check,
					rangelength: "La ville doit être entre 4 et 50 caractères"
				},
				'user_country': {
					required: "Veuillez saisir un pays",
					noOnlyWhiteSpaces: noOnlyWhiteSpaces_check,
					rangelength: "Le pays doit être entre 3 et 30 caractères"
				},
				'user_website': {
					rangelength: "Le site web doit être entre 4 et 100 caractères"
				},
				'user_structure': {
					required: "Veuillez choisir une structure"
				}
			}
		});

	});

})(jQuery);