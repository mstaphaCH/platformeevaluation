(function ($) {
	"use strict";

	$(document).ready(function(){

		/* Date session range */
		$('.session-form .input-daterange').datepicker({
		    todayBtn: "linked",
		    clearBtn: true,
		    language: "fr",
		    autoclose: true,
		    todayHighlight: true
		});

		/* Form validation */
		$( ".session-form" ).validate({
			rules: {
				'plf_ref': {number:true, min:1, max:99999, required:true, remote:{url:'Session/checkSessionReference', type:'post'}},
				'plf_title': {required:true, noOnlyWhiteSpaces:true, rangelength:[4,50]},
				'plf_resp_firstname': {noOnlyWhiteSpaces:true, required:true, rangelength:[4,30]},
				'plf_resp_lastname': {required:true, noOnlyWhiteSpaces:true, rangelength:[4,30]},
				'plf_type': {required:true},
				'session_start': {required:true, noWhiteSpaces:true, noOnlyWhiteSpaces:true},
				'session_end': {required:true, noWhiteSpaces:true, noOnlyWhiteSpaces:true}
				},
			messages: {
				'plf_ref': 
					{number: "Veuillez à entrer un entier positive",
					min: "Veuillez à entrer un numéro entre 00001 et 99999",
					max: "Veuillez à entrer un numéro entre 00001 et 99999",
					required: "Veuillez à entrer un numéro entre 00001 et 99999",
					remote: "Numéro utilisé"},
				'plf_title': 
					{required: "Titre obligatoire",
					noOnlyWhiteSpaces: noOnlyWhiteSpaces_check,
					rangelength: "Le prénom doit être entre 4 et 40 caractères"},
				'plf_resp_firstname':
					{noOnlyWhiteSpaces: noOnlyWhiteSpaces_check,
					required: "Nom obligatoire",
					rangelength: "Le nom doit être entre 4 et 40 caractères"},
				'plf_resp_lastname': 
					{required: "Prénom obligatoire",
					email: "Veuillez saisir un prénom valide",
					rangelength: "Le prénom doit être entre 4 et 40 caractères"},
				'plf_type': 
					{required: "La saisie d'un type est obligatoire"},
				'session_start': 
					{required: "Date début session est obligatoire",
					noOnlyWhiteSpaces: noOnlyWhiteSpaces_check},
				'session_end': 
					{required: "Date fin session est obligatoire",
					noOnlyWhiteSpaces: noOnlyWhiteSpaces_check}
				}
			});

		/* Add form button */
		$('.session-button-add').on('click', function() {
			var form = $('.session-form');
			form.find('.contact-error').fadeOut();
			form.find('.contact-success').fadeOut();
			form.find('.contact-loading').fadeOut();
			if($('.session-form').valid()) {
				var action = form.attr('action');
				$.ajax({
					url : action,
					type: "POST",
					data: {
						thisID: form.find('.thisID').val(),
						plf_ref: form.find('.plf_ref').val(),
						plf_title: form.find('.plf_title').val(),
						plf_resp_firstname: form.find('.plf_resp_firstname').val(),
						plf_resp_lastname: form.find('.plf_resp_lastname').val(),
						plf_type: form.find('.plf_type').val(),
						session_end: form.find('.session_end').val(),
						session_start: form.find('.session_start').val(),
					},
					success: function(result) {
						console.log(result);
						window.location = URL+'Session';
					},
					error: function(result) {
						form.find('.contact-loading').fadeOut();
						form.find('.contact-error').find('.message').html('Désolé, une erreur est survenue');
						form.find('.contact-error').fadeIn();
					}
				});
			}else{
				form.find('.contact-error').fadeOut();
				form.find('.contact-success').fadeOut();
				form.find('.contact-loading').fadeOut();
				form.find('.contact-error').find('.message').html('Veuillez à remplir tous les champs');
				form.find('.contact-error').fadeIn();
			}
			return false;
		});

		/* Info button redirection */
		$(document).delegate('.session_info','click',function(e) {
			var x = $(this).attr('id');
			window.location = 'session_edit.php?pid=' + x + '&op=inf';
		});

		/* Affect evaluation to a session */
		$(".savee").on('click', function () {
			var checkbox_value = "";
			var idd = $(this).attr('id');
			$("input[type='checkbox'][name='"+idd+"']").each(function () {
				var ischecked = $(this).is(":checked");
				if (ischecked) {
					checkbox_value += "|" + $(this).val();
				}
				console.log(checkbox_value);
			});
			$.ajax({
				type: 'POST',
				url: 'scripts/session/eval_planif.php',
				data: { 
					'plf_id': idd,
					'checkbox': checkbox_value
				},
				success: function(result){
					$("#"+idd).fadeOut("slow");
					setTimeout(function(){
						$("#"+idd).text('succès');
						$("#"+idd).css('background-color','#0add08');
						$("#"+idd).fadeIn("slow");
					},1000);
					setTimeout(function(){
						$("#"+idd).fadeOut("slow");
					},3000);
					setTimeout(function(){
						$("#"+idd).text('enregistrer');
						$("#"+idd).css('background-color','');
						$("#"+idd).fadeIn("slow");
					},4000);
				},
				error: function(error){
					$("#"+idd).fadeOut("slow");
					setTimeout(function(){
						$("#"+idd).text("erreur");
						$("#"+idd).css('background-color','red');
						$("#"+idd).fadeIn("slow");
					},1000);
					setTimeout(function(){
						$("#"+idd).fadeOut("slow");
					},3000);
					setTimeout(function(){
						$("#"+idd).text('enregistrer');
						$("#"+idd).css('background-color','');
						$("#"+idd).fadeIn("slow");
					},4000);
				}
			});
		});
		/* Affect evaluation to a session */
		$(".saveee").on('click', function () {
			var checkbox_value = "";
			var idd = $(this).attr('id');
			$("input[type='checkbox'][name='"+idd+"']").each(function () {
				var ischecked = $(this).is(":checked");
				if (ischecked) {
					checkbox_value += "|" + $(this).val();
				}
				console.log(checkbox_value);
			});
			$.ajax({
				type: 'POST',
				url: 'scripts/session/eval_planif.php',
				data: { 
					'plf_id': idd,
					'checkbox': checkbox_value
				},
				success: function(result){
					$("#"+idd).fadeOut("slow");
					setTimeout(function(){
						$("#"+idd).text('succès');
						$("#"+idd).css('background-color','#0add08');
						$("#"+idd).fadeIn("slow");
					},1000);
					setTimeout(function(){
						$("#"+idd).fadeOut("slow");
					},3000);
					setTimeout(function(){
						$("#"+idd).text('enregistrer');
						$("#"+idd).css('background-color','');
						$("#"+idd).fadeIn("slow");
					},4000);
				},
				error: function(error){
					$("#"+idd).fadeOut("slow");
					setTimeout(function(){
						$("#"+idd).text("erreur");
						$("#"+idd).css('background-color','red');
						$("#"+idd).fadeIn("slow");
					},1000);
					setTimeout(function(){
						$("#"+idd).fadeOut("slow");
					},3000);
					setTimeout(function(){
						$("#"+idd).text('enregistrer');
						$("#"+idd).css('background-color','');
						$("#"+idd).fadeIn("slow");
					},4000);
				}
			});
		});
		
	});

})(jQuery);
