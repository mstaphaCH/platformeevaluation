$(document).ready(function() {

	$('.candidate.delete').on('click', function(){
		var candidate_id = $(this).attr('id');
		Confirm.render('Delete candidate?','delete','candidate',candidate_id);
	});

	$('#a_search_firstname').on('keyup', function() {
		$('#a_search_lastname').val(''); $('#a_search_email').val(''); $('#a_search_structure').val('');
	});
	$('#a_search_lastname').on('keyup', function() {
		$('#a_search_firstname').val(''); $('#a_search_email').val(''); $('#a_search_structure').val('');
	});
	$('#a_search_email').on('keyup', function() {
		$('#a_search_firstname').val(''); $('#a_search_lastname').val(''); $('#a_search_structure').val('');
	});
	$('#a_search_structure').on('click', function() {
		$('#a_search_firstname').val(''); $('#a_search_lastname').val(''); $('#a_search_email').val('');
	});

	$('#na_search_firstname').on('keyup', function() {
		$('#na_search_lastname').val(''); $('#na_search_email').val(''); $('#na_search_structure').val('');
	});
	$('#na_search_lastname').on('keyup', function() {
		$('#na_search_firstname').val(''); $('#na_search_email').val(''); $('#na_search_structure').val('');
	});
	$('#na_search_email').on('keyup', function() {
		$('#na_search_firstname').val(''); $('#na_search_lastname').val(''); $('#na_search_structure').val('');
	});
	$('#na_search_structure').on('click', function() {
		$('#na_search_firstname').val(''); $('#na_search_lastname').val(''); $('#na_search_email').val('');
	});

	$('.active-candidate.list.filter').bind('keyup change', function(e){
		e.preventDefault();
		$.ajax({
			type: 'post',
			url: URL+'Candidate/Index',
			processData: false,
			data: $(this).serialize(),
			success: function(result) {
				$('#activeTable tbody').html(result);
			},
			error: function(result) {
				console.log("error");
			}
		});
	});

	$('.inactive-candidate.list.filter').bind('keyup change', function(e){
		e.preventDefault();
		$.ajax({
			type: 'post',
			url: URL+'Candidate/Index',
			processData: false,
			data: $(this).serialize(),
			success: function(result) {
				$('#inactiveTable tbody').html(result);
			},
			error: function(result) {
				console.log("error");
			}
		});
	});

    // $('.candidat_firstname').keyup(function() {
    // 	var value = $(this).val();
    //     $.post("scripts/candidate/cdt_livesearch.php", {name:value},function(data) {
    //         $("#cdt-livesearch").html(data);
    //     }); 
    // });
    /* change password by an administrator */
    $(".cdt_chgpaswd").hide();
    $(".cdt_passwd").on('click', function(){
		$(".cdt-edit").hide();
		$(".cdt_chgpaswd").show();
	});
	$(".return.cdt-passwd").on('click', function(){
		$(".cdt-edit").show();
		$(".cdt_chgpaswd").hide();
	});
	$('.cdt-passwd').on('click', function() {
		var form = $('.passwd');
		form.find('.contact-error').fadeOut();
		form.find('.contact-success').fadeOut();
		form.find('.contact-loading').fadeOut();
		if(form.valid()) {
			var action = form.attr('action');
			$.ajax({
				url : action,
				type: "POST",
				data: {
					user_id: form.find('.thisID').val(),
					new_passwd: form.find('.new_passwd').val(),
					new_repasswd: form.find('.new_repasswd').val()
				},
				success: function(result) {
					console.log(result);
					refresh('candidat_list.php');
				},
				error: function(result) {
					form.find('.contact-loading').fadeOut();
					form.find('.contact-error').find('.message').html('Désolé, une erreur est survenue');
					form.find('.contact-error').fadeIn();
				}
			});
		}else{
			form.find('.contact-error').fadeOut();
			form.find('.contact-success').fadeOut();
			form.find('.contact-loading').fadeOut();
			form.find('.contact-error').find('.message').html('Veuillez à remplir tous les champs');
			form.find('.contact-error').fadeIn();
		}
		return false;
	});
    /* get dropdown sessions to the add form candidate */
    $('.session_job_start.add, .session_job_end.add').on('change', function() {
    	var session_job_start = $('.session_job_start.add').val();
    	var session_job_end = $('.session_job_end.add').val();
        $.post("Candidate/addSessionJob", {session_job_start:session_job_start, session_job_end:session_job_end},
        function(data) {
            $("#session-job-add").html(data);
        }); 
    });
    /* get dropdown sessions to the edit form candidate */
    $('.session_job_start.edit, .session_job_end.edit').on('change', function() {
    	var session_job_start = $('.session_job_start.edit').val();
    	var session_job_end = $('.session_job_end.edit').val();
    	var thisID = $('.thisID').val();
        $.post("Candidate/editSessionJob", {session_job_start:session_job_start, session_job_end:session_job_end, thisID:thisID},
        function(data) {
            $("#session-job-edit").html(data);
        }); 
    });
	/* details button redirection */
	$(document).delegate('.candidat_info','click',function(e) {
		var x = $(this).attr('id');
		window.location = URL+'Candidate/Details/'+x;		
	});
	/* date session range */
	$('.candidate-form .input-daterange').datepicker({
	    todayBtn: "linked",
	    clearBtn: true,
	    language: "fr",
	    autoclose: true,
	    todayHighlight: true
	});
	$('.candidate-form .acces').on('change', function() {
		var selected = $(this).find("option:selected").val();
		if (selected=='evaluation') {
			$('.candidate-form .session-date input').each(function() {
			    $(this).datepicker("clearDates");
			});
			$('.candidate-form .session-date .session_end').attr('disabled', 'disabled');
		}else{
			$('.candidate-form .session-date .session_end').attr('disabled', false);
		}
	});
	
	/* Form validation */
	$( ".candidate-form" ).validate({
	rules: {
		'candidat_firstname': {required:true, noOnlyWhiteSpaces:true, rangelength:[4,50]},
		'candidat_lastname': {required:true, noOnlyWhiteSpaces:true, rangelength:[4,50]},
		'candidat_login': {noOnlyWhiteSpaces:true, required:true, rangelength:[4,30]},
		'candidat_passwd': {required:true, noWhiteSpaces:true, rangelength:[9,50]},
		'candidat_repasswd': {required:true, noWhiteSpaces:true, rangelength:[9,50], passwordMuch:'.candidat_passwd'},
		'candidat_email': {required:true, email:true, rangelength:[6,50]},
		'an': {number:true,	min:1, max:99999, required:true},
		'candidat_rue': {required:true, noOnlyWhiteSpaces:true, rangelength:[5,60]},
		'candidat_ville': {required:true, noOnlyWhiteSpaces:true, rangelength:[5,40]},
		'candidat_pays': {required:true, noOnlyWhiteSpaces:true, rangelength:[5,30]},
		'candidat_tel': {noOnlyWhiteSpaces:false},
		'candidat_structure': {required:true},
		'acces': {required:true},
		'session_start': {required:true, noWhiteSpaces:true, noOnlyWhiteSpaces:true},
		'session_end': {required:true, noWhiteSpaces:true, noOnlyWhiteSpaces:true},
		'session_job_list': {required:true}
		},
		messages: {
			'candidat_firstname': 
				{required: "Nom obligatoire",
				noOnlyWhiteSpaces: noOnlyWhiteSpaces_check,
				rangelength: "Le nom doit être entre 4 et 50 caractères"},
			'candidat_lastname': 
				{required: "Prénom obligatoire",
				noOnlyWhiteSpaces: noOnlyWhiteSpaces_check,
				rangelength: "Le prénom doit être entre 4 et 50 caractères"},
			'candidat_login':
				{noOnlyWhiteSpaces: noOnlyWhiteSpaces_check,
				required: "Veuillez saisir un login",
				rangelength: "Le login doit être entre 4 et 30 caractères"},
			'candidat_passwd': 
				{required: "Le mot de passe est obligatoire",
				noWhiteSpaces: noWhiteSpaces_check,
				rangelength: "Le mot de passe doit avoir au minimum 9 caractères"},
			'candidat_repasswd': 
				{required: "Le mot de passe est obligatoire",
				noWhiteSpaces: noWhiteSpaces_check,
				rangelength: "Le mot de passe doit avoir au minimum 9 caractères",
				passwordMuch: "Mots de passe non identiques"},
			'candidat_email': 
				{required: "E-mail obligatoire",
				email: "Veuillez saisir une adresse email valide",
				rangelength: "L'email doit être entre 6 et 50 caractères"},
			'an': 
				{number: "Le numéro doit être un entier",
				min: "Le numéro doit être entre 1 et 99999",
				max: "Le numéro doit être entre 1 et 99999",
				required: "Le numéro est obligatoire"},
			'candidat_rue': 
				{required: "L'adresse est obligatoire",
				noOnlyWhiteSpaces: noOnlyWhiteSpaces_check,
				rangelength: "L'adresse doit être entre 5 et 60 caractères"},
			'candidat_ville': 
				{required: "La saisie d'une ville est obligatoire",
				noOnlyWhiteSpaces: noOnlyWhiteSpaces_check,
				rangelength: "La ville doit être entre 5 et 40 caractères"},
			'candidat_pays': 
				{required: "La saisie d'un pays est obligatoire",
				noOnlyWhiteSpaces: noOnlyWhiteSpaces_check,
				rangelength: "Le pays doit être entre 5 et 30 caractères"},
			'candidat_tel': 
				{noOnlyWhiteSpaces: noOnlyWhiteSpaces_check},
			'candidat_structure': 
				{required: "La saisie d'une structure est obligatoire"},
			'acces': 
				{required: "La saisie d'un type acces est obligatoire"},
			'input-daterange': 
				{required: "La saisie d'une date est obligatoire",
				noOnlyWhiteSpaces: noOnlyWhiteSpaces_check,
				noWhiteSpaces: noWhiteSpaces_check},
			'session_job_list': 
				{required: "La saisie d'une session de travail est obligatoire"},
			}
		});

	/* Add form button */
	$('.candidate-button-add').on('click', function() {
		var form = $('.candidate-form.add');
		form.find('.contact-error').fadeOut();
		form.find('.contact-success').fadeOut();
		form.find('.contact-loading').fadeOut();

		var data = {
			candidat_firstname: form.find('.candidat_firstname').val(),
			candidat_lastname: form.find('.candidat_lastname').val(),
			candidat_login: form.find('.candidat_login').val(),
			candidat_email: form.find('.candidat_email').val(),
			session_end: form.find('.session_end').val(),
			session_start: form.find('.session_start').val(),
			candidat_passwd: form.find('.candidat_passwd').val(),
			candidat_repasswd: form.find('.candidat_repasswd').val(),
			candidat_structure: form.find('.candidat_structure').val(),
			an: form.find('.an').val(),
			candidat_rue: form.find('.candidat_rue').val(),
			candidat_ville: form.find('.candidat_ville').val(),
			candidat_pays: form.find('.candidat_pays').val(),
			acces: form.find('.acces').val(),
			candidat_tel: form.find('.candidat_tel').val(),
			session_job_add: form.find('.session-job-add').val(),
		}

		if($('.candidate-form.add').valid()) {
			var action = form.attr('action');
			$.ajax({
				url : action,
				type: "post",
				data: data,
				success: function(result) {
					window.location = URL + 'Candidate';
				},
				error: function(result) {
					form.find('.contact-loading').fadeOut();
					form.find('.contact-error').find('.message').html('Désolé, une erreur est survenue');
					form.find('.contact-error').fadeIn();
				}
			});
		}else{
			form.find('.contact-error').fadeOut();
			form.find('.contact-success').fadeOut();
			form.find('.contact-loading').fadeOut();
			form.find('.contact-error').find('.message').html('Veuillez à remplir tous les champs');
			form.find('.contact-error').fadeIn();
		}
		return false;
	});
	/* candidate ajax edit form */
	$('.candidate-button.edit').on('click', function() {
		var form = $('.candidate-form.edit');
		form.find('.contact-error').fadeOut();
		form.find('.contact-success').fadeOut();
		form.find('.contact-loading').fadeOut();
		if($('.candidate-form.edit').valid()) {
			var action = form.attr('action');
			$.ajax({
				url : action,
				type: "POST",
				data: {
					thisID: form.find('.thisID').val(),
					candidat_firstname: form.find('.candidat_firstname').val(),
					candidat_lastname: form.find('.candidat_lastname').val(),
					candidat_login: form.find('.candidat_login').val(),
					candidat_email: form.find('.candidat_email').val(),
					session_end: form.find('.session_end').val(),
					session_start: form.find('.session_start').val(),
					candidat_passwd: form.find('.candidat_passwd').val(),
					candidat_repasswd: form.find('.candidat_repasswd').val(),
					candidat_structure: form.find('.candidat_structure').val(),
					an: form.find('.an').val(),
					candidat_rue: form.find('.candidat_rue').val(),
					candidat_ville: form.find('.candidat_ville').val(),
					candidat_pays: form.find('.candidat_pays').val(),
					acces: form.find('.acces').val(),
					candidat_tel: form.find('.candidat_tel').val(),
					// session_job_edit: form.find('.session-job-edit').val(),
				},
				success: function(result) {
					window.location = URL + "Candidate";
				},
				error: function(result) {
					form.find('.contact-loading').fadeOut();
					form.find('.contact-error').find('.message').html('Désolé, une erreur est survenue');
					form.find('.contact-error').fadeIn();
				}
			});
		}else{
			form.find('.contact-error').fadeOut();
			form.find('.contact-success').fadeOut();
			form.find('.contact-loading').fadeOut();
			form.find('.contact-error').find('.message').html('Veuillez à remplir tous les champs');
			form.find('.contact-error').fadeIn();
		}
		return false;
	});
	
	/* info button */
	$(document).delegate('.candidate-button.details','click',function(e) {
		window.location = URL+'Candidate';
	});
	
});
