$(document).ready(function() {
	var all_quest = getOrSetCookiesData('all_quest', all_quest, evaluation_id);
	var qst_filter = getOrSetCookiesData('qst_filter', qst_filter, evaluation_id);

	function windowOpenInPost(actionUrl,windowName, windowFeatures, keyParams, valueParams){
		var data = {};
		if (keyParams && valueParams && (keyParams.length == valueParams.length)){
			for (var i = 0; i < keyParams.length; i++){
				data[keyParams[i]] = valueParams[i];
			}
		}

		$.post( actionUrl, data).done(function( data ) {
			$('#dialog').dialog(
			{
				autoOpen: true,
				width: 600,
				buttons: {
					"Quitter": function () {
						$(this).dialog("close");
					}
				},
				open: function(event) {
					$('.ui-dialog-buttonpane').find('button').removeAttr('class').attr('class', 'btn btn-primary');
				}
			});
			$(".dialog_content").html(data);
		});
	}

	function windowOpenReset(actionUrl,windowName, windowFeatures, keyParams, valueParams){
		var data = {};
		if (keyParams && valueParams && (keyParams.length == valueParams.length)){
			for (var i = 0; i < keyParams.length; i++){
				data[keyParams[i]] = valueParams[i];
			}
		}

		$.post( actionUrl, data).done(function( data ) {
			$(".dialog_content").html(data);
		});
		$('#dialog').dialog(
		{
			autoOpen: true,
			width: 600,
			buttons: {
				"Confirmer": function () {
					$(this).dialog("close");
					resetEvaluation(evaluation_id);
					window.location=URL+'Evaluation/Question/'+evaluation_id;
				},"Annuler": function () {
					$(this).dialog("close");
				}
			},
			open: function(event) {
				$('.ui-dialog-buttonpane').find('button').removeAttr('class').attr('class', 'btn btn-primary');
			}
		});
	}

	function windowOpenResetAptitude(actionUrl,windowName, windowFeatures, keyParams, valueParams){
		var data = {};
		if (keyParams && valueParams && (keyParams.length == valueParams.length)){
			for (var i = 0; i < keyParams.length; i++){
				data[keyParams[i]] = valueParams[i];
			}
		}

		$.post( actionUrl, data).done(function( data ) {
			$(".dialog_content").html(data);
		});
		$('#dialog').dialog(
		{
			autoOpen: true,
			width: 600,
			buttons: {
				"Confirmer": function () {
					$(this).dialog("close");
					resetEvaluation(evaluation_id);
					window.location='question_aptitude.php?id='+evaluation_id;

				},"Annuler": function () {
					$(this).dialog("close");
				}

			},
			open: function(event) {
				$('.ui-dialog-buttonpane').find('button').removeAttr('class').attr('class', 'btn btn-primary');
			}
		});
	}

	function etat_rep()
	{
		var response_data = {};
		$.each($("div.radio input[type='radio']:checked"), function( index, value ) {
			var current = $(value);
			var qst_id = current.attr("id").split("_")[1];
			response_data[qst_id] = current.val();
		});

		width = 600;
		height = 500;
		if(window.innerWidth)
		{
			var left = (window.innerWidth-width)/2;
			var top = (window.innerHeight-height)/2;
		}
		else
		{
			var left = (document.body.clientWidth-width)/2;
			var top = (document.body.clientHeight-height)/2;
		}

		var prop = 'menubar=no, scrollbars=no, top='+top+', left='+left+', width='+width+', height='+height+'';
		windowOpenInPost('etatRep.php?id='+evaluation_id, 'Etat Réponse', prop, ["data"],[JSON.stringify(response_data)])
	}


	$('.new-evaluation.general').on('click', function() {
		width = 600;
		height = 500;
		if(window.innerWidth) {
			var left = (window.innerWidth-width)/2;
			var top = (window.innerHeight-height)/2;
		}else{
			var left = (document.body.clientWidth-width)/2;
			var top = (document.body.clientHeight-height)/2;
		}
		var prop = 'menubar=no,scrollbars=no,location=no,top='+top+', left='+left+', width='+width+', height='+height+'';
		windowOpenReset(URL+'Evaluation/newProbation', 'La réinitialisation', prop, ["data"],[JSON.stringify(all_quest)]);
	});

	$('.new-evaluation.aptitude').on('click', function() {
		width = 600;
		height = 500;
		if(window.innerWidth) {
			var left = (window.innerWidth-width)/2;
			var top = (window.innerHeight-height)/2;
		}else{
			var left = (document.body.clientWidth-width)/2;
			var top = (document.body.clientHeight-height)/2;
		}
		var prop = 'menubar=no,scrollbars=no,location=no,top='+top+', left='+left+', width='+width+', height='+height+'';
		windowOpenResetAptitude(URL+'Evaluation/newProbation', 'La réinitialisation', prop, ["data"],[JSON.stringify(all_quest)]);
	});

	$('.exit-probation').on('click', function() {
		window.location=URL+'Home';
	});

	$("header").block({ message: null });
	$("#accept").prop("disabled", true);
});