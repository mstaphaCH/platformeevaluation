
(function ($) {
	"use strict";

	$(document).ready(function() {
		$('.structure.delete').on('click', function(){
			var structure_id = $(this).attr('id');
			Confirm.render('Delete Structure?','delete','structure',structure_id);
		});

		$('.active-structure').on('click', function(){
			var structure_id = $(this).attr('id');
			$.ajax({
				type: 'POST',
				url: URL + 'Structure/changeStatus',
				data: {
					'structure_id' : structure_id
				},
				success: function(result) {
					loadAjaxTable('active');
					loadAjaxTable('inactive');
				},
				error: function(result) {
					console.log("failed");
				}
			});
		});
		
		$('.inactive-structure').on('click', function(){
			var structure_id = $(this).attr('id');
			$.ajax({
				type: 'POST',
				url: URL + 'Structure/changeStatus',
				data: {
					'structure_id' : structure_id
				},
				success: function(result) {
					loadAjaxTable('active');
					loadAjaxTable('inactive');
				},
				error: function(result) {
					console.log("failed");
				}
			});
		});
	});

	info return button
	$(document).delegate('.structure-button.details','click',function(e) {
		window.location = URL+'Structure';
	});

	info button
	$(document).delegate('.structure_info','click',function(e) {
		var x = $(this).attr('id');
		window.location = URL+'Structure/Details/'+x;
	});

	$('.struct_status').on('click', function(){
		var strName = $(this).attr('name');
		var strId = '#' + $(this).attr('id');

		if(strName != '1') {
			$.ajax({
				type: 'POST',
				url: 'Structure/changeStatus',
				data: {
					'str_id' : strName
				},
				success: function(result) {
					if($(strId).find('.ion-ios-checkmark-outline').length) {
						$(strId).find('i').addClass('ion-ios-circle-outline');
						$(strId).find('i').removeClass('ion-ios-checkmark-outline');
					}else if($(strId).find('.ion-ios-circle-outline').length) {
						$(strId).find('i').addClass('ion-ios-checkmark-outline');
						$(strId).find('i').removeClass('ion-ios-circle-outline');
					}
				}
			});
		}
	});

	$('.structure.search').on('click', function(){
		var search_name = $('#search-name').find(":selected").val();
		var search_country = $('#search-country').find(":selected").val();
		var search_city = $('#+').find(":selected").val();

		$.ajax({
			type: 'post',
			url: 'Structure/getActiveStructures',
			data: {
				'search' : true,
				'search_name' : search_name,
				'search_country' : search_country,
				'search_city' : search_city
			},
			success: function(result) {
				console.log(result);
			}
		});

	});

	$('.structure-filter.active').on('submit', function(e){
		e.preventDefault();
		$.ajax({
			type: 'post',
			url: URL+'Structure/Index',
			processData: false,
			data: $(this).serialize(),
			success: function(result) {
				$('#active-table tbody').html(result);
			},
			error: function(result) {
				console.log("error");
			}
		});
	});
	$('.structure-filter.inactive').on('submit', function(e){
		e.preventDefault();
		$.ajax({
			type: 'post',
			url: URL+'Structure/Index',
			processData: false,
			data: $(this).serialize(),
			success: function(result) {
				$('#inactive-table tbody').html(result);
			},
			error: function(result) {
				console.log("error");
			}
		});
	});

	function loadAjaxTable(status) {
		var st = status;
		$.ajax({
			type: 'post',
			url: URL+'Structure/Index',
			processData: true,
			data: {'structure':st},
			success: function(result) {
				if(status==='active') {
					$('#active-table tbody').html(result);
				}else if(status==='inactive') {
					$('#inactive-table tbody').html(result);
				}
			},
			error: function(result) {
				console.log("Ajax table error rendring !");
			}
		});
	}

	$(".str-form-validation").validate({
		rules: {
			"str_nom": {required:true, noOnlyWhiteSpaces:true, rangelength:[4,100]},
			"str_description": {rangelength:[4,200]},
			"str_nom_contact": {noOnlyWhiteSpaces:true, required:true, rangelength:[4,50]},
			"str_prenom_contact": {required:true, noOnlyWhiteSpaces:true, rangelength:[4,50]},
			"str_email_contact": {required:true, email:true, rangelength:[6,50]},
			"an": {number:true, min:1, max:99999, required:true},
			"adr_rue": {noOnlyWhiteSpaces:true, required:true, rangelength:[4,50]},
			"adr_code_postal": {number:true, min:0, max:9999999999, required:true},
			"adr_ville": {required:true, noOnlyWhiteSpaces:true, rangelength:[4,50]},
			"adr_pays": {required:true, noOnlyWhiteSpaces:true, rangelength:[4,50]},
			"str_tel": {required:true, telnbrtooshort:true},
			"str_siteweb": {rangelength:[1,50]}},
			messages: {
				'str_nom': {
					required: "Nom obligatoire",
					noOnlyWhiteSpaces: noOnlyWhiteSpaces_check,
					rangelength: "Le nom doit être entre 4 et 100 caractères"
				},
				'str_description': {
					rangelength: "Le prénom doit être entre 4 et 200 caractères"
				},
				'str_nom_contact': {
					noOnlyWhiteSpaces: noOnlyWhiteSpaces_check,
					required: "Nom contact obligatoire",
					rangelength: "Le nom doit être entre 4 et 50 caractères"
				},
				'str_prenom_contact': {
					required: "Prénom contact est obligatoire",
					noOnlyWhiteSpaces: noOnlyWhiteSpaces_check,
					rangelength: "Le prénom doit être entre 4 et 50 caractères"
				},
				'str_email_contact': {
					required: "E-mail contact obligatoire",
					email: "Veuillez saisir une adresse email valide",
					rangelength: "L'email à un maximum de 50 caractères"
				},
				'an': {
					number: "Le numéro doit être un entier",
					min: "Le numéro doit être entre 1 et 99999",
					max: "Le numéro doit être entre 1 et 99999",
					required: "Numéro obligatoire"
				},
				'adr_rue': {
					noOnlyWhiteSpaces: noOnlyWhiteSpaces_check,
					required: "La saisie d'une adresse est obligatoire",
					rangelength: "L'adresse doit être entre 4 et 50 caractères"
				},
				'adr_code_postal': {
					number: "Saisir un entier",
					min: "Code postal entre 1 et 9999999999",
					max: "Code postal entre 1 et 9999999999",
					required: "Code postal obligatoire"
				},
				'adr_ville': {
					required: ville_required_lenght,
					noOnlyWhiteSpaces: noOnlyWhiteSpaces_check,
					rangelength: ville_required_lenght
				},
				'adr_pays': {
					required: pays_required_lenght,
					noOnlyWhiteSpaces: noOnlyWhiteSpaces_check,
					rangelength: pays_required_lenght
				},
				'str_tel': {
					required: phone_required,
					telnbrtooshort: "Numéro de téléphone très court"
				},
				'str_siteweb': {
					rangelength: "L'adresse ne peut pas dépasser 50 caractères"
				}
			}
		});

		$('#str_tel').intlTelInput({
			initialCountry: 'auto',
			geoIpLookup: function(callback) {
				$.get('http://ipinfo.io', function() {}, "jsonp").always(function(resp) {
					var countryCode = (resp && resp.country) ? resp.country : "";
					callback(countryCode);
				});
			},
			autoPlaceholder: true,
			responsiveDropdown: true,
			preferredCountries: ['fr','us','gb'],
			utilsScript: URL + 'public/plugins/intl-tel-input/build/js/utils.js'
		});

})(jQuery);
