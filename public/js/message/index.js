(function ($) {
	"use strict";

$(document).ready(function(){

	$('.message.delete').on('click', function(){
		var message_id = $(this).attr('id');
		Confirm.render('Delete message?','delete','message',message_id);
	});

	/* competence message form */
	$('.message-form').validate({
		rules: {
			'msg_ref': {number:true, min:1, max:999999, required:true, remote:{url:'Message/checkMessageReference', type:'post'}},
			'msg_title': {required:true, noOnlyWhiteSpaces:true, rangelength:[4,40]},
			'msg_cat': {required:true},
			'msg_content': {required:true, noOnlyWhiteSpaces:true, rangelength:[4,1000]}
		},
		messages: {
			'msg_ref': {
				number: "Veuillez à entrer un entier positive",
				min: "Veuillez à entrer un numéro entre 1 et 999999",
				max: "Veuillez à entrer un numéro entre 1 et 999999",
				required: "Veuillez à entrer un numéro entre 1 et 999999",
				remote: "Numéro utilisé"
			},
			'msg_title':
			{
				required: "Veuillez à saisir un libelle du message",
				noOnlyWhiteSpaces: noOnlyWhiteSpaces_check,
				rangelength: "Veuillez à entrer un libelle entre 4 et 40 caractères"
			},
			'msg_cat':
			{
				required: "Veuillez à saisir une catégorie"
			},
			'msg_content':
			{
				required: "Veuillez à saisir un message",
				noOnlyWhiteSpaces: noOnlyWhiteSpaces_check,
				rangelength: "Veuillez à saisir un message entre 4 et 1000 caractères"
			}
		}
	});

	/* Add button message */
	$('.message-button-add').on('click', function() {
		var form = $('.message-form-add');
		form.find('.contact-error').fadeOut();
		form.find('.contact-success').fadeOut();
		form.find('.contact-loading').fadeOut();
		if($('.message-form-add').valid()) {
			var action = form.attr('action');
			$.ajax({
				url : action,
				type: "POST",
				data: {
					thisID: form.find('.thisID').val(),
					msg_ref: form.find('.msg_ref').val(),
					msg_title: form.find('.msg_title').val(),
					msg_cat: form.find('.msg_cat').val(),
					msg_content: form.find('.msg_content').val()
				},
				success: function(result) {
					console.log(result);
					refresh(URL + 'Message');
				},
				error: function(result) {
					form.find('.contact-loading').fadeOut();
					form.find('.contact-error').find('.message').html('Désolé, une erreur est survenue');
					form.find('.contact-error').fadeIn();
				}
			});
	}else{
		form.find('.contact-error').fadeOut();
		form.find('.contact-success').fadeOut();
		form.find('.contact-loading').fadeOut();
		form.find('.contact-error').find('.message').html('Veuillez à remplir tous les champs');
		form.find('.contact-error').fadeIn();
	}
	return false;
	});

	/* Update message */
	$('.message-button-edit').on('click', function() {
		var form = $('.message-form-add');
		form.find('.contact-error').fadeOut();
		form.find('.contact-success').fadeOut();
		form.find('.contact-loading').fadeOut();
		if($('.message-form-add').valid()) {
			var action = form.attr('action');
			$.ajax({
				url : action,
				type: "POST",
				data: {
					thisID: form.find('.thisID').val(),
					msg_ref: form.find('.msg_ref').val(),
					msg_title: form.find('.msg_title').val(),
					msg_cat: form.find('.msg_cat').val(),
					msg_content: form.find('.msg_content').val()
				},
				success: function(result) {
					console.log(result);
					refresh(URL + 'Message');
				},
				error: function(result) {
					form.find('.contact-loading').fadeOut();
					form.find('.contact-error').find('.message').html('Désolé, une erreur est survenue');
					form.find('.contact-error').fadeIn();
				}
			});
	}else{
		form.find('.contact-error').fadeOut();
		form.find('.contact-success').fadeOut();
		form.find('.contact-loading').fadeOut();
		form.find('.contact-error').find('.message').html('Veuillez à remplir tous les champs');
		form.find('.contact-error').fadeIn();
	}
	return false;
	});

});
})(jQuery);
