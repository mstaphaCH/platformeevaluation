<?php

class Controller
{
    /**
     * @var null Database Connection
     */
    public $db = null;

    /**
     * @var null Model
     */
    public $model = null;

    public $profil = null;
    
    public $general_aptitude_dropdown_list = null;
    public $theme_aptitude_dropdown_list = null;
    public $generalCompetenceManagementDropdownList = null;
    public $themeCompetenceManagementDropdownList = null;
    public $generalCompetenceTechniqueDropdownList = null;
    public $themeCompetenceTechniqueDropdownList = null;

    /**
     * Whenever controller is created, open a database connection too and load "the model".
     */
    function __construct()
    {
        $this->openDatabaseConnection();
        $this->createTables();
        $this->defaultQuery();
        $this->loadModel();
        $this->navbar();

        if (isset($_SESSION["user"])) {
            $this->getProfil($_SESSION["id"], $_SESSION["user"], $_SESSION["password"]);
        }else{
            $this->profil = -1;
        }
    }

    /**
     * Open the database connection with the credentials from application/config/config.php
     */
    private function openDatabaseConnection()
    {
        // set the (optional) options of the PDO connection. in this case, we set the fetch mode to
        // "objects", which means all results will be objects, like this: $result->user_name !
        // For example, fetch mode FETCH_ASSOC would return results like this: $result["user_name] !
        // @see http://www.php.net/manual/en/pdostatement.fetch.php
        $options = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ, PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING);

        // generate a database connection, using the PDO connector
        // @see http://net.tutsplus.com/tutorials/php/why-you-should-be-using-phps-pdo-for-database-access/
        $this->db = new PDO(DB_TYPE . ':host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=' . DB_CHARSET, DB_USER, DB_PASS, $options);
    }

    private function createTables() {
        $sql = "CREATE TABLE IF NOT EXISTS edb_profil (
            prf_id int(11) NOT NULL auto_increment,
            prf_nom varchar(24) NOT NULL,
            prf_description varchar(30) NOT NULL,
            PRIMARY KEY (prf_id),
            UNIQUE KEY nom (prf_nom)
            )";
        $query = $this->db->prepare($sql);
        $parameters = array();
        $query->execute($parameters);
        /* Create adresse table */
        $sql = "CREATE TABLE IF NOT EXISTS edb_adresse (
            adr_id int(11) NOT NULL auto_increment,
            adr_num int(11) NOT NULL,
            adr_rue varchar(150) NOT NULL,
            adr_code_postal varchar(50) NOT NULL,
            adr_ville varchar(50) NOT NULL,
            adr_pays varchar(50) NOT NULL,
            PRIMARY KEY (adr_id)
            )";
        $query = $this->db->prepare($sql);
        $parameters = array();
        $query->execute($parameters);
        /* Create structure table */
        $sql = "CREATE TABLE IF NOT EXISTS edb_structure (
            str_id int(11) NOT NULL auto_increment,
            str_nom varchar(50) NOT NULL,
            str_tel varchar(50) NOT NULL,
            str_description varchar(1000) NOT NULL,
            str_nom_contact varchar(50) NOT NULL,
            str_prenom_contact varchar(50) NOT NULL,
            str_email_contact varchar(50) NOT NULL,
            str_siteweb varchar(60) NOT NULL,
            str_status int(11) NOT NULL,
            adr_id int(11) NOT NULL,
            FOREIGN KEY (adr_id) REFERENCES edb_adresse(adr_id),
            PRIMARY KEY (str_id)
            )";
        $query = $this->db->prepare($sql);
        $parameters = array();
        $query->execute($parameters);
        /* Create user table */
        $sql = "CREATE TABLE IF NOT EXISTS edb_utilisateur (
            util_id int(11) NOT NULL auto_increment,
            util_nom varchar(24) NOT NULL,
            util_prenom varchar(24) NOT NULL,
            util_login varchar(24) NOT NULL,
            util_mot_de_passe varchar(100) NOT NULL,
            util_code varchar(100) NOT NULL,
            util_email varchar(50) NOT NULL,
            util_siteweb varchar(50) NOT NULL,
            util_statut varchar(24) NOT NULL,
            user_picture varchar(255),
            util_ipaddress varchar(24),
            util_attempts int(11),
            util_active boolean NOT NULL,
            util_dern_cnx timestamp NOT NULL,
            util_date_creation timestamp NOT NULL,
            util_date_passwd timestamp NOT NULL,
            prf_id int(11) NOT NULL,
            str_id int(11) NOT NULL,
            adr_id int(11) NOT NULL,
            PRIMARY KEY (util_id),
            FOREIGN KEY (prf_id) REFERENCES edb_profil(prf_id),
            FOREIGN KEY (str_id) REFERENCES edb_structure(str_id),
            FOREIGN KEY (adr_id) REFERENCES edb_adresse(adr_id),
            UNIQUE KEY email (util_email)
            )";
        $query = $this->db->prepare($sql);
        $parameters = array();
        $query->execute($parameters);
        /* Create candidat table */
        $sql = "CREATE TABLE IF NOT EXISTS edb_candidat (
            cdt_id int(11) NOT NULL auto_increment,
            cdt_nom varchar(50) NOT NULL,
            cdt_prenom varchar(50) NOT NULL,
            cdt_login varchar(50) NOT NULL,
            cdt_mot_de_passe varchar(50) NOT NULL,
            cdt_email varchar(50) NOT NULL,
            candidate_picture varchar(255),
            cdt_tel varchar (50) NOT NULL,
            cdt_startSession timestamp,
            cdt_endSession timestamp,
            cdt_statut varchar(24) NOT NULL,
            cdt_active boolean NOT NULL,
            cdt_dern_cnx timestamp NOT NULL,
            cdt_date_creation timestamp NOT NULL,
            str_id int(11) NOT NULL,
            adr_id int(11) NOT NULL,
            PRIMARY KEY (cdt_id),
            FOREIGN KEY (str_id) REFERENCES edb_structure(str_id),
            FOREIGN KEY (adr_id) REFERENCES edb_adresse(adr_id),
            UNIQUE KEY email (cdt_login)
            )";
        $query = $this->db->prepare($sql);
        $parameters = array();
        $query->execute($parameters);
        /* Create theme table */
        $sql = "CREATE TABLE IF NOT EXISTS edb_theme (
            thm_id int(11) NOT NULL auto_increment,
            thm_titre varchar(100) NOT NULL,
            thm_description varchar(1000) NOT NULL,
            thm_ponderation varchar(50) NOT NULL,
            PRIMARY KEY (thm_id),
            UNIQUE KEY titre (thm_titre)
            )";
        $query = $this->db->prepare($sql);
        $parameters = array();
        $query->execute($parameters);
        /* Create type table */
        $sql = "CREATE TABLE IF NOT EXISTS edb_type (
            type_id int(11) NOT NULL auto_increment,
            type_nom varchar(50) NOT NULL,
            PRIMARY KEY (type_id),
            UNIQUE KEY titre (type_nom)
            )";
        $query = $this->db->prepare($sql);
        $parameters = array();
        $query->execute($parameters);
        /* Create typology table */
        $sql = "CREATE TABLE IF NOT EXISTS edb_typology (
            typ_id int(11) NOT NULL auto_increment,
            typ_title varchar(255) NOT NULL,
            typ_description varchar(1000) NOT NULL,
            PRIMARY KEY (typ_id),
            UNIQUE KEY title (typ_title)
            )";
        $query = $this->db->prepare($sql);
        $parameters = array();
        $query->execute($parameters);
        /* Create complexity table */
        $sql = "CREATE TABLE IF NOT EXISTS edb_complexity (
            comp_id int(11) NOT NULL auto_increment,
            comp_title varchar(255) NOT NULL,
            comp_description varchar(1000) NOT NULL,
            PRIMARY KEY (comp_id),
            UNIQUE KEY title (comp_title)
            )";
        $query = $this->db->prepare($sql);
        $parameters = array();
        $query->execute($parameters);
        /* Create question table */
        $sql = "CREATE TABLE IF NOT EXISTS edb_question (
            qst_id int(11) NOT NULL auto_increment,
            qst_ref int(50) NOT NULL,
            qst_description varchar(1000) NOT NULL,
            qst_objectif varchar(1000),
            qst_num_rep_correct varchar(50) NOT NULL,
            qst_explication varchar(1000) NOT NULL,
            qst_importance varchar(1000) NOT NULL,
            qst_ref_livre varchar(50) NOT NULL,
            qst_type varchar(50) NOT NULL,
            thm_id int(11),
            type_id int(11),
            typ_id int(11),
            comp_id int(11),
            qst_image varchar(255),
            PRIMARY KEY (qst_id),
            FOREIGN KEY (thm_id) REFERENCES edb_theme(thm_id),
            FOREIGN KEY (type_id) REFERENCES edb_type(type_id),
            FOREIGN KEY (typ_id) REFERENCES edb_typology(typ_id)
            )";
        $query = $this->db->prepare($sql);
        $parameters = array();
        $query->execute($parameters);
        /* Create option table */
        $sql = "CREATE TABLE IF NOT EXISTS edb_option (
            opt_id int(11) NOT NULL auto_increment,
            opt_description varchar(1000) NOT NULL,
            opt_valeur int(11) NOT NULL,
            opt_code varchar(20),
            qst_id int(11) NOT NULL,
            PRIMARY KEY (opt_id),
            FOREIGN KEY (qst_id) REFERENCES edb_question(qst_id)
            )";
        $query = $this->db->prepare($sql);
        $parameters = array();
        $query->execute($parameters);
        /* Create réponse table */
        $sql = "CREATE TABLE IF NOT EXISTS edb_reponse (
            rep_id int(11) NOT NULL auto_increment,
            rep_date varchar(24) NOT NULL,
            rep_choix varchar(24) NOT NULL,
            cdt_id int(11) NOT NULL,
            qst_id int(11) NOT NULL,
            PRIMARY KEY (rep_id),
            FOREIGN KEY (cdt_id) REFERENCES edb_candidat(cdt_id),
            FOREIGN KEY (qst_id) REFERENCES edb_question(qst_id)
            )";
        $query = $this->db->prepare($sql);
        $parameters = array();
        $query->execute($parameters);
        /* Create evaluation table */
        $sql = "CREATE TABLE IF NOT EXISTS edb_evaluation (
            eval_id int(11) NOT NULL auto_increment,
            eval_nom varchar(50) NOT NULL,
            eval_type varchar(100) NOT NULL,
            eval_seuil int(11) NOT NULL,
            eval_description varchar(1000) NOT NULL,
            eval_princ varchar(1000) NOT NULL,
            eval_access varchar(100) NOT NULL,
            eval_cat varchar(255) NOT NULL,
            eval_date_creation timestamp NOT NULL,
            eval_duration timestamp NOT NULL,
            eval_active boolean NOT NULL,
            PRIMARY KEY (eval_id)
            )";
        $query = $this->db->prepare($sql);
        $parameters = array();
        $query->execute($parameters);
        /* Create planif table */
        $sql = "CREATE TABLE IF NOT EXISTS edb_planif (
            plf_id int(11) NOT NULL auto_increment,
            plf_ref varchar(50) NOT NULL,
            plf_title varchar(50) NOT NULL,
            plf_start_date timestamp NOT NULL,
            plf_end_date timestamp NOT NULL,
            plf_resp_firstname varchar(50) NOT NULL,
            plf_resp_lastname varchar(50) NOT NULL,
            plf_type varchar(50) NOT NULL,
            plf_year varchar(50) NOT NULL,
            plf_status varchar(24) NOT NULL,
            plf_active boolean NOT NULL,
            cdt_id int(11),
            util_id int(11) NOT NULL,
            str_id int(11) NOT NULL,
            PRIMARY KEY (plf_id),
            FOREIGN KEY (cdt_id) REFERENCES edb_candidat(cdt_id),
            FOREIGN KEY (util_id) REFERENCES edb_utilisateur(util_id),
            FOREIGN KEY (str_id) REFERENCES edb_structure(str_id)
            )";
        $query = $this->db->prepare($sql);
        $parameters = array();
        $query->execute($parameters);
        /* Create numero table */
        $sql = "CREATE TABLE IF NOT EXISTS edb_evalQues (
            evalQues_id int(11) NOT NULL,
            eval_id int(11) NOT NULL,
            qst_id int(11) NOT NULL,
            PRIMARY KEY (eval_id,qst_id),
            FOREIGN KEY (eval_id) REFERENCES edb_evaluation(eval_id),
            FOREIGN KEY (qst_id) REFERENCES edb_question(qst_id)
            )";
        $query = $this->db->prepare($sql);
        $parameters = array();
        $query->execute($parameters);
        $sql = "CREATE TABLE IF NOT EXISTS edb_evalPlanif (
            evalPlan_id int(11) NOT NULL,
            eval_id int(11) NOT NULL,
            plf_id int(11) NOT NULL,
            PRIMARY KEY (eval_id,plf_id),
            FOREIGN KEY (eval_id) REFERENCES edb_evaluation(eval_id),
            FOREIGN KEY (plf_id) REFERENCES edb_planif(plf_id)
            )";
        $query = $this->db->prepare($sql);
        $parameters = array();
        $query->execute($parameters);
        /* Create message type table */
        $sql = "CREATE TABLE IF NOT EXISTS edb_msgType (
            msgType_id int(11) NOT NULL auto_increment,
            msgType_title varchar(100) NOT NULL,
            msgType_description varchar(1000) NOT NULL,
            PRIMARY KEY (msgType_id)
            )";
        $query = $this->db->prepare($sql);
        $parameters = array();
        $query->execute($parameters);
        /* Create message table */
        $sql = "CREATE TABLE IF NOT EXISTS edb_message (
            msg_id int(11) NOT NULL auto_increment,
            msg_ref int(11) NOT NULL,
            msg_title varchar(100) NOT NULL,
            msg_content varchar(1000) NOT NULL,
            msg_type varchar(100) NOT NULL,
            msg_status varchar(50) NOT NULL,
            msg_active boolean NOT NULL,
            msg_creation_date timestamp NOT NULL,
            util_id int(11) NOT NULL,
            msgType_id int(11) NOT NULL,
            PRIMARY KEY (msg_id),
            FOREIGN KEY (util_id) REFERENCES edb_utilisateur(util_id),
            FOREIGN KEY (msgType_id) REFERENCES edb_msgType(msgType_id)
            )";
        $query = $this->db->prepare($sql);
        $parameters = array();
        $query->execute($parameters);
        /* Create message edb_reponseAptitude */
        $sql = "CREATE TABLE IF NOT EXISTS edb_reponseAptitude (
            ra_id int(11) NOT NULL auto_increment,
            ra_date varchar(24) NOT NULL,
            eval_id int(11) NOT NULL,
            qst_id int(11) NOT NULL,
            cdt_id int(11) NOT NULL,
            ra_option1 varchar(24) NOT NULL,
            ra_option2 varchar(24) NOT NULL,
            ra_option3 varchar(24) NOT NULL,
            ra_option4 varchar(24) NOT NULL,
            ra_idOp1 int(11) NOT NULL,
            ra_idOp2 int(11) NOT NULL,
            ra_idOp3 int(11) NOT NULL,
            ra_idOp4 int(11) NOT NULL,
            PRIMARY KEY (ra_id),
            FOREIGN KEY (qst_id) REFERENCES edb_question(qst_id),
            FOREIGN KEY (eval_id) REFERENCES edb_evaluation(eval_id)
            )";
        $query = $this->db->prepare($sql);
        $parameters = array();
        $query->execute($parameters);        
    }

    private function defaultQuery() { 
        /* Insert two default profil ('global' and 'local') if profil table is empty */
        $sql = "SELECT * FROM edb_profil";
        $query = $this->db->prepare($sql);
        $parameters = array();
        $query->execute($parameters);
           
        if($query->rowCount() < 1) {
            $sql = "INSERT INTO edb_profil (prf_id, prf_nom, prf_description) VALUES (1, 'global', 'Administrateur Global')";
            $query = $this->db->prepare($sql);
            $parameters = array();
            $query->execute($parameters);
            $sql = "INSERT INTO edb_profil (prf_id, prf_nom, prf_description) VALUES (2, 'local', 'Administrateur Local')";
            $query = $this->db->prepare($sql);
            $parameters = array();
            $query->execute($parameters);
            $sql = "INSERT INTO edb_profil (prf_id, prf_nom, prf_description) VALUES (3, 'formateur', 'Formateur')";
            $query = $this->db->prepare($sql);
            $parameters = array();
            $query->execute($parameters);
            $sql = "INSERT INTO edb_profil (prf_id, prf_nom, prf_description) VALUES (4, 'evaluateur', 'Evaluateur')";
            $query = $this->db->prepare($sql);
            $parameters = array();
            $query->execute($parameters);
            $sql = "INSERT INTO edb_profil (prf_id, prf_nom, prf_description) VALUES (5, 'consultant', 'Consultant')";
            $query = $this->db->prepare($sql);
            $parameters = array();
            $query->execute($parameters);
        }
        /* Insert a default adresse for structure and admin ('') */
        $sql = "SELECT * FROM edb_adresse";
        $query = $this->db->prepare($sql);
        $parameters = array();
        $query->execute($parameters);
        if($query->rowCount() < 1) {
            $sql = "INSERT INTO edb_adresse (adr_id, adr_num, adr_rue, adr_code_postal, adr_ville, adr_pays) VALUES (1, 1, '', '', '', '')";
            $query = $this->db->prepare($sql);
            $parameters = array();
            $query->execute($parameters);
            $sql = "INSERT INTO edb_adresse (adr_id,adr_num,adr_rue,adr_code_postal,adr_ville,adr_pays) VALUES (2, 2, '', '', '', '')";
            $query = $this->db->prepare($sql);
            $parameters = array();
            $query->execute($parameters);
            $sql = "INSERT INTO edb_adresse (adr_id,adr_num,adr_rue,adr_code_postal,adr_ville,adr_pays) VALUES (3, 3, '', '', '', '')";
            $query = $this->db->prepare($sql);
            $parameters = array();
            $query->execute($parameters);
        }
        /* Insert a default structure ('excellium consulting') */
        $sql = "SELECT * FROM edb_structure";
        $query = $this->db->prepare($sql);
        $parameters = array();
        $query->execute($parameters);
        if($query->rowCount() < 1) {
        $sql = "INSERT INTO edb_structure (str_id,str_nom,str_tel,str_description,str_nom_contact,str_prenom_contact,str_email_contact,str_siteweb,str_status,adr_id) VALUES (1, 'excellium consulting', '', '', '', '', '', '', 1, 1)";
        $query = $this->db->prepare($sql);
        $parameters = array();
        $query->execute($parameters);
        $sql = "INSERT INTO edb_structure (str_id,str_nom,str_tel,str_description,str_nom_contact,str_prenom_contact,str_email_contact,str_siteweb,str_status,adr_id) VALUES (2, 'candidats libres', '', '', '', '', '', '', 1, 3)";
        }
        $query = $this->db->prepare($sql);
        $parameters = array();
        $query->execute($parameters);
        /* Insert three default types */
        $sql = "SELECT * FROM edb_type";
        $query = $this->db->prepare($sql);
        $parameters = array();
        $query->execute($parameters);
        if($query->rowCount() < 1) {
            $sql = "INSERT INTO edb_type (type_id,type_nom) VALUES (1, 'Une seule réponse possible')";
            $query = $this->db->prepare($sql);
            $parameters = array();
            $query->execute($parameters);
            $sql = "INSERT INTO edb_type (type_id,type_nom) VALUES (2, 'Plusieurs réponses possibles')";
            $query = $this->db->prepare($sql);
            $parameters = array();
            $query->execute($parameters);
            $sql = "INSERT INTO edb_type (type_id,type_nom) VALUES (3, 'Plusieurs réponses avec ordre')";
            $query = $this->db->prepare($sql);
            $parameters = array();
            $query->execute($parameters);
        }
        /* Insert default typology values */
        $sql = "SELECT * FROM edb_typology";
        $query = $this->db->prepare($sql);
        $parameters = array();
        $query->execute($parameters);
        if($query->rowCount() < 1) {
            $sql = "INSERT INTO edb_typology (typ_id, typ_title, typ_description) VALUES (1, 'calcul', 'Calcul')";
            $query = $this->db->prepare($sql);
            $parameters = array();
            $query->execute($parameters);
            $sql = "INSERT INTO edb_typology (typ_id, typ_title, typ_description) VALUES (2, 'connaissance théorique', 'Connaissance théorique')";
            $query = $this->db->prepare($sql);
            $parameters = array();
            $query->execute($parameters);
            $sql = "INSERT INTO edb_typology (typ_id, typ_title, typ_description) VALUES (3, 'fondement', 'Fondement')";
            $query = $this->db->prepare($sql);
            $parameters = array();
            $query->execute($parameters);
            $sql = "INSERT INTO edb_typology (typ_id, typ_title, typ_description) VALUES (4, 'modèle', 'Modèle')";
            $query = $this->db->prepare($sql);
            $parameters = array();
            $query->execute($parameters);
            $sql = "INSERT INTO edb_typology (typ_id, typ_title, typ_description) VALUES (5, 'processus', 'Processus')";
            $query = $this->db->prepare($sql);
            $parameters = array();
            $query->execute($parameters);
        }

        /* Insert default complexity values */
        $sql = "SELECT * FROM edb_complexity";
        $query = $this->db->prepare($sql);
        $parameters = array();
        $query->execute($parameters);
        if($query->rowCount() < 1) {
            $sql = "INSERT INTO edb_complexity (comp_id, comp_title, comp_description) VALUES (1, 'facile', 'Facile')";
            $query = $this->db->prepare($sql);
            $parameters = array();
            $query->execute($parameters);
            $sql = "INSERT INTO edb_complexity (comp_id, comp_title, comp_description) VALUES (2, 'moyen', 'Moyen')";
            $query = $this->db->prepare($sql);
            $parameters = array();
            $query->execute($parameters);
            $sql = "INSERT INTO edb_complexity (comp_id, comp_title, comp_description) VALUES (3, 'difficile', 'Difficile')";
            $query = $this->db->prepare($sql);
            $parameters = array();
            $query->execute($parameters);
        }

        /* Insert default msgType values */
        $sql = "SELECT * FROM edb_msgType";
        $query = $this->db->prepare($sql);
        $parameters = array();
        $query->execute($parameters);
        if($query->rowCount() < 1) {
            $sql = "INSERT INTO edb_msgType (msgType_id, msgType_title, msgType_description) VALUES (1, 'rapport aptitude', 'Rapport aptitude')";
            $query = $this->db->prepare($sql);
            $parameters = array();
            $query->execute($parameters);
            $sql = "INSERT INTO edb_msgType (msgType_id, msgType_title, msgType_description) VALUES (2, 'rapport technique', 'Rapport technique')";
            $query = $this->db->prepare($sql);
            $parameters = array();
            $query->execute($parameters);
        }
    }

    /**
     * Loads the "model".
     * @return object model
     */
    public function loadModel()
    {
        require APP . 'model/model.php';
        require APP . 'model/home.php';
        require APP . 'model/evaluation.php';
        require APP . 'model/candidate.php';
        require APP . 'model/structure.php';
        require APP . 'model/theme.php';
        require APP . 'model/session.php';
        require APP . 'model/message.php';
        require APP . 'model/aptitudeQuestion.php';
        require APP . 'model/competenceQuestion.php';
        require APP . 'model/user.php';
        require APP . 'model/profil.php';
        require APP . 'model/report.php';
        require APP . 'model/about.php';

        // create new "model" (and pass the database connection)
        $this->model = new Model($this->db);
    }

    public function getProfil($sess_id, $sess_user, $sess_password) {
        $this->profil = $this->model->getProfil($sess_id, $sess_user, $sess_password);
    }

    public function navbar() {
        $this->general_aptitude_dropdown_list = $this->model->getGeneralAptitudeDropdownList();
        $this->theme_aptitude_dropdown_list = $this->model->getThemeAptitudeDropdownList();
        $this->generalCompetenceManagementDropdownList = $this->model->getGeneralCompetenceManagementDropdownList();
        $this->themeCompetenceManagementDropdownList = $this->model->getThemeCompetenceManagementDropdownList();
        $this->generalCompetenceTechniqueDropdownList = $this->model->getGeneralCompetenceTechniqueDropdownList();
        $this->themeCompetenceTechniqueDropdownList = $this->model->getThemeCompetenceTechniqueDropdownList();
    }
}
