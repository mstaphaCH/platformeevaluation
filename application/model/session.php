<?php

/**
* 
*/
class Session extends Model
{
	public function getAllActiveSessionsForAdmin($request = null) {

		if($request != null) {
            $search_type = $request['search_type'];
            $search_debut_session = $request['search_debut_session'];
            $search_fin_session = $request['search_fin_session'];
            if (empty($search_type)) {
                if (empty($search_debut_session)) {
                	if (empty($search_fin_session)) {
                		$sql = "SELECT * FROM edb_planif WHERE plf_active=1 ORDER BY plf_id ASC";
                	}else{
                		$sql = "SELECT * FROM edb_planif WHERE plf_active=1 AND CAST(plf_end_date AS DATE) <= '$search_fin_session' ORDER BY plf_id ASC";
                	}                    
                }else{
                    if (empty($search_fin_session)) {
                		$sql = "SELECT * FROM edb_planif WHERE plf_active=1 AND CAST(plf_start_date AS DATE) >= '$search_debut_session' ORDER BY plf_id ASC";
                	}else{
                		$sql = "SELECT * FROM edb_planif WHERE plf_active=1 AND CAST(plf_start_date AS DATE) >= '$search_debut_session' AND CAST(plf_end_date AS DATE) <= '$search_fin_session' ORDER BY plf_id ASC";
                	} 
                }
            }else{
                if (empty($search_debut_session)) {
                	if (empty($search_fin_session)) {
                		$sql = "SELECT * FROM edb_planif WHERE plf_active=1 AND plf_type='$search_type' ORDER BY plf_id ASC";
                	}else{
                		$sql = "SELECT * FROM edb_planif WHERE plf_active=1 AND plf_type='$search_type' AND CAST(plf_end_date AS DATE) <= '$search_fin_session' ORDER BY plf_id ASC";
                	}                    
                }else{
                    if (empty($search_fin_session)) {
                		$sql = "SELECT * FROM edb_planif WHERE plf_active=1 AND plf_type='$search_type' AND CAST(plf_start_date AS DATE) >= '$search_debut_session' ORDER BY plf_id ASC";
                	}else{
                		$sql = "SELECT * FROM edb_planif WHERE plf_active=1 AND plf_type='$search_type' AND CAST(plf_start_date AS DATE) >= '$search_debut_session' AND CAST(plf_end_date AS DATE) <= '$search_fin_session' ORDER BY plf_id ASC";
                	} 
                }
            }

        }else{
        	$sql = "SELECT * FROM edb_planif WHERE plf_active=1 ORDER BY plf_id ASC";
        }

		
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
	
	public function getAllActiveSessionsForNotAdmin($strutil_id, $request = null) {

		if($request != null) {
            $search_type = $request['search_type'];
            $search_debut_session = $request['search_debut_session'];
            $search_fin_session = $request['search_fin_session'];
            if (empty($search_type)) {
                if (empty($search_debut_session)) {
                	if (empty($search_fin_session)) {
                		$sql = "SELECT * FROM edb_planif WHERE str_id='$strutil_id' AND plf_active=1 ORDER BY plf_id ASC";
                	}else{
                		$sql = "SELECT * FROM edb_planif WHERE str_id='$strutil_id' AND plf_active=1 AND CAST(plf_end_date AS DATE) <= '$search_fin_session' ORDER BY plf_id ASC";
                	}                    
                }else{
                    if (empty($search_fin_session)) {
                		$sql = "SELECT * FROM edb_planif WHERE str_id='$strutil_id' AND plf_active=1 AND CAST(plf_start_date AS DATE) >= '$search_debut_session' ORDER BY plf_id ASC";
                	}else{
                		$sql = "SELECT * FROM edb_planif WHERE str_id='$strutil_id' AND plf_active=1 AND CAST(plf_start_date AS DATE) >= '$search_debut_session' AND CAST(plf_end_date AS DATE) <= '$search_fin_session' ORDER BY plf_id ASC";
                	} 
                }
            }else{
                if (empty($search_debut_session)) {
                	if (empty($search_fin_session)) {
                		$sql = "SELECT * FROM edb_planif WHERE str_id='$strutil_id' AND plf_active=1 AND plf_type='$search_type' ORDER BY plf_id ASC";
                	}else{
                		$sql = "SELECT * FROM edb_planif WHERE str_id='$strutil_id' AND plf_active=1 AND plf_type='$search_type' AND CAST(plf_end_date AS DATE) <= '$search_fin_session' ORDER BY plf_id ASC";
                	}                    
                }else{
                    if (empty($search_fin_session)) {
                		$sql = "SELECT * FROM edb_planif WHERE str_id='$strutil_id' AND plf_active=1 AND plf_type='$search_type' AND CAST(plf_start_date AS DATE) >= '$search_debut_session' ORDER BY plf_id ASC";
                	}else{
                		$sql = "SELECT * FROM edb_planif WHERE str_id='$strutil_id' AND plf_active=1 AND plf_type='$search_type' AND CAST(plf_start_date AS DATE) >= '$search_debut_session' AND CAST(plf_end_date AS DATE) <= '$search_fin_session' ORDER BY plf_id ASC";
                	} 
                }
            }

        }else{
        	$sql = "SELECT * FROM edb_planif WHERE str_id='$strutil_id' AND plf_active=1 ORDER BY plf_id ASC";
        }

        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getAllInactiveSessionsForAdmin() {
		$sql = "SELECT * FROM edb_planif WHERE plf_active=0 ORDER BY plf_id ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
	
	public function getAllInactiveSessionsForNotAdmin($strutil_id) {
		$sql = "SELECT * FROM edb_planif WHERE str_id='$strutil_id' AND plf_active=0 ORDER BY plf_id ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getSessionReference($year, $userStructure_id) {
		$sql = "SELECT MAX(plf_ref) as plf_ref, plf_year, str_id FROM edb_planif WHERE plf_year='$year' AND str_id='$userStructure_id' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getSessionByRef($plf_ref) {
		$sql = "SELECT plf_ref FROM edb_planif WHERE plf_ref='$plf_ref' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function addSession($plf_ref, $plf_title, $session_start, $session_end, $plf_resp_firstname, $plf_resp_lastname, $plf_type, $plf_year, $id, $strutil_id) {
		$sql = "INSERT INTO edb_planif (plf_ref, plf_title, plf_start_date, plf_end_date, plf_resp_firstname, plf_resp_lastname, plf_type, plf_year, plf_status, plf_active, util_id, str_id) VALUES ('$plf_ref','$plf_title', '$session_start', '$session_end','$plf_resp_firstname','$plf_resp_lastname','$plf_type','$plf_year', '', 1, '$id', '$strutil_id');";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function getSessionByRefAndYearAndStructure($plf_ref, $year, $strutil_id) {
		$sql = "SELECT plf_id FROM edb_planif WHERE plf_ref='$plf_ref' AND plf_year='$year' AND str_id='$strutil_id' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getUserStructure() {
		return $this->strutil_id;
	}

	public function getSessionById($targetID) {
		$sql = "SELECT * FROM edb_planif WHERE plf_id='$targetID' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll()[0];
	}

	public function update($plf_ref, $plf_title, $session_start, $session_end, $plf_resp_firstname, $plf_resp_lastname, $plf_type, $pid) {
		$sql = "UPDATE edb_planif SET 
            plf_ref='$plf_ref', 
            plf_title='$plf_title', 
            plf_start_date='$session_start', 
            plf_end_date='$session_end', 
            plf_resp_firstname='$plf_resp_firstname', 
            plf_resp_lastname='$plf_resp_lastname', 
            plf_type='$plf_type'
            WHERE plf_id=$pid";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function deleteEvaluationReferences($id_to_delete) {
		$sql = "DELETE FROM edb_evalPlanif WHERE plf_id='$id_to_delete'";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function deleteSession($id_to_delete) {
		$sql = "DELETE FROM edb_planif WHERE plf_id='$id_to_delete' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function fetchInactiveSession($targetID) {
		$sql = "SELECT plf_id FROM edb_planif WHERE plf_id='$targetID' AND plf_active=0 LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function fetchActiveSession($targetID) {
		$sql = "SELECT plf_id FROM edb_planif WHERE plf_id='$targetID' AND plf_active=1 LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function activateSession($targetID) {
		$sql = "UPDATE edb_planif SET plf_active=1 WHERE plf_id='$targetID'";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function deactivateSession($targetID) {
		$sql = "UPDATE edb_planif SET plf_active=0 WHERE plf_id='$targetID'";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function getEvalPlanif($plf_id, $eval_id) {
		$sql = "SELECT * FROM edb_evalPlanif WHERE plf_id='$plf_id' AND eval_id='$eval_id'";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
	public function getSessionByDate($datedebut){
		
		$sql ="SELECT* FROM edb_evalPlanif WHERE DATE_FORMAT(plf_start_date,'%r')=DATE_FORMAT('$datedebut','%r')";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll(); ;
		}
		public function getSessionByDateFin($datedebut){
		
		$sql ="SELECT* FROM edb_evalPlanif WHERE DATE_FORMAT(plf_end_date,'%r')=DATE_FORMAT('$datedebut','%r')";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll(); ;
		}
	public function getSessionByType($type){
		$sql ="SELECT* FROM edb_evalPlanif WHERE plf_type='$type'"; 
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll() ;}

    public function existe_Eval_Session($plf_id, $eval_id){
        $sql = "SELECT * FROM edb_evalPlanif WHERE plf_id='$plf_id' AND eval_id='$eval_id'";
        $query = $this->db->prepare($sql);
        $query->execute();

        if( $query->rowCount() < 1){
            return false;
        }else{
            return true;
        }
    }

    public function ajout_Eval_Session($plf_id, $eval_id){
        $evalPlan_id = $plf_id + $eval_id;
        $sql = "INSERT INTO edb_evalPlanif (evalPlan_id, eval_id, plf_id) VALUES ('$evalPlan_id', '$eval_id', '$plf_id');";
        $query = $this->db->prepare($sql);
        $query->execute();
    }

    public function supp_Eval_Session($plf_id, $eval_id){
        $sql = "DELETE FROM edb_evalPlanif WHERE plf_id='$plf_id' AND eval_id='$eval_id'";
        $query = $this->db->prepare($sql);
        $query->execute();
    }

}
