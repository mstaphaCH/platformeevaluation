<?php

/**
* 
*/
class Structure extends Model
{
	public function getStructureTitlesForAdmin() {
		$sql = "SELECT str_nom FROM edb_structure ORDER BY str_nom ASC, LENGTH(str_nom) DESC";
  $query = $this->db->prepare($sql);
  $query->execute();

  return $query->fetchAll();
 }

 public function getStructureTitlesForNotAdmin() {
  $sql = "SELECT str_nom FROM edb_structure WHERE str_id!=1 ORDER BY str_nom ASC, LENGTH(str_nom) DESC";
  $query = $this->db->prepare($sql);
  $query->execute();

  return $query->fetchAll();
 }

 public function getStructureCountries() {
  $sql = "SELECT DISTINCT a.adr_pays FROM edb_structure s, edb_adresse a WHERE s.adr_id=a.adr_id ORDER BY a.adr_pays ASC, LENGTH(a.adr_pays) DESC";
  $query = $this->db->prepare($sql);
  $query->execute();
  return $query->fetchAll();
 }

 public function getStructureCities() {
  $sql = "SELECT DISTINCT a.adr_ville FROM edb_structure s, edb_adresse a WHERE s.adr_id=a.adr_id ORDER BY a.adr_ville ASC, LENGTH(a.adr_ville) DESC";
  $query = $this->db->prepare($sql);
  $query->execute();
  return $query->fetchAll();
 }

 public function getAllStructuresForAdmin($status) {
  $sql = "SELECT * FROM edb_structure WHERE str_status = '$status' ORDER BY str_nom ASC";
  $query = $this->db->prepare($sql);
  $query->execute();

  return $query->fetchAll();
 }

 public function getAllStructuresForNotAdmin($status) {
  $sql = "SELECT * FROM edb_structure WHERE str_status = '$status' AND str_id!=1 ORDER BY str_nom ASC";
  $query = $this->db->prepare($sql);
  $query->execute();

  return $query->fetchAll();
 }

 public function getStructuresByNameForAdmin($status, $search_name) {
  $sql = "SELECT * FROM edb_structure WHERE str_status='$status' AND str_nom='$search_name' ORDER BY str_nom ASC";
  $query = $this->db->prepare($sql);
  $query->execute();

  return $query->fetchAll();
 }

 public function getStructuresByNameForNotAdmin($status, $search_name) {
  $sql = "SELECT * FROM edb_structure WHERE str_status='$status' AND str_nom='$search_name' WHERE str_id!=1 ORDER BY str_nom ASC";
  $query = $this->db->prepare($sql);
  $query->execute();

  return $query->fetchAll();
 }
/* Country search */
 public function getStructuresByCountryForAdmin($status, $search_country) {
  $sql = "SELECT * FROM edb_structure s, edb_adresse a WHERE s.str_status='$status' AND s.adr_id=a.adr_id AND a.adr_pays='$search_country' ORDER BY a.adr_pays ASC";
  $query = $this->db->prepare($sql);
  $query->execute();
  return $query->fetchAll();
 }
 public function getStructuresByCountryForNotAdmin($status, $search_country) {
  $sql = "SELECT * FROM edb_structure s, edb_adresse a WHERE s.str_status='$status' AND s.adr_id=a.adr_id AND s.str_id!=1 AND a.adr_pays='$search_country' ORDER BY a.adr_pays ASC";
  $query = $this->db->prepare($sql);
  $query->execute();
  return $query->fetchAll();  
 }
/* City search */
 public function getStructuresByCityForAdmin($status, $search_city) {
  $sql = "SELECT * FROM edb_structure s, edb_adresse a WHERE s.str_status = '$status' AND s.adr_id=a.adr_id AND a.adr_ville='$search_city' ORDER BY a.adr_ville ASC";
  $query = $this->db->prepare($sql);
  $query->execute();
  return $query->fetchAll();
 }
 public function getStructuresByCityForNotAdmin($status, $search_city) {
  $sql = "SELECT * FROM edb_structure s, edb_adresse a WHERE s.str_status = '$status' AND s.adr_id=a.adr_id AND s.str_id!=1 AND a.adr_pays='$search_country' ORDER BY a.adr_pays ASC";
  $query = $this->db->prepare($sql);
  $query->execute();

  return $query->fetchAll();  
 }
/* Name & Country search */
 public function getStructuresByNameAndCountryForAdmin($status, $search_name, $search_country) {
  $sql = "SELECT * FROM edb_structure s, edb_adresse a WHERE s.str_status='$status' AND s.str_nom='$search_name' AND s.adr_id=a.adr_id AND a.adr_pays='$search_country' ORDER BY a.adr_pays ASC";
  $query = $this->db->prepare($sql);
  $query->execute();
  return $query->fetchAll();
 }
 public function getStructuresByNameAndCountryForNotAdmin($status, $search_name, $search_country) {
  $sql = "SELECT * FROM edb_structure s, edb_adresse a WHERE s.str_status='$status' AND s.str_nom='$search_name' AND s.adr_id=a.adr_id AND s.str_id!=1 AND a.adr_pays='$search_country' ORDER BY a.adr_pays ASC";
  $query = $this->db->prepare($sql);
  $query->execute();
  return $query->fetchAll();
 }
/* Name & City search */
 public function getStructuresByCityAndNameForAdmin($status, $search_city, $search_name) {
  $sql = "SELECT * FROM edb_structure s, edb_adresse a WHERE s.str_status='$status' AND s.str_nom='$search_name' AND s.adr_id=a.adr_id AND a.adr_ville='$search_city' ORDER BY a.adr_ville ASC";
  $query = $this->db->prepare($sql);
  $query->execute();
  return $query->fetchAll();
 }
 public function getStructuresByCityAndNameForNotAdmin($status, $search_city, $search_name) {
  $sql = "SELECT * FROM edb_structure s, edb_adresse a WHERE s.str_status='$status' AND s.str_nom='$search_name' AND s.adr_id=a.adr_id AND s.str_id!=1 AND a.adr_ville='$search_city' ORDER BY a.adr_ville ASC";
  $query = $this->db->prepare($sql);
  $query->execute();
  return $query->fetchAll();
 }
/* City & Country search */
 public function getStructuresByCityAndCountryForAdmin($status, $search_city, $search_country) {
  $sql = "SELECT * FROM edb_structure s, edb_adresse a WHERE s.str_status='$status' AND s.adr_id=a.adr_id AND a.adr_ville='$search_city' AND a.adr_pays='$search_country' ORDER BY a.adr_pays ASC";
  $query = $this->db->prepare($sql);
  $query->execute();
  return $query->fetchAll();
 }
 public function getStructuresByCityAndCountryForNotAdmin($status, $search_city, $search_country) {
  $sql = "SELECT * FROM edb_structure s, edb_adresse a WHERE s.str_status='$status' AND s.adr_id=a.adr_id AND s.str_id!=1 AND a.adr_ville='$search_city' AND a.adr_pays='$search_country' ORDER BY a.adr_pays ASC";
  $query = $this->db->prepare($sql);
  $query->execute();
  return $query->fetchAll();
 }
/* Name, City & Country search */
 public function getStructuresByCityAndNameAndCountryForAdmin($status, $search_city, $search_name, $search_country) {
  $sql = "SELECT * FROM edb_structure s, edb_adresse a WHERE s.str_status='$status' AND s.str_nom='$search_name' AND s.adr_id=a.adr_id AND a.adr_ville='$search_city' AND a.adr_pays='$search_country' ORDER BY a.adr_pays ASC";
  $query = $this->db->prepare($sql);
  $query->execute();
  return $query->fetchAll();
 }
 public function getStructuresByCityAndNameAndCountryForNotAdmin($status, $search_city, $search_name, $search_country) {
  $sql = "SELECT * FROM edb_structure s, edb_adresse a WHERE s.str_status='$status' AND s.str_nom='$search_name' AND s.adr_id=a.adr_id AND s.str_id!=1 AND a.adr_ville='$search_city' AND a.adr_pays='$search_country' ORDER BY a.adr_pays ASC";
  $query = $this->db->prepare($sql);
  $query->execute();
  return $query->fetchAll();
 }

 public function getStructureCity($address_id) {
  $sql = "SELECT adr_ville FROM edb_adresse WHERE adr_id='$address_id' LIMIT 1";
  $query = $this->db->prepare($sql);
  $query->execute();

  return $query->fetchAll();
 }

 public function getStructureCountry($address_id) {
  $sql = "SELECT adr_pays FROM edb_adresse WHERE adr_id='$address_id' LIMIT 1";
  $query = $this->db->prepare($sql);
  $query->execute();

  return $query->fetchAll();
 }

 public function addAddress($an, $adr_rue, $adr_code_postal, $adr_ville, $adr_pays) {
  $sql = "INSERT INTO edb_adresse (adr_num, adr_rue, adr_code_postal, adr_ville, adr_pays) 
  VALUES ('$an','$adr_rue', '$adr_code_postal', '$adr_ville', '$adr_pays')";
  $query = $this->db->prepare($sql);
  $query->execute();

  return $this->db->lastInsertId();
 }

 public function addStructure($str_nom, $str_tel, $str_description, $str_nom_contact, $str_prenom_contact, $str_email_contact, $str_siteweb, $str_status, $aid) {
  $sql = "INSERT INTO edb_structure (str_nom, str_tel, str_description, str_nom_contact, str_prenom_contact, str_email_contact, str_siteweb, str_status, adr_id) VALUES ('$str_nom', '$str_tel', '$str_description', '$str_nom_contact', '$str_prenom_contact', '$str_email_contact', '$str_siteweb', $str_status, $aid)";
  $query = $this->db->prepare($sql);
  $query->execute();

  return $this->db->lastInsertId();
 }

 public function ReadStructure($targetID) {
  $sql = "SELECT * FROM edb_structure WHERE str_id='$targetID' LIMIT 1";
  $query = $this->db->prepare($sql);
  $query->execute();

  return $query->fetchAll()[0];
 }

 public function ReadAddress($address_id) {
  $sql = "SELECT * FROM edb_adresse WHERE adr_id='$address_id' LIMIT 1";
  $query = $this->db->prepare($sql);
  $query->execute();

  return $query->fetchAll()[0];
 }

 public function getStatus($structure_id) {
  $sql = "SELECT str_status FROM edb_structure WHERE str_id='$structure_id' LIMIT 1";
  $query = $this->db->prepare($sql);
  $query->execute();

  return $query->fetchAll()[0];
 }

 public function deactivateStructure($str_id) {
  $sql = "UPDATE edb_structure SET str_status=0 WHERE str_id='$str_id'";
  $query = $this->db->prepare($sql);
  $query->execute();
 }

 public function activateStructure($str_id) {
  $sql = "UPDATE edb_structure SET str_status=1 WHERE str_id='$str_id'";
  $query = $this->db->prepare($sql);
  $query->execute();
 }

 public function deleteUserByStructureId($str_id) {
  $sql = "DELETE FROM edb_utilisateur WHERE str_id='$str_id'";
  $query = $this->db->prepare($sql);
  $query->execute();
 }

 public function deleteStructure($id_to_delete) {
  $sql = "DELETE FROM edb_structure WHERE str_id='$id_to_delete' LIMIT 1";
  $query = $this->db->prepare($sql);
  $query->execute();
 }

 public function deleteAddress($adr_id) {
  $sql = "DELETE FROM edb_adresse WHERE adr_id='$adr_id' LIMIT 1";
  $query = $this->db->prepare($sql);
  $query->execute();
 }

 public function updateAddress($an, $adr_rue, $adr_code_postal, $adr_ville, $adr_pays, $aid) {
  $sql = "UPDATE edb_adresse SET adr_num='$an', adr_rue='$adr_rue', adr_code_postal='$adr_code_postal', adr_ville='$adr_ville', adr_pays='$adr_pays' WHERE adr_id='$aid'";
  $query = $this->db->prepare($sql);
  $query->execute();
 }

 public function updateStructure($str_nom, $str_tel, $str_description, $str_nom_contact, $str_prenom_contact, $str_email_contact, $str_siteweb, $pid) {
  $sql = "UPDATE edb_structure SET str_nom='$str_nom', str_tel='$str_tel', str_description='$str_description', str_nom_contact='$str_nom_contact', str_prenom_contact='$str_prenom_contact', str_email_contact='$str_email_contact', str_siteweb='$str_siteweb' WHERE str_id='$pid'";
  $query = $this->db->prepare($sql);
  $query->execute();
 }
}
