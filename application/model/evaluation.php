<?php

/**
* 
*/
class Evaluation extends Model
{
	public function getAllEvaluations() {
		$sql = "SELECT * FROM edb_evaluation ORDER BY eval_id ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getEvaluationDescription($eval_id) {
		$sql = "SELECT edb_evaluation.*, DATE_FORMAT(eval_duration,'%H:%i:%s') as edb_time, DATE_FORMAT(eval_duration,'%H')*3600+ DATE_FORMAT(eval_duration,'%i')*60 as edb_timer FROM edb_evaluation WHERE eval_id=$eval_id";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getEvaluationByCategory($category) {
		$sql = "SELECT * FROM edb_evaluation WHERE eval_cat='$category' ORDER BY eval_id ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getEvaluationByType($type) {
		$sql = "SELECT * FROM edb_evaluation WHERE eval_type='$type' ORDER BY eval_id ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getEvaluationByCategoryAndType($category, $type) {
		$sql = "SELECT * FROM edb_evaluation WHERE eval_type='$type' AND eval_cat='$category' ORDER BY eval_id ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getEvaluationByState($state) {
		$sql = "SELECT * FROM edb_evaluation WHERE eval_active='$state' ORDER BY eval_id ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getEvaluationByCategoryAndState($category, $state) {
		$sql = "SELECT * FROM edb_evaluation WHERE eval_active='$state' AND eval_cat='$category' ORDER BY eval_id ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getEvaluationByStateAndType($state, $type) {
		$sql = "SELECT * FROM edb_evaluation WHERE eval_active='$state' AND eval_type='$type' ORDER BY eval_id ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getEvaluationByCategoryStateAndType($state, $type, $category) {
		$sql = "SELECT * FROM edb_evaluation WHERE eval_active='$state' AND eval_type='$type' AND eval_cat='$category' ORDER BY eval_id ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getEvaluationByName($et) {
		$sql = "SELECT eval_id FROM edb_evaluation WHERE eval_nom='$et' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function addEvaluation($et, $etpe, $eval_seuil, $ed, $eval_princ, $eval_access, $eval_cat, $eval_tmp) {
		$sql = "INSERT INTO edb_evaluation (eval_nom, eval_type, eval_seuil, eval_description, eval_princ, eval_access, eval_cat, eval_date_creation, eval_active, eval_duration) VALUES ('$et', '$etpe', '$eval_seuil', '$ed', '$eval_princ', '$eval_access', '$eval_cat', now(), 0,'$eval_tmp')";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $this->db->lastInsertId();
	}

	public function getQuestionsByEvalId($id_to_delete) {
		$sql = "SELECT * FROM edb_evalQues WHERE eval_id='$id_to_delete' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();	
	}
	public function getQuestionsByEvalId_2($eval_id) {
		$sql = "SELECT qst_id FROM edb_evalQues WHERE eval_id='$eval_id'  ORDER BY eval_id ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();	
	}

	public function getEvaluation($id_to_delete) {
		$sql = "SELECT * FROM edb_evaluation WHERE eval_id='$id_to_delete' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();	
	}

	public function deleteEvaluation($id_to_delete) {
		$sql = "DELETE FROM edb_evaluation WHERE eval_id='$id_to_delete' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function getEvaluationById($targetID) {
		$sql = "SELECT * FROM edb_evaluation WHERE eval_id='$targetID' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function update($etpe, $eval_seuil, $ed, $eval_princ, $eval_access, $eval_cat, $eval_tmp, $pid) {
		$sql = "UPDATE edb_evaluation SET eval_type='$etpe', eval_seuil='$eval_seuil', eval_description='$ed', eval_princ='$eval_princ', eval_access='$eval_access', eval_cat='$eval_cat', eval_date_creation=now(), eval_duration='$eval_tmp' WHERE eval_id='$pid'";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function getThemeQuestionsCountByEvalId($eval_id) {
		$sql = "SELECT COUNT(qst_id) as Count FROM edb_evalQues WHERE eval_id='$eval_id'";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getQuestionsCountByEvalId($eval_id) {
		$sql = "SELECT COUNT(q.qst_id) as Count, t.thm_titre as ThemeTitle FROM edb_evalQues eq, edb_question q, edb_theme t WHERE q.thm_id=t.thm_id AND eq.eval_id='$eval_id' GROUP BY t.thm_titre";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function activate($eval_id) {
		$sql = "UPDATE edb_evaluation SET eval_active=1 WHERE eval_id='$eval_id'";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function desactivate($eval_id) {
		$sql = "UPDATE edb_evaluation SET eval_active=0 WHERE eval_id='$eval_id'";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function getCandidate($cdt_id) {
		$sql = "SELECT CONCAT(cdt_nom, ' ',cdt_prenom) as fullname FROM edb_candidat WHERE cdt_id='$cdt_id'";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function sumResponse($cdt_id,$eval_id) {
		$sql = "SELECT sum(IF(er.rep_choix = eq.qst_num_rep_correct , 1 , 0) ) as rep_correct, 
				sum(IF(er.rep_choix != eq.qst_num_rep_correct AND er.rep_choix!='none', 1, 0)) as rep_incorrect, 
				sum(IF(er.rep_choix = 'none' , 1 , 0) ) as rep_vide,
				DATE_FORMAT(rep_date,'%e-%m-%Y') as reDate,
				sum(IF(er.rep_choix != 'none' , 1 , 0) ) as rep
				FROM edb_question eq, edb_reponse er, edb_candidat ec, edb_evaluation ev, edb_evalQues evq 
				WHERE eq.qst_id = er.qst_id 
				AND ec.cdt_id = er.cdt_id 
				AND ev.eval_id=evq.eval_id 
				AND evq.qst_id = eq.qst_id 
				AND ec.cdt_id=$cdt_id AND ev.eval_id=$eval_id";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getNameUtilisateur($util_id) {
		$sql = "SELECT CONCAT(util_nom, ' ',util_prenom) as fullname FROM edb_utilisateur WHERE util_id='$util_id'";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getQuestions($eval_id) {
		$sql = "SELECT edb_question.* FROM edb_question,edb_evalQues,edb_evaluation 
				WHERE edb_question.qst_id = edb_evalQues.qst_id 
				AND edb_evalQues.eval_id = edb_evaluation.eval_id 
				AND edb_evalQues.eval_id=$eval_id";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getQuestionOptions($question_id) {
		$sql = "SELECT * FROM edb_option WHERE qst_id=$question_id";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getEvalType($eval_id) {
		$sql = "SELECT eval_type FROM edb_evaluation WHERE eval_id='$eval_id' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function deleteResponseAptitude($cdt_id, $key) {
		$sql = "DELETE FROM edb_reponseAptitude WHERE cdt_id=$cdt_id AND qst_id=$key";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function insertResponseAptitude($eval_id, $key, $cdt_id, $value, $id1, $id2, $id3, $id4) {
		$sql = "INSERT INTO edb_reponseAptitude VALUES (NULL, date('Y-m-d H:i:s'), $eval_id, $key, $cdt_id, $value[0], $value[1], $value[2], $value[3], $id1, $id2, $id3, $id4)";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $this->db->lastInsertId();
	}

	public function getResponsesAptitude($cdt_id, $eval_id) {
		$sql = "SELECT r.ra_option1,r.ra_option2,r.ra_option3,r.ra_option4,r.ra_idOp1,r.ra_idOp2,r.ra_idOp3,r.ra_idOp4,q.qst_num_rep_correct,r.ra_date FROM edb_question q,edb_reponseAptitude r WHERE r.cdt_id='$cdt_id' AND r.qst_id=q.qst_id AND eval_id='$eval_id'";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function deleteResponse($cdt_id, $key) {
		$sql = "DELETE FROM edb_reponse WHERE cdt_id=$cdt_id AND qst_id=$key";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function insertResponse($value, $cdt_id, $key) {
		$sql = "INSERT INTO edb_reponse VALUES (NULL, 'date(\'Y-m-d H:i:s\')', '$value', $cdt_id, $key)";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $this->db->lastInsertId();
	}

	public function getResponseState($eval_id) {
		$sql = "SELECT edb_question.* FROM edb_question,edb_evalQues,edb_evaluation 
				WHERE edb_question.qst_id=edb_evalQues.qst_id 
				AND edb_evalQues.eval_id=edb_evaluation.eval_id 
				AND edb_evalQues.eval_id=$eval_id";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getExplication($question_id) {
		$sql = "SELECT edb_question.qst_explication,edb_option.opt_description,edb_option.opt_valeur 
				FROM edb_question,edb_option WHERE edb_question.qst_id=$question_id 
				AND edb_question.qst_num_rep_correct=edb_option.opt_id
				AND edb_option.qst_id=$question_id";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
	public function getEvaluationcategories(){
		$sql="SELECT eval_cat FROM edb_evaluation ORDER BY eval_cat ASC, LENGTH(eval_cat) DESC";
		$query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
	}
}
