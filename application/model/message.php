<?php


/**
* 
*/
class Message extends Model
{
	public $msg_id;
	public $msg_ref;
	public $msg_title;
	public $msg_content;
	public $msg_type;
	public $msg_status;
	public $msg_active;
	public $msg_creation_date;

	public $util_id;
	public $msgType_id;
	
	public $test;

	public function Up() {
		$sqlCommand = "CREATE TABLE IF NOT EXISTS edb_message (
		msg_id int(11) NOT NULL auto_increment,
		msg_ref int(11) NOT NULL,
		msg_title varchar(100) NOT NULL,
		msg_content varchar(1000) NOT NULL,
		msg_type varchar(100) NOT NULL,
		msg_status varchar(50) NOT NULL,
		msg_active boolean NOT NULL,
		msg_creation_date timestamp NOT NULL,
		util_id int(11) NOT NULL,
		msgType_id int(11) NOT NULL,
		PRIMARY KEY (msg_id),
		FOREIGN KEY (util_id) REFERENCES edb_utilisateur(util_id),
		FOREIGN KEY (msgType_id) REFERENCES edb_msgType(msgType_id)
		)";
		if (!mysqli_query($mysqli,$sqlCommand)){
			return "Database: edb_message table has not been created.";
		}
	}

	public function getStructureTitlesForAdmin() {
		$sql = "SELECT str_nom FROM edb_structure ORDER BY str_nom ASC, LENGTH(str_nom) DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function deleteMessage($id_to_delete) {
		$sql = "DELETE FROM edb_message WHERE msg_id='$id_to_delete' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function getAllActiveMessages() {
		$sql = "SELECT * FROM edb_message WHERE msg_active=1 ORDER BY msg_id ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getActiveMessagesByCategory($categorie) {
		$sql = "SELECT msgType_id FROM edb_msgType WHERE msgType_title='$categorie' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();	
	}

	public function getActiveMessagesByTypeId($messageType_id) {
		$sql = "SELECT * FROM edb_message WHERE msgType_id=$messageType_id AND msg_active=1 ORDER BY msg_id ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getAllInactiveMessages() {
		$sql = "SELECT * FROM edb_message WHERE msg_active=0 ORDER BY msg_id ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getInactiveMessagesByCategory($categorie) {
		$sql = "SELECT msgType_id FROM edb_msgType WHERE msgType_title='$categorie' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();	
	}

	public function getInactiveMessagesByTypeId($messageType_id) {
		$sql = "SELECT * FROM edb_message WHERE msgType_id=$messageType_id AND msg_active=0 ORDER BY msg_id ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getMessageTitles() {
		$sql = "SELECT * FROM edb_msgType ORDER BY msgType_title ASC, LENGTH(msgType_title) DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();	
	}

	public function getAllCategoryMessages() {
		$sql = "SELECT * FROM edb_msgType ORDER BY msgType_title DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getMessageReference() {
		$sql = "SELECT MAX(msg_ref) as msg_ref FROM edb_message LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getMessageByReference($msg_ref) {
		$sql = "SELECT msg_id FROM edb_message WHERE msg_ref='$msg_ref' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getMessageTypeByCategory($msg_cat) {
		$sql = "SELECT msgType_id FROM edb_msgType WHERE msgType_title='$msg_cat' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function addMessage($msg_ref, $msg_title, $msg_content, $msg_active, $id, $msgType_id) {
		$sql = "INSERT INTO edb_message (msg_ref, msg_title, msg_content, msg_type, msg_status, msg_active, msg_creation_date, util_id, msgType_id) VALUES ('$msg_ref', '$msg_title', '$msg_content', '', '', '$msg_active', now(), 1, '$msgType_id')";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $this->db->lastInsertId();
	}

	public function getMessageCategory($msgType_id) {
		$sql = "SELECT msgType_title FROM edb_msgType WHERE msgType_id='$msgType_id' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getMessageTypeTitles() {
		$sql = "SELECT msgType_title FROM edb_msgType ORDER BY msgType_title DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getMessageById($id) {
		$sql = "SELECT * FROM edb_message WHERE msg_id = $id LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();	
	}

	public function getMessageType($msgType_id) {
		$sql = "SELECT msgType_title FROM edb_msgType WHERE msgType_id=$msgType_id LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();	
	}

	public function getMessageCategoryByTitle($msg_cat) {
		$sql = "SELECT msgType_id FROM edb_msgType WHERE msgType_title='$msg_cat' ORDER BY msgType_title DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function updateMessage($msg_ref, $msg_title, $msg_content, $msgType_id, $pid) {
		$sql = "UPDATE edb_message SET msg_ref='$msg_ref', msg_title='$msg_title', msg_content='$msg_content', msgType_id='$msgType_id' WHERE msg_id=$pid";
        $query = $this->db->prepare($sql);
        $query->execute();
	}
	public function getInactiveMessageById($targetID) {
		$sql = "SELECT msg_id FROM edb_message WHERE msg_id='$targetID' AND msg_active=0 LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();	
	}

	public function Activate($targetID) {
		$sql = "UPDATE edb_message SET msg_active=1 WHERE msg_id='$targetID'";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function getActiveMessageById($targetID) {
		$sql = "SELECT msg_id FROM edb_message WHERE msg_id='$targetID' AND msg_active=1 LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function Inactivate($targetID) {
		$sql = "UPDATE edb_message SET msg_active=0 WHERE msg_id='$targetID'";
        $query = $this->db->prepare($sql);
        $query->execute();
	}
}
