<?php

/**
* 
*/
class CompetenceQuestion extends Model
{
	public function getCompetenceEvaluationNames() {
		$sql = "SELECT eval_nom FROM edb_evaluation WHERE eval_cat='competence' ORDER BY eval_nom ASC, LENGTH(eval_nom) DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getCompetenceEvaluation() {
		$sql = "SELECT * FROM edb_evaluation WHERE eval_cat='competence' ORDER BY eval_nom ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getLatestQuestionNumber() {
		$sql = "SELECT MAX(qst_ref) as qst_ref FROM edb_question WHERE qst_type='competence' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getComplexityId($complexite) {
		$sql = "SELECT comp_id FROM edb_complexity WHERE comp_title='$complexite' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
	public function getCompetenceQuestionBycomplexite($complexite_id){
		$sql= "select * from edb_question where comp_id='$complexite_id' LIMIT 1" ;
		$query =$this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll();
	}
	// 
	public function getAllCompetenceQuestions() {
		$sql = "SELECT * FROM edb_question q LEFT JOIN edb_theme t ON q.thm_id=t.thm_id WHERE q.qst_type='competence' ORDER BY q.qst_ref ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
	
	public function getCompetenceQuestionsByTheme($theme) {
		$sql = "SELECT * FROM edb_question q LEFT JOIN edb_theme t ON q.thm_id=t.thm_id WHERE q.qst_type='competence' AND t.thm_titre='$theme' ORDER BY q.qst_ref ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
	
	public function getCompetenceQuestionsByEvaluation($evaluation) {
		$sql = "SELECT * FROM edb_question q JOIN edb_evalQues eq ON q.qst_id=eq.qst_id JOIN edb_evaluation e ON eq.eval_id=e.eval_id LEFT JOIN edb_theme t ON q.thm_id=t.thm_id WHERE q.qst_type='competence' AND e.eval_nom='$evaluation' ORDER BY q.qst_ref ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
	
	public function getCompetenceQuestionsByThemeAndEvaluation($theme, $evaluation) {
		$sql = "SELECT * FROM edb_question q JOIN edb_evalQues eq ON q.qst_id=eq.qst_id JOIN edb_evaluation e ON eq.eval_id=e.eval_id LEFT JOIN edb_theme t ON q.thm_id=t.thm_id WHERE q.qst_type='competence' AND e.eval_nom='$evaluation' AND t.thm_titre='$theme' ORDER BY q.qst_ref ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
	
	public function getCompetenceQuestionsByCompetence($comp_id) {
		$sql = "SELECT * FROM edb_question q LEFT JOIN edb_theme t ON q.thm_id=t.thm_id WHERE q.qst_type='competence' AND q.comp_id='$comp_id' ORDER BY q.qst_ref ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
	
	public function getCompetenceQuestionsByThemeAndCompetence($theme, $comp_id) {
		$sql = "SELECT * FROM edb_question q LEFT JOIN edb_theme t ON q.thm_id=t.thm_id WHERE q.qst_type='competence' AND t.thm_titre='$theme' AND q.comp_id='$comp_id' ORDER BY q.qst_ref ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
	
	public function getCompetenceQuestionsByEvaluationAndCompetence($evaluation, $comp_id) {
		$sql = "SELECT * FROM edb_question q JOIN edb_evalQues eq ON q.qst_id=eq.qst_id JOIN edb_evaluation e ON eq.eval_id=e.eval_id LEFT JOIN edb_theme t ON q.thm_id=t.thm_id WHERE q.qst_type='competence' AND e.eval_nom='$evaluation' AND q.comp_id='$comp_id' ORDER BY q.qst_ref ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
	
	public function getCompetenceQuestionsByThemeAndEvaluationAndCompetence($theme, $evaluation, $comp_id) {
		$sql = "SELECT * FROM edb_question q JOIN edb_evalQues eq ON q.qst_id=eq.qst_id JOIN edb_evaluation e ON eq.eval_id=e.eval_id LEFT JOIN edb_theme t ON q.thm_id=t.thm_id WHERE q.qst_type='competence' AND e.eval_nom='$evaluation' AND t.thm_titre='$theme' AND q.comp_id='$comp_id' ORDER BY q.qst_ref ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getTypeNames() {
		$sql = "SELECT type_nom FROM edb_type ORDER BY type_nom DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();	
	}

	public function getThemeTitles() {
		$sql = "SELECT thm_titre FROM edb_theme ORDER BY thm_titre DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();	
	}

	public function getTypologyTitles() {
		$sql = "SELECT typ_title FROM edb_typology ORDER BY typ_title DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();	
	}

	public function getComplexityTitles() {
		$sql = "SELECT comp_title FROM edb_complexity ORDER BY comp_id ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();	
	}

	public function getCompetenceQuestionByReference($qst_num) {
		$sql = "SELECT qst_id FROM edb_question WHERE qst_ref='$qst_num' AND qst_type='competence' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getThemeByTitle($qst_thm) {
		$sql = "SELECT thm_id FROM edb_theme WHERE thm_titre='$qst_thm' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();		
	}

	public function getTypeByName($qst_tpe) {
		$sql = "SELECT type_id FROM edb_type WHERE type_nom='$qst_tpe' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function addQuestion($qst_num, $qst_desc, $qst_obj, $qst_rep_cor, $qst_expl, $qst_imp, $thm_id, $type_id, $typ_id, $comp_id, $qst_image) {
		$sql = "INSERT INTO edb_question (qst_ref, qst_description, qst_objectif, qst_num_rep_correct, qst_explication, qst_importance, qst_ref_livre, qst_type, thm_id, type_id, typ_id, comp_id, qst_image) VALUES ('$qst_num', '$qst_desc', '$qst_obj', '$qst_rep_cor', '$qst_expl', '$qst_imp', 'none', 'competence', '$thm_id', '$type_id', '$typ_id', '$comp_id', '$qst_image')";
        $query = $this->db->prepare($sql);
        $query->execute();
        $qst_id = $this->db->lastInsertId();
		$this->AddImageQuestion($qst_id,$qst_image);
		return $qst_id;
	}

	public function addOption($qst_op, $qst_val_op, $qst_id) {
		$sql = "INSERT INTO edb_option (opt_description, opt_valeur, qst_id) VALUES ('$qst_op', '$qst_val_op', '$qst_id')";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $this->db->lastInsertId();
	}

	public function updateQuestionResponse($rep_id, $qst_id) {
		$sql = "UPDATE edb_question SET qst_num_rep_correct='$rep_id' WHERE qst_id='$qst_id'";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function getEvaluationQuestions($qst_id, $eval_id) {
		$sql = "SELECT * FROM edb_evalQues WHERE qst_id=$qst_id AND eval_id='$eval_id'";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getQuestionWithTheme($targetID) {
		$sql = "SELECT * FROM edb_question q LEFT JOIN edb_theme t ON q.thm_id=t.thm_id WHERE q.qst_id='$targetID' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getOptionsByQuestionId($id) {
		$sql = "SELECT * FROM edb_option WHERE qst_id=$id ORDER BY opt_id ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getComplexityById($comp_id) {
		$sql = "SELECT comp_title FROM edb_complexity WHERE comp_id=$comp_id LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getTypologyById($typ_id) {
		$sql = "SELECT typ_title FROM edb_typology WHERE typ_id=$typ_id LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getThemeById($thm_id) {
		$sql = "SELECT thm_titre FROM edb_theme WHERE thm_id=$thm_id LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getTypeById($type_id) {
		$sql = "SELECT type_nom FROM edb_type WHERE type_id=$type_id LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();	
	}

	public function getAllTypes() {
		$sql = "SELECT type_nom FROM edb_type ORDER BY type_nom DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getAllThemes() {
		$sql = "SELECT thm_titre FROM edb_theme ORDER BY thm_titre DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getAllTypologies() {
		$sql = "SELECT typ_title FROM edb_typology ORDER BY typ_title DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getAllComplexities() {
		$sql = "SELECT comp_title FROM edb_complexity ORDER BY comp_title DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getTypeByTitle($qst_tpe) {
		$sql = "SELECT type_id FROM edb_type WHERE type_nom='$qst_tpe' ORDER BY type_nom DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getTypologyByTitle($question_typologie) {
		$sql = "SELECT typ_id FROM edb_typology WHERE typ_title='$question_typologie' ORDER BY typ_title DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getComplexityByTitle($qst_comp) {
		$sql = "SELECT comp_id FROM edb_complexity WHERE comp_title='$qst_comp' ORDER BY comp_title DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function updateOption($opp, $opp_val, $op) {
		$sql = "UPDATE edb_option SET opt_description='$opp', opt_valeur='$opp_val' WHERE opt_id='$op'";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function updateQuestion($qst_desc, $qst_obj, $qst_rep_correct, $qst_expl, $qst_imp, $thm_id, $type_id, $typ_id, $comp_id, $pid, $qst_image) {
		$sql = "UPDATE edb_question SET qst_description='$qst_desc', qst_objectif='$qst_obj', qst_num_rep_correct='$qst_rep_correct', qst_explication='$qst_expl', qst_importance='$qst_imp', thm_id='$thm_id', type_id='$type_id', typ_id='$typ_id', comp_id='$comp_id', qst_image='$qst_image' WHERE qst_id='$pid'";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function getQuestionComplexity($comp_id) {
		$sql = "SELECT comp_title FROM edb_complexity WHERE comp_id='$comp_id' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function deleteOptions($id_to_delete) {
		$sql = "DELETE FROM edb_option WHERE qst_id='$id_to_delete'";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function deleteEvalQues($id_to_delete) {
		$sql = "DELETE FROM edb_evalQues WHERE qst_id='$id_to_delete'";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function deleteQuestion($id_to_delete) {
		$sql = "DELETE FROM edb_question WHERE qst_id='$id_to_delete' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function getQuestion($id_to_delete) {
		$sql = "SELECT * FROM edb_question WHERE qst_id='$id_to_delete' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
	public function AddImageQuestion($qst_id,$path){
		$sql ="INSERT INTO edb_images(image_id,qst_id,path) VALUES ('','$qst_id','$path')";
		$query = $this->db->prepare($sql);
		$query->execute();
	}
}