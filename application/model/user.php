<?php

class User extends Model
{
	public function getStructureNames() {
		$sql = "SELECT str_nom FROM edb_structure ORDER BY str_nom ASC, LENGTH(str_nom) DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getProfilTitles() {
		$sql = "SELECT prf_nom, prf_description FROM edb_profil ORDER BY prf_description DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getAllActiveUsersForAdmin() {
		$sql = "SELECT * FROM edb_utilisateur u, edb_profil p, edb_structure s WHERE u.util_active=1 AND u.prf_id=p.prf_id AND u.str_id=s.str_id ORDER BY u.util_dern_cnx DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
	
	public function getActiveUsersByStructure($str_id) {
		$sql = "SELECT * FROM edb_utilisateur u, edb_profil p, edb_structure s WHERE u.util_active=1 AND u.prf_id=p.prf_id AND u.str_id=s.str_id AND u.str_id=".$str_id." ORDER BY u.util_dern_cnx DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
	
	public function getActiveUsersByTypeForAdmin($type) {
		$sql = "SELECT * FROM edb_utilisateur u, edb_profil p, edb_structure s WHERE u.util_active=1 AND u.prf_id=p.prf_id AND u.str_id=s.str_id AND p.prf_nom='$type' ORDER BY u.util_dern_cnx DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
	
	public function getActiveUsersByStructureAndType($type, $str_id) {
		$sql = "SELECT * FROM edb_utilisateur u, edb_profil p, edb_structure s WHERE u.util_active=1 AND u.prf_id=p.prf_id AND u.str_id=s.str_id AND p.prf_nom='$type' AND u.str_id=$str_id ORDER BY u.util_dern_cnx DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
	
	public function getActiveUsersByUserStructureForAdmin($user_structure) {
		$sql = "SELECT * FROM edb_utilisateur u, edb_profil p, edb_structure s WHERE u.util_active=1 AND u.prf_id=p.prf_id AND u.str_id=s.str_id AND s.str_nom='$user_structure' ORDER BY u.util_dern_cnx DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
	
	public function getActiveUsersByUserAndStructureId($user_structure, $str_id) {
		$sql = "SELECT * FROM edb_utilisateur u, edb_profil p, edb_structure s WHERE u.util_active=1 AND u.prf_id=p.prf_id AND u.str_id=s.str_id AND s.str_nom='$user_structure' AND u.str_id=$str_id ORDER BY u.util_dern_cnx DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
	
	public function getActiveUsersByStructureAndTypeForAdmin($user_structure ,$type) {
		$sql = "SELECT * FROM edb_utilisateur u, edb_profil p, edb_structure s WHERE u.util_active=1 AND u.prf_id=p.prf_id AND u.str_id=s.str_id AND s.str_nom='$user_structure' AND p.prf_nom='$type' ORDER BY u.util_dern_cnx DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
	
	public function getActiveUsersByStructureAndTypeAndStrId($user_structure, $type, $str_id) {
		$sql = "SELECT * FROM edb_utilisateur u, edb_profil p, edb_structure s WHERE u.util_active=1 AND u.prf_id=p.prf_id AND u.str_id=s.str_id AND s.str_nom='$user_structure' AND p.prf_nom='$type' AND u.str_id=$str_id ORDER BY u.util_dern_cnx DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getAllInactiveUsersForAdmin() {
		$sql = "SELECT * FROM edb_utilisateur u, edb_profil p, edb_structure s WHERE u.util_active=0 AND u.prf_id=p.prf_id AND u.str_id=s.str_id ORDER BY u.util_dern_cnx DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
	
	public function getInactiveUsersByStructure($str_id) {
		$sql = "SELECT * FROM edb_utilisateur u, edb_profil p, edb_structure s WHERE u.util_active=0 AND u.prf_id=p.prf_id AND u.str_id=s.str_id AND u.str_id=$str_id ORDER BY u.util_dern_cnx DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
	
	public function getInactiveUsersByTypeForAdmin($type) {
		$sql = "SELECT * FROM edb_utilisateur u, edb_profil p, edb_structure s WHERE u.util_active=0 AND u.prf_id=p.prf_id AND u.str_id=s.str_id AND p.prf_nom='$type' ORDER BY u.util_dern_cnx DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
	
	public function getInactiveUsersByStructureAndType($type, $str_id) {
		$sql = "SELECT * FROM edb_utilisateur u, edb_profil p, edb_structure s WHERE u.util_active=0 AND u.prf_id=p.prf_id AND u.str_id=s.str_id AND p.prf_nom='$type' AND u.str_id=$str_id ORDER BY u.util_dern_cnx DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
	
	public function getInactiveUsersByUserStructureForAdmin($user_structure) {
		$sql = "SELECT * FROM edb_utilisateur u, edb_profil p, edb_structure s WHERE u.util_active=0 AND u.prf_id=p.prf_id AND u.str_id=s.str_id AND s.str_nom='$user_structure' ORDER BY u.util_dern_cnx DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
	
	public function getInactiveUsersByUserAndStructureId($user_structure, $str_id) {
		$sql = "SELECT * FROM edb_utilisateur u, edb_profil p, edb_structure s WHERE u.util_active=0 AND u.prf_id=p.prf_id AND u.str_id=s.str_id AND s.str_nom='$user_structure' AND u.str_id=$str_id ORDER BY u.util_dern_cnx DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
	
	public function getInactiveUsersByStructureAndTypeForAdmin($user_structure ,$type) {
		$sql = "SELECT * FROM edb_utilisateur u, edb_profil p, edb_structure s WHERE u.util_active=0 AND u.prf_id=p.prf_id AND u.str_id=s.str_id AND s.str_nom='$user_structure' AND p.prf_nom='$type' ORDER BY u.util_dern_cnx DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
	
	public function getInactiveUsersByStructureAndTypeAndStrId($user_structure, $type, $str_id) {
		$sql = "SELECT * FROM edb_utilisateur u, edb_profil p, edb_structure s WHERE u.util_active=0 AND u.prf_id=p.prf_id AND u.str_id=s.str_id AND s.str_nom='$user_structure' AND p.prf_nom='$type' AND u.str_id=$str_id ORDER BY u.util_dern_cnx DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getProfilsForAdmin() {
		$sql = "SELECT prf_nom, prf_description FROM edb_profil";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
	
	public function getProfilsForNotAdmin() {
		$sql = "SELECT prf_nom, prf_description FROM edb_profil WHERE prf_id!=1 AND prf_id!=2";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
	
	public function getAllStructures() {
		$sql = "SELECT * FROM edb_structure WHERE str_id!=1 AND str_id!=2";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
	
	public function getStructureByUser($str_id) {
		$sql = "SELECT * FROM edb_structure WHERE str_id=$str_id";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getProfileIdByName($user_profile) {
		$sql = "SELECT prf_id FROM edb_profil WHERE prf_nom='$user_profile' AND prf_id IN (1,2,3,4,5) LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getProfileId($user_profile) {
		$sql = "SELECT prf_id FROM edb_profil WHERE prf_nom='$user_profile' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll()[0];
	}

	public function addAddress($user_address_number, $user_street, $adr_code_postal, $user_city, $user_country) {
		$sql = "INSERT INTO edb_adresse (adr_num, adr_rue, adr_code_postal, adr_ville, adr_pays) 
                    VALUES ('$user_address_number', '$user_street', '$adr_code_postal', '$user_city', '$user_country')";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $this->db->lastInsertId();
	}

	public function insertUser($user_firstname, $user_lastname, $user_login, $user_password, $user_code, $user_email, $user_website, $util_statut, $util_ipaddress, $util_attempts, $util_active, $prf_id, $structure_id, $adr_id) {
		$sql = "INSERT INTO edb_utilisateur (util_nom, util_prenom, util_login, util_mot_de_passe, util_code, util_email, util_siteweb, util_statut, util_ipaddress, util_attempts, util_active, util_dern_cnx, util_date_creation, util_date_passwd, prf_id, str_id, adr_id) 
		VALUES ('$user_firstname', '$user_lastname', '$user_login', '$user_password', '$user_code', '$user_email', '$user_website', '$util_statut', '$util_ipaddress', '$util_attempts', '$util_active', '0000-00-00 00:00:00', now(), now(), '$prf_id', '$structure_id', '$adr_id')";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $this->db->lastInsertId();
	}

	public function getUserByEmail($user_email) {
		$sql = "SELECT util_id FROM edb_utilisateur WHERE util_email='$user_email' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getStructure($user_structure) {
		$sql = "SELECT * FROM edb_structure WHERE str_nom='$user_structure' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll()[0];
	}

	public function getUserById($id) {
		$sql = "SELECT * FROM edb_utilisateur WHERE util_id='$id' AND util_active!=0 LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll()[0];
	}

	public function getAddressById($adr_id) {
		$sql = "SELECT * FROM edb_adresse WHERE adr_id='$adr_id' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll()[0];
	}

	public function update($user_firstname, $user_lastname, $user_login, $user_website, $pid) {
		$sql = "UPDATE edb_utilisateur SET util_nom='$user_firstname', util_prenom='$user_lastname', util_login='$user_login', util_siteweb='$user_website' WHERE util_id='$pid'";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function updateAddress($user_address_number, $user_street, $user_city, $user_country, $aid) {
		$sql = "UPDATE edb_adresse SET adr_num='$user_address_number', adr_rue='$user_street', adr_ville='$user_city', adr_pays='$user_country' WHERE adr_id='$aid'";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function fetchActiveUser($targetID) {
		$sql = "SELECT util_id FROM edb_utilisateur WHERE util_id='$targetID' AND util_active=1 LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll()[0];
	}

	public function deactivateUser($targetID) {
		$sql = "UPDATE edb_utilisateur SET util_active=0 WHERE util_id='$targetID'";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function fetchInactiveUser($targetID) {
		$sql = "SELECT util_id FROM edb_utilisateur WHERE util_id='$targetID' AND util_active=0 LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll()[0];
	}

	public function activateUser($targetID) {
		$sql = "UPDATE edb_utilisateur SET util_active=1, util_attempts=3 WHERE util_id='$targetID'";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function deleteUser($id_to_delete) {
		$sql = "DELETE FROM edb_utilisateur WHERE util_id='$id_to_delete' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function deleteAddress($address_id) {
		$sql = "DELETE FROM edb_adresse WHERE adr_id='$address_id' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();	
	}
}
