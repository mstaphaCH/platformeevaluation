<?php

class Model
{
    /**
     * @param object $db A PDO database connection
     */
    
    public $id = 0;
    public $strutil_id = null;

    function __construct($db)
    {
        try {
            $this->db = $db;
        } catch (PDOException $e) {
            exit('Database connection could not be established.');
        }
    }

    // User model function
    public function getProfileIdByName($user_profile) {
        $sql = "SELECT prf_id FROM edb_profil WHERE prf_nom='$user_profile' AND prf_id IN (1,2,3,4,5) LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
    }

    public function getProfil($sid, $user, $password) {
        $sql = "SELECT * FROM edb_utilisateur WHERE util_id = :sid AND util_email = :user AND util_mot_de_passe LIKE '%{$password}%' AND util_active=1 LIMIT 1";
        $query = $this->db->prepare($sql);
        $parameters = array(':sid' => $sid, ':user' => $user);

        // useful for debugging: you can see the SQL behind above construction by using:
        // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();

        $query->execute($parameters);

        if($query->rowCount() === 1) {
            $row = $query->fetch();
            $passwd_arr = explode(",", $row->util_mot_de_passe);

            if ($sid == $row->util_id && $user == $row->util_email && 
                ($passwd_arr[0] == $password || (isset($passwd_arr[1]) && $passwd_arr[1] == $password))) {
                
                $this->strutil_id = $row->str_id;

                /* structure activation test */
                $sql = "SELECT str_id, str_status FROM edb_structure WHERE str_id = :strutil_id";
                $str_query = $this->db->prepare($sql);
                $parameters = array(':strutil_id' => $this->strutil_id);
                $str_query->execute($parameters);
                
                if ($str_query->rowCount() === 1) {
                    while($str_row = $str_query->fetch()) {
                        if($str_row->str_status===0) {
                            header('location: ../logout.php');
                            exit();
                        }
                    }
                }
                /* end */
                $id = $row->util_id;
                $this->id = $id;
                
                $prf_id = $row->prf_id;

                $sql = "SELECT * FROM edb_profil WHERE prf_id = :prf_id";
                $prf_query = $this->db->prepare($sql);
                $parameters = array(':prf_id' => $prf_id);
                $prf_query->execute($parameters);
                
                if ($prf_query->rowCount() === 1) { // Evaluate the count
                    while($prf_row = $prf_query->fetch()) {
                        if($prf_row->prf_nom === 'global')    $profil = 0; // global user
                        elseif($prf_row->prf_nom === 'local')   $profil = 1;  // local user
                        elseif($prf_row->prf_nom === 'formateur')   $profil = 3;  // formateur user
                        elseif($prf_row->prf_nom === 'evaluateur')   $profil = 4;  // evaluateur user
                        elseif($prf_row->prf_nom === 'consultant')   $profil = 5;  // consultant user
                    }
                }
            }
        }else{
            $sql = "SELECT * FROM edb_candidat WHERE cdt_id = :sid AND cdt_email = :user AND cdt_mot_de_passe = :password AND cdt_active=1 LIMIT 1";
            $cdt_query = $this->db->prepare($sql);
            $parameters = array(':sid' => $sid, ':user' => $user, ':password' => $password);
            $cdt_query->execute($parameters);

            if ($cdt_query->rowCount() === 1) {
                while($cdt_row = $cdt_query->fetch()) {
                    if ($sid == $cdt_row->cdt_id && $user == $cdt_row->cdt_email && $password == $cdt_row->cdt_mot_de_passe) {
                        $this->strutil_id = $cdt_row->str_id;
                        /* structure activation test */
                        $sql = "SELECT str_status FROM edb_structure WHERE str_id = :strutil_id";
                        $str_query = $this->db->prepare($sql);
                        $parameters = array(':strutil_id' => $this->strutil_id);
                        $str_query->execute($parameters);

                        if ($str_query->rowCount() === 1) {
                            while($str_row = $str_query->fetch()) {
                                if($str_row->str_status === 0) {
                                    header('location: ../logout.php');
                                    exit();
                                }
                            }
                        }
                        /* end */
                        $profil = 2; // candidate
                    }
                }
            }else{
                $profil = -1; // not connected
            }
        }

        // fetch() is the PDO method that get exactly one result
        return $profil;
    }

    /**
     * Get all songs from database
     */
    public function getAllSongs()
    {
        $sql = "SELECT id, artist, track, link FROM song";
        $query = $this->db->prepare($sql);
        $query->execute();

        // fetchAll() is the PDO method that gets all result rows, here in object-style because we defined this in
        // core/controller.php! If you prefer to get an associative array as the result, then do
        // $query->fetchAll(PDO::FETCH_ASSOC); or change core/controller.php's PDO options to
        // $options = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC ...
        return $query->fetchAll();
    }

    /**
     * Add a song to database
     * TODO put this explanation into readme and remove it from here
     * Please note that it's not necessary to "clean" our input in any way. With PDO all input is escaped properly
     * automatically. We also don't use strip_tags() etc. here so we keep the input 100% original (so it's possible
     * to save HTML and JS to the database, which is a valid use case). Data will only be cleaned when putting it out
     * in the views (see the views for more info).
     * @param string $artist Artist
     * @param string $track Track
     * @param string $link Link
     */
    public function addSong($artist, $track, $link)
    {
        $sql = "INSERT INTO song (artist, track, link) VALUES (:artist, :track, :link)";
        $query = $this->db->prepare($sql);
        $parameters = array(':artist' => $artist, ':track' => $track, ':link' => $link);

        // useful for debugging: you can see the SQL behind above construction by using:
        // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();

        $query->execute($parameters);
    }

    /**
     * Delete a song in the database
     * Please note: this is just an example! In a real application you would not simply let everybody
     * add/update/delete stuff!
     * @param int $song_id Id of song
     */
    public function deleteSong($song_id)
    {
        $sql = "DELETE FROM song WHERE id = :song_id";
        $query = $this->db->prepare($sql);
        $parameters = array(':song_id' => $song_id);

        // useful for debugging: you can see the SQL behind above construction by using:
        // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();

        $query->execute($parameters);
    }

    /**
     * Get a song from database
     */
    public function getSong($song_id)
    {
        $sql = "SELECT id, artist, track, link FROM song WHERE id = :song_id LIMIT 1";
        $query = $this->db->prepare($sql);
        $parameters = array(':song_id' => $song_id);

        // useful for debugging: you can see the SQL behind above construction by using:
        // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();

        $query->execute($parameters);

        // fetch() is the PDO method that get exactly one result
        return $query->fetch();
    }

    /**
     * Update a song in database
     * // TODO put this explaination into readme and remove it from here
     * Please note that it's not necessary to "clean" our input in any way. With PDO all input is escaped properly
     * automatically. We also don't use strip_tags() etc. here so we keep the input 100% original (so it's possible
     * to save HTML and JS to the database, which is a valid use case). Data will only be cleaned when putting it out
     * in the views (see the views for more info).
     * @param string $artist Artist
     * @param string $track Track
     * @param string $link Link
     * @param int $song_id Id
     */
    public function updateSong($artist, $track, $link, $song_id)
    {
        $sql = "UPDATE song SET artist = :artist, track = :track, link = :link WHERE id = :song_id";
        $query = $this->db->prepare($sql);
        $parameters = array(':artist' => $artist, ':track' => $track, ':link' => $link, ':song_id' => $song_id);

        // useful for debugging: you can see the SQL behind above construction by using:
        // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();

        $query->execute($parameters);
    }

    /**
     * Get simple "stats". This is just a simple demo to show
     * how to use more than one model in a controller (see application/controller/songs.php for more)
     */
    public function getAmountOfSongs()
    {
        $sql = "SELECT COUNT(id) AS amount_of_songs FROM song";
        $query = $this->db->prepare($sql);
        $query->execute();

        // fetch() is the PDO method that get exactly one result
        return $query->fetch()->amount_of_songs;
    }

        public function getGeneralAptitudeDropdownList() {
        $sql = "SELECT eval_id, eval_nom FROM edb_evaluation WHERE eval_cat='aptitude' AND eval_type='général' AND eval_active=1 ORDER BY eval_nom ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
    }
    
    public function getThemeAptitudeDropdownList() {
        $sql = "SELECT eval_id, eval_nom FROM edb_evaluation WHERE eval_cat='aptitude' AND eval_type='thème' AND eval_active=1 ORDER BY eval_nom ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
    }
    
    public function getGeneralCompetenceManagementDropdownList() {
        $sql = "SELECT eval_id, eval_nom FROM edb_evaluation WHERE eval_type='général' AND eval_active=1 AND eval_cat='compétencemanagement' ORDER BY eval_nom ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
    }
    
    public function getThemeCompetenceManagementDropdownList() {
        $sql = "SELECT eval_id, eval_nom FROM edb_evaluation WHERE eval_type='thème' AND eval_active=1 AND eval_cat='compétencemanagement' ORDER BY eval_nom ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
    }
    
    public function getGeneralCompetenceTechniqueDropdownList() {
        $sql = "SELECT eval_id, eval_nom FROM edb_evaluation WHERE eval_cat='compétencetechnique' AND eval_type='général' AND eval_active=1 ORDER BY eval_nom ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
    }

    public function getThemeCompetenceTechniqueDropdownList() {
        $sql = "SELECT eval_id, eval_nom FROM edb_evaluation WHERE eval_cat='compétencetechnique' AND eval_type='thème' AND eval_active=1 ORDER BY eval_nom ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
    }
}
