<?php

/**
* 
*/
class Profil extends Model
{
	public function getCandidateDetails($id) {
		$sql = "SELECT * FROM edb_candidat WHERE cdt_id=$id AND cdt_active=1 LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();	
	}

	public function getUserById($id) {
		$sql = "SELECT * FROM edb_utilisateur u, edb_profil p WHERE u.util_active=1 AND u.prf_id=p.prf_id AND u.util_id=$id LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getAddressById($adr_id) {
		$sql = "SELECT * FROM edb_adresse WHERE adr_id='$adr_id'";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function updateCandidate($firstname, $lastname, $login, $phone_number, $pid) {
		$sql = "UPDATE edb_candidat SET cdt_nom='$firstname', cdt_prenom='$lastname', cdt_login='$login', cdt_tel='$phone_number' WHERE cdt_id='$pid'";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function getCandidateById($pid) {
		$sql = "SELECT * FROM edb_candidat WHERE cdt_id='$pid' AND cdt_active!=0 LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();	
	}

	public function updateUser($firstname, $lastname, $login, $website, $pid) {
		$sql = "UPDATE edb_utilisateur SET util_nom='$firstname', util_prenom='$lastname', util_login='$login', util_siteweb='$website' WHERE util_id='$pid'";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function updateAddress($address_number, $address_street, $address_city, $address_country, $aid) {
		$sql = "UPDATE edb_adresse SET adr_num='$address_number', adr_rue='$address_street', adr_ville='$address_city', adr_pays='$address_country' WHERE adr_id='$aid'";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function updateUserPassword($new_passwd, $user_id) {
		$sql = "UPDATE edb_utilisateur SET util_mot_de_passe='$new_passwd', util_date_passwd=now() WHERE util_id='$user_id' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function updateCandidatePassword($new_passwd, $user_id) {
		$sql = "UPDATE edb_candidat SET cdt_mot_de_passe='$new_passwd' WHERE cdt_id='$user_id' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function InactivateUser($targetID) {
		$sql = "UPDATE edb_utilisateur SET util_active=0 WHERE util_id='$targetID'";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function InactivateCandidate($targetID) {
		$sql = "UPDATE edb_candidat SET cdt_active=0 WHERE cdt_id='$targetID'";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function saveImageForCandidate($fileName, $id) {
		$sql = "UPDATE edb_candidat SET candidate_picture='$fileName' WHERE cdt_id='$id'";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function saveImageForUser($fileName, $id) {
		$sql = "UPDATE edb_utilisateur SET user_picture='$fileName' WHERE util_id='$id'";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

}
