<?php


class Report extends Model
{
	public function getCandidatesFirstname() {
		$sql = "SELECT cdt_nom FROM edb_candidat ORDER BY cdt_nom ASC, LENGTH(cdt_nom) DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
}
