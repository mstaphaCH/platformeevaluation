<?php

/**
* 
*/
class AptitudeQuestion extends Model
{
	public function getAptitudeEvaluationNames() {
		$sql = "SELECT eval_nom FROM edb_evaluation WHERE eval_cat='aptitude' ORDER BY eval_nom ASC, LENGTH(eval_nom) DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getAptitudeEvaluations() {
		$sql = "SELECT * FROM edb_evaluation WHERE eval_cat='aptitude' ORDER BY eval_nom ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getAllAptitudeQuestion() {
		$sql = "SELECT * FROM edb_question WHERE qst_type='aptitude' ORDER BY qst_ref ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getAptitudeQuestionByEvaluation($evaluation) {
		$sql = "SELECT * FROM edb_question q JOIN edb_evalQues eq ON q.qst_id=eq.qst_id JOIN edb_evaluation e ON eq.eval_id=e.eval_id WHERE qst_type='aptitude' AND e.eval_nom='$evaluation' ORDER BY q.qst_ref ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getLatestQuestionNumber() {
		$sql = "SELECT MAX(qst_ref) as qst_ref FROM edb_question WHERE qst_type='aptitude' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();	
	}

	public function getTypeNames() {
		$sql = "SELECT type_nom FROM edb_type ORDER BY type_nom DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();	
	}

	public function getTypeByName($qst_tpe) {
		$sql = "SELECT type_id FROM edb_type WHERE type_nom='$qst_tpe' ORDER BY type_nom DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function addQuestion($qst_num, $qst_desc, $qst_expl, $qst_imp, $type_id) {
		$sql = "INSERT INTO edb_question (qst_ref, qst_description, qst_num_rep_correct, qst_explication, qst_importance, qst_ref_livre, qst_type, type_id) VALUES ('$qst_num', '$qst_desc', 0, '$qst_expl', '$qst_imp', 'none', 'aptitude', '$type_id')";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $this->db->lastInsertId();
	}

	public function addOption($qst_op, $qst_val_op, $code_option, $qst_id) {
		$sql = "INSERT INTO edb_option (opt_description, opt_valeur, opt_code, qst_id) VALUES ('$qst_op', '$qst_val_op', '$code_option', '$qst_id')";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $this->db->lastInsertId();
	}

	public function updateQuestion($rep_id, $qst_id) {
		$sql = "UPDATE edb_question SET qst_num_rep_correct='$rep_id' WHERE qst_id='$qst_id'";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function getEvalQuestionByEvalId($qst_id, $eval_id) {
		$sql = "SELECT * FROM edb_evalQues WHERE qst_id=$qst_id AND eval_id='$eval_id'";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getQuestionById($id_to_delete) {
		$sql = "SELECT * FROM edb_question WHERE qst_id='$id_to_delete' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function deleteOptionByQuestionId($id_to_delete) {
		$sql = "DELETE FROM edb_option WHERE qst_id='$id_to_delete'";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function deleteEvalQuesByQuestionId($id_to_delete) {
		$sql = "DELETE FROM edb_evalQues WHERE qst_id='$id_to_delete'";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function deleteQuestionById($id_to_delete) {
		$sql = "DELETE FROM edb_question WHERE qst_id='$id_to_delete' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function getOptionsByQuestion($pid) {
		$sql = "SELECT * FROM edb_option WHERE qst_id=$pid ORDER BY opt_id ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function updateOption($opp, $opp_val, $code_option, $op, $i) {
		$sql = "UPDATE edb_option SET opt_description='$opp[$i]', opt_valeur='$opp_val[$i]', opt_code='$code_option[$i]' WHERE opt_id='$op'";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function updateOptionByQuestionId($qst_desc, $qst_obj, $qst_expl, $qst_imp, $type_id, $pid) {
		$sql = "UPDATE edb_question SET qst_description='$qst_desc', qst_objectif='$qst_obj', qst_num_rep_correct='none', qst_explication='$qst_expl', qst_importance='$qst_imp', type_id='$type_id' WHERE qst_id='$pid'";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function getTypeById($type_id) {
		$sql = "SELECT type_nom FROM edb_type WHERE type_id=$type_id LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getAllTypes() {
		$sql = "SELECT type_nom FROM edb_type ORDER BY type_nom DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function deleteEvalQuest($qst_id) {
		$sql = "DELETE FROM edb_evalQues WHERE qst_id='$qst_id'";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function getEvaluationByName($boxs, $i) {
		$sql = "SELECT eval_id FROM edb_evaluation WHERE eval_nom='$boxs[$i]'";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function insertEvalQues($id, $qst_id) {
		$sql = "INSERT INTO edb_evalQues (eval_id, qst_id) VALUES ('$id', '$qst_id')";
        $query = $this->db->prepare($sql);
        $query->execute();

         return $this->db->lastInsertId();
	}
}
