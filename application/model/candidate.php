<?php

/**
* 
*/
class Candidate extends Model
{
	public function getCandidatesFirstname() {
		$sql = "SELECT cdt_nom FROM edb_candidat ORDER BY cdt_nom ASC, LENGTH(cdt_nom) DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getCandidatesLastname() {
		$sql = "SELECT cdt_prenom FROM edb_candidat ORDER BY cdt_prenom ASC, LENGTH(cdt_prenom) DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getCandidatesEmail() {
		$sql = "SELECT cdt_email FROM edb_candidat ORDER BY cdt_email ASC, LENGTH(cdt_email) DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getCandidatesStructure() {
		$sql = "SELECT str_nom FROM edb_structure WHERE str_id!=1 ORDER BY str_nom ASC, LENGTH(str_nom) DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getCandidateById($id_to_delete) {
		$sql = "SELECT * FROM edb_candidat WHERE cdt_id='$id_to_delete' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll()[0];
	}

	public function deleteCandidate($id_to_delete) {
		$sql = "DELETE FROM edb_candidat WHERE cdt_id='$id_to_delete' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function deleteAddress($address_id) {
		$sql = "DELETE FROM edb_adresse WHERE adr_id='$address_id' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getDefaultActiveCandidateForGlobalUser() {
		$sql = "SELECT * FROM edb_candidat WHERE cdt_active=1 ORDER BY cdt_dern_cnx DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
	}

	public function getDefaultActiveCandidateForLocalUser($strutil_id) {
		$sql = "SELECT * FROM edb_candidat c, edb_structure s WHERE s.str_id=$strutil_id AND c.str_id=s.str_id AND c.cdt_active=1 ORDER BY c.cdt_dern_cnx DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();	
	}

	public function getActiveCandidateByStructureForGlobalUser($a_search_structure) {
		$sql = "SELECT * FROM edb_candidat c, edb_structure s WHERE c.cdt_active=1 AND c.str_id=s.str_id AND s.str_nom='$a_search_structure' ORDER BY cdt_dern_cnx DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getActiveCandidateByStructureForLocalUser($strutil_id, $a_search_structure) {
		$sql = "SELECT * FROM edb_candidat c, edb_structure s WHERE s.str_id=$strutil_id AND c.str_id=s.str_id AND s.str_nom='$a_search_structure' AND c.cdt_active=1 ORDER BY c.cdt_dern_cnx DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getActiveCandidateByEmailForGlobalUser($a_search_email) {
		$sql = "SELECT * FROM edb_candidat WHERE cdt_active=1 AND cdt_email LIKE '%$a_search_email%' ORDER BY cdt_dern_cnx DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();	
	}

	public function getActiveCandidateByEmailForLocalUser($strutil_id, $a_search_email) {
		$sql = "SELECT * FROM edb_candidat c, edb_structure s WHERE s.str_id=$strutil_id AND c.str_id=s.str_id AND c.cdt_email LIKE '%$a_search_email%' AND c.cdt_active=1 ORDER BY c.cdt_dern_cnx DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getActiveCandidateByLastnameForGlobalUser($a_search_lastname) {
		$sql = "SELECT * FROM edb_candidat WHERE cdt_active=1 AND cdt_prenom LIKE '%$a_search_lastname%' ORDER BY cdt_dern_cnx DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getActiveCandidateByLastnameForLocalUser($strutil_id, $a_search_lastname) {
		$sql = "SELECT * FROM edb_candidat c, edb_structure s WHERE s.str_id=$strutil_id AND c.str_id=s.str_id AND c.cdt_prenom LIKE '%$a_search_lastname%' AND c.cdt_active=1 ORDER BY c.cdt_dern_cnx DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();	
	}

	public function getActiveCandidateByFirstnameForGlobalUser($a_search_firstname) {
		$sql = "SELECT * FROM edb_candidat WHERE cdt_active=1 AND cdt_nom LIKE '%$a_search_firstname%' ORDER BY cdt_dern_cnx DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();	
	}

	public function getActiveCandidateByFirstnameForLocalUser($strutil_id, $a_search_firstname) {
		$sql = "SELECT * FROM edb_candidat c, edb_structure s WHERE s.str_id=$strutil_id AND c.str_id=s.str_id AND c.cdt_nom LIKE '%$a_search_firstname%' AND c.cdt_active=1 ORDER BY c.cdt_dern_cnx DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();	
	}

	// 
	public function getDefaultInactiveCandidateForGlobalUser() {
		$sql = "SELECT * FROM edb_candidat WHERE cdt_active=0 ORDER BY cdt_dern_cnx DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getDefaultInactiveCandidateForLocalUser($strutil_id) {
		$sql = "SELECT * FROM edb_candidat c, edb_structure s WHERE s.str_id=$strutil_id AND c.str_id=s.str_id AND c.cdt_active=0 ORDER BY c.cdt_dern_cnx DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();	
	}

	public function getInactiveCandidateByStructureForGlobalUser($a_search_structure) {
		$sql = "SELECT * FROM edb_candidat c, edb_structure s WHERE c.cdt_active=0 AND c.str_id=s.str_id AND s.str_nom='$a_search_structure' ORDER BY cdt_dern_cnx DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getInactiveCandidateByStructureForLocalUser($strutil_id, $a_search_structure) {
		$sql = "SELECT * FROM edb_candidat c, edb_structure s WHERE s.str_id=$strutil_id AND c.str_id=s.str_id AND s.str_nom='$a_search_structure' AND c.cdt_active=0 ORDER BY c.cdt_dern_cnx DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getInactiveCandidateByEmailForGlobalUser($a_search_email) {
		$sql = "SELECT * FROM edb_candidat WHERE cdt_active=0 AND cdt_email LIKE '%$a_search_email%' ORDER BY cdt_dern_cnx DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();	
	}

	public function getInactiveCandidateByEmailForLocalUser($strutil_id, $a_search_email) {
		$sql = "SELECT * FROM edb_candidat c, edb_structure s WHERE s.str_id=$strutil_id AND c.str_id=s.str_id AND c.cdt_email LIKE '%$a_search_email%' AND c.cdt_active=0 ORDER BY c.cdt_dern_cnx DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getInactiveCandidateByLastnameForGlobalUser($a_search_lastname) {
		$sql = "SELECT * FROM edb_candidat WHERE cdt_active=0 AND cdt_prenom LIKE '%$a_search_lastname%' ORDER BY cdt_dern_cnx DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getInactiveCandidateByLastnameForLocalUser($strutil_id, $a_search_lastname) {
		$sql = "SELECT * FROM edb_candidat c, edb_structure s WHERE s.str_id=$strutil_id AND c.str_id=s.str_id AND c.cdt_prenom LIKE '%$a_search_lastname%' AND c.cdt_active=0 ORDER BY c.cdt_dern_cnx DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();	
	}

	public function getInactiveCandidateByFirstnameForGlobalUser($a_search_firstname) {
		$sql = "SELECT * FROM edb_candidat WHERE cdt_active=0 AND cdt_nom LIKE '%$a_search_firstname%' ORDER BY cdt_dern_cnx DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();	
	}

	public function getInactiveCandidateByFirstnameForLocalUser($strutil_id, $a_search_firstname) {
		$sql = "SELECT * FROM edb_candidat c, edb_structure s WHERE s.str_id=$strutil_id AND c.str_id=s.str_id AND c.cdt_nom LIKE '%$a_search_firstname%' AND c.cdt_active=0 ORDER BY c.cdt_dern_cnx DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
	// 

	public function getStructureById($candidat_str) {
		$sql = "SELECT * FROM edb_structure WHERE str_id='$candidat_str' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function fetchInactiveCandidate($targetID) {
		$sql = "SELECT cdt_id FROM edb_candidat WHERE cdt_id='$targetID' AND cdt_active=0 LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function activateCandidate($targetID) {
		$sql = "UPDATE edb_candidat SET cdt_active=1 WHERE cdt_id='$targetID'";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function fetchActiveCandidate($targetID) {
		$sql = "SELECT cdt_id FROM edb_candidat WHERE cdt_id='$targetID' AND cdt_active=1 LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function deactivateCandidate($targetID) {
		$sql = "UPDATE edb_candidat SET cdt_active=0 WHERE cdt_id='$targetID'";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
	
	public function getStructure($candidat_structure) {
		$sql = "SELECT str_id FROM edb_structure WHERE str_nom='$candidat_structure' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll()[0];
	}
	
	public function getCandidate($candidat_login) {
		$sql = "SELECT cdt_id FROM edb_candidat WHERE cdt_login='$candidat_login' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
	
	public function getSessions() {
		$sql = "SELECT plf_ref, plf_title, plf_year, str_id FROM edb_planif";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
	
	public function getActiveCandidatesByStructure($strutil_id) {
		$sql = "SELECT * FROM edb_candidat c, edb_structure s WHERE c.cdt_active=1 AND c.str_id=s.str_id AND c.str_id=$strutil_id";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getInactiveCandidatesByStructure($strutil_id) {
		$sql = "SELECT * FROM edb_candidat c, edb_structure s WHERE c.cdt_active=1 AND c.str_id=s.str_id AND c.str_id=$strutil_id";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getSessionsByStructure($strutil_id) {
		$sql = "SELECT plf_ref, plf_title, plf_year, str_id FROM edb_planif WHERE str_id='$strutil_id'";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
	
	public function getSessionsByDate($session_job_start, $session_job_end) {
		$sql = "SELECT plf_id, plf_ref, plf_title, plf_year, str_id FROM edb_planif WHERE plf_start_date >= '$session_job_start' AND plf_start_date <= '$session_job_end'";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
	
	public function getSessionsByDateAndStructure($strutil_id, $session_job_start, $session_job_end) {
		$sql = "SELECT plf_id, plf_ref, plf_title, plf_year, str_id FROM edb_planif WHERE str_id='$strutil_id' AND plf_start_date >= '$session_job_start' AND plf_start_date <= '$session_job_end'";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}
	
	public function addAddress($an, $candidat_rue, $candidat_ville, $candidat_pays) {
		$sql = "INSERT INTO edb_adresse (adr_num, adr_rue, adr_code_postal, adr_ville, adr_pays) VALUES ('$an', '$candidat_rue', '', '$candidat_ville', '$candidat_pays')";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $this->db->lastInsertId();
	}
	
	public function addCandidate($candidat_firstname, $candidat_lastname, $candidat_login, $candidat_passwd, $candidat_email, $candidat_tel, $session_start, $session_end, $acces, $structure_id, $aid) {
		// $sql = "INSERT INTO edb_candidat (cdt_nom, cdt_prenom, cdt_login, cdt_mot_de_passe, cdt_email, cdt_tel, cdt_startSession, cdt_endSession, cdt_statut, cdt_active, cdt_dern_cnx, cdt_date_creation, str_id, adr_id) VALUES ('$candidat_firstname','$candidat_lastname', '$candidat_login', '$candidat_passwd','$candidat_email','$candidat_tel','$session_start','$session_end','$acces',1,'0000-00-00 00:00:00',now(), $structure_id, $aid)";
		$sql = "INSERT INTO edb_candidat (cdt_nom, cdt_prenom, cdt_login, cdt_mot_de_passe, cdt_email, cdt_tel, cdt_startSession, cdt_endSession, cdt_statut, cdt_active, cdt_dern_cnx, cdt_date_creation, str_id, adr_id) VALUES ('$candidat_firstname','$candidat_lastname', '$candidat_login', '$candidat_passwd','$candidat_email','$candidat_tel','$session_start','$session_end','$acces',1,now(),now(), $structure_id, $aid)";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $this->db->lastInsertId();
	}

	public function getAddressById($address_id) {
		$sql = "SELECT * FROM edb_adresse WHERE adr_id='$address_id' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll()[0];
	}

	public function update($candidat_firstname, $candidat_lastname, $candidat_login, $candidat_email, $candidat_tel, $session_start, $session_end, $acces, $pid) {
		$sql = "UPDATE edb_candidat SET cdt_nom='$candidat_firstname', cdt_prenom='$candidat_lastname', cdt_login='$candidat_login', cdt_email='$candidat_email', cdt_tel='$candidat_tel', cdt_startSession='$session_start', cdt_endSession='$session_end', cdt_statut='$acces' WHERE cdt_id='$pid'";
        $query = $this->db->prepare($sql);
        $query->execute();
	}

	public function updateAddress($an, $candidat_rue, $candidat_ville, $candidat_pays, $aid) {
		$sql = "UPDATE edb_adresse SET adr_num='$an', adr_rue='$candidat_rue', adr_ville='$candidat_ville', adr_pays='$candidat_pays' WHERE adr_id='$aid'";
        $query = $this->db->prepare($sql);
        $query->execute();
	}
}
