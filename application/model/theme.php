<?php

/**
* 
*/
class Theme extends Model
{
	public function getActiveThemes() {
		$sql = "SELECT * FROM edb_theme ORDER BY thm_titre DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getQuestionNumbers($theme_id) {
		$sql = "SELECT COUNT(qst_id) as count FROM edb_question WHERE thm_id='$theme_id'";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll()[0];
	}

	public function getThemeByTitle($theme_title) {
		$sql = "SELECT thm_id FROM edb_theme WHERE thm_titre='$theme_title' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function addTheme($theme_title, $theme_description, $theme_weighting) {
		$sql = "INSERT INTO edb_theme (thm_titre, thm_description, thm_ponderation) VALUES (:theme_title,:theme_description,:theme_weighting)";
        $query = $this->db->prepare($sql);
        $query->bindParam(':theme_title', $theme_title);
        $query->bindParam(':theme_description', $theme_description);
        $query->bindParam(':theme_weighting', $theme_weighting);
        $query->execute();

        return $this->db->lastInsertId();
	}

	public function getQuestionById($id_to_delete) {
		$sql = "SELECT qst_id FROM edb_question WHERE thm_id='$id_to_delete'";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
	}

	public function getThemeById($id_to_delete) {
		$sql = "SELECT * FROM edb_theme WHERE thm_id='$id_to_delete' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
	}

	public function deleteTheme($id_to_delete) {
		$sql = "DELETE FROM edb_theme WHERE thm_id='$id_to_delete' LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute();	
	}

	public function update($theme_title, $theme_description, $theme_weighting, $pid) {
		$sql = "UPDATE edb_theme SET thm_titre='$theme_title', thm_description='$theme_description', thm_ponderation='$theme_weighting' WHERE thm_id='$pid'";
        $query = $this->db->prepare($sql);
        $query->execute();	
	}
}
