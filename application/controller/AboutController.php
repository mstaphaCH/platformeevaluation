<?php

class AboutController extends Controller
{
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/home/index (which is the default page btw)
     */
    
    public function index()
    {
        // if($this->profil === -1) {
        //     header('location: ' . URL . 'Login');
        // }
        // dark-background
        $header_class = '';
        // ' current'
        $current_class = '';
        // ' current-sub'
        $current_subclass = '';
        
        $this->model = new Report($this->db);
        
        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/_templates/navbar.php';
        require APP . 'view/about/index.php';
        require APP . 'view/_templates/footer.php';
    }
}
