<?php

/**
 * Class Home
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class AccountController extends Controller
{
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/home/index (which is the default page btw)
     */
    
    public function index()
    {
        if (isset($_SESSION["user"])) {
            header('location: ' . URL);
            exit();
        }
        // dark-background
        $header_class = 'dark-background';
        // ' current'
        $current_class = '';
        // ' current-sub'
        $current_subclass = '';

        $this->model = new User($this->db);
        
        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/_templates/navbar.php';
        require APP . 'view/account/index.php';
        require APP . 'view/_templates/footer.php';
    }
    /* post method but it is get => to correct */
    public function userLogin($request) {
        if (isset($request['login_email']) && isset($request['login_passwd'])) {
            if (empty($request['login_email']) || empty($request['login_passwd'])) {echo "false"; exit();}
            // echo "loll";
            // $user = $security->textFilter($_POST["login_email"]);
            // $password = $security->textFilter($_POST["login_passwd"]);

            $user = $request['login_email'];
            $password = $request['login_passwd'];

            if (isset($request['login_code'])) {
                // $code = $security->textFilter($_POST["login_code"]);
                $code = $request['login_code'];
                $codesha = sha1('{/[*'.$code.'{/[*');
            }

            $password = $request['login_passwd'];

            $password = sha1('{/[*'.$password.'{/[*');
            $pass = sha1('{/[*123456789{/[*');
            $cd = sha1('{/[*000000{/[*');

            $sql = "SELECT * FROM edb_utilisateur u, edb_profil p WHERE u.prf_id=p.prf_id AND p.prf_nom='global' LIMIT 1";
            $query = $this->db->prepare($sql);
            $parameters = array();
            $query->execute($parameters);
            if($query->rowCount() < 1) {
                /* Create default admin */
                $sql = "SELECT * FROM edb_adresse WHERE adr_id=2 LIMIT 1";
                $adr_query = $this->db->prepare($sql);
                $parameters = array();
                $adr_query->execute($parameters);
                if($adr_query->rowCount() < 1) {
                    $sql = "INSERT INTO edb_adresse (adr_id,adr_num,adr_rue,adr_code_postal,adr_ville,adr_pays) VALUES (2, 1, '', '', '', '')";
                    $adr = $this->db->prepare($sql);
                    $parameters = array();
                    $adr->execute($parameters);
                }
                $sql = "INSERT INTO edb_utilisateur (util_id,util_nom,util_prenom,util_login,util_mot_de_passe,util_code,util_email,util_siteweb,util_statut,util_ipaddress,util_attempts,util_active,util_dern_cnx,util_date_creation,util_date_passwd,prf_id,str_id,adr_id) VALUES (1, '', '', 'admin', '$pass,$pass', '$cd', 'admin@admin', '', 0, '', 3, 1, now(), now(), now(), 1, 1, 2)";
                $query = $this->db->prepare($sql);
                $parameters = array();
                $query->execute($parameters);
            }
            $sql = "SELECT util_id, util_dern_cnx, str_id, util_attempts FROM edb_utilisateur WHERE util_email='$user' AND util_mot_de_passe LIKE '%{$password}%' AND util_active=1 LIMIT 1";
            $query = $this->db->prepare($sql);
            $parameters = array();
            $query->execute($parameters);
            if($query->rowCount() === 1) {
                if (isset($code) && !empty($code)) {
                    $sql = "SELECT util_id, util_dern_cnx, str_id, util_date_passwd FROM edb_utilisateur WHERE util_email='$user' AND util_mot_de_passe LIKE '%{$password}%' AND util_code='$codesha' AND util_active=1 LIMIT 1";
                    $util_query = $this->db->prepare($sql);
                    $parameters = array();
                    $util_query->execute($parameters);
                    if($util_query->rowCount() === 1) {
                        while($row = $util_query->fetch()){ 
                            $id = $row->util_id;
                            $lastTime = $row->util_dern_cnx;
                            $strId = $row->str_id;
                            $util_date_passwd = $row->util_date_passwd;
                        }
                        /* Structure activation test */
                        $sql = "SELECT str_status FROM edb_structure WHERE str_id = $strId";
                        $str_query = $this->db->prepare($sql);
                        $parameters = array();
                        $str_query->execute($parameters);
                        if($str_query->rowCount() === 1) {
                            while($roww = $str_query->fetch()){
                                if($roww->str_status == 0) {
                                    echo "structure_inactive";
                                    exit();
                                }
                            }
                        }
                        /* end */
                        $_SESSION["id"] = $id;
                        $_SESSION["user"] = $user;
                        $_SESSION["password"] = $password;
                        $_SESSION["accessTime"] = $lastTime;
                        $sql = "UPDATE edb_utilisateur SET util_dern_cnx=now(), util_attempts=3 WHERE util_id='$id'";
                        $query = $this->db->prepare($sql);
                        $parameters = array();
                        $query->execute($parameters);
                        
                        $util_date_passwd = strtotime('+3 months', strtotime($util_date_passwd));
                        $date = new DateTime();
                        $now = $date->getTimestamp();
                        if ($util_date_passwd <= $now) {
                            echo "success,passwd";
                            exit();
                        }
                        echo "success";
                        exit();
                    }else{
                        $row = $util_query->fetch();
                        $id = $row->util_id;
                        if ($id == 1) {
                            echo "code_error,-1";
                            exit();                 
                        }
                        /* Decrement login count */
                        $attempts = --$row->util_attempts;
                        $sql = "UPDATE edb_utilisateur SET util_attempts=$attempts WHERE util_id='$id' AND util_id!=1 LIMIT 1";
                        $query = $this->db->prepare($sql);
                        $parameters = array();
                        $query->execute($parameters);

                        /* Fetch login count... */
                        $sql = "SELECT util_attempts FROM edb_utilisateur WHERE util_id='$id' AND util_id!=1 LIMIT 1";
                        $query = $this->db->prepare($sql);
                        $parameters = array();
                        $query->execute($parameters);
                        if($query->rowCount() === 1) {
                            while($row = $query->fetch()) {
                                if($row->util_attempts == 0) {
                                    $sql = "UPDATE edb_utilisateur SET util_active=0 WHERE util_id='$id' AND util_id!=1 LIMIT 1";
                                    $query = $this->db->prepare($sql);
                                    $parameters = array();
                                    $query->execute($parameters);
                                }
                            }
                        }
                        echo "code_error,$attempts";
                        exit();
                    }
                }else{
                    echo "code";
                    exit();
                }
            }else{
                $sql = "SELECT cdt_id, str_id, cdt_dern_cnx, cdt_endSession FROM edb_candidat WHERE cdt_email='$user' AND cdt_mot_de_passe='$password' AND cdt_active=1 LIMIT 1";
                $query = $this->db->prepare($sql);
                $parameters = array();
                $query->execute($parameters);
                if($query->rowCount() === 1) {
                    $row = $query->fetch();
                    $id = $row->cdt_id;
                    $lastTime = $row->cdt_dern_cnx;
                    
                    /* Session validity check */
                    $session_end = $row->cdt_endSession;
                    $session_end = strtotime('+1 day', strtotime($session_end));
                    $date = new DateTime();
                    $now = $date->getTimestamp();
                    if ($session_end <= $now) {
                        echo "false";
                        exit();
                    }

                    $strId = $row->str_id;
                    /* structure activation test */
                    $sql = "SELECT str_status FROM edb_structure WHERE str_id = $strId";
                    $str_query = $this->db->prepare($sql);
                    $parameters = array();
                    $str_query->execute($parameters);
                    if($str_query->rowCount() === 1) {
                        while($str_row = $str_query->fetch()) {
                            if($str_row->str_status == 0) {
                                echo "structure_inactive";
                                exit();
                            }
                        }
                    }
                    /* Structure login attempt test */
                    // if (isset($_POST['struct']) && !empty($_POST['struct'])) {
                    //  if ($_POST['struct'] == 0) {
                    //      $sql = "UPDATE edb_structure SET str_status=0 WHERE str_id='$str_id'";
                    //      echo "structure_inactive_yes";
                    //      exit();
                    //  }
                    // }
                    /* end */
                    $_SESSION["id"] = $id;
                    $_SESSION["user"] = $user;
                    $_SESSION["password"] = $password;
                    $_SESSION["accessTime"] = $lastTime;
                    $sql = "UPDATE edb_candidat SET cdt_dern_cnx=now() WHERE cdt_id = $id";
                    $query = $this->db->prepare($sql);
                    $parameters = array();
                    $query->execute($parameters);
                    if ($_SESSION["accessTime"]=='0000-00-00 00:00:00') {
                        echo "competance";
                        exit();
                    } else {
                        echo "success";
                        exit();
                    }
                    echo "success";
                    exit();
                }else{
                    echo "false";
                    exit();         
                }
            }
            echo "false";
            exit();
        }
        echo "false";
        exit();
    }

    public function logout() {
        $_SESSION = array();
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
                );
        }
        session_destroy();
        // redirect to home page
        $this->index();
    }
}
