<?php

/**
 * Class Home
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class ProfilController extends Controller
{
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/home/index (which is the default page btw)
     */
    
    public $firstname;
    public $lastname;
    public $login;
    public $email;
    public $phone;
    public $picture;
    public $website;
    public $address_street;
    public $address_city;
    public $address_number;
    public $address_country;
    public $util_date_creation;

    public function index()
    {
        /* session and user test */
        if (!isset($_SESSION["user"])) {
            header('location: ' . URL . 'Account');
            exit();
        }
        if ($this->profil === -1) {
            header('location: ' . URL . 'Account/Logout');
            exit();
        }
        // anonymous user or hacker
        if($this->profil!==0 && $this->profil!==1 && $this->profil!==3 && $this->profil!==4) {
            header('location: ' . URL);
            exit();
        }
        // redirect to home page if isn't admin
        // dark-background
        $header_class = '';
        // ' current'
        $current_class = '';
        // ' current-sub'
        $current_subclass = '';

        $this->model = new Profil($this->db);

        $pageTitle = $this->PageTitle();
        $userDetails = $this->getUser();

        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/_templates/navbar.php';
        require APP . 'view/profil/index.php';
        require APP . 'view/_templates/footer.php';
    }

    public function Expired() {
        /* session and user test */
        if (!isset($_SESSION["user"])) {
            header('location: ' . URL . 'Account');
            exit();
        }
        if ($this->profil === -1) {
            header('location: ' . URL . 'Account/Logout');
            exit();
        }
        // anonymous user or hacker
        if($this->profil!==0 && $this->profil!==1 && $this->profil!==3 && $this->profil!==4) {
            header('location: ' . URL);
            exit();
        }
        // redirect to home page if isn't admin
        // dark-background
        $header_class = '';
        // ' current'
        $current_class = '';
        // ' current-sub'
        $current_subclass = '';

        $this->model = new Profil($this->db);

        $pageTitle = $this->PageTitle();
        $userDetails = $this->getUser();

        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/_templates/navbar.php';
        require APP . 'view/profil/expired.php';
        require APP . 'view/_templates/footer.php';
    }

    public function PageTitle() {
        if(!empty($pfn) || !empty($pln)) {
            return ucfirst($pfn).' '.ucfirst($pln);
        }else{
            return 'Mon profil';
        }
    }

    public function getUser() {
        if (isset($_SESSION["user"])) {
            $profile = "";
            $userId = $_SESSION['id'];
            if ($this->profil === 2) {
                $candidateDetails = $this->model->getCandidateDetails($userId)[0];
                if ($candidateDetails != null) {
                    $this->model->id = $candidateDetails->cdt_id;
                    $this->firstname = $candidateDetails->cdt_nom;
                    $this->lastname = $candidateDetails->cdt_prenom;
                    $this->login = $candidateDetails->cdt_login;
                    $this->email = $candidateDetails->cdt_email;
                    $this->phone = $candidateDetails->cdt_tel;
                    $this->picture = $candidateDetails->candidate_picture;
                    $adr_id = $candidateDetails->adr_id;
                    $this->util_date_creation = strftime("%b %d, %Y", strtotime($candidateDetails->cdt_date_creation));

                    $targetID = $this->model->id;
                }
            }elseif($this->profil===0 || $this->profil===1 || $this->profil===3 || $this->profil===4 || $this->profil===5) {
                $userDetails =  $this->model->getUserById($userId)[0];
                if ($userDetails != null) {
                    $this->model->id = $userDetails->util_id;
                    $this->firstname = $userDetails->util_nom;
                    $this->lastname = $userDetails->util_prenom;
                    $this->login = $userDetails->util_login;
                    $this->email = $userDetails->util_email;
                    $this->website = $userDetails->util_siteweb;
                    $this->picture = $userDetails->user_picture;
                    $adr_id = $userDetails->adr_id;
                    $this->util_date_creation = strftime("%b %d, %Y", strtotime($userDetails->util_date_creation));

                    $targetID = $this->model->id;
                }
            }
            $address = $this->model->getAddressById($adr_id)[0];
            if ($address != null) {
                $this->address_number = $address->adr_num;
                $this->address_street = $address->adr_rue;
                $this->address_city = $address->adr_ville;
                $this->address_country = $address->adr_pays;
            }
        }
    }

    public function Update($request) {
        /* saving form data */
        if (isset($request['submit'])) {  
            if (!empty($request['pl']) && 
                !empty($request['an']) && 
                !empty($request['pr']) && 
                !empty($request['pv']) && 
                !empty($request['pp'])) {
                
                $this->model = new Profil($this->db);

                $pid = (int) $request['thisID'];
                $firstname = $request['pfn'];
                $lastname = $request['pln'];
                $login = $request['pl'];
                $address_number = $request['an'];
                $address_street = $request['pr'];
                $address_city = $request['pv'];
                $address_country = $request['pp'];

                if ($this->profil === 2) {
                    $phone_number = $request['pt'];
                    $this->model->updateCandidate($firstname, $lastname, $login, $phone_number, $pid);
                    
                    $user = $this->model->getCandidateById($pid)[0];
                }else{
                    $website = $request['ps'];
                    $this->model->updateUser($firstname, $lastname, $login, $website, $pid);
                    $user = $this->model->getUserById($pid)[0];
                }
                if ($user != null) {
                    $aid = $user->adr_id;
                }

                $this->model->updateAddress($address_number, $address_street, $address_city, $address_country, $aid);
                
                header('location: ' . URL . 'Profil');
            }
        }
    }

    public function ImageUpload($request) {
        print_r($request);
        if(isset($_FILES['file']['type'])) {
            $this->model = new Profil($this->db);
            $user = $this->getUser();
            $validExtensions = array("jpeg", "jpg", "png");
            $temporary = explode(".", $_FILES["file"]["name"]);
            $file_extension = end($temporary);
            if (
                (($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/jpeg")) && ($_FILES["file"]["size"] < 100000)//Approx. 100kb files can be uploaded.
                && in_array($file_extension, $validExtensions)) {
                if ($_FILES["file"]["error"] > 0) {
                    echo $_FILES["file"]["error"];
                }else{
                    if (file_exists(URL."public/img/profils/".$_FILES["file"]["name"])) {
                        echo $_FILES["file"]["name"]." exists";
                    }else{
                        $sourcePath = $_FILES['file']['tmp_name']; // Storing source path of the file in a variable
                        $targetPath = URL."public/img/profils/".$_FILES['file']['name']; // Target path where file is to be stored
                        $targetPath = "d:/workspace/apache/excellium_consulting/env-recette/public/img/profils/".$request['name'];
                        echo $targetPath;
                        if($this->profil === 2) {
                            $this->model->saveImageForCandidate($_FILES['file']['name'], $this->model->id);
                        }else{
                            $this->model->saveImageForUser($_FILES['file']['name'], $this->model->id);
                        }   
                        if(!move_uploaded_file($_FILES['file']['tmp_name'],$targetPath)) {
                            echo "error";
                            exit();    
                        }
                        echo "success";
                    }
                }
            }else{
                echo "error";
            }
        }
    }

    public function changePassword($request) {
        if (!empty($request['new_passwd']) && !empty($request['new_repasswd']) && !empty($request['user_id'])) {
            $user_id = $request['user_id'];
            $new_passwd = $request['new_passwd'];
            $new_repasswd = $request['new_repasswd'];
            $priority = 1;
        }elseif (!empty($request['new_passwd_2']) && !empty($request['new_repasswd_2']) && !empty($request['user_id'])) {
            $user_id = $request['user_id'];
            $new_passwd = $request['new_passwd_2'];
            $new_repasswd = $request['new_repasswd_2'];
            $priority = 2;
        }else{
            echo "false";
            exit();
        }

        if ($new_passwd!=$new_repasswd) { echo 'not identical'; exit(); }

        $this->model = new Profil($this->db);
        if($this->profil===0 || $this->profil===1 || $this->profil===3 || $this->profil===4 || $this->profil===5) {
            $user = $this->model->getUserById($user_id)[0];
            if ($user != null) {
                $old_passwd = $user->util_mot_de_passe;
            }
            $old_passwd_arr = explode(",", $old_passwd);
            $new_passwd = sha1('{/[*'.$new_passwd.'{/[*');

            if ($this->profil === 0) { 
                if ($priority===1 && isset($old_passwd_arr[1]) && $old_passwd_arr[0]==$old_passwd_arr[1]) {
                    $old_passwd_arr[0] = $new_passwd;
                    $old_passwd_arr[1] = $new_passwd;
                }else{
                    $old_passwd_arr[$priority-1] = $new_passwd;
                }
                $new_passwd = implode(",", $old_passwd_arr);
            }
            
            $this->model->updateUserPassword($new_passwd, $user_id);
        }elseif($this->profil===2) {
            $candidate = $this->model->getCandidateById($user_id)[0];
            
            if ($candidate != null) {
                $new_passwd = sha1('{/[*'.$new_passwd.'{/[*');
                $this->model->updateCandidatePassword($new_passwd, $user_id);
            }
        }

        /* Destroy session if first password changed not the second */
        if($priority === 1) {
            $_SESSION = array();
            if (ini_get("session.use_cookies")) {
                $params = session_get_cookie_params();
                setcookie(session_name(), '', time() - 42000,
                    $params["path"], $params["domain"],
                    $params["secure"], $params["httponly"]
                    );
            }
            session_destroy();
        }

        echo "success";
    }

    public function Inactive($id) {
        $targetID = (int) $id;
        $this->model = new Profil($this->db);
        if($this->profil===0 || $this->profil===1 || $this->profil===3 || $this->profil===4 || $this->profil===5) {
            $user = $this->model->getUserById($targetID)[0];

            if ($user != null) {
                $this->model->InactivateUser($targetID);   
            }
        }elseif($this->profil===2) {
            $candidate = $this->model->getCandidateById($targetID)[0];
            
            if ($candidate != null) {
                $this->model->InactivateCandidate($targetID);
            }
        }
        header('location: ' . URL . 'Account/Logout');
    }
}
