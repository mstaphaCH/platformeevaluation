<?php

/**
 * Class Home
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class AdministrationController extends Controller
{
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/home/index (which is the default page btw)
     */
    
    public function index()
    {
        if (!isset($_SESSION["user"])) {
            header('location: ' . URL);
            exit();
        }
        if ($this->profil === -1) {
            header('location: Account/Logout');
            exit();
        }
        if($this->profil !== 0 && $this->profil !== 1) {
            header('location: ' . URL);
            exit();
        }
        // dark-background
        $header_class = '';
        // ' current'
        $current_class = '';
        // ' current-sub'
        $current_subclass = '';

        $id = $_SESSION['id'];
        
        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/_templates/navbar.php';
        require APP . 'view/administration/index.php';
        require APP . 'view/_templates/footer.php';
    }
}
