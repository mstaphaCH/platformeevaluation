<?php

/**
 * Class Home
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class CompetenceQuestionController extends Controller
{
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/home/index (which is the default page btw)
     */
    public function index($request = null)
    {
        /* session and user test */
        if (!isset($_SESSION["user"])) {
            header('location: ' . URL . 'Account');
            exit();
        }
        if ($this->profil === -1) {
            header('location: ' . URL . 'Account/Logout');
            exit();
        }
        // anonymous user or hacker
        if($this->profil!==0 && $this->profil!==1 && $this->profil!==3 && $this->profil!==4) {
            header('location: ' . URL);
            exit();
        }
        // redirect to home page if isn't admin
        // dark-background
        $header_class = '';
        // ' current'
        $current_class = '';
        // ' current-sub'
        $current_subclass = '';
        
        $this->model = new CompetenceQuestion($this->db);

        $request = $_POST;
        if($request === null){
            $active_questions = $this->getActiveQuestions();
        }else{
            $active_questions = $this->getActiveQuestions($request);
        }
        
        
        $competenceEvaluationNames = $this->model->getCompetenceEvaluationNames();
        $competenceEvaluations = $this->model->getCompetenceEvaluation();
        $qst_idd = $this->getLatestQuestionNumber();

        $typeNames = $this->model->getTypeNames();
        $themeTitles = $this->model->getThemeTitles();
        $typologyTitles = $this->model->getTypologyTitles();
        $complexityTitles = $this->model->getComplexityTitles();

        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/_templates/navbar.php';
        require APP . 'view/competenceQuestion/index.php';
        require APP . 'view/_templates/footer.php';
    }

    // public function Details($request) {
    //     /* session and user test */
    //     if (!isset($_SESSION["user"])) {
    //         header('location: ' . URL . 'Account');
    //         exit();
    //     }
    //     if ($this->profil === -1) {
    //         header('location: ' . URL . 'Account/Logout');
    //         exit();
    //     }
    //     // anonymous user or hacker
    //     if($this->profil!==0 && $this->profil!==1 && $this->profil!==3 && $this->profil!==4) {
    //         header('location: ' . URL);
    //         exit();
    //     }
    //     // redirect to home page if isn't admin
    //     // dark-background
    //     $header_class = '';
    //     // ' current'
    //     $current_class = '';
    //     // ' current-sub'
    //     $current_subclass = '';
        
    //     $this->model = new CompetenceQuestion($this->db);

    //     $types = $this->model->getAllTypes();
    //     $themes = $this->model->getAllThemes();
    //     $typologies = $this->model->getAllTypologies();
    //     $complexities = $this->model->getAllComplexities();

    //     /* extract question data from the database when page loaded */
    //     $targetID = $request;

    //     $questionWithTheme = $this->model->getQuestionWithTheme($targetID)[0];
        
    //     if ($questionWithTheme != null) {
    //         $id = $questionWithTheme->qst_id;
    //         $qst_ref = $questionWithTheme->qst_ref;
    //         $qst_description = $questionWithTheme->qst_description;
    //         $qst_num_rep_correct = $questionWithTheme->qst_num_rep_correct;

    //         $options = $this->model->getOptionsByQuestionId($id);
            
    //         $i=0;
    //         if ($options != null) {
    //             foreach ($options as $key => $option) {
    //                 $op[$i] = $option->opt_description;
    //                 $op_val[$i] = $option->opt_valeur;

    //                 if($qst_num_rep_correct === $option->opt_id) {
    //                     if ($i==0) {
    //                         $correct = 'qst_op1';
    //                     }
    //                     if ($i==1) {
    //                         $correct = 'qst_op2';
    //                     }
    //                     if ($i==2) {
    //                         $correct = 'qst_op3';
    //                     }
    //                     if ($i==3) {
    //                         $correct = 'qst_op4';
    //                     }
    //                 }
    //                 $i++;
    //             }
    //         }
    //         $qst_explication = $questionWithTheme->qst_explication;
    //         $qst_importance = $questionWithTheme->qst_importance;
    //         $qst_objectif = $questionWithTheme->qst_objectif;
    //         $questionImage = $questionWithTheme->qst_image;

    //         $comp_id = $questionWithTheme->comp_id;
    //         $thm_id = $questionWithTheme->thm_id;
    //         $typ_id = $questionWithTheme->typ_id;
    //         $type_id = $questionWithTheme->type_id;

    //         if (!is_null($comp_id)) {
    //             $complexity = $this->model->getComplexityById($comp_id)[0];
    //             if ($complexity != null) {
    //                 $complexity_title = $complexity->comp_title;
    //             }
    //         }else{
    //             $complexity_title = "null";
    //         }

    //         if (!is_null($typ_id)) {
    //             $typology = $this->model->getTypologyById($typ_id)[0];
                
    //             if ($typology != null) {
    //                 $typology_title = $typology->typ_title;
    //             }
    //         }else{
    //             $typology_title = "null";
    //         }

    //         if (!is_null($thm_id)) {
    //             $theme = $this->model->getThemeById($thm_id)[0];
                
    //             if ($theme != null) {
    //                 $theme_title = $theme->thm_titre;
    //             }
    //         }else{
    //             $theme_title = "null";
    //         }

    //         if (!is_null($type_id)) {
    //             $type = $this->model->getTypeById($type_id)[0];
                
    //             if ($type != null) {
    //                 $type_title = $type->type_nom;
    //             }
    //         }else{
    //             $type_title = "null";
    //         }
    //     }
    
    //     // load views
    //     require APP . 'view/_templates/header.php';
    //     require APP . 'view/_templates/navbar.php';
    //     require APP . 'view/competenceQuestion/details.php';
    //     require APP . 'view/_templates/footer.php';
    // }

    public function Add($request) {

        

                
        /* Form question's creation */
        if (!empty($request['qst_num']) && 
            !empty($request['qst_desc']) && 
            !empty($request['qst_comp']) && 
            !empty($request['qst_rep_cor']) && 
            !empty($request['qst_op1']) && 
            !empty($request['qst_op2']) && 
            !empty($request['qst_op3']) && 
            !empty($request['qst_op4']) && 
            !empty($request['qst_thm']) && 
            !empty($request['qst_tpe']) &&  
            isset($request['qst_val_op1']) && 
            isset($request['qst_val_op2']) && 
            isset($request['qst_val_op3']) && 
            isset($request['qst_val_op4']) && 
            !empty($request['question_typologie'])) {

                $this->model = new CompetenceQuestion($this->db);

                $qst_thm = $request['qst_thm'];

                $theme = $this->model->getThemeByTitle($qst_thm)[0];
                
                if ($theme != null) {
                    $thm_id = $theme->thm_id;
                }

                $qst_tpe = $request['qst_tpe'];

                $type = $this->model->getTypeByName($qst_tpe)[0];
                
                if ($type != null) {
                    $type_id = $type->type_id;
                }

                $question_typologie = $request['question_typologie'];

                $typology = $this->model->getTypologyByTitle($question_typologie)[0];
                
                if ($typology != null) {
                    $typ_id = $typology->typ_id;
                }

                $qst_comp = $request['qst_comp'];

                $complexity = $this->model->getComplexityByTitle($qst_comp)[0];
                
                if ($complexity != null) {
                    $comp_id = $complexity->comp_id;
                }

                $qst_num = $request['qst_num'];
                $qst_desc = $request['qst_desc'];
                $qst_op1 = $request['qst_op1'];
                $qst_op2 = $request['qst_op2'];
                $qst_op3 = $request['qst_op3'];
                $qst_op4 = $request['qst_op4'];

                $qst_val_op1 = $request['qst_val_op1'];
                $qst_val_op2 = $request['qst_val_op2'];
                $qst_val_op3 = $request['qst_val_op3'];
                $qst_val_op4 = $request['qst_val_op4'];

                $qst_rep_cor = $request['qst_rep_cor'];
                $qst_obj = $request['qst_obj'];
                $qst_expl = $request['qst_expl'];
                $qst_imp = $request['qst_imp'];
                $qst_obj = $request['qst_obj'];

                if (isset($_FILES['question_image'])) {
                    $qst_image = $this->imageUpload($_FILES['question_image']);
                }else{
                    $qst_image = "";
                }

                echo "<pre>";
                print_r($_FILES['question_image']);
                echo "</pre>";
                echo "<br/>".$qst_image;
                //exit();


                /* Question insert */
               $qst_id = $this->model->addQuestion($qst_num, $qst_desc, $qst_obj, $qst_rep_cor, $qst_expl, $qst_imp, $thm_id, $type_id, $typ_id, $comp_id, $qst_image);

                /* Options insert */
                $rep1_id = $this->model->addOption($qst_op1, $qst_val_op1, $qst_id);

                if($qst_rep_cor==='qst_op1') {
                    $rep_id = $rep1_id;
                }

                $rep2_id = $this->model->addOption($qst_op2, $qst_val_op2, $qst_id);

                if($qst_rep_cor==='qst_op2') {
                    $rep_id = $rep2_id;
                }

                $rep3_id = $this->model->addOption($qst_op3, $qst_val_op3, $qst_id);

                if($qst_rep_cor==='qst_op3') {
                    $rep_id = $rep3_id;
                }

                $rep4_id = $this->model->addOption($qst_op4, $qst_val_op4, $qst_id);

                if($qst_rep_cor==='qst_op4') {
                    $rep_id = $rep4_id;
                }

                $this->model->updateQuestionResponse($rep_id, $qst_id);
                header('location: '.URL.'CompetenceQuestion');
            }
    }

    public function Edit($request) {
        /* session and user test */
        if (!isset($_SESSION["user"])) {
            header('location: ' . URL . 'Account');
            exit();
        }
        if ($this->profil === -1) {
            header('location: ' . URL . 'Account/Logout');
            exit();
        }
        // anonymous user or hacker
        if($this->profil!==0 && $this->profil!==1 && $this->profil!==3 && $this->profil!==4) {
            header('location: ' . URL);
            exit();
        }
        // redirect to home page if isn't admin
        // dark-background
        $header_class = '';
        // ' current'
        $current_class = '';
        // ' current-sub'
        $current_subclass = '';
        
        $this->model = new CompetenceQuestion($this->db);

        $types = $this->model->getAllTypes();
        $themes = $this->model->getAllThemes();
        $typologies = $this->model->getAllTypologies();
        $complexities = $this->model->getAllComplexities();

        /* extract question data from the database when page loaded */
        $targetID = $request;

        $questionWithTheme = $this->model->getQuestionWithTheme($targetID)[0];
        
        if ($questionWithTheme != null) {
            $id = $questionWithTheme->qst_id;
            $qst_ref = $questionWithTheme->qst_ref;
            $qst_description = $questionWithTheme->qst_description;
            $qst_num_rep_correct = $questionWithTheme->qst_num_rep_correct;
            $qst_image = $questionWithTheme->qst_image;

            $options = $this->model->getOptionsByQuestionId($id);
            
            $i=0;
            if ($options != null) {
                foreach ($options as $key => $option) {
                    $op[$i] = $option->opt_description;
                    $op_val[$i] = $option->opt_valeur;

                    if($qst_num_rep_correct === $option->opt_id) {
                        if ($i==0) {
                            $correct = 'qst_op1';
                        }
                        if ($i==1) {
                            $correct = 'qst_op2';
                        }
                        if ($i==2) {
                            $correct = 'qst_op3';
                        }
                        if ($i==3) {
                            $correct = 'qst_op4';
                        }
                    }
                    $i++;
                }
            }
            $qst_explication = $questionWithTheme->qst_explication;
            $qst_importance = $questionWithTheme->qst_importance;
            $qst_objectif = $questionWithTheme->qst_objectif;
            
            $comp_id = $questionWithTheme->comp_id;
            $thm_id = $questionWithTheme->thm_id;
            $typ_id = $questionWithTheme->typ_id;
            $type_id = $questionWithTheme->type_id;
            $questionImage = $questionWithTheme->qst_image;

            if (!is_null($comp_id)) {
                $complexity = $this->model->getComplexityById($comp_id)[0];
                if ($complexity != null) {
                    $complexity_title = $complexity->comp_title;
                }
            }else{
                $complexity_title = "null";
            }

            if (!is_null($typ_id)) {
                $typology = $this->model->getTypologyById($typ_id)[0];
                
                if ($typology != null) {
                    $typology_title = $typology->typ_title;
                }
            }else{
                $typology_title = "null";
            }

            if (!is_null($thm_id)) {
                $theme = $this->model->getThemeById($thm_id)[0];
                
                if ($theme != null) {
                    $theme_title = $theme->thm_titre;
                }
            }else{
                $theme_title = "null";
            }

            if (!is_null($type_id)) {
                $type = $this->model->getTypeById($type_id)[0];
                
                if ($type != null) {
                    $type_title = $type->type_nom;
                }
            }else{
                $type_title = "null";
            }
        }
    
        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/_templates/navbar.php';
        require APP . 'view/competenceQuestion/edit.php';
        require APP . 'view/_templates/footer.php';   
    }

    public function Details($request) {
        /* session and user test */
        if (!isset($_SESSION["user"])) {
            header('location: ' . URL . 'Account');
            exit();
        }
        if ($this->profil === -1) {
            header('location: ' . URL . 'Account/Logout');
            exit();
        }
        // anonymous user or hacker
        if($this->profil!==0 && $this->profil!==1 && $this->profil!==3 && $this->profil!==4) {
            header('location: ' . URL);
            exit();
        }
        // redirect to home page if isn't admin
        // dark-background
        $header_class = '';
        // ' current'
        $current_class = '';
        // ' current-sub'
        $current_subclass = '';
        
        $this->model = new CompetenceQuestion($this->db);

        $types = $this->model->getAllTypes();
        $themes = $this->model->getAllThemes();
        $typologies = $this->model->getAllTypologies();
        $complexities = $this->model->getAllComplexities();

        /* extract question data from the database when page loaded */
        $targetID = $request;

        $questionWithTheme = $this->model->getQuestionWithTheme($targetID)[0];
        
        if ($questionWithTheme != null) {
            $id = $questionWithTheme->qst_id;
            $qst_ref = $questionWithTheme->qst_ref;
            $qst_description = $questionWithTheme->qst_description;
            $qst_num_rep_correct = $questionWithTheme->qst_num_rep_correct;

            $options = $this->model->getOptionsByQuestionId($id);
            
            $i=0;
            if ($options != null) {
                foreach ($options as $key => $option) {
                    $op[$i] = $option->opt_description;
                    $op_val[$i] = $option->opt_valeur;

                    if($qst_num_rep_correct === $option->opt_id) {
                        if ($i==0) {
                            $correct = 'qst_op1';
                        }
                        if ($i==1) {
                            $correct = 'qst_op2';
                        }
                        if ($i==2) {
                            $correct = 'qst_op3';
                        }
                        if ($i==3) {
                            $correct = 'qst_op4';
                        }
                    }
                    $i++;
                }
            }
            $qst_explication = $questionWithTheme->qst_explication;
            $qst_importance = $questionWithTheme->qst_importance;
            $qst_objectif = $questionWithTheme->qst_objectif;
            
            $comp_id = $questionWithTheme->comp_id;
            $thm_id = $questionWithTheme->thm_id;
            $typ_id = $questionWithTheme->typ_id;
            $type_id = $questionWithTheme->type_id;

            if (!is_null($comp_id)) {
                $complexity = $this->model->getComplexityById($comp_id)[0];
                if ($complexity != null) {
                    $complexity_title = $complexity->comp_title;
                }
            }else{
                $complexity_title = "null";
            }

            if (!is_null($typ_id)) {
                $typology = $this->model->getTypologyById($typ_id)[0];
                
                if ($typology != null) {
                    $typology_title = $typology->typ_title;
                }
            }else{
                $typology_title = "null";
            }

            if (!is_null($thm_id)) {
                $theme = $this->model->getThemeById($thm_id)[0];
                
                if ($theme != null) {
                    $theme_title = $theme->thm_titre;
                }
            }else{
                $theme_title = "null";
            }

            if (!is_null($type_id)) {
                $type = $this->model->getTypeById($type_id)[0];
                
                if ($type != null) {
                    $type_title = $type->type_nom;
                }
            }else{
                $type_title = "null";
            }
        }
    
        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/_templates/navbar.php';
        require APP . 'view/competenceQuestion/details.php';
        require APP . 'view/_templates/footer.php';
    }

    public function Update($request) {
        if(isset($request['submit'])) {


            $this->model = new CompetenceQuestion($this->db);
            /* Get the theme id from the database */
            if (!empty($request['qst_thm'])) {
                $qst_thm = $request['qst_thm'];
                $theme = $this->model->getThemeByTitle($qst_thm)[0];
                
                if ($theme != null) {
                    $thm_id = $theme->thm_id;
                }
            }
            /* Get the type id from the database */
            if (!empty($request['qst_tpe'])) {
                $qst_tpe = $request['qst_tpe'];

                $type = $this->model->getTypeByTitle($qst_tpe)[0];
                
                if ($type != null) {
                    $type_id = $type->type_id;
                }
            }
            /* Get the typology id from the database */
            if (!empty($request['question_typologie'])) {
                $question_typologie = $request['question_typologie'];

                $typology = $this->model->getTypologyByTitle($question_typologie)[0];
                
                if ($typology != null) {
                    $typ_id = $typology->typ_id;
                }
            }
            /* Get the complexity id from the database */
            if (!empty($request['qst_comp'])) {
                $qst_comp = $request['qst_comp'];

                $complexity = $this->model->getComplexityByTitle($qst_comp)[0];
                
                if ($complexity != null) {
                    $comp_id = $complexity->comp_id;
                }
            }
            /* Save changes when form submit */
            if (!empty($request['qst_desc']) && !empty($request['qst_rep_cor']) && !empty($request['qst_op1']) && 
                !empty($request['qst_op2']) && !empty($request['qst_op3']) && !empty($request['qst_op4']) && 
                !empty($request['qst_thm']) && !empty($request['qst_tpe']) && isset($request['qst_val_op1']) && 
                isset($request['qst_val_op2']) && isset($request['qst_val_op3']) && isset($_FILES['question_image']) && isset($request['qst_val_op4']) && !empty($request['question_typologie'])) {

                $pid = (int) $request['thisID'];
                $qst_desc = $request['qst_desc'];
                $qst_op1 = $request['qst_op1'];
                $qst_op2 = $request['qst_op2'];
                $qst_op3 = $request['qst_op3'];
                $qst_op4 = $request['qst_op4'];

                $qst_val_op1 = $request['qst_val_op1'];
                $qst_val_op2 = $request['qst_val_op2'];
                $qst_val_op3 = $request['qst_val_op3'];
                $qst_val_op4 = $request['qst_val_op4'];

                $qst_rep_cor = $request['qst_rep_cor'];
                $qst_obj = $request['qst_obj'];
                $qst_expl = $request['qst_expl'];
                $qst_imp = $request['qst_imp'];
                $qst_obj = $request['qst_obj'];

                $qst_image = $this->imageUpload($_FILES['question_image']);

                $opp = array($qst_op1, $qst_op2, $qst_op3, $qst_op4);
                $opp_val = array($qst_val_op1, $qst_val_op2, $qst_val_op3, $qst_val_op4);

                $options = $this->model->getOptionsByQuestionId($pid);
                
                $i=0;
                if ($options != null) {
                    foreach ($options as $key => $option) {
                        $op = $option->opt_id;
                        $this->model->updateOption($opp[$i], $opp_val[$i], $op);

                        if($qst_rep_cor==='qst_op1' && $i==0) {
                            $qst_rep_correct = $option->opt_id;
                        }
                        if($qst_rep_cor==='qst_op2' && $i==1) {
                            $qst_rep_correct = $option->opt_id;
                        }
                        if($qst_rep_cor==='qst_op3' && $i==2) {
                            $qst_rep_correct = $option->opt_id;
                        }
                        if($qst_rep_cor==='qst_op4' && $i==3) {
                            $qst_rep_correct = $option->opt_id;
                        }
                        $i++;
                    }
                }
                $this->model->updateQuestion($qst_desc, $qst_obj, $qst_rep_correct, $qst_expl, $qst_imp, $thm_id, $type_id, $typ_id, $comp_id, $pid, $qst_image);
            }
        }
        header('location: '.URL.'CompetenceQuestion');
    }

    public function Delete($question_id) {
        $id_to_delete = (int) $question_id;
        $this->model = new CompetenceQuestion($this->db);

        $question = $this->model->getQuestion($id_to_delete)[0];
        
        if ($question === null) {
            header("location: message.php?fl=$file&msg=delete_not_exist");
            exit();
        }
        $this->model->deleteOptions($id_to_delete);
        $this->model->deleteEvalQues($id_to_delete);
        $this->model->deleteQuestion($id_to_delete);
        header('location: '.URL.'CompetenceQuestion');
    }

    public function evaluationQuestions($qst_id, $eval_id) {
        return $this->model->getEvaluationQuestions($qst_id, $eval_id);
    } 

    public function checkQuestionId($request) {
        $qst_num = $request['qst_num'];
        $this->model = new CompetenceQuestion($this->db);
        
        // print_r($qst_num);
        $question = $this->model->getCompetenceQuestionByReference($qst_num);

        // header("Content-type: application/json");
        if ($question != null) {
            echo "false";
        }else{
            echo "true";
        }
    }

    public function getLatestQuestionNumber() {
        $number = $this->model->getLatestQuestionNumber();
        if ($number != null) {
            return $number[0]->qst_ref + 1;
        }
    }

     public function getActiveQuestions($request = null) {
        if (isset($request['search_competence'])) {
            $theme = $request['theme'];
            $complexite = $request['complexite'];

            if(!empty($complexite)){
                $comp = $this->model->getComplexityId($complexite)[0];
                $comp_id = $comp->comp_id;
            }

            $evaluation = $request['evaluation'];

            if (empty($complexite)) {
                if (empty($evaluation)) {
                    if (empty($theme)) {
                        return $this->model->getAllCompetenceQuestions();
                    }else{
                        return $this->model->getCompetenceQuestionsByTheme($theme);
                    }
                }else{
                    if (empty($theme)) {
                        return $this->model->getCompetenceQuestionsByEvaluation($evaluation);
                    }else{
                        return $this->model->getCompetenceQuestionsByThemeAndEvaluation($theme, $evaluation);
                    }
                }
            }else{
                if (empty($evaluation)) {
                    if (empty($theme)) {
                        return $this->model->getCompetenceQuestionsByCompetence($comp_id);
                    }else{
                        return $this->model->getCompetenceQuestionsByThemeAndCompetence($theme, $comp_id);
                    }
                }else{
                    if (empty($theme)) {
                        return $this->model->getCompetenceQuestionsByEvaluationAndCompetence($evaluation, $comp_id);
                    }else{
                        return $this->model->getCompetenceQuestionsByThemeAndEvaluationAndCompetence($theme, $evaluation, $comp_id);
                    }
                }
            }
        }else{
            return $this->model->getAllCompetenceQuestions();
        }
    } 
	

    public function imageUpload($imageName) {
        //print_r($imageName);

        if(isset($imageName['type'])) {
            $validExtensions = array("jpeg", "jpg", "png");
            $temporary = explode(".", $_FILES["question_image"]["name"]);
            $file_extension = end($temporary);
            if ((($_FILES["question_image"]["type"] == "image/png") || ($_FILES["question_image"]["type"] == "image/jpg") || ($_FILES["question_image"]["type"] == "image/jpeg")) && ($_FILES["question_image"]["size"] < 100000000)//Approx. 100kb files can be uploaded.
                && in_array($file_extension, $validExtensions)) {
                if ($_FILES["question_image"]["error"] > 0) {
                    echo $_FILES["question_image"]["error"];
                }else{
                    // if (file_exists("d:/workspace/apache/excellium_consulting/env-recette/public/img/questions/".$_FILES["question_image"]["name"])) {
                    //     echo $_FILES["question_image"]["name"]." exists";
                    // }else{
                    $sourcePath = $imageName['tmp_name']; // Storing source path of the file in a variable
                    $targetPath = ROOT."public\img\questions\\".$imageName['name']; // Target path where file is to be stored
                    move_uploaded_file($sourcePath,$targetPath); // Moving Uploaded file
                    return $imageName['name'];
                    // }
                }
                return "2";
            }else{
                return "3";
            }
            return "4";
        }
        return "5";
    }
	}
	
/*	public function getActiveQuestions($request = null){
		if ($request != null){
			$complexite = isset($request['complexite']) ? $request['complexite'] : '';
			$evaluation = isset($REQUEST['evaluation']) ? $REQUEST['evaluation'] : '';
			$theme = isset($request['theme']) ? $request['theme'] : '' ;
			$complexite_id = $this->model->getComplexityId($complexite);
			if (empty($complexite) && empty($evaluation) && empty($theme)){
				return $this->model->getAllCompetenceQuestions();
			}
			if (empty($complexite) && empty($evaluation) && !empty($theme)){
                return $this->model->getCompetenceQuestionsByTheme($theme);
			}
			if (empty($complexite) && !empty($evaluation) && empty($theme)){
				return $this->model->getCompetenceQuestionsByEvaluation($evaluation);
            }
			if (!empty($complexite) && empty($evaluation) && empty($theme)){
				return $this->model->getCompetenceQuestionBycomplexite($complexite_id);
			}
		}
}*/

