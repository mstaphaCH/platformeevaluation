<?php

/**
 * Class Songs
 * This is a demo class.
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class MessageController extends Controller
{
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/home/index (which is the default page btw)
     */
    
    public function index()
    {
        // if($this->profil === -1) {
        //     header('location: ' . URL . 'Login');
        // }
        // dark-background
        $header_class = '';
        // ' current'
        $current_class = '';
        // ' current-sub'
        $current_subclass = '';
        
        $this->model = new Message($this->db);

        $active_messages = $this->getActiveMessages();
        $inactive_messages = $this->getInactiveMessages();
        $message_type_titles = $this->getMessageTitles();
        $message_categories = $this->getAllCategoryMessages();
        $msg_ref = $this->getMessageReference();

        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/_templates/navbar.php';
        require APP . 'view/message/index.php';
        require APP . 'view/_templates/footer.php';
    }

    public function Add($request) {
        if (empty($request['msg_ref']) || 
            empty($request['msg_title']) || 
            empty($request['msg_cat']) || 
            empty($request['msg_content'])) { echo "fail"; exit(); }

            $this->model = new Message($this->db);

        $msg_ref = $request['msg_ref'];
        $msg_title = $request['msg_title'];
        $msg_content = $request['msg_content'];
        $msg_active = 1;
        $msg_cat = $request['msg_cat'];

        $messageType = $this->model->getMessageTypeByCategory($msg_cat)[0];
        
        if ($messageType != null) {
            $msgType_id = $messageType->msgType_id;
        }

        $message = $this->model->getMessageByReference($msg_ref);
        
        if ($message != null) {
            echo 'message exist';
            exit();
        }
        
        $id = $this->model->addMessage($msg_ref, $msg_title, $msg_content, $msg_active, $this->model->id, $msgType_id);
        
                    header('location: ' . URL . 'Message');

    } 

    public function Edit($request) {
        // if($this->profil === -1) {
        //     header('location: ' . URL . 'Login');
        // }
        // dark-background
        $header_class = '';
        // ' current'
        $current_class = '';
        // ' current-sub'
        $current_subclass = '';
        
        $this->model = new Message($this->db);

        $messageType_titles = $this->model->getMessageTypeTitles();
        $message = $this->model->getMessageById($request)[0];
        // $type = $this->
        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/_templates/navbar.php';
        require APP . 'view/message/edit.php';
        require APP . 'view/_templates/footer.php';
    }

    public function Update($request) {
        /* Update form */
        if (empty($request['thisID']) || 
            empty($request['msg_ref']) || 
            empty($request['msg_title']) || 
            empty($request['msg_cat']) || 
            empty($request['msg_content'])) { echo "fail"; exit(); }

        $this->model = new Message($this->db);
        $pid = (int) $request['thisID'];

        $msg_ref = $request['msg_ref'];
        $msg_title = $request['msg_title'];
        $msg_cat = $request['msg_cat'];
        $msg_content = $request['msg_content'];

        $messageType = $this->model->getMessageCategoryByTitle($msg_cat)[0];
        
        if ($messageType != null) {
            $msgType_id = $messageType->msgType_id;
        }

        $this->model->updateMessage($msg_ref, $msg_title, $msg_content, $msgType_id, $pid);

        echo "success";
    }

    public function Delete($request) {
        $id_to_delete = (int) $request['message_id'];
        $this->model = new Message($this->db);
        $message = $this->model->getMessageById($id_to_delete);
        if ($message == null) {
            echo "Message doesn't exist";
            exit();
        }
        $this->model->deleteMessage($id_to_delete);
        echo "success";
    }

    public function Active($request) {
        $targetID = $request;
        $this->model = new Message($this->db);

        $message = $this->model->getInactiveMessageById($targetID)[0];
        
        if ($message != null) {
            $this->model->Activate($targetID);
        }
        header('location: ' . URL . 'Message');
    }

    public function Inactive($request) {
        $targetID = $request;
        $this->model = new Message($this->db);

        $message = $this->model->getActiveMessageById($targetID)[0];
        
        if ($message != null) {
            $this->model->Inactivate($targetID);
        }
        header('location: ' . URL . 'Message');   
    }

    public function getActiveMessages() {
        if (isset($_SESSION["user"])) {
            if (isset($REQUEST['search1'])) {
                $categorie = $REQUEST['categorie'];
                $messagesByCategory = $this->model->getActiveMessagesByCategory($categorie);
                if (empty($categorie)) {
                    return $this->model->getAllActiveMessages();
                }else{
                    return $this->model->getActiveMessagesByTypeId($messagesByCategory[0]->msgType_id);
                }
            }else{
                return $this->model->getAllActiveMessages();
            }
        }
    }

    public function getType($msgType_id) {
        if (!is_null($msgType_id)) {
            $messageType = $this->model->getMessageType($msgType_id)[0];
            if ($messageType != null) {
                return $messageType->msgType_title;
            }
        }else{
            return null;
        }
    }

    public function getInactiveMessages() {
        if (isset($_SESSION["user"])) {
            if (isset($REQUEST['search2'])) {
                $categorie = $REQUEST['categorie'];
                $messagesByCategory = $this->model->getInactiveMessagesByCategory($categorie);
                if (empty($categorie)) {
                    return $this->model->getAllInactiveMessages();
                }else{
                    return $this->model->getInactiveMessagesByTypeId($messagesByCategory[0]->msgType_id);
                }
            }else{
                return $this->model->getAllInactiveMessages();
            }
        }
    }

    public function getMessageTitles() {
        return $this->model->getMessageTitles();
    }

    public function getAllCategoryMessages() {
        return $this->model->getAllCategoryMessages();
    }

    public function getMessageReference() {
        $reference = $this->model->getMessageReference();
        if ($reference != null) {
            return $reference[0]->msg_ref + 1;
        }
    }

    public function checkMessageReference($request) {
        $msg_ref = $request['msg_ref'];
        
        $this->model = new Message($this->db);
        
        $message = $this->model->getMessageByReference($msg_ref);
        
        if ($message != null) {
            echo 'false';
        }else{
            echo 'true';
        }
    }

    public function getMessageCategory($id) {
        $categorie = $this->model->getMessageCategory($id)[0];

        if ($categorie != null) {
            return ucfirst($categorie->msgType_title);
        }
    }
}
