<?php

/**
 * Class Home
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class ThemeController extends Controller
{
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/home/index (which is the default page btw)
     */
    
    public function index()
    {
    	/* Session existing test */
		if (!isset($_SESSION["user"])) {
			header('location: ' . URL . 'Account');
			exit();
		}
		/* Check user profil */
		if ($this->profil===-1) {
			header('location: ' . URL . 'Account/Logout');
			exit();
		}
		if($this->profil !== 0) {
			header('location: ' . URL);
			exit();
		}
        // redirect to home page if isn't admin
        // dark-background
        $header_class = '';
        // ' current'
        $current_class = '';
        // ' current-sub'
        $current_subclass = '';

        $this->model = new Theme($this->db);

        $themes = $this->getActiveThemes();
        
        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/_templates/navbar.php';
        require APP . 'view/theme/index.php';
        require APP . 'view/_templates/footer.php';
    }

    public function Add($request) {
        /* Submit form operation: insert */
        if (isset($request['submit'])) {
            if (!empty($request['theme_title'])) {
                $theme_title = $request['theme_title'];
                $theme_weighting = $request['theme_weighting'];
                $theme_description = $request['theme_description'];

                $this->model = new Theme($this->db);

                $theme = $this->model->getThemeByTitle($theme_title);

                if ($theme != null) {
                    header("location: message.php?pg=theme&msg=theme_exist");
                    exit();
                }

                $pid = $this->model->addTheme($theme_title, $theme_description, $theme_weighting);
                
                header('location: ' . URL . 'Theme');
            }
        }
    }

    public function Edit($request) {
        /* Session existing test */
        if (!isset($_SESSION["user"])) {
            header('location: ' . URL . 'Account');
            exit();
        }
        /* Check user profil */
        if ($this->profil===-1) {
            header('location: ' . URL . 'Account/Logout');
            exit();
        }
        if($this->profil !== 0) {
            header('location: ' . URL);
            exit();
        }
        // redirect to home page if isn't admin
        // dark-background
        $header_class = '';
        // ' current'
        $current_class = '';
        // ' current-sub'
        $current_subclass = '';
        /* Get ID and fetch data from database */
        if (isset($request)) {
            
            $this->model = new Theme($this->db);
            $targetID = $request;
            $theme = $this->model->getThemeById($targetID)[0];
            
            if($theme != null) {
                $theme_title = $theme->thm_titre;
                $theme_weighting = $theme->thm_ponderation;
                $theme_description = $theme->thm_description;
            }
        }
        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/_templates/navbar.php';
        require APP . 'view/theme/edit.php';
        require APP . 'view/_templates/footer.php';
    }

    public function Update($request) {
        if (isset($request['submit'])) {
            $this->model = new Theme($this->db);
            if(!empty($request['theme_title'])) {
                $pid = (int) $request['thisID'];
                $theme_title = $request['theme_title'];
                $theme_weighting = $request['theme_weighting'];
                $theme_description = $request['theme_description'];

                /* Update query */
                $this->model->update($theme_title, $theme_description, $theme_weighting, $pid);

                header('location: ' . URL . 'Theme');
                // header("location: theme_list.php");
            }
        }
    }

    public function Delete($request) {
        $id_to_delete = (int) $request['theme_id'];
        $this->model = new Theme($this->db);

        $question = $this->model->getQuestionById($id_to_delete);

        if ($question != null) {
            header("location: message.php?pg=theme&msg=delete_not_permitted");
            exit();
        }
        $theme = $this->model->getThemeById($id_to_delete);
        
        if ($theme == null) {
            header("location: message.php?pg=theme&msg=delete_not_exist");
            exit();
        }

        $this->model->deleteTheme($id_to_delete);
        echo "success";
    }

    // public function delete($deleteId) {
    //     header('location: ' . URL . 'Message/Delete/Theme' . $deleteId);
    //     exit();
    // }

    public function getActiveThemes() {
        return $this->model->getActiveThemes();
    }

    public function getQuestionNumbers($theme_id) {
        return $this->model->getQuestionNumbers($theme_id);
    }
}
