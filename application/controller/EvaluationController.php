<?php

/**
 * Class Home
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class EvaluationController extends Controller
{
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/home/index (which is the default page btw)
     */
    
    public function index()
    {
        /* session and user test */
        if (!isset($_SESSION["user"])) {
            header('location: ' . URL . 'Account');
            exit();
        }
        if ($this->profil === -1) {
            header('location: ' . URL . 'Account/Logout');
            exit();
        }
        // anonymous user or hacker
        if($this->profil!==0 && $this->profil!==1 && $this->profil!==3 && $this->profil!==4) {
            header('location: ' . URL);
            exit();
        }
        // redirect to home page if isn't admin
        // dark-background
        $header_class = '';
        // ' current'
        $current_class = '';
        // ' current-sub'
        $current_subclass = '';

        $this->model = new Evaluation($this->db);
        $evaluations = $this->getAllActiveEvaluations();
		$evaluationcategories = $this->getEvaluationcategories();
        
        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/_templates/navbar.php';
        require APP . 'view/evaluation/index.php';
        require APP . 'view/_templates/footer.php';
    }

    public function Add($request) {
        if ($this->profil===0 && isset($request['submit'])) {
            if (!empty($request['eval_titre']) && !empty($request['eval_tpe'])) {
                
                $this->model = new Evaluation($this->db);

                $et = $request['eval_titre'];
                $etpe = $request['eval_tpe'];
                $eval_cat = $request['eval_cat'];
                $eval_princ = $request['eval_princ'];
                
                if(!isset($request['eval_access'])) {
                    $request['eval_access'] = "entretien";
                }else{
                    $request['eval_access'] = "libres";
                }

                $eval_access = $request['eval_access'];
                $eval_seuil = $request['eval_seuil'];
                $ed = $request['eval_desc'];
                $hours = $request['eval_tmp_h'];
                $minutes = $request['eval_tmp_m'];
                
                $hours    = isset($hours) ? $hours : 0;
                $minutes = isset($minutes) ? $minutes : 0;

                $eval_tmp = ($hours * 60 * 60) + ($minutes * 60);
                $eval_tmp   = date('Y-m-d H:i:s',$eval_tmp);

                $evaluation = $this->model->getEvaluationByName($et);

                if ($evaluation != null) {
                    header("location: message.php?msg=evaluation_exist");
                    exit();
                }
                /* Question insert */
                $qst_id = $this->model->addEvaluation($et, $etpe, $eval_seuil, $ed, $eval_princ, $eval_access, $eval_cat, $eval_tmp);

                header('location: ' . URL . 'Evaluation');
                // header("location: evaluation_list.php");
            }
        }
    }

    public function Edit($request) {
        /* session and user test */
        if (!isset($_SESSION["user"])) {
            header('location: ' . URL . 'Account');
            exit();
        }
        if ($this->profil === -1) {
            header('location: ' . URL . 'Account/Logout');
            exit();
        }
        // anonymous user or hacker
        if($this->profil!==0 && $this->profil!==1 && $this->profil!==3 && $this->profil!==4) {
            header('location: ' . URL);
            exit();
        }
        // redirect to home page if isn't admin
        // dark-background
        $header_class = '';
        // ' current'
        $current_class = '';
        // ' current-sub'
        $current_subclass = '';

        $this->model = new Evaluation($this->db);
        
        if (isset($request)) {
            $targetID = $request;
            $user_active_list = "";

            $evaluation = $this->model->getEvaluationById($targetID)[0];
            
            if ($evaluation != null) {
                $eval_id = $evaluation->eval_id;
                $eval_nom = ucfirst($evaluation->eval_nom);
                $eval_type = $evaluation->eval_type;
                $eval_description = ucfirst($evaluation->eval_description);

                $eval_access = $evaluation->eval_access;

                $eval_princ = ucfirst($evaluation->eval_princ);
                $eval_cat = $evaluation->eval_cat;
                
                $eval_seuil = ucfirst($evaluation->eval_seuil);
                $eval_date_creation = $evaluation->eval_date_creation;
                $eval_duration = strtotime($evaluation->eval_duration);
                $eval_duration_h = date("H", $eval_duration);
                $eval_duration_m = date("i", $eval_duration);
            }
        }else{
            header('location :' . URL . 'Evaluation');
            // header("location: evaluation_list.php");
        }

        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/_templates/navbar.php';
        require APP . 'view/evaluation/edit.php';
        require APP . 'view/_templates/footer.php';   
    }

    public function Update($request) {
        if (isset($request['submit'])) {
            if (!empty($request['eval_tpe'])) {

                $this->model = new Evaluation($this->db);

                $pid = (int) $request['thisID'];
                $etpe = $request['eval_tpe'];

                $eval_cat = $request['eval_cat'];
                $eval_princ = $request['eval_princ'];
                
                if(!isset($request['eval_access'])) {
                    $request['eval_access'] = "entretien";
                }else{
                    $request['eval_access'] = "libres";
                }
                $eval_access = $request['eval_access'];
                
                $eval_seuil = $request['eval_seuil'];
                $ed = $request['eval_desc'];
                $hours = $request['eval_tmp_h'];
                $minutes = $request['eval_tmp_m'];
                $hours   = isset($hours) ? $hours : 0;
                $minutes = isset($minutes) ? $minutes : 0;
                $eval_tmp   = (($hours-1) * 60 * 60) + ($minutes * 60); 
                
                echo $hours;
                echo "<br/>";
                echo $eval_tmp;
                
                $eval_tmp   = date('Y-m-d H:i:s',$eval_tmp);
                /* Question insert */
                $this->model->update($etpe, $eval_seuil, $ed, $eval_princ, $eval_access, $eval_cat, $eval_tmp, $pid);
                
                header('location: ' . URL . 'Evaluation');
                // header("location: evaluation_list.php");
            }
        }
    }

    public function Delete($request) {
        if(isset($request)) {
            $this->model = new Evaluation($this->db);
            $id_to_delete = (int) $request['evaluation_id'];

            $questions = $this->model->getQuestionsByEvalId($id_to_delete);
            
            if ($questions != null) {
                header("location: message.php?pg=evaluation&msg=delete_not_permitted");
                exit();
            }

            $evaluation = $this->model->getEvaluation($id_to_delete);
            
            if ($evaluation == null) {
                header("location: message.php?pg=evaluation&msg=delete_not_exist");
                exit();
            }

            $this->model->deleteEvaluation($id_to_delete);
            
        }
    }

    public function checkEvalName($request) {
        $eval_title = $request['eval_titre'];
        $this->model = new Evaluation($this->db);

        $evaluation = $this->model->getEvaluationByName($eval_title);
        
        if ($evaluation != null) {
            echo 'false';
        }else{
            echo 'true';
        }
    }

    public function changeEvaluationStatus($request) {
        if(isset($request['eval_id'])) {
            $this->model = new Evaluation($this->db);
            $eval_id = $request['eval_id'];
            $evaluation = $this->model->getEvaluationById($eval_id);
            if ($evaluation != null) {
                $eval_status = $evaluation[0]->eval_active;
                if ($eval_status == 1) {
                    $this->model->desactivate($eval_id);
                }elseif ($eval_status == 0) {
                    $this->model->activate($eval_id);
                }
            }
        }
    }

    public function getQuestionNumberByEvaluation($eval_type, $eval_id) {
        $this->model = new Evaluation($this->db);
        if ($eval_type==='Thème') {
            $questions = $this->model->getThemeQuestionsCountByEvalId($eval_id);

            if ($questions != null) {
                return $questions[0]->Count;
            }
        }else{
            $questions = $this->model->getQuestionsCountByEvalId($eval_id);
            
            if ($questions != null) {
                return $questions[0]->Count . ' : ' . $questions[0]->ThemeTitle;
            }
        }
    }

    public function getAllActiveEvaluations() {
        if (isset($REQUEST['search'])) {
            $category = $REQUEST['cat'];
            $type = $REQUEST['type'];
            $state = $REQUEST['state'];
            if (empty($state)) {
                if (empty($type)) {
                    if (empty($category)) {
                        return $this->model->getAllEvaluations();
                        $sql = mysqli_query($mysqli,"SELECT * FROM edb_evaluation ORDER BY eval_id ASC") or die (mysqli_error($mysqli));
                    }else{
                        return $this->model->getEvaluationByCategory($category);
                        $sql = mysqli_query($mysqli,"SELECT * FROM edb_evaluation 
                            WHERE eval_cat='$category'
                            ORDER BY eval_id ASC") or die (mysqli_error($mysqli));
                    }
                }else{
                    if (empty($category)) {
                        return $this->model->getEvaluationByType($type);
                        $sql = mysqli_query($mysqli,"SELECT * FROM edb_evaluation WHERE eval_type='$type'
                            ORDER BY eval_id ASC") or die (mysqli_error($mysqli));
                    }else{
                        return $this->model->getEvaluationByCategoryAndType($category, $type);
                        $sql = mysqli_query($mysqli,"SELECT * FROM edb_evaluation 
                            WHERE eval_type='$type' AND eval_cat='$category'
                            ORDER BY eval_id ASC") or die (mysqli_error($mysqli));
                    }
                }
            }else{
                if (empty($type)) {
                    if (empty($category)) {
                        return $this->model->getEvaluationByState($state);
                        $sql = mysqli_query($mysqli,"SELECT * FROM edb_evaluation WHERE eval_active='$state'
                            ORDER BY eval_id ASC") or die (mysqli_error($mysqli));
                    }else{
                        return $this->model->getEvaluationByCategoryAndState($category, $state);
                        $sql = mysqli_query($mysqli,"SELECT * FROM edb_evaluation 
                            WHERE eval_active='$state' AND eval_cat='$category'
                            ORDER BY eval_id ASC") or die (mysqli_error($mysqli));
                    }
                }else{
                    if (empty($category)) {
                        return $this->model->getEvaluationByStateAndType($state, $type);
                        $sql = mysqli_query($mysqli,"SELECT * FROM edb_evaluation WHERE eval_active='$state' AND eval_type='$type'
                            ORDER BY eval_id ASC") or die (mysqli_error($mysqli));
                    }else{
                        return $this->model->getEvaluationByCategoryStateAndType($category, $state, $type);
                        $sql = mysqli_query($mysqli,"SELECT * FROM edb_evaluation 
                            WHERE eval_active='$state' AND eval_type='$type' AND eval_cat='$category'
                            ORDER BY eval_id ASC") or die (mysqli_error($mysqli));
                    }
                }
            }
        }else{
            return $this->model->getAllEvaluations();
            $sql = mysqli_query($mysqli,"SELECT * FROM edb_evaluation ORDER BY eval_id ASC") or die (mysqli_error($mysqli));
        }
    }

    public function Probation($request) {
        // redirect to home page if isn't admin
        // dark-background
        $header_class = '';
        // ' current'
        $current_class = '';
        // ' current-sub'
        $current_subclass = '';

        if ($this->profil===-1) {header("location: logout.php"); exit();}

        $this->model = new Evaluation($this->db);
        $eval_id = $request;
        $evaluation = $this->model->getEvaluationById($eval_id);
        // $row['eval_description'],$row['eval_princ'],$row['edb_time'],$row['edb_timer']
        $evaluationDescr = $this->model->getEvaluationDescription($eval_id);
        
        // load views
        require_once APP . 'view/_templates/header.php';
        require_once APP . 'view/_templates/navbar.php';
        require_once APP . 'view/evaluation/probation.php';
        require_once APP . 'view/_templates/footer.php';
    }

    public function newProbation() {
        require APP . 'view/evaluation/modals/newProbation.php';
    }

    public function Question($request) {
        $eval_id = $request;
        // redirect to home page if isn't admin
        // dark-background
        $header_class = '';
        // ' current'
        $current_class = '';
        // ' current-sub'
        $current_subclass = '';

        if ($this->profil===-1) {header("location: logout.php"); exit();}
        
        $this->model = new Evaluation($this->db);

        // if($this->profil == 0 || $this->profil == 5) {header("location: index.php"); exit();}
        if($this->profil == 2) {
            // Cas du candidat
            $cdt_id = $_SESSION["id"];
            // $nom = getName($cdt_id);
            // $result =sumResponse($cdt_id,$eval_id);
            $user = $this->model->getCandidate($cdt_id)[0];
            $result = $this->model->sumResponse($cdt_id,$eval_id);
            $dateEval=$result['reDate'];
            $nbrep=$result['rep'];  
        }else{
            // Cas d'administrateur local,formateur,evaluateur
            $util_id = $_SESSION["id"];
            $user = $this->model->getNameUtilisateur($util_id)[0];
            /*
            $result =sumResponse(-$util_id,$eval_id);
            $dateEval=$result['reDate'];
            $nbrep=$result['rep'];
            */
            $dateEval = "Aucune";
            $nbrep = 0;
        }

        $evaluation = $this->model->getEvaluationById($eval_id);
        // $row['eval_description'],$row['eval_princ'],$row['edb_time'],$row['edb_timer']
        $evaluationDescr = $this->model->getEvaluationDescription($eval_id);
        $timer = $evaluationDescr[0]->edb_timer;
        // print_r($evaluationDescr[0]->edb_timer);exit();
        $questions = $this->model->getQuestions($eval_id);
        // load views
        require_once APP . 'view/_templates/header.php';
        require_once APP . 'view/_templates/navbar.php';
        require_once APP . 'view/evaluation/question.php';
        require_once APP . 'view/_templates/footer.php';
    }

    public function getQuestionOptions($qst_id) {
        return $this->model->getQuestionOptions($qst_id);
    }

    public function Result($request) {
        // if ($this->profil===-1) { header("location: logout.php"); exit(); }
        // elseif($this->profil == 0 || $this->profil == 5) {
        //     // header("location: index.php"); exit(); // Accee interdit pour administrateur global et consultant
        // }
                // dark-background
        $header_class = '';
        // ' current'
        $current_class = '';
        // ' current-sub'
        $current_subclass = '';

        $this->model = new Evaluation($this->db);
        $evalTypeArray = $this->model->getEvalType($request['eval_id']);
        foreach($evalTypeArray as $e)
        {
            $evalType = $e->eval_type;
        }
        if($evalType == 'aptitude')
        {
            // TODO passer l'id de category
            $eval_id=$request['eval_id'];
            if($this->profil == 2)
            {
                /* Cas du candidat */
                $cdt_id=$_SESSION["id"];
                
                if(isset($request['questions']))
                {
                    $this->saveResponsesAptitude($request['questions'],$cdt_id,$request['eval_id']); 
                }
                
                $result = $this->sumResponseAptitude($cdt_id,$eval_id);
            }
            else
            {
                /* Cas d'administrateur local,formateur,evaluateur */
                $util_id=$_SESSION["id"];
                
                if(isset($request['questions']))
                {
                    $this->saveResponsesAptitude($request['questions'],-$util_id,$request['eval_id']);
                }
                
                $result = $this->sumResponseAptitude(-$util_id,$eval_id);
            }
            
            $right_answer = $result["rep_correct"];
            $wrong_answer = $result["rep_incorrect"];
            $unanswered = $result["rep_vide"];
        }
        else
        {
            if(isset($request['questions']))
            {
                $this->saveResponses($request['questions'],$_SESSION['id']); 
            }
            
            // TODO passer l'id de category
            $eval_id=$request['eval_id'];
            $cdt_id=$_SESSION["id"];
            $result = $this->model->sumResponse($cdt_id,$eval_id)[0];
            $right_answer= $result->rep_correct;
            $wrong_answer= $result->rep_incorrect;
            $unanswered= $result->rep_vide;
        }

        $evalId = $request['eval_id'];

        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/_templates/navbar.php';
        require APP . 'view/evaluation/result.php';
        require APP . 'view/_templates/footer.php';
    }

    public function saveResponsesAptitude($questions,$cdt_id,$eval_id) {
        $compteur = 0;
        foreach ($questions as $key => $value) {
            $id1 = $_SESSION["optionsId"][$compteur][0];
            $id2 = $_SESSION["optionsId"][$compteur][1];
            $id3 = $_SESSION["optionsId"][$compteur][2];
            $id4 = $_SESSION["optionsId"][$compteur][3];
            $compteur++;
            $this->model->deleteResponseAptitude($cdt_id, $key);
            $this->model->insertResponseAptitude($eval_id, $key, $cdt_id, $value, $id1, $id2, $id3, $id4);
        }
    }

    public function sumResponseAptitude($util_id,$eval_id) {
        
        $responsesAptitude = $this->model->getResponsesAptitude($cdt_id, $eval_id);

        $tab = array();
        $right_answer = 0;
        $wrong_answer = 0;
        $unanswered = 0;
        $rep = 0;
        $ra_date = "";
        foreach($responsesAptitude as $ligne)
        {
            if($ligne[0] == "" && $ligne[1] == "" && $ligne[2] == "" && $ligne[3] == "")
            {
                $unanswered++;
            }
            elseif($ligne[0]== "1")
            {
                if($ligne[4] == $ligne[8])
                {
                    $right_answer++;
                }
                else
                {
                    $wrong_answer++;
                }
                $rep++;
            }
            elseif($ligne[1] == "1")
            {
                if($ligne[5] == $ligne[8])
                {
                    $right_answer++;
                }
                else
                {
                    $wrong_answer++;
                }
                $rep++;
            }
            elseif($ligne[2] == "1")
            {
                if($ligne[6] == $ligne[8])
                {
                    $right_answer++;
                }
                else
                {
                    $wrong_answer++;
                }
                $rep++;
            }
            elseif($ligne[3] == "1")
            {
                if($ligne[7] == $ligne[8])
                {
                    $right_answer++;
                }
                else
                {
                    $wrong_answer++;
                }
                $rep++;
            }
            else
            {
                $wrong_answer++;
                $rep++;
            }
            $ra_date = $ligne[9]; 
        }
        $tab['rep_correct'] = $right_answer;
        $tab['rep_incorrect'] = $wrong_answer;
        $tab['rep_vide'] = $unanswered;
        $format1 = 'Y-m-d H:i:s';
        $format2 = 'Y-m-d';
        if($ra_date != "")
        {
            $date1 = date_create_from_format($format1, $ra_date);
            $date2 = $date1->format($format2);
            $tab['reDate'] = $date2;
        }
        else
        {
            $tab['reDate'] = "Aucune";
        }
        $tab['rep'] = $rep;
        
        return $tab;
    }

    public function saveResponses($questions,$cdt_id){
        foreach ($questions as $key => $value) {
            $this->model->deleteResponse($cdt_id, $key);
            $this->model->insertResponse($value, $cdt_id, $key);
        }
    }

    public function Popup($request) {

        //require_once 'template/header.php';
        $eval_data = json_decode($request['data'], true);

        $no_response = array_filter($eval_data, function($v) {
            return $v == 'none';
        });

        $review = array_filter($eval_data, function($v) {
            return $v == 'review';
        });

        require APP . 'view/evaluation/modals/popup.php';
    }

    public function EtatReponse($request) {
        $eval_id = $request;
        $this->model = new Evaluation($this->db);
        $evalution = $this->model->getEvaluation($eval_id);
        $eval_data = json_decode($_POST['data'], true);

        $result = $this->model->getResponseState($eval_id);

        $rows = $this->model->getQuestions($eval_id);

        require APP . 'view/evaluation/modals/responseState.php';   
    }

    public function SeeResponse($request) {
        // dark-background
        $header_class = '';
        // ' current'
        $current_class = '';
        // ' current-sub'
        $current_subclass = '';

        $question_id = $request;
        $this->model = new Evaluation($this->db);
        $explication = $this->model->getExplication($question_id);

        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/_templates/navbar.php';
        require APP . 'view/evaluation/seeResponse.php';
        require APP . 'view/_templates/footer.php';
    }

    public function Reset() {

        require APP . 'view/evaluation/modals/reset.php';
    }

    public function suiviSession() {
                // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/_templates/navbar.php';
        require APP . 'view/evaluation/suiviSession.php';
        require APP . 'view/_templates/footer.php';
    }
	 public function getEvaluationsecondemethode($request = null) {
        if($request != null) {
            $search_categories = isset($request['search_categories']) ? $request['search_categories'] : '';
            $search_type = isset($request['search_type']) ? $request['search_type'] : '';
            $search_etat = isset($request['search_etat']) ? $request['search_etat'] : '';
            
            $this->model = new Evaluation($this->db);
            if (empty($search_country)) {
                if (empty($search_categories)) {
                    if (empty($search_type)) {
                        if ($this->profil===0) {
                            return $this->model->getAllEvaluations();
                        }else{
                            return $this->model->getAllEvaluations();
                        }
                    }else{
                        if ($this->profil===0) {
                            return $this->model->getEvaluationByState($search_etat);
                        }else{
                           return $this->model->getEvaluationByState($search_etat);
                        }
                    }
                }else{
                    if (empty($search_etat)) {
                        if ($this->profil===0) {
                            return $this->model->getEvaluationByCategory($search_categories);
                        }else{
                            return $this->model->getEvaluationByCategory($search_categories);
                        }
                    }else{
                        if ($this->profil===0) {
                            return $this->model->getEvaluationByCategoryAndState($search_categories,$search_etat);
                        }else{
                            return $this->model->getEvaluationByCategoryAndState($search_categories,$search_etat);                        }
                    }
                }
            }else{
                if (empty($search_categories)) {
                    if (empty($search_etat)) {
                        if ($this->profil===0) {
                            return $this->model->getEvaluationByType($search_type);
                        }else{
                            return $this->model->getEvaluationByType($search_type);
                        }
                    }else{
                        if ($this->profil===0) {
                            return $this->model->getEvaluationByCategoryAndType($search_categories,$search_type);
                        }else{
                            return $this->model->getEvaluationByCategoryAndType($search_categories,$search_type);
                        }
                    }
                }else{
                    if (empty($search_etat)) {
                        if ($this->profil===0) {
                            return $this->model->getEvaluationByCategoryAndType($search_categories,$search_type);
                        }else{
                            return $this->model->getEvaluationByCategoryAndType($search_categories,$search_type);
                        }
                    }else{
                        if ($this->profil===0) {
                            return $this->model->getEvaluationByCategoryStateAndType($search_categories, $search_type, $search_etat);
                        }else{
                            return $this->model->getEvaluationByCategoryStateAndType($search_categories, $search_type, $search_etat);
                        }
                    }
                }
            }
            
        }else{
            if ($this->profil===0) {
                return $this->model->getAllEvaluations();
            }else{
                return $this->model->getAllEvaluations();
            }
        }
    }
	public function getEvaluationcategories(){
		return $this->model->getEvaluationcategories();
	}
}
