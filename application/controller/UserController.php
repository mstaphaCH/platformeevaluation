<?php

/**
 * Class Home
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class UserController extends Controller
{
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/home/index (which is the default page btw)
     */
    
    public function index($request = null)
    {
    	/* Session existing test */
		if (!isset($_SESSION["user"])) {
			header('location: ' . URL . 'Account');
			exit();
		}
		/* Check user profil */
		if ($this->profil===-1) {
			header('location: ' . URL . 'Account/Logout');
			exit();
		}
		if($this->profil !== 0 && $this->profil !== 1) {
			header('location: ' . URL);
			exit();
		}
        // redirect to home page if isn't admin
        // dark-background
        $header_class = '';
        // ' current'
        $current_class = '';
        // ' current-sub'
        $current_subclass = '';

        $this->model = new User($this->db);

        $structureNames = $this->model->getStructureNames();
        $profilTitles = $this->model->getProfilTitles();
        if($request === null) {
            $activeUsers = $this->getActiveUsers();
            $inactiveUsers = $this->getInactiveUsers();
        }elseif($request['user']==='active') {            
            $activeUsers = $this->getActiveUsers($request);
            require APP . 'view/user/activeTable.php';
            exit();
        }elseif($request['user']==='inactive') {
            $inactiveUsers = $this->getInactiveUsers($request);
            require APP . 'view/user/inactiveTable.php';
            exit();
        }

        $profils = $this->getProfils();

        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/_templates/navbar.php';
        require APP . 'view/user/index.php';
        require APP . 'view/_templates/footer.php';
    }

    public function Add($request) {
        if (isset($request['submit'])) {
            if (!empty($request['user_email']) && 
                !empty($request['user_password']) && 
                !empty($request['user_repassword']) && 
                !empty($_POST['user_structure'])) {
                
                $user_profile = $request['user_profile'];
                $user_password = $request['user_password'];
                $user_repassword = $request['user_repassword'];
                
                $this->model = new User($this->db);

                $profile = $this->model->getProfileId($user_profile);
                
                if ($profile != null) {
                    $profile_id = $profile->prf_id;
                    if ($profile_id == 1 || $profile_id == 2) {
                        $user_recode = $request['user_recode'];
                        $user_code = $request['user_code'];
                        if ($user_code!==$user_recode) {
                            header("location: ../message.php?fl=zdb-users/$file&msg=codes_non_identiques");
                            exit();
                        }
                    }else{
                        $user_code = '0000aa';
                    }
                }

                if ($user_password!==$user_repassword) {
                    header("location: ../message.php?fl=zdb-users/$file&msg=mot_de_passes_non_identiques");
                    exit();
                }

                $user_password = sha1('{/[*'.$user_password.'{/[*');
                $user_code = sha1('{/[*'.$user_code.'{/[*');

                $user_email = $request['user_email'];
                $user_login = $request['user_login'];
                $user_website = $request['user_website'];
                $user_lastname = $request['user_lastname'];
                $user_firstname = $request['user_firstname'];
                $user_structure = $request['user_structure'];

                $adr_code_postal = '';

                $util_statut = 0;
                $util_ipaddress = '';
                $util_attempts = 3;
                $util_active = 1;
                $prf_id = $profile_id;

                $structure_id = $this->model->getStructure($user_structure)->str_id;
                $adr_id = $this->model->getStructure($user_structure)->adr_id;

                $user = $this->model->getUserByEmail($user_email);

                if ($user != null) {
                    header("location: ../message.php?pg=user&msg=utilisateur_exist");
                }
                
                if ($user_profile === "local" || $this->profil === 1) {
                    $prf_id = 2;
                }elseif($user_profile === "global" && $this->profil === 0) {
                    $prf_id = 1;
                    $structure_id = 1;
                }else{
                }
                

                $pid = $this->model->insertUser($user_firstname, $user_lastname, $user_login, 
                                     $user_password, $user_code, $user_email, 
                                     $user_website, $util_statut, $util_ipaddress, 
                                     $util_attempts, $util_active, $prf_id, 
                                     $structure_id, $adr_id);

                header('location: ' . URL . 'User');
            }
        }
    }

    public function Edit($request) {
        /* Session existing test */
        if (!isset($_SESSION["user"])) {
            header('location: ' . URL . 'Account');
            exit();
        }
        /* Check user profil */
        if ($this->profil===-1) {
            header('location: ' . URL . 'Account/Logout');
            exit();
        }
        if($this->profil !== 0 && $this->profil !== 1) {
            header('location: ' . URL);
            exit();
        }
        // redirect to home page if isn't admin
        // dark-background
        $header_class = '';
        // ' current'
        $current_class = '';
        // ' current-sub'
        $current_subclass = '';

        $this->model = new User($this->db);

        if (isset($request)) {
            $targetID = $request;
            $user = $this->model->getUserById($targetID);
            if ($user != null) {
                $user_firstname = $user->util_nom;
                $user_lastname = $user->util_prenom;
                $user_login = $user->util_login;
                $user_email = $user->util_email;
                $user_website = $user->util_siteweb;
                $adr_id = $user->adr_id;
                $util_dern_cnx = strftime("%ss:%mm:%hh %b %d, %Y", strtotime($user->util_dern_cnx));
            }
            $address = $this->model->getAddressById($adr_id);
            if ($address != null) {
                $user_address_number = $address->adr_num;
                $user_street = $address->adr_rue;
                $user_city = $address->adr_ville;
                $user_country = $address->adr_pays;
            }
        }else{
            header("location: " . URL . "User");
            exit();
        }

        // $structureNames = $this->model->getStructureNames();
        // $profilTitles = $this->model->getProfilTitles();
        

        // $profils = $this->getProfils();

        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/_templates/navbar.php';
        require APP . 'view/user/edit.php';
        require APP . 'view/_templates/footer.php';
    }

    public function Update($request) {
        $this->model = new User($this->db);
        
        if (!empty($request['user_login'])) {
            $pid = (int) $request['thisID'];
            $user_login = $request['user_login'];
            $user_website = $request['user_website'];
            $user_lastname = $request['user_lastname'];
            $user_firstname = $request['user_firstname'];

            $this->model->update($user_firstname, $user_lastname, $user_login, $user_website, $pid);
        }
        header("location: " . URL . "User");
    }
/*
    public function delete($deleteId) {
        header('location: ' . URL . 'Message/Delete/User/' . $deleteId);
        exit();
    }*/

    public function Delete($request) {
        $id_to_delete = (int) $request['user_id'];
        $this->model = new User($this->db);
        $candidate = $this->model->getUserById($id_to_delete);
        if ($candidate == null) {
            header('location: ' . URL . 'Message/ConfirmDelete/User/NotExist');
            exit();
        }
        $this->model->deleteUser($id_to_delete);
        echo "success";
    }

    public function Active($activeid) {
        $this->model = new User($this->db);
        $targetID = $activeid;
        $user = $this->model->fetchInactiveUser($targetID);
        if(!empty($user)) {
            $this->model->activateUser($targetID);
        }
        header('location: ' . URL . 'User');
    }

    public function Inactive($inactiveid) {
        $this->model = new User($this->db);
        $targetID = $inactiveid;
        $user = $this->model->fetchActiveUser($targetID);
        if(!empty($user)) {
            $this->model->deactivateUser($targetID);
        }
        header('location: ' . URL . 'User');
    }

    public function getActiveUsers($request = null) {
        if($request != null) {
            $user_structure = $request['user_structure'];
            $type = $request['type'];
            if (empty($user_structure)) {
                if (empty($type)) {
                    if ($this->profil === 0) {
                        return $this->model->getAllActiveUsersForAdmin();
                    } elseif($this->profil === 1) {
                        return $this->model->getActiveUsersByStructure(Model::$strutil_id);
                    }
                }else{
                    if ($this->profil === 0) {
                        return $this->model->getActiveUsersByTypeForAdmin($type);
                    } elseif($this->profil === 1) {
                        return $this->model->getActiveUsersByStructureAndType($type, Model::$strutil_id);
                    }
                }
            }else{
                if (empty($type)) {
                    if ($this->profil === 0) {
                        return $this->model->getActiveUsersByUserStructureForAdmin($user_structure);
                    } elseif($this->profil === 1) {
                        return $this->model->getActiveUsersByUserAndStructureId($user_structure, Model::$strutil_id);
                    }
                }else{
                    if ($this->profil === 0) {
                        return $this->model->getActiveUsersByStructureAndTypeForAdmin($user_structure ,$type);
                    } elseif($this->profil === 1) {
                        return $this->model->getActiveUsersByStructureAndTypeAndStrId($user_structure, $type, Model::$strutil_id);
                    }
                }
            }
        }else{
            if ($this->profil === 0) {
                return $this->model->getAllActiveUsersForAdmin();
            } elseif($this->profil === 1) {
                return $this->model->getActiveUsersByStructure(Model::$strutil_id);
            }
        }
    }

    public function getInactiveUsers() {
        if (isset($REQUEST['search2'])) {
            $user_structure = $REQUEST['user_structure'];
            $type = $REQUEST['type'];
            if (empty($user_structure)) {
                if (empty($type)) {
                    if ($this->profil === 0) {
                        return $this->model->getAllInactiveUsersForAdmin();
                    } elseif($this->profil === 1) {
                        return $this->model->getInactiveUsersByStructure($str_id);
                    }
                }else{
                    if ($this->profil === 0) {
                        return $this->model->getInactiveUsersByTypeForAdmin($type);
                    } elseif($this->profil === 1) {
                        return $this->model->getInactiveUsersByStructureAndType($type, $str_id);
                    }
                }
            }else{
                if (empty($type)) {
                    if ($this->profil === 0) {
                        return $this->model->getInactiveUsersByUserStructureForAdmin($user_structure);
                    } elseif($this->profil === 1) {
                        return $this->model->getInactiveUsersByUserAndStructureId($user_structure, $str_id);
                    }
                }else{
                    if ($this->profil === 0) {
                        return $this->model->getInactiveUsersByStructureAndTypeForAdmin($user_structure ,$type);
                    } elseif($this->profil === 1) {
                        return $this->model->getInactiveUsersByStructureAndTypeAndStrId($user_structure, $type, $str_id);
                    }
                }
            }
        }else{
            if ($this->profil === 0) {
                return $this->model->getAllInactiveUsersForAdmin();
            } elseif($this->profil === 1) {
                return $this->model->getInactiveUsersByStructure($str_id);
            }
        }
    }

    public function getProfils() {
        if ($this->profil === 0) {
            return $this->model->getProfilsForAdmin();
        } else {
            return $this->model->getProfilsForNotAdmin();
        }
    }

    public function getStructures($str_id = null) {
        if ($this->profil === 0) {
            return $this->model->getAllStructures();
        }elseif($this->profil === 1) {
            return $this->model->getStructureByUser($str_id);
        }
    }

    public function requireUserCode($request) {
        if (isset($request['user_profile'])) {
            $user_profile = $request['user_profile'];

            $profile_id = $this->model->getProfileIdByName($user_profile);
            
            if ($profile_id != null) {
                echo 'true';
            }else{
                echo 'false';
            }
        }
    }
} 


