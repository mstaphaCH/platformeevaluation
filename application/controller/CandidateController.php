<?php

/**
 * Class Home
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class CandidateController extends Controller
{
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/home/index (which is the default page btw)
     */
    public $a_search_firstname = null;
    public $a_search_lastname = null;
    public $a_search_structure = null;
    public $a_search_email = null;

    public function index($request = null)
    {
        /* session and user test */
        if (!isset($_SESSION["user"])) {
            header('location: ' . URL . 'Account');
            exit();
        }
        if ($this->profil === -1) {
            header('location: ' . URL . 'Account/Logout');
            exit();
        }
        // anonymous user or hacker
        if($this->profil !== 0 && $this->profil !== 1 && $this->profil !== 3 && $this->profil !== 4) {
            header('location: ' . URL);
            exit();
        }
        // redirect to home page if isn't admin
        // dark-background
        $header_class = '';
        // ' current'
        $current_class = '';
        // ' current-sub'
        $current_subclass = '';

        $this->model = new Candidate($this->db);

        $candidate_firstnames = $this->getCandidatesFirstname();
        $candidate_lastnames = $this->getCandidatesLastname();
        $candidate_emails = $this->getCandidatesEmail();
        $candidate_structures = $this->getCandidatesStructure();

        if($request === null) {
            $active_candidate = $this->getActiveCandidate();
            $inactive_candidate = $this->getInactiveCandidate();
        }elseif($request['candidate']==='active') {
            $active_candidate = $this->getActiveCandidate($request);
            require APP . 'view/candidate/activeTable.php';
            exit();
        }elseif($request['candidate']==='inactive') {
            $inactive_candidate = $this->getInactiveCandidate($request);
            require APP . 'view/candidate/inactiveTable.php';
            exit();
        }

        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/_templates/navbar.php';
        require APP . 'view/candidate/index.php';
        require APP . 'view/_templates/footer.php';
    }

    public function Add($request) {
        /* Submit form operation: add */
        if (!empty($request['candidat_firstname']) && 
            !empty($request['candidat_lastname']) && 
            !empty($request['candidat_login']) && 
            !empty($request['candidat_email']) && 
            !empty($request['candidat_passwd']) && 
            !empty($request['candidat_repasswd']) && 
            !empty($request['session_end']) && 
            !empty($request['session_start']) && 
            ($request['candidat_passwd']===$request['candidat_repasswd']) && 
            !empty($request['candidat_structure']) && 
            !empty($request['an']) && 
            !empty($request['candidat_rue']) && 
            !empty($request['candidat_ville']) && 
            !empty($request['candidat_pays']) && 
            !empty($request['acces'])) {

            $candidat_firstname = $request['candidat_firstname'];
            $candidat_lastname = $request['candidat_lastname'];

            // $session_start = date('Y-m-d H:i:s',$security->numberFilter($request['session_start']));
            // $session_end = date('Y-m-d H:i:s',$security->numberFilter($request['session_end']));

            list($day,$mois,$year) = explode("/", $request['session_start']);
            $timestamp = mktime(0,0,0,$mois,$day,$year);
            $session_start = date("Y-m-d",$timestamp);

            $acces = $request['acces'];

            if ($acces == 'evaluation') {
                $session_end = $session_start;
            }else{
                list($day,$mois,$year) = explode("/", $request['session_end']);
                $timestamp = mktime(0,0,0,$mois,$day,$year);
                $session_end = date("Y-m-d",$timestamp);
            }

            $candidat_login = $request['candidat_login'];
            $candidat_email = $request['candidat_email'];
            $candidat_tel = $request['candidat_tel'];
            $acces = $request['acces'];
            $candidat_repasswd = $request["candidat_repasswd"];
            $candidat_passwd = $request["candidat_passwd"];
            $candidat_passwd = sha1('{/[*'.$candidat_passwd.'{/[*');
            $an = $request['an'];
            $candidat_rue = $request['candidat_rue'];
            $candidat_ville = $request['candidat_ville'];
            $candidat_pays = $request['candidat_pays'];
            /* Get structure ID from database */
            $candidat_structure = $request['candidat_structure'];

            $this->model = new Candidate($this->db);
            $structure = $this->model->getStructure($candidat_structure);
            if ($structure != null) {
                $structure_id = $structure->candidate->str_id;;
            }

            $candidate = $this->model->getCandidate($candidat_login);
            if ($candidate != null) {
                echo 'Le candidat est déja inscrit..<a href="candidat_list.php">click here</a>';
                exit();
            }
            $aid = $this->model->addAddress($an, $candidat_rue, $candidat_ville, $candidat_pays);
            $pid = $this->model->addCandidate($candidat_firstname, $candidat_lastname, $candidat_login, $candidat_passwd, $candidat_email, $candidat_tel, $session_start, $session_end, $acces, $structure_id, $aid);
            
        }
    }

    public function Edit($request) {
        // redirect to home page if isn't admin
        // dark-background
        $header_class = '';
        // ' current'
        $current_class = '';
        // ' current-sub'
        $current_subclass = '';

        if (isset($request)) {
            $targetID = $request;

            $this->model = new Model($this->db);
            $user_structure_id = $this->model->candidate->str_id;;

            $this->model = new Candidate($this->db);
            $structure = $this->model->getStructureById($user_structure_id);
            $candidate = $this->model->getCandidateById($targetID);

            if ($candidate != null) {
                $candidat_firstname = $candidate->cdt_nom;
                $candidat_lastname = $candidate->cdt_prenom;
                
                $session_start = strftime("%d/%m/%Y", strtotime($candidate->cdt_startSession));
                $session_end = strftime("%d/%m/%Y", strtotime($candidate->cdt_endSession));

                $candidat_login = $candidate->cdt_login;
                $candidat_email = $candidate->cdt_email;
                $acs = $candidate->cdt_statut;
                $candidat_tel = $candidate->cdt_tel;
                $adr_id = $candidate->adr_id;
                $str_id = str_id;
                $util_dern_cnx = strftime("%ss:%mm:%hh %b %d, %Y", strtotime($candidate->cdt_dern_cnx));
            }

            $address = $this->model->getAddressById($adr_id);

            if ($address != null) {
                $an = $address->adr_num;
                $candidat_rue = $address->adr_rue;
                $candidat_ville = $address->adr_ville;
                $candidat_pays = $address->adr_pays;
            }

            $structure = $this->model->getStructureById(str_id);

            if ($structure != null) {
                $str_nom = $structure[0]->str_nom;
            }

            // load views
            require APP . 'view/_templates/header.php';
            require APP . 'view/_templates/navbar.php';
            require APP . 'view/candidate/edit.php';
            require APP . 'view/_templates/footer.php';
        }else{
            header('location: ' . URL . 'Candidate');
        }
    }

    public function Update($request) {
        $this->model = new Candidate($this->db);
        print_r($request);
        /* Query the submit form */
        if (!empty($request['candidat_firstname']) && !empty($request['candidat_lastname']) && 
            !empty($request['candidat_login']) && !empty($request['candidat_email']) && 
            !empty($request['session_end']) && !empty($request['session_start']) && 
            !empty($request['candidat_structure']) && !empty($request['an']) && 
            !empty($request['candidat_rue']) && !empty($request['candidat_ville']) && 
            !empty($request['candidat_pays']) && !empty($request['acces']) && 
            // !empty($request['session_job_edit']) &&
            !empty($request['thisID'])) 
        {
            $pid = (int) $request['thisID'];
            $candidat_firstname = $request['candidat_firstname'];
            $candidat_lastname = $request['candidat_lastname'];
            $candidat_login = $request['candidat_login'];
            $candidat_email = $request['candidat_email'];
            $candidat_tel = $request['candidat_tel'];
            $acces = $request['acces'];
            $an = $request['an'];
            $candidat_rue = $request['candidat_rue'];
            $candidat_ville = $request['candidat_ville'];
            $candidat_pays = $request['candidat_pays'];
            $session_job_edit = $request['session_job_edit'];
            
            list($day,$mois,$year) = explode("/", $request['session_start']);
            $timestamp = mktime(0,0,0,$mois,$day,$year);
            $session_start = date("Y-m-d",$timestamp);

            if ($acces == 'evaluation') {
                $session_end = $session_start;
            }else{
                list($day,$mois,$year) = explode("/", $request['session_end']);
                $timestamp = mktime(0,0,0,$mois,$day,$year);
                $session_end = date("Y-m-d",$timestamp);
            }

            $this->model->update($candidat_firstname, $candidat_lastname, $candidat_login, $candidat_email, $candidat_tel, $session_start, $session_end, $acces, $pid);
            
            $candidate = $this->model->getCandidateById($pid);

            if ($candidate != null) {
                $aid = $candidate->adr_id;
            }

            $this->model->updateAddress($an, $candidat_rue, $candidat_ville, $candidat_pays, $aid);

            echo "success";
            header('location: ' . URL . 'Candidate');
        }
    }

    public function Details($request) {
        // redirect to home page if isn't admin
        // dark-background
        $header_class = '';
        // ' current'
        $current_class = '';
        // ' current-sub'
        $current_subclass = '';

        if (!isset($request)) { header('location: ' . URL . 'Candidate'); exit(); }

        $targetID = $request;

        $this->model = new Model($this->db);
        $user_structure_id = $this->model->candidate->str_id;;

        $this->model = new Candidate($this->db);
        $structure = $this->model->getStructureById($user_structure_id);
        $candidate = $this->model->getCandidateById($targetID);

        if ($candidate != null) {
            $candidat_firstname = $candidate->cdt_nom;
            $candidat_lastname = $candidate->cdt_prenom;
            
            $session_start = strftime("%d/%m/%Y", strtotime($candidate->cdt_startSession));
            $session_end = strftime("%d/%m/%Y", strtotime($candidate->cdt_endSession));

            $candidat_login = $candidate->cdt_login;
            $candidat_email = $candidate->cdt_email;
            $acs = $candidate->cdt_statut;
            $candidat_tel = $candidate->cdt_tel;
            $adr_id = $candidate->adr_id;
            $str_id = str_id;
            $util_dern_cnx = strftime("%ss:%mm:%hh %b %d, %Y", strtotime($candidate->cdt_dern_cnx));
        }

        $address = $this->model->getAddressById($adr_id);

        if ($address != null) {
            $an = $address->adr_num;
            $candidat_rue = $address->adr_rue;
            $candidat_ville = $address->adr_ville;
            $candidat_pays = $address->adr_pays;
        }

        $structure = $this->model->getStructureById(str_id);

        if ($structure != null) {
            $str_nom = $structure[0]->str_nom;
        }

        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/_templates/navbar.php';
        require APP . 'view/candidate/details.php';
        require APP . 'view/_templates/footer.php';
    }

    public function editSessionJob($request) {
        $this->model = new Candidate($this->db);
        /* exit if candidate id doesn't exist or empty */
        if (!isset($request['thisID']) || empty($request['thisID'])) {exit();}
        $thisID = $request['thisID'];
        /* load all sessions without filtering if date range is empty */
        if (empty($request['session_job_start']) || empty($request['session_job_end'])) {
            if ($this->profil===0) { // global admin test to fetch all sessions
                $sessions = $this->model->getSessions();
            }elseif($this->profil===1) { // local admin test to fetch specifique structure sessions
                $sessions = $this->model->getSessionsByStructure(str_id);
            }
            if ($sessions != null) {
                foreach ($sessions as $key => $session) {
                    $plf_reff = $session->plf_year . '-' . $session->candidate->str_id . '-' . $session->plf_ref;
                    if ($thisID == $session->cdt_id) {
                        echo '<option value="' . $session->plf_ref . '" selected>' . $plf_reff . ' &nbsp;&nbsp; ' . ucfirst($session->plf_title) . '</option>';
                    }else{
                        echo '<option value="' . $session->plf_ref . '">' . $plf_reff . ' &nbsp;&nbsp; ' . ucfirst($session->plf_title) . '</option>';  
                    }
                }
            }
            exit();
        }

        $session_job_start = $request['session_job_start'];
        $session_job_end = $request['session_job_end'];

        list($day,$mois,$year) = explode("/", $request['session_job_start']);
        $timestamp = mktime(0,0,0,$mois,$day,$year);
        $session_job_start = date("Y-m-d",$timestamp);

        list($day,$mois,$year) = explode("/", $request['session_job_end']);
        $timestamp = mktime(0,0,0,$mois,$day,$year);
        $session_job_end = date("Y-m-d",$timestamp);

        if ($this->profil===0) { // global admin test to fetch all sessions
            $sessions = $this->model->getSessionsByDate($session_job_start, $session_job_end);
        }elseif ($this->profil===1) { // local admin test to fetch specifique structure sessions
            $sessions = $this->model->getSessionsByDateAndStructure(str_id, $session_job_start, $session_job_end);
        }

        if ($sessions != null) {
            foreach ($sessions as $key => $session) {
                $plf_reff = $session->plf_year . '-' . $session->candidate->str_id . '-' . $session->plf_ref;
                if ($thisID == $session->cdt_id) {
                    echo '<option value="' . $session->plf_ref . '" selected>' . $plf_reff . ' &nbsp;&nbsp; ' . ucfirst($session->plf_title).'</option>';
                }else{
                    echo '<option value="' . $session->plf_ref . '">' . $plf_reff . ' &nbsp;&nbsp; '.ucfirst($session->plf_title) . '</option>';  
                }
            }
        }

    }

    public function addSessionJob() {
        $this->model = new Candidate($this->db);
        if (empty($request['session_job_start']) || empty($request['session_job_end'])) {
            if ($this->profil===0) { // global admin test to fetch all sessions
                $sessions = $this->model->getSessions();
            }elseif($this->profil===1) { // local admin test to fetch specifique structure sessions 
                $sessions = $this->model->getSessionsByStructure(str_id);
            }

            if ($sessions != null) {
                foreach ($sessions as $key => $session) {
                    $plf_reff = $session->plf_year.'-'.$session->candidate->str_id.'-'.$session->plf_ref;
                    echo '<option value="'.$session->plf_ref.'">'.$plf_reff.' &nbsp;&nbsp; '.ucfirst($session->plf_title).'</option>';
                }
            }
            exit();
        }

        $session_job_start = $request['session_job_start'];
        $session_job_end = $request['session_job_end'];

        list($day,$mois,$year) = explode("/", $request['session_job_start']);
        $timestamp = mktime(0,0,0,$mois,$day,$year);
        $session_job_start = date("Y-m-d",$timestamp);

        list($day,$mois,$year) = explode("/", $request['session_job_end']);
        $timestamp = mktime(0,0,0,$mois,$day,$year);
        $session_job_end = date("Y-m-d",$timestamp);

        if ($this->profil===0) { // global admin test to fetch all sessions
            $sessions = $this->model->getSessionsByDate($session_job_start, $session_job_end);
        }elseif ($this->profil===1) { // local admin test to fetch specifique structure sessions 
            $sessions = $this->model->getSessionsByDateAndStructure(str_id, $session_job_start, $session_job_end);
        }

        if ($sessions != null) {
            foreach ($sessions as $key => $session) {
                $plf_reff = $session->plf_year.'-'.$session->candidate->str_id.'-'.$session->plf_ref;
                echo '<option value="'.$session->plf_id.'">'.$plf_reff.' &nbsp;&nbsp; '.ucfirst($session->plf_title).'</option>';
            }
        }
    }

    public function Delete($request) {
        $this->model = new Candidate($this->db);
        $id_to_delete = (int) htmlentities($request['candidate_id']);

        $candidate = $this->model->getCandidateById($id_to_delete);

        if ($candidate == null) {
            header('location: ' . URL . 'Candidate/Message/ConfirmDelete/NotExist');
            exit();
        }
        $address_id = $candidate->adr_id;

        $this->model->deleteCandidate($id_to_delete);
        $this->model->deleteAddress($address_id);
        echo "success";
    }

    public function getActiveCandidate($request = null) {
        if($request != null) {
            $a_search_firstname = isset($request['a_search_firstname']) ? $request['a_search_firstname'] : '';
            $a_search_lastname = isset($request['a_search_lastname']) ? $request['a_search_lastname'] : '';
            $a_search_email = isset($request['a_search_email']) ? $request['a_search_email'] : '';
            $a_search_structure = $request['a_search_structure'];
			

            if (empty($a_search_firstname) && empty($a_search_lastname) && empty($a_search_email) && empty($a_search_structure)) {
                if ($this->profil === 0) {
                    return $this->model->getDefaultActiveCandidateForGlobalUser();
                }elseif($this->profil===1 || $this->profil===3 || $this->profil===4) {
                    return $this->model->getDefaultActiveCandidateForLocalUser(str_id);
                }
            }
            if (empty($a_search_firstname) && empty($a_search_lastname) && empty($a_search_email) && !empty($a_search_structure)) {
                if ($this->profil===0) {
                    return $this->model->getActiveCandidateByStructureForGlobalUser($a_search_structure);
                }elseif($this->profil===1 || $this->profil===3 || $this->profil===4) {
                    return $this->model->getActiveCandidateByStructureForLocalUser(str_id, $a_search_structure);
                }
            }
            if (empty($a_search_firstname) && empty($a_search_lastname) && !empty($a_search_email) && empty($a_search_structure)) {
                if ($this->profil===0) {
                    return $this->model->getActiveCandidateByEmailForGlobalUser($a_search_email);
                }elseif($this->profil===1 || $this->profil===3 || $this->profil===4) {
                    return $this->model->getActiveCandidateByEmailForLocalUser(str_id, $a_search_email);
                }
            }
            if (empty($a_search_firstname) && !empty($a_search_lastname) && empty($a_search_email) && empty($a_search_structure)) {
                if ($this->profil===0) {
                    return $this->model->getActiveCandidateByLastnameForGlobalUser($a_search_lastname);
                }elseif($this->profil===1 || $this->profil===3 || $this->profil===4) {
                    return $this->model->getActiveCandidateByLastnameForLocalUser(str_id, $a_search_lastname);
                }
            }
            if (!empty($a_search_firstname) && empty($a_search_lastname) && empty($a_search_email) && empty($a_search_structure)) {
                if ($this->profil===0) {
                    return $this->model->getActiveCandidateByFirstnameForGlobalUser($a_search_firstname);
                }elseif($this->profil===1 || $this->profil===3 || $this->profil===4) {
                    return $this->model->getActiveCandidateByFirstnameForLocalUser($request['str_id'], $a_search_firstname);
                }
            }
        }else{
            if ($this->profil===0) {
                return $this->model->getDefaultActiveCandidateForGlobalUser();
            }elseif($this->profil===1 || $this->profil===3 || $this->profil===4) {
                return $this->model->getDefaultActiveCandidateForLocalUser(null);
            }
        }
    }

    public function getInactiveCandidate($request = null) {
        if($request != null) {
            $na_search_firstname = isset($request['na_search_firstname']) ? $request['na_search_firstname'] : '';
            $na_search_lastname = isset($request['na_search_lastname']) ? $request['na_search_lastname'] : '';
            $na_search_email = isset($request['na_search_email']) ? $request['na_search_email'] : '';
            $na_search_structure = $request['na_search_structure']; 

            if (empty($na_search_firstname) && empty($na_search_lastname) && empty($na_search_email) && empty($na_search_structure)) {
                if ($this->profil === 0) {
                    return $this->model->getDefaultInactiveCandidateForGlobalUser();
                }elseif($this->profil===1 || $this->profil===3 || $this->profil===4) {
                    return $this->model->getDefaultActiveCandidateForLocalUser(str_id);
                }
            }
            if (empty($na_search_firstname) && empty($na_search_lastname) && empty($na_search_email) && !empty($na_search_structure)) {
                if ($this->profil===0) {
                    return $this->model->getInactiveCandidateByStructureForGlobalUser($na_search_structure);
                }elseif($this->profil===1 || $this->profil===3 || $this->profil===4) {
                    return $this->model->getInactiveCandidateByStructureForLocalUser(str_id, $na_search_structure);
                }
            }
            if (empty($na_search_firstname) && empty($na_search_lastname) && !empty($na_search_email) && empty($na_search_structure)) {
                if ($this->profil===0) {
                    return $this->model->getInactiveCandidateByEmailForGlobalUser($na_search_email);
                }elseif($this->profil===1 || $this->profil===3 || $this->profil===4) {
                    return $this->model->getInactiveCandidateByEmailForLocalUser(str_id, $na_search_email);
                }
            }
            if (empty($na_search_firstname) && !empty($na_search_lastname) && empty($na_search_email) && empty($na_search_structure)) {
                if ($this->profil===0) {
                    return $this->model->getInactiveCandidateByLastnameForGlobalUser($na_search_lastname);
                }elseif($this->profil===1 || $this->profil===3 || $this->profil===4) {
                    return $this->model->getInactiveCandidateByLastnameForLocalUser(str_id, $na_search_lastname);
                }
            }
            if (!empty($na_search_firstname) && empty($na_search_lastname) && empty($na_search_email) && empty($na_search_structure)) {
                if ($this->profil===0) {
                    return $this->model->getInactiveCandidateByFirstnameForGlobalUser($na_search_firstname);
                }elseif($this->profil===1 || $this->profil===3 || $this->profil===4) {
                    return $this->model->getInactiveCandidateByFirstnameForLocalUser(str_id, $na_search_firstname);
                }
            }
        }else{
            if ($this->profil===0) {
                return $this->model->getDefaultInactiveCandidateForGlobalUser();
            }elseif($this->profil===1 || $this->profil===3 || $this->profil===4) {
                return $this->model->getDefaultInactiveCandidateForLocalUser(null);
            }
        }
    }

    public function StructureById($candidat_str) {
        return $this->model->getStructureById($candidat_str);
    }

    public function Active($activeid) {
        $targetID = $activeid;
        $this->model = new Candidate($this->db);
        $candidate = $this->model->fetchInactiveCandidate($targetID);
        if(!empty($candidate)) {
            $this->model->activateCandidate($targetID);
        }
        header('location: ' . URL . 'Candidate');
    }

    public function Desactive($inactiveid) {
        $targetID = $inactiveid;
        $this->model = new Candidate($this->db);
        $candidate = $this->model->fetchActiveCandidate($targetID);
        if(!empty($candidate)) {
            $this->model->deactivateCandidate($targetID);
        }
        header('location: ' . URL . 'Candidate');
    }

    public function getCandidatesFirstname() {
        return $this->model->getCandidatesFirstname();
    }
    public function getCandidatesLastname() {
        return $this->model->getCandidatesLastname();
    }
    public function getCandidatesEmail() {
        return $this->model->getCandidatesEmail();
    }
    public function getCandidatesStructure() {
        return $this->model->getCandidatesStructure();
    }
}
