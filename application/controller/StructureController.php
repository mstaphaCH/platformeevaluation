<?php
/**
 * Class Home
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class StructureController extends Controller
{
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/home/index (which is the default page btw)
     */
    public $structure = null;
    public $address = null;

    public function index($request = null)
    {
    	/* Session existing test */
		if (!isset($_SESSION["user"])) {
			header('location: ' . URL . 'Account');
			exit();
		}
		/* Check user profil */
		if ($this->profil===-1) {
			header('location: ' . URL . 'Account/Logout');
			exit();
		}
		if($this->profil !== 0 && $this->profil !== 1) {
			header('location: ' . URL);
			exit();
		}
        // redirect to home page if isn't admin
        // dark-background
        $header_class = '';
        // ' current'
        $current_class = '';
        // ' current-sub'
        $current_subclass = '';

        $this->model = new Structure($this->db);
        $structure_titles = $this->getStructureTitles();
        $countries = $this->getStructureCountries();
        $cities = $this->getStructureCities();

        if($request === null) {
            $active_structures = $this->getActiveStructures();
            $inactive_structures = $this->getInactiveStructures();
        }elseif($request['structure']==='active'){
            $active_structures = $this->getActiveStructures($request);
            require APP . 'view/structure/activeTable.php';
            exit();
        }elseif($request['structure']==='inactive'){
            $inactive_structures = $this->getInactiveStructures($request);
            require APP . 'view/structure/inactiveTable.php';
            exit();
        }

        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/_templates/navbar.php';
        require APP . 'view/structure/index.php';
        require APP . 'view/_templates/footer.php';
    }

    public function Add($request) {
        if (isset($request['submit'])) {
            if (!empty($request['str_nom']) && !empty($request['an']) && !empty($request['adr_code_postal']) && !empty($request['str_email_contact']) && !empty($request['str_nom_contact']) && !empty($request['str_prenom_contact']) && !empty($request['adr_rue']) && !empty($request['adr_ville']) && !empty($request['adr_pays'])) {
                $str_nom = $request['str_nom'];
                $str_description = $request['str_description'];
                $str_nom_contact = $request['str_nom_contact'];
                $str_prenom_contact = $request['str_prenom_contact'];
                $str_email_contact = $request['str_email_contact'];
                $an = $request['an'];
                $adr_rue = $request['adr_rue'];
                $adr_code_postal = $request['adr_code_postal'];
                $adr_ville = $request['adr_ville'];
                $adr_pays = $request['adr_pays'];
                $str_tel = $request['str_tel'];
                $str_siteweb = $request['str_siteweb'];
                $str_status = 0;

                $this->model = new Structure($this->db);
                $aid = $this->model->addAddress($an, $adr_rue, $adr_code_postal, $adr_ville, $adr_pays);
                 
                $pid = $this->model->addStructure($str_nom, $str_tel, $str_description, $str_nom_contact, $str_prenom_contact, $str_email_contact, $str_siteweb, $str_status, $aid);

                header('location: ' . URL . 'Structure');
                // exit();
            }
        }
    }

    public function Edit($request) {
        // dark-background
        $header_class = '';
        // ' current'
        $current_class = '';
        // ' current-sub'
        $current_subclass = '';

        $this->model = new Structure($this->db);

        $this->ReadStructure($request);

        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/_templates/navbar.php';
        require APP . 'view/structure/edit.php';
        require APP . 'view/_templates/footer.php';
    }

    public function Update($request) {
        if (isset($request['submit'])) {
            if (!empty($request['str_nom']) && !empty($request['adr_code_postal']) && !empty($request['an']) && !empty($request['adr_rue']) && !empty($request['adr_ville']) && !empty($request['adr_pays'])) {

                $str_nom = $request['str_nom'];
                $str_description = $request['str_description'];
                $str_nom_contact = $request['str_nom_contact'];
                $str_prenom_contact = $request['str_prenom_contact'];
                $str_email_contact = $request['str_email_contact'];
                $str_tel = $request['str_tel'];
                $str_siteweb = $request['str_siteweb'];
                $pid = $request['thisID'];
                $adr_code_postal = $request['adr_code_postal'];
                $an = $request['an'];
                $adr_rue = $request['adr_rue'];
                $adr_ville = $request['adr_ville'];
                $adr_pays = $request['adr_pays'];

                $this->model = new Structure($this->db);
                $structure = $this->model->ReadStructure($pid);
                if ($structure != null) {
                    $aid = $structure->adr_id;
                }
                $this->model->updateAddress($an, $adr_rue, $adr_code_postal, $adr_ville, $adr_pays, $aid);
                $this->model->updateStructure($str_nom, $str_tel, $str_description, $str_nom_contact, $str_prenom_contact, $str_email_contact, $str_siteweb, $pid);
            }
            header('location: ' . URL . 'Structure');
        }
    }

    public function Details($request) {
        // dark-background
        $header_class = '';
        // ' current'
        $current_class = '';
        // ' current-sub'
        $current_subclass = '';

        $this->model = new Structure($this->db);

        $this->ReadStructure($request);

        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/_templates/navbar.php';
        require APP . 'view/structure/details.php';
        require APP . 'view/_templates/footer.php';
    }

    public function ReadStructure($id) {
        if (isset($id)) {
            $targetID = $id;
            $this->structure = $this->model->ReadStructure($targetID);
            $this->address = $this->model->ReadAddress($this->structure->adr_id);
        }else{
            header('location: ' . URL . 'Structure');
        }
    }

    public function changeStatus($request) {
        if(isset($request['structure_id']) && $request['structure_id'] != 1) {
            $str_id = $request['structure_id'];

            $this->model = new Structure($this->db);
            $structure = $this->model->getStatus($str_id);
            
            if ($structure != null) {
                $str_status = $structure->str_status;
                if ($str_status==1) {
                    // desactivate structure
                    $this->model->deactivateStructure($str_id);
                    // desactivate all users within the structure
                    $this->model = new User($this->db);
                    $users = $this->model->getActiveUsersByStructure($str_id);
                    foreach ($users as $key => $user) {
                        $this->model->deactivateUser($user->util_id);
                    }
                    // desactivate all candidates within the structure
                    $this->model = new Candidate($this->db);
                    $candidates = $this->model->getActiveCandidatesByStructure($str_id);
                    foreach ($candidates as $key => $candidate) {
                        $this->model->deactivateCandidate($candidate->cdt_id);   
                    }
                }elseif ($str_status==0) {
                    // activate structure
                    $this->model->activateStructure($str_id);
                    // activate all users within the structure
                    $this->model = new User($this->db);
                    $users = $this->model->getInactiveUsersByStructure($str_id);
                    foreach ($users as $key => $user) {
                        $this->model->activateUser($user->util_id);
                    }
                    // activate all candidates within the structure
                    $this->model = new Candidate($this->db);
                    $candidates = $this->model->getInactiveCandidatesByStructure($str_id);
                    foreach ($candidates as $key => $candidate) {
                        $this->model->activateCandidate($candidate->cdt_id);   
                    }
                }
            }
        }
    }

    public function getActiveStructures($request = null) {
        $active = true;
        return $this->getStructures($active, $request);
    }

    public function getInactiveStructures($request = null) {
        $active = false;
        return $this->getStructures($active, $request);
    }

    public function getStructures($status,$request = null) {
        if($request != null) {
            $search_name = isset($request['search_name']) ? $request['search_name'] : '';
            $search_country = isset($request['search_country']) ? $request['search_country'] : '';
            $search_city = isset($request['search_city']) ? $request['search_city'] : '';
            
            $this->model = new Structure($this->db);
            if (empty($search_country)) {
                if (empty($search_name)) {
                    if (empty($search_city)) {
                        if ($this->profil===0) {
                            return $this->model->getAllStructuresForAdmin($status);
                        }else{
                            return $this->model->getAllStructuresForNotAdmin($status);
                        }
                    }else{
                        if ($this->profil===0) {
                            return $this->model->getStructuresByCityForAdmin($status,$search_city);
                        }else{
                            return $this->model->getStructuresByCityForNotAdmin($status,$search_city);
                        }
                    }
                }else{
                    if (empty($search_city)) {
                        if ($this->profil===0) {
                            return $this->model->getStructuresByNameForAdmin($status,$search_name);
                        }else{
                            return $this->model->getStructuresByNameForNotAdmin($status,$search_name);
                        }
                    }else{
                        if ($this->profil===0) {
                            return $this->model->getStructuresByCityAndNameForAdmin($status,$search_city,$search_name);
                        }else{
                            return $this->model->getStructuresByCityAndNameForNotAdmin($status,$search_city,$search_name);
                        }
                    }
                }
            }else{
                if (empty($search_name)) {
                    if (empty($search_city)) {
                        if ($this->profil===0) {
                            return $this->model->getStructuresByCountryForAdmin($status,$search_country);
                        }else{
                            return $this->model->getStructuresByCountryForNotAdmin($status,$search_country);
                        }
                    }else{
                        if ($this->profil===0) {
                            return $this->model->getStructuresByCityAndCountryForAdmin($status,$search_country);
                        }else{
                            return $this->model->getStructuresByCityAndCountryForNotAdmin($status,$search_country);
                        }
                    }
                }else{
                    if (empty($search_city)) {
                        if ($this->profil===0) {
                            return $this->model->getStructuresByNameAndCountryForAdmin($status,$search_name, $search_country);
                        }else{
                            return $this->model->getStructuresByNameAndCountryForNotAdmin($status,$search_name, $search_country);
                        }
                    }else{
                        if ($this->profil===0) {
                            return $this->model->getStructuresByCityAndNameAndCountryForAdmin($status,$search_city, $search_name, $search_country);
                        }else{
                            return $this->model->getStructuresByCityAndNameAndCountryForNotAdmin($status,$search_city, $search_name, $search_country);
                        }
                    }
                }
            }
            
        }else{
            if ($this->profil===0) {
                return $this->model->getAllStructuresForAdmin($status);
            }else{
                return $this->model->getAllStructuresForNotAdmin($status);
            }
        }
    }

/*    public function Delete($deleteId) {
        header('location: ' . URL . 'Message/Delete/Structure/' . $deleteId);
        exit();
    }*/

    public function Delete($request) {
        $id_to_delete = (int) $request['structure_id'];
        if ($id_to_delete===1 || $id_to_delete===2) {
            header("location: ../message.php?pg=structure&msg=cannot_delete");
            exit();
        }

        $this->model = new Structure($this->db);
        $structure = $this->model->ReadStructure($id_to_delete);
        
        if ($structure == null) {
            header("location: ../message.php?pg=structure&msg=delete_not_exist");
            exit();
        }
        $adr_id = $structure->adr_id;
        $str_id = $structure->str_id;
        $this->model->deleteUserByStructureId($str_id);
        $this->model->deleteStructure($id_to_delete);
        $this->model->deleteAddress($adr_id);
        echo "success";
    }

    public function getStructureTitles() {
        if ($this->profil===0) {
            return $this->model->getStructureTitlesForAdmin();
        }else{
            return $this->model->getStructureTitlesForNotAdmin();
        }
    }

    public function getStructureCountries() {
        return $this->model->getStructureCountries();
    }

    public function getStructureCities() {
        return $this->model->getStructureCities();
    }

    public function getCity($address_id) {
        return $this->model->getStructureCity($address_id)[0];
    }

    public function getCountry($address_id) {
        return $this->model->getStructureCountry($address_id)[0];
    }
}
