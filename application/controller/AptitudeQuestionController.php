<?php

/**
 * Class Home
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class AptitudeQuestionController extends Controller
{
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/home/index (which is the default page btw)
     */
    public function index($request = null)
    {
        /* session and user test */
        if (!isset($_SESSION["user"])) {
            header('location: ' . URL . 'Account');
            exit();
        }
        if ($this->profil === -1) {
            header('location: ' . URL . 'Account/Logout');
            exit();
        }
        // anonymous user or hacker
        if($this->profil!==0 && $this->profil!==1 && $this->profil!==3 && $this->profil!==4) {
            header('location: ' . URL);
            exit();
        }
        // redirect to home page if isn't admin
        // dark-background
        $header_class = '';
        // ' current'
        $current_class = '';
        // ' current-sub'
        $current_subclass = '';
        
        $this->model = new AptitudeQuestion($this->db);

        $request = $_POST;
        if($request === null){
            $aptitudeQuestions = $this->getAptitudeQuestion();
        }else{
            $aptitudeQuestions = $this->getAptitudeQuestion($request);
        }
        
        $aptitudeEvaluationNames = $this->model->getAptitudeEvaluationNames();
        $aptitudeEvaluations = $this->model->getAptitudeEvaluations();
        
        $qst_idd = $this->getLatestQuestionNumber();
        $typeNames = $this->model->getTypeNames();

        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/_templates/navbar.php';
        require APP . 'view/aptitudeQuestion/index.php';
        require APP . 'view/_templates/footer.php';
    }

    public function Add($request) {
        if (!empty($request['qst_num']) && !empty($request['qst_desc']) && 
            !empty($request['qst_op1']) && !empty($request['qst_op2']) && 
            !empty($request['qst_op3']) && !empty($request['qst_op4']) && 
            !empty($request['qst_tpe']) && isset($request['qst_val_op1']) && 
            !empty($request['code_option_a']) && !empty($request['code_option_b']) && 
            !empty($request['code_option_c']) && isset($request['code_option_d']) && 
            isset($request['qst_val_op2']) && isset($request['qst_val_op3']) && 
            isset($request['qst_val_op4'])) {

            $this->model = new AptitudeQuestion($this->db);

            $qst_tpe = $request['qst_tpe'];

            $type = $this->model->getTypeByName($qst_tpe)[0];
            
            if ($type != null) {
                $type_id = $type->type_id;
            }

            $qst_num = $request['qst_num'];
            $qst_desc = $request['qst_desc'];
            $qst_op1 = $request['qst_op1'];
            $qst_op2 = $request['qst_op2'];
            $qst_op3 = $request['qst_op3'];
            $qst_op4 = $request['qst_op4'];

            $qst_val_op1 = $request['qst_val_op1'];
            $qst_val_op2 = $request['qst_val_op2'];
            $qst_val_op3 = $request['qst_val_op3'];
            $qst_val_op4 = $request['qst_val_op4'];

            $code_option_a = $request['code_option_a'];
            $code_option_b = $request['code_option_b'];
            $code_option_c = $request['code_option_c'];
            $code_option_d = $request['code_option_d'];

            $qst_expl = $request['qst_expl'];
            $qst_imp = $request['qst_imp'];
            
            /* Question insert */
            $qst_id = $this->model->addQuestion($qst_num, $qst_desc, $qst_expl, $qst_imp, $type_id);
            /* Options insert */
            $this->model->addOption($qst_op1, $qst_val_op1, $code_option_a, $qst_id);
            $this->model->addOption($qst_op2, $qst_val_op2, $code_option_b, $qst_id);
            $this->model->addOption($qst_op3, $qst_val_op3, $code_option_c, $qst_id);
            $this->model->addOption($qst_op4, $qst_val_op4, $code_option_d, $qst_id);
            
            $rep_id = 0;
            $this->model->updateQuestion($rep_id, $qst_id);
            header('location: '.URL.'AptitudeQuestion');
        }
    }

    public function Delete($id) {
        $id_to_delete = (int) $id;
        $this->model = new AptitudeQuestion($this->db);

        $question = $this->model->getQuestionById($id_to_delete)[0];

        if ($question == null) {
            echo "Question doesn't exist";
            exit();
        }
        $this->model->deleteOptionByQuestionId($id_to_delete);
        $this->model->deleteEvalQuesByQuestionId($id_to_delete);
        $this->model->deleteQuestionById($id_to_delete);

        header('location: '.URL.'AptitudeQuestion');
    }

    public function Edit($request) {
        /* session and user test */
        if (!isset($_SESSION["user"])) {
            header('location: ' . URL . 'Account');
            exit();
        }
        if ($this->profil === -1) {
            header('location: ' . URL . 'Account/Logout');
            exit();
        }
        // anonymous user or hacker
        if($this->profil!==0 && $this->profil!==1 && $this->profil!==3 && $this->profil!==4) {
            header('location: ' . URL);
            exit();
        }
        // redirect to home page if isn't admin
        // dark-background
        $header_class = '';
        // ' current'
        $current_class = '';
        // ' current-sub'
        $current_subclass = '';
        
        $this->model = new AptitudeQuestion($this->db);
        
        $targetID = $request;
        $question = $this->model->getQuestionById($targetID)[0];
        
        if ($question != null) {
            $id = $question->qst_id;
            $qst_ref = $question->qst_ref;
            $qst_description = $question->qst_description;
            $qst_num_rep_correct = $question->qst_num_rep_correct;

            $options = $this->model->getOptionsByQuestion($id);
            
            $i=0;
            if ($options != null) {
                foreach ($options as $key => $option) {
                    $op[$i] = $option->opt_description;
                    $op_val[$i] = $option->opt_valeur;
                    $cd_option[$i] = $option->opt_code;

                    if($qst_num_rep_correct===$option->opt_id) {
                        if ($i==0) {
                            $correct = 'qst_op1';
                        }
                        if ($i==1) {
                            $correct = 'qst_op2';
                        }
                        if ($i==2) {
                            $correct = 'qst_op3';
                        }
                        if ($i==3) {
                            $correct = 'qst_op4';
                        }
                    }
                    $i++;
                }
            }

            $qst_explication = $question->qst_explication;
            $qst_importance = $question->qst_importance;
            $qst_objectif = $question->qst_objectif;

            $type_id = $question->type_id;

            if (!is_null($type_id)) {
                $questionType = $this->model->getTypeById($type_id)[0];
                
                if ($questionType != null) {
                    $questionType = $questionType->type_nom;
                }
            }else{
                $questionType = "null";
            }
            
        }

        $types = $this->model->getAllTypes();

        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/_templates/navbar.php';
        require APP . 'view/aptitudeQuestion/edit.php';
        require APP . 'view/_templates/footer.php';
    }

    public function Details($request) {
        /* session and user test */
        if (!isset($_SESSION["user"])) {
            header('location: ' . URL . 'Account');
            exit();
        }
        if ($this->profil === -1) {
            header('location: ' . URL . 'Account/Logout');
            exit();
        }
        // anonymous user or hacker
        if($this->profil!==0 && $this->profil!==1 && $this->profil!==3 && $this->profil!==4) {
            header('location: ' . URL);
            exit();
        }
        // redirect to home page if isn't admin
        // dark-background
        $header_class = '';
        // ' current'
        $current_class = '';
        // ' current-sub'
        $current_subclass = '';
        
        $this->model = new AptitudeQuestion($this->db);
        
        $targetID = $request;
        $question = $this->model->getQuestionById($targetID)[0];
        
        if ($question != null) {
            $id = $question->qst_id;
            $qst_ref = $question->qst_ref;
            $qst_description = $question->qst_description;
            $qst_num_rep_correct = $question->qst_num_rep_correct;

            $options = $this->model->getOptionsByQuestion($id);
            
            $i=0;
            if ($options != null) {
                foreach ($options as $key => $option) {
                    $op[$i] = $option->opt_description;
                    $op_val[$i] = $option->opt_valeur;
                    $cd_option[$i] = $option->opt_code;

                    if($qst_num_rep_correct===$option->opt_id) {
                        if ($i==0) {
                            $correct = 'qst_op1';
                        }
                        if ($i==1) {
                            $correct = 'qst_op2';
                        }
                        if ($i==2) {
                            $correct = 'qst_op3';
                        }
                        if ($i==3) {
                            $correct = 'qst_op4';
                        }
                    }
                    $i++;
                }
            }

            $qst_explication = $question->qst_explication;
            $qst_importance = $question->qst_importance;
            $qst_objectif = $question->qst_objectif;

            $type_id = $question->type_id;

            if (!is_null($type_id)) {
                $questionType = $this->model->getTypeById($type_id)[0];
                
                if ($questionType != null) {
                    $questionType = $questionType->type_nom;
                }
            }else{
                $questionType = "null";
            }
            
        }

        $types = $this->model->getAllTypes();

        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/_templates/navbar.php';
        require APP . 'view/aptitudeQuestion/details.php';
        require APP . 'view/_templates/footer.php';
    }

    public function Update($request) {
        $this->model = new AptitudeQuestion($this->db);
        /* Get the type id from the database */
        if (!empty($request['qst_tpe'])) {
            $qst_tpe = $request['qst_tpe'];
            $type = $this->model->getTypeByName($qst_tpe)[0];
            
            if ($type != null) {
                $type_id = $type->type_id;
            }
        }
        /* Save changes when form submit */
        if (!empty($request['qst_desc']) && 
            !empty($request['qst_op1']) && 
            !empty($request['qst_op2']) && 
            !empty($request['qst_op3']) && 
            !empty($request['qst_op4']) && 
            !empty($request['qst_tpe']) && 
            isset($request['qst_val_op1']) && 
            isset($request['qst_val_op2']) && 
            !empty($request['code_option_a']) && 
            !empty($request['code_option_b']) && 
            isset($request['code_option_c']) && 
            isset($request['code_option_d']) && 
            isset($request['qst_val_op3']) && 
            isset($request['qst_val_op4'])) {

            $pid = (int) $request['thisID'];
            $qst_desc = $request['qst_desc'];
            $qst_op1 = $request['qst_op1'];
            $qst_op2 = $request['qst_op2'];
            $qst_op3 = $request['qst_op3'];
            $qst_op4 = $request['qst_op4'];
            $qst_obj = $request['qst_obj'];
            $qst_val_op1 = $request['qst_val_op1'];
            $qst_val_op2 = $request['qst_val_op2'];
            $qst_val_op3 = $request['qst_val_op3'];
            $qst_val_op4 = $request['qst_val_op4'];

            $code_option_a = $request['code_option_a'];
            $code_option_b = $request['code_option_b'];
            $code_option_c = $request['code_option_c'];
            $code_option_d = $request['code_option_d'];

            $qst_expl = $request['qst_expl'];
            $qst_imp = $request['qst_imp'];
            
            $opp = array($qst_op1, $qst_op2, $qst_op3, $qst_op4);
            $opp_val = array($qst_val_op1, $qst_val_op2, $qst_val_op3, $qst_val_op4);
            $code_option = array($code_option_a, $code_option_b, $code_option_c, $code_option_d);

            $options = $this->model->getOptionsByQuestion($pid);

            $i=0;
            if ($options != null) {
                foreach ($options as $key => $option) {
                    $op = $option->opt_id;
                    $this->model->updateOption($opp, $opp_val, $code_option, $op, $i);
                    $i++;
                }
            }
            $this->model->updateOptionByQuestionId($qst_desc, $qst_obj, $qst_expl, $qst_imp, $type_id, $pid);
            
        }
        header('location: '.URL.'AptitudeQuestion');
    }

    public function UpdateEvaluations($request) {
        $qst_id = (int) preg_replace('#[^0-9]#i', '', $request['qst_id']);
        $checkbox = $request['checkbox'];
        $boxs = explode('|', $checkbox);

        $this->model = new AptitudeQuestion($this->db);
        $this->model->deleteEvalQuest($qst_id);
        for ($i=1; $i < count($boxs); $i++) {
            $evaluation = $this->model->getEvaluationByName($boxs, $i)[0];
            
            if ($evaluation != null) {
                $id = $evaluation->eval_id;
            }
            $this->model->insertEvalQues($id, $qst_id);
        }
    }

    public function evalQuesByEvalId($qst_id, $eval_id) {
         $this->model = new AptitudeQuestion($this->db);
        return $this->model->getEvalQuestionByEvalId($qst_id, $eval_id);
    }

    public function getAptitudeQuestion($request = null) {
        if (isset($request['search'])) {
             $evaluation = $request['evaluation'];
             if (empty($evaluation)) {
                 return $this->model->getAllAptitudeQuestion();
             }else{
                 return $this->model->getAptitudeQuestionByEvaluation($evaluation);
             }
         }else{
             return $this->model->getAllAptitudeQuestion();
         }
    }

    public function getLatestQuestionNumber() {
        $number = $this->model->getLatestQuestionNumber();
        if ($number != null) {
            return $number[0]->qst_ref + 1;
        }
    }
}
