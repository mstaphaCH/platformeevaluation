<?php

/**
 * Class Home
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class SessionController extends Controller
{
    private $plf_ref = null;
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/home/index (which is the default page btw)
     */
    
    public function index($request = null)
    {        
        /* session and user test */
        if (!isset($_SESSION["user"])) {
            header('location: ' . URL . 'Account');
            exit();
        }
        if ($this->profil === -1) {
            header('location: ' . URL . 'Account/Logout');
            exit();
        }
        // anonymous user or hacker
        if($this->profil!==0 && $this->profil!==1 && $this->profil!==3 && $this->profil!==4) {
            header('location: ' . URL);
            exit();
        }
        // redirect to home page if isn't admin
        // dark-background
        $header_class = '';
        // ' current'
        $current_class = '';
        // ' current-sub'
        $current_subclass = '';
        $_search_type = null ;
        $_search_date = null ;
        $this->model = new Evaluation($this->db);

        $active_evaluations = $this->model->getAllEvaluations();
        $inactive_evaluations = $this->model->getAllEvaluations();

        $this->model = new Session($this->db);  

        $request = $_POST;        
        if(isset($request['search3'])){

            if($request['structure']==='active') {       
                $active_sessions = $this->getAllActiveSessions($this->model->strutil_id, $request);
                $inactive_sessions = $this->getAllInactiveSessions($this->model->strutil_id);
            }elseif($request['user']==='inactive') {
                $active_sessions = $this->getAllActiveSessions($this->model->strutil_id);
                $inactiveUsers = $this->getInactiveUsers($request);
            }
        }else{
            $active_sessions = $this->getAllActiveSessions($this->model->strutil_id);
            $inactive_sessions = $this->getAllInactiveSessions($this->model->strutil_id);
        }

        $plf_reff = $this->SessionReference();
        $plf_ref = $this->getPlf_ref();
        $search_session = $this->Search_Session();

        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/_templates/navbar.php';
        require APP . 'view/session/index.php';
        require APP . 'view/_templates/footer.php';
    }

    public function Add($request) {
        /* Submit form */
        if (empty($request['plf_ref']) || 
            empty($request['plf_type']) || 
            empty($request['plf_title']) || 
            empty($request['session_end']) || 
            empty($request['session_start']) || 
            empty($request['plf_resp_lastname']) || 
            empty($request['plf_resp_firstname'])) { echo "fail"; exit(); }


        list($day,$mois,$year) = explode("/", $request['session_start']);
        $timestamp = mktime(0,0,0,$mois,$day,$year);
        $session_start = date("Y-m-d",$timestamp);

        list($day,$mois,$year) = explode("/", $request['session_end']);
        $timestamp = mktime(0,0,0,$mois,$day,$year);
        $session_end = date("Y-m-d",$timestamp);

        $plf_ref = $request['plf_ref'];
        $plf_type = $request['plf_type'];
        $plf_title = $request['plf_title'];
        $plf_resp_lastname = $request['plf_resp_lastname'];
        $plf_resp_firstname = $request['plf_resp_firstname'];


        $plf_year = date('Y');
        

        $strutil_id = $this->model->strutil_id;
        $id = $this->model->id;

        $this->model = new Session($this->db);
        $session = $this->model->getSessionByRef($plf_ref);
        
        if ($session != null) {
            echo "false";
        }else{
            $this->model->addSession($plf_ref, $plf_title, $session_start, $session_end, $plf_resp_firstname, $plf_resp_lastname, $plf_type, $plf_year, $id, $strutil_id);

            echo "true";
        }

        
    }

    public function Edit($request) {
        /* Session existing test */
        if (!isset($_SESSION["user"])) {
            header('location: ' . URL . 'Account');
            exit();
        }
        /* Check user profil */
        if ($this->profil===-1) {
            header('location: ' . URL . 'Account/Logout');
            exit();
        }
        if($this->profil !== 0 && $this->profil !== 1) {
            header('location: ' . URL);
            exit();
        }
        // redirect to home page if isn't admin
        // dark-background
        $header_class = '';
        // ' current'
        $current_class = '';
        // ' current-sub'
        $current_subclass = '';

        $this->model = new Session($this->db);
        if (isset($request)) {
            $targetID = $request;
            $session = $this->model->getSessionById($targetID);

            if ($session != null) {
                $plf_ref = $session->plf_ref;
                $plf_title = $session->plf_title;

                $plf_year = $session->plf_year;
                $str_id = $session->str_id;

                $plf_reff = $plf_year.'-'.$str_id;

                $session_start = strftime("%d/%m/%Y", strtotime($session->plf_start_date));
                $session_end = strftime("%d/%m/%Y", strtotime($session->plf_end_date));

                $plf_resp_firstname = $session->plf_resp_firstname;
                $plf_resp_lastname = $session->plf_resp_lastname;
                
                $plf_type = $session->plf_type;
            }
        }else{
            header('location: ' . URL . 'Session');
            exit();
        }
        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/_templates/navbar.php';
        require APP . 'view/session/edit.php';
        require APP . 'view/_templates/footer.php';
    }

    public function Update($request) {
        $this->model = new Session($this->db);
        /* Update form */
        if (empty($request['thisID']) || 
            empty($request['plf_ref']) || 
            empty($request['plf_type']) || 
            empty($request['plf_title']) || 
            empty($request['session_end']) || 
            empty($request['session_start']) || 
            empty($request['plf_resp_lastname']) || 
            empty($request['plf_resp_firstname'])) { echo "fail"; exit(); }


        list($day,$mois,$year) = explode("/", $request['session_start']);
        $timestamp = mktime(0,0,0,$mois,$day,$year);
        $session_start = date("Y-m-d",$timestamp);

        list($day,$mois,$year) = explode("/", $request['session_end']);
        $timestamp = mktime(0,0,0,$mois,$day,$year);
        $session_end = date("Y-m-d",$timestamp);

        $pid = (int) $request['thisID'];

        $plf_ref = $request['plf_ref'];
        $plf_type = $request['plf_type'];
        $plf_title = $request['plf_title'];
        $plf_resp_lastname = $request['plf_resp_lastname'];
        $plf_resp_firstname = $request['plf_resp_firstname'];

        $this->model->update($plf_ref, $plf_title, $session_start, $session_end, $plf_resp_firstname, $plf_resp_lastname, $plf_type, $pid);
        echo "success";
        header('location: ' . URL . 'Session');
    }

    public function Delete($request) {
        $id_to_delete = (int) $request['session_id'];
        $this->model = new Session($this->db);
        $session= $this->model->getSessionById($id_to_delete);
        
        if ($session == null) {
            header("location: message.php?fl=$file&msg=delete_not_exist");
            exit();
        }
        
        $this->model->deleteEvaluationReferences($id_to_delete);
        $this->model->deleteSession($id_to_delete);
        exit();
        echo "success";
    }

    public function Active($activeid) {
        $this->model = new Session($this->db);
        $targetID = $activeid;
        $session = $this->model->fetchInactiveSession($targetID);
        if(!empty($session)) {
            $this->model->activateSession($targetID);
        }
        header('location: ' . URL . 'Session');
    }

    public function Inactive($inactiveid) {
        $this->model = new Session($this->db);
        $targetID = $inactiveid;
        $session = $this->model->fetchActiveSession($targetID);
        if(!empty($session)) {
            $this->model->deactivateSession($targetID);
        }
        header('location: ' . URL . 'Session');
    }

    public function getAllActiveSessions($strutil_id = null, $request = null) {
        if ($this->profil === 0) {
            return $this->model->getAllActiveSessionsForAdmin($request);
        }else{
            return $this->model->getAllActiveSessionsForNotAdmin($strutil_id, $request);
        }
    }

    public function getAllInactiveSessions($strutil_id = null) {
        if ($this->profil === 0) {
            return $this->model->getAllInactiveSessionsForAdmin();
            $sql = mysqli_query($mysqli,"SELECT * FROM edb_planif WHERE plf_active=0 ORDER BY plf_id ASC") or die (mysqli_error($mysqli));
        }else{
            return $this->model->getAllInactiveSessionsForNotAdmin($strutil_id);
            $sql = mysqli_query($mysqli,"SELECT * FROM edb_planif WHERE str_id='$strutil_id' AND plf_active=0 ORDER BY plf_id ASC") or die (mysqli_error($mysqli));
        }
    }

    public function SessionReference() {
        $year = date('Y');
        $sessionReference = $this->model->getSessionReference($year, $this->model->strutil_id);
        if ($sessionReference !== null) {
            $this->plf_ref = $sessionReference[0]->plf_ref + 1;
            $str_id = $sessionReference[0]->str_id;
            $plf_year = $sessionReference[0]->plf_year;
            if ($plf_year!=date('Y')) {
                $plf_year = date('Y');
                $this->plf_ref = 1;
                $str_id = $this->model->strutil_id;
            }

            $plf_reff = $str_id.'-';
        }else{
            $plf_reff = $this->model->strutil_id.'-';
        }
        return $plf_reff;
    }

    public function checkSessionReference($request) {
        $plf_ref = $request['plf_ref'];
        $year = date('Y');

        $this->model = new Session($this->db);
        $strutil_id = $this->model->getUserStructure();
        $session = $this->model->getSessionByRefAndYearAndStructure($plf_ref, $year, $strutil_id);
        if ($session != null) {
            echo 'false';
        }else{
            echo 'true';
        }
    }

    public function getPlf_ref() {
        return $this->plf_ref;
    }

    public function evalPlanif($plf_id, $eval_id) {
        return $this->model->getEvalPlanif($plf_id, $eval_id);
    }
    public function getsessionStartdate($date){
        return $this->model->getSessionByDate($date);
    }
    public function getsessionBytype($type){
        return $this->model->getSessionByType($type);
    }
    public function Search_Session($request = null){
        if ($request!= null){
        $search_type = isset($request['type']) ? $request['type'] : '';
        $search_date = $request['search_date'];
		$search_debut_session = $request['search_debut_session'];
        $search_fin_session = $request['search_fin_session'];

           if(!empty($search_debut_session) && empty($search_type)&& empty($search_fin_session)){return $this->model->getSessionByDate($search_debut_session);}
    if(empty($search_debut_session) && !empty($search_type)&& empty($search_fin_session)){return $this->model->getSessionByType($search_type);}
        if(empty($search_debut_session) && empty($search_type)&& empty($search_fin_session)){return $this->model->getAllActiveSessions();}}
        if (empty($search_debut_session) && empty($search_type)&& !empty($search_fin_session)){return $this->model->getSessionByDateFin($search_fin_session);}
          }

    /* public function getsession ($request){
            $search_type = isset($request['search_type']) ? $request['search_type'] : '';
            $search_date = isset($request['search_date']) ? $request['search_date'] : '';
        if($request != null) {
            if(empty($search_type)&& empty($search_date){
                return $this->model->getAllActiveSessions();}
            
            if(empty($search_type) && !empty($search_date)){
                return $this->model->getSessionByDate($search_date);}
            
            if (!empty($search_type) $$ empty($search_date)){
                return $this->model->getSessionByType($search_type);}
        
        }
            else {return $this->model->getAllActiveSessionsForAdmin();}
            
     }*/
    
    public function eval_planif($request = null){

        if( !empty($request['plf_id']) ){
            $this->model_eval = new Evaluation($this->db);
            $active_evaluations = $this->model_eval->getAllEvaluations();

            $this->model = new Session($this->db);
            $plf_id = str_replace('en', '',$request['plf_id']);
            $eval_ids = explode("|",$request['checkbox']);
            foreach ($active_evaluations as $key => $active_evaluation) {
                if (in_array($active_evaluation->eval_id, $eval_ids)){

                    echo $active_evaluation->eval_id;
                    $verif_existance = $this->model->existe_Eval_Session($plf_id, $active_evaluation->eval_id) ;
                    echo $verif_existance;
                    if( !$verif_existance ){                       
                        $this->model->ajout_Eval_Session($plf_id, $active_evaluation->eval_id);
                    }

                }else{
                        if( $this->model->existe_Eval_Session($plf_id, $active_evaluation->eval_id) ){
                            echo "Ajout :" . $active_evaluation->eval_id;
                            $this->model->supp_Eval_Session($plf_id, $active_evaluation->eval_id);
                        }

                    }           

                    
                
            }

            echo "true";
        }else{
            echo "false";
        }
    }
        
    
}
