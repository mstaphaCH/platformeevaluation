<div class="row">
	<div class="pull-right"><a href="message_list.php#form">+ Ajouter un message</a></div>
	<div class="clear"><br><br></div>
	<div class="col-md-6 col-md-offset-3" style="padding:0px;border: 1px solid rgba(0, 0, 0, 0.34);">
		<h3 style="font-size:18px;text-align:center;color:#337ab7;background-color:gainsboro;line-height:3;">Modifier message</h3>
		<br><br>
		<form action="<?php echo URL . 'Message/Update'; ?>" method="post" class="form-horizontal message-form message-form-add">
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Référence <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input type="text" class="msg_ref" name="msg_ref" required value="<?php echo $message->msg_ref; ?>" disabled/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Libelle <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input type="text" class="msg_title" name="msg_title" value="<?php echo $message->msg_title; ?>" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Catégorie <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<select class="form-control msg_cat" data-style="btn-primary" name="msg_cat" required />
						<option value=""></option>
						<?php if ($messageType_titles != null) { ?>						
							<?php foreach ($messageType_titles as $key => $messageType_title) { ?>
								<?php if($this->getType($message->msgType_id)===$messageType_title->msgType_title) { ?>
									<option value="<?php echo $messageType_title->msgType_title; ?>" selected>
										<?php echo ucfirst($messageType_title->msgType_title); ?>
									</option>
								<?php }else{ ?>
									<option value="<?php echo $messageType_title->msgType_title; ?>">
										<?php echo ucfirst($messageType_title->msgType_title); ?>
									</option>
								<?php } ?>
							<?php } ?>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Message <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<textarea class="msg_content" name="msg_content" rows="4" required><?php echo $message->msg_content; ?></textarea>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<input class="thisID" name="thisID" type="hidden" value="<?php echo $message->msg_id; ?>" />
					<button class="button solid-button pull-right message-button-edit" type="button">Modifier</button>
				</div>
			</div>
			<div class="contact-loading alert alert-info form-alert">
				<span class="message">Chargement...</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
			<div class="contact-success alert alert-success form-alert">
				<span class="message">Succès!</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
			<div class="contact-error alert alert-danger form-alert">
				<span class="message">Erreur!</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
		</form>
	</div>
</div>