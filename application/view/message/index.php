
<div class="row">
	<h3 style="font-size:18px;text-align:center;background-color:gainsboro;padding-top:20px;margin:auto;color:#337ab7;">Gestion des messages<hr style="margin-top:10px;margin-bottom:10px;" /></h3>
	<br>
	<div style="float:right;"><a href="message_list.php#form">+ Ajouter un message</a></div>
</div>
<br>
<div class="row">
	<div class="horizontal">
		<h2 style="text-align:left;font-size:18px;margin-bottom:25px;">Messages actifs</h2>
		<form action="" method="post" class="form-horizontal" style="width:1170px; margin: 30px auto 10px auto;font-size:13px;">
		<div class="form-group row">
			<label class="col-md-1 col-sm-1 col-xs-1 control-label" style="padding-top:0px;text-align:right;width:6%;" for="categorie">Catégorie</label>
			<div class="col-md-2 col-sm-2 col-xs-2">
				<select name="categorie" id="categorie">
					<option value="">Tout</option>
					<?php foreach ($message_type_titles as $key => $message_type_title) { ?>
						<?php if(isset($categorie) && $categorie === $message_type_title->msgType_title) { ?>
							<option value="<?php echo $message_type_title->msgType_title; ?>" selected>
								<?php echo ucfirst($message_type_title->msgType_title); ?>
							</option>
						<?php }else{ ?>
							<option value="<?php echo $message_type_title->msgType_title; ?>">
								<?php echo ucfirst($message_type_title->msgType_title); ?>
							</option>
						<?php } ?>
					<?php } ?>
				</select>
			</div>
			<div class="col-md-1 col-sm-1 col-xs-1">
				<input class="btn btn-default search-form" type="submit" name="search1" id="search" value="Rechercher"/>
			</div>
		</div>
	</form>
		<table class="table table-striped table-bordered table-condensed table-hide-1">
			<thead>
				<tr style="width:100%;">
					<th style="color:#31b0d5; text-align:center;min-width:30px;"><strong><a style='padding:0px;' onmousedown='return false;' class='btn btn-plus' role='button'><i class="ion-minus-round" style="margin:0% !important;color:#31b0d5;"></i></a></strong></th>
					<th style="color:#31b0d5; text-align:center;width:5%;" class='thn2'><strong>Référence</strong></th>
					<th style="color:#31b0d5; text-align:center;width:20%;" class='thn3'><strong>Libelle</strong></th>
					<th style="color:#31b0d5; text-align:center;width:40%;" class='thn4'><strong>Message</strong></th>
					<th style="color:#31b0d5; text-align:center;width:10%;" class='thn5'><strong>Catégorie</strong></th>
					<th style="color:#31b0d5; text-align:center;width:20%;" class='thn5'><strong>Date de création</strong></th>
					<th style="color:#31b0d5; text-align:center;width:5%;" class='thn5'><strong>Opération</strong></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($active_messages as $key => $active_message) { ?>
				<tr style='width:100%;'><td class='first-td'></td>
					<td style='width:5%;'><?php echo $active_message->msg_ref; ?></td>
					<td style='width:20%;'><?php echo ucfirst($active_message->msg_title); ?></td>
					<td style='width:30%;'><?php echo ucfirst($active_message->msg_content); ?></td>
					<td style='width:10%;'><?php echo $this->getMessageCategory($active_message->msgType_id); ?></td>
					<td style='width:20%;text-align:center;'><?php echo strftime("%d %b %Y", strtotime($active_message->msg_creation_date)); ?></td>
					<td class='button-operation-3'><a class='btn' role='button' href="<?php echo URL . 'Message/Edit/' . $active_message->msg_id; ?>"><i class='ion-compose' style='color:green;'></i></a><a class='btn' role='button' href="<?php echo URL . 'Message/Inactive/' . $active_message->msg_id; ?>"><i class='ion-log-out' style='color:blue;'></i></a><a class='btn message delete' id="<?php echo $active_message->msg_id; ?>" role="button"><i class='ion-close-circled' style='color:red;'></i></a></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>
<br>
<div class="row">
	<div class="horizontal">
		<h2 style="text-align:left;font-size:18px;margin-bottom:25px;">Messages inactifs</h2>
		<form action="" method="post" class="form-horizontal" style="width:1170px; margin: 30px auto 10px auto;font-size:13px;">
		<div class="form-group row">
			<label class="col-md-1 col-sm-1 col-xs-1 control-label" style="padding-top:0px;text-align:right;width:6%;" for="categorie">Catégorie</label>
			<div class="col-md-2 col-sm-2 col-xs-2">
				<select name="categorie" id="categorie">
					<option value="">Tout</option>
					<?php foreach ($message_type_titles as $key => $message_type_title) { ?>
						<?php if(isset($categorie) && $categorie === $message_type_title->msgType_title) { ?>
							<option value="<?php echo $message_type_title->msgType_title; ?>" selected>
								<?php echo ucfirst($message_type_title->msgType_title); ?>
							</option>
						<?php }else{ ?>
							<option value="<?php echo $message_type_title->msgType_title; ?>">
								<?php echo ucfirst($message_type_title->msgType_title); ?>
							</option>
						<?php } ?>
					<?php } ?>
				</select>
			</div>
			<div class="col-md-1 col-sm-1 col-xs-1">
				<input class="btn btn-default search-form" type="submit" name="search2" id="search" value="Rechercher"/>
			</div>
		</div>
	</form>
		<table class="table table-striped table-bordered table-condensed table-hide-2">
			<thead>
				<tr style="width:100%;">
					<th style="color:#31b0d5; text-align:center;min-width:30px;"><strong><a style='padding:0px;' onmousedown='return false;' class='btn btn-plus' role='button'><i class="ion-minus-round" style="margin:0% !important;color:#31b0d5;"></i></a></strong></th>
					<th style="color:#31b0d5; text-align:center;width:5%;" class='thn2'><strong>Référence</strong></th>
					<th style="color:#31b0d5; text-align:center;width:20%;" class='thn3'><strong>Libelle</strong></th>
					<th style="color:#31b0d5; text-align:center;width:40%;" class='thn4'><strong>Message</strong></th>
					<th style="color:#31b0d5; text-align:center;width:10%;" class='thn5'><strong>Catégorie</strong></th>
					<th style="color:#31b0d5; text-align:center;width:20%;" class='thn5'><strong>Date de création</strong></th>
					<th style="color:#31b0d5; text-align:center;width:5%;" class='thn5'><strong>Opération</strong></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($inactive_messages as $key => $inactive_message) { ?>
				<tr style='width:100%;'><td class='first-td'></td>
					<td style='width:5%;'><?php echo $inactive_message->msg_ref; ?></td>
					<td style='width:20%;'><?php echo ucfirst($inactive_message->msg_title); ?></td>
					<td style='width:30%;'><?php echo ucfirst($inactive_message->msg_content); ?></td>
					<td style='width:10%;'><?php echo $this->getMessageCategory($inactive_message->msgType_id); ?></td>
					<td style='width:20%;text-align:center;'><?php echo strftime("%d %b %Y", strtotime($inactive_message->msg_creation_date)); ?></td>
					<td class='button-operation-3'><a class='btn' role='button' href="<?php echo URL . 'Message/Edit/' . $inactive_message->msg_id; ?>"><i class='ion-compose' style='color:green;'></i></a><a class='btn' role='button' href="<?php echo URL . 'Message/Active/' . $inactive_message->msg_id; ?>"><i class='ion-log-in' style='color:blue;'></i></a><a class='btn message delete' id="<?php echo $inactive_message->msg_id; ?>" role="button"><i class='ion-close-circled' style='color:red;'></i></a></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>
<div class="row" style="margin:100px 0px;">
	<div class="col-md-6 col-md-offset-3" style="padding:0px;border: 1px solid rgba(0, 0, 0, 0.34);">
		<h3 style="font-size:18px;text-align:center;color:#337ab7;background-color:gainsboro;line-height:3;">Ajouter un message</h3>
		<br><br>
		<form action="<?php echo URL . 'Message/add'; ?>" method="post" class="form-horizontal message-form message-form-add">
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Référence <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input type="text" class="msg_ref" name="msg_ref" required value="<?php echo $msg_ref; ?>"/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Libelle <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input type="text" class="msg_title" name="msg_title" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Catégorie <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<select class="form-control msg_cat" data-style="btn-primary" name="msg_cat" required>
						<option value=""></option>
						<?php foreach ($message_categories as $key => $message_categorie) { ?>
							<option value="<?php echo $message_categorie->msgType_title; ?>">
								<?php echo ucfirst($message_categorie->msgType_title); ?>
							</option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Message <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<textarea class="msg_content" name="msg_content" rows="4" required></textarea>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-12 col-sm-12 col-xs-12">
						<input class="btn btn-default evaluation-button-add" type="submit" name="submit" value="Ajouter">
				</div>
			</div>
			<div class="contact-loading alert alert-info form-alert">
				<span class="message">Chargement...</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
			<div class="contact-success alert alert-success form-alert">
				<span class="message">Succès!</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
			<div class="contact-error alert alert-danger form-alert">
				<span class="message">Erreur!</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
		</form>
	</div>
</div>
