</div>
</div>
</div>
<div class="clear"></div>
</div>
</div>
</div>
<div class="scrolltotop"><i class="fa fa-chevron-up"></i></div>
<!-- confirmation popup -->
<div id="dialogoverlay"></div>
<div id="dialogbox">
  <div>
    <div id="dialogboxhead"></div>
    <div id="dialogboxbody"></div>
    <div id="dialogboxfoot"></div>
  </div>
</div>

<!-- variables -->
<script type="text/javascript">
    var URL = '<?php echo URL; ?>';
</script>
<!-- jQuery Easing -->
<!-- SmoothScroll -->
<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="<?php echo URL; ?>public/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo URL; ?>public/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
<script src="<?php echo URL; ?>public/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php echo URL; ?>public/plugins/jquery-mousewheel/jquery.mousewheel.min.js"></script>
<script src="<?php echo URL; ?>public/plugins/smooth-scroll/smooth-scroll.min.js"></script>

<script src="<?php echo URL; ?>public/plugins/jquery.easing/js/jquery.easing.min.js"></script>
<script src="<?php echo URL; ?>public/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="<?php echo URL; ?>public/plugins/jquery-validation/dist/additional-methods.min.js"></script>
<script src="<?php echo URL; ?>public/plugins/jquery-backstretch/jquery.backstretch.min.js"></script>
<script src="<?php echo URL; ?>public/js/jquery.plugin.min.js"></script> 
<script src="<?php echo URL; ?>public/plugins/jquery.countdown/dist/jquery.countdown.min.js"></script>

<!-- <script src="template/js/smoothscroll.js"></script> -->
<script src="<?php echo URL; ?>public/plugins/intl-tel-input/build/js/intlTelInput.min.js"></script>
<script src="<?php echo URL; ?>public/plugins/intl-tel-input/build/js/utils.js"></script>
<!-- typeahead -->
<script src="<?php echo URL; ?>public/plugins/bootstrap3-typeahead/bootstrap3-typeahead.min.js"></script>
<!-- date -->
<script src="<?php echo URL; ?>public/plugins/moment/min/moment.min.js"></script>
<!-- <script src="template/timepicker/js/bootstrap-datetimepicker.min.js"></script> -->
<script src="<?php echo URL; ?>public/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo URL; ?>public/plugins/bootstrap-datepicker/dist/locales/bootstrap-datepicker.fr.min.js"></script>


<script src="<?php echo URL; ?>public/plugins/toastr/toastr.min.js"></script>
<script src="<?php echo URL; ?>public/plugins/js-cookie/src/js.cookie.js"></script>
<script src="<?php echo URL; ?>public/js/main.js"></script>
<script src="<?php echo URL; ?>public/js/dialog.js"></script>

<script src="<?php echo URL; ?>public/plugins/blockUI/jquery.blockUI.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/3.10.1/lodash.js"></script>

<script src="<?php echo URL; ?>public/plugins/label_better-master/jquery.label_better.min.js"></script>

<script src="<?php echo URL; ?>public/js/date.js"></script>

<script src="<?php echo URL; ?>public/js/<?php echo strtolower(get_class($this->model)).'/'.strtolower(debug_backtrace()[1]['function']); ?>.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.delete').click(function(){
			$('.pop-outer').fadeIn('slow');
		});
		$('.close').click(function(){
			$('.pop-outer').fadeOut('slow');
		});
	});
</script>
<!-- <script src="<?php //echo URL; ?>public/js/test.js"></script> -->
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</body>
</html>
