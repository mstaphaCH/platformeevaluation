<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="" type="image/x-icon" />
	<title>Project manager profiler</title>
	<meta name="Description" content="Excellium Consuting, conseil en management de projets, mise en place de centres de services et formation." />
	<meta name="Keywords" content="excellium,consuling,management,formation,projet,service,formation,organisation,collaboration" />
	<!-- <base href="http://localhost:1337/excellium_consulting/Env-Recette/" /> -->
	<!-- <base href="http://www.excellium-consulting.com/Env-Recette/" /> -->
	<!-- Bootstrap -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<!-- <link rel="stylesheet" type="text/css" href="<?php //echo URL; ?>public/css/bootstrap.min.css"> -->
	<!-- Font Awesome -->
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo URL; ?>public/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" />

	<link rel="stylesheet" type="text/css" href="<?php echo URL; ?>public/plugins/Ionicons/css/ionicons.min.css">
	<!-- timer -->
	<!-- <link rel="stylesheet" href="public/plugins/jquery.countdown/dist/jquery.countdown.min.js"> -->
	<!-- Style.css -->
	<link rel="stylesheet" type="text/css" href="<?php echo URL; ?>public/css/style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL; ?>public/css/dialog.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL; ?>public/css/jquery-ui-custom.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo URL; ?>public/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css">
	<!-- country phone style sheet -->
	<link rel="stylesheet" type="text/css" href="<?php echo URL; ?>public/plugins/intl-tel-input/build/css/intlTelInput.css">
	<!-- <link rel="stylesheet" href="template/timepicker/css/bootstrap-datetimepicker.css"> -->
	<link rel="stylesheet" type="text/css" href="<?php echo URL; ?>public/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css" />
	<!-- <link rel="stylesheet" href="template/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/> -->
	<link rel="stylesheet" type="text/css" href="<?php echo URL; ?>public/plugins/bootstrap-select/dist/css/bootstrap-select.min.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo URL; ?>public/plugins/toastr/toastr.min.css" />
</head>
<body ondragstart="return false" draggable="false" ondragenter="event.dataTransfer.dropEffect='none'; event.stopPropagation(); event.preventDefault();" ondragover="event.dataTransfer.dropEffect='none';event.stopPropagation(); event.preventDefault();" ondrop="event.dataTransfer.dropEffect='none';event.stopPropagation(); event.preventDefault();" data-twttr-rendered="true" data-spy="scroll" data-target="#my-nav">
	<!--div id="load_screen"><div id="loading">Chargement ...</div></div-->
	<div class="container-fluid <?php echo $header_class; ?>">
