<header>
	<nav class="navbar navbar-inverse">
  <div class="container-fluid">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="<?php echo URL .'Home'; ?>">Insight</a>
	    </div>
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right">
				<?php if(($this->profil == 0 || $this->profil == 1 ) && isset($_SESSION['user'])) : ?>
				<li class="dropdown<?php echo $current_class; ?>">

					<a class="dropdown-toggle external" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
						Administration <span class="caret"></span>
					</a>
					
					<ul class="dropdown-menu">
						<?php if($this->profil==0 || $this->profil==1) : ?>
						<li <?php echo $current_subclass; ?>>
							<a class="external" href="<?php echo URL . 'Administration'; ?>">
								<i class="ion-person margin-right-icon"></i>Gestion des utilisateurs
							</a>
						</li>
						<?php endif; ?>
						<li <?php echo $current_subclass; ?>>
							<a class="external" href="<?php echo URL . 'Candidate'; ?>">
								<i class="ion-ios-people margin-right-icon"></i>Gestion des candidats
							</a>
						</li>
						<?php if ($this->profil == 0) : ?>
						<li <?php echo $current_subclass; ?>>
							<a class="external" href="<?php echo URL . 'Structure'; ?>">
								<i class="ion-home margin-right-icon"></i>Gestion des structures
							</a>
						</li>
						<li <?php echo $current_subclass; ?>>
							<a class="external" href="<?php echo URL . 'Theme'; ?>">
								<i class="ion-pie-graph margin-right-icon"></i>Gestion des thèmes
							</a>
						</li>
						<li <?php echo $current_subclass; ?>>
							<a class="external" href="<?php echo URL . 'CompetenceQuestion'; ?>">
								<i class="ion-clipboard margin-right-icon"></i>Gestion des questions compétence
							</a>
						</li>
						<li <?php echo $current_subclass; ?>>
							<a class="external" href="<?php echo URL . 'AptitudeQuestion'; ?>">
								<i class="ion-clipboard margin-right-icon"></i>Gestion des questions aptitude
							</a>
						</li>
						<?php endif; ?>
						<li <?php echo $current_subclass; ?>>
							<a class="external" href="<?php echo URL . 'Evaluation'; ?>">
								<i class="ion-compose margin-right-icon"></i>Gestion des évaluations
							</a>
						</li>
						<li <?php echo $current_subclass; ?>>
							<a class="external" href="<?php echo URL . 'Session'; ?>">
								<i class="ion-ios-clock margin-right-icon"></i>Planification des Sessions
							</a>
						</li>
						<li <?php echo $current_subclass; ?>>
							<a class="external" href="<?php echo URL . 'Message'; ?>">
								<i class="ion-email margin-right-icon"></i>Gestion des messages
							</a>
						</li>
					</ul>
				</li>
				<?php elseif($this->profil == 2) : ?>
				<li <?php echo $current_class; ?>>
					<a class="external" href="<?php echo URL . 'Profile'; ?>">Votre profil</a>
				</li>
				<?php endif; ?>

				<?php if (isset($_SESSION['user'])) : ?>
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
						Aptitude<span class="caret"></span>
					</a>
					<ul class="dropdown-menu" id="evaluation">
						<li class="dropdown-header dropdown-type">Général</li>
						<?php foreach ($this->general_aptitude_dropdown_list as $key => $value) { ?>
							<?php //if ($file==$value->eval_nom) { ?>
								<!--<li class="current-sub">
									<a class="external" href="<?php //echo URL . 'Evaluation/' . $value->eval_id; ?>">
										<?php //echo ucfirst($value->eval_nom); ?>
									<!--</a>
								</li-->
							<?php //}else{ ?>
								<li>
									<a class="external" href="<?php echo URL . 'Evaluation/Probation/' . $value->eval_id; ?>">
										<?php echo ucfirst($value->eval_nom); ?>
									</a>
								</li>
							<?php //} ?>
						<?php } ?>
						<li class="dropdown-header dropdown-type">Par thème</li>
						<?php foreach ($this->theme_aptitude_dropdown_list as $key => $value) { ?>
							<?php //if ($file==$value->eval_nom) { ?>
								<!--<li class="current-sub">
									<a class="external" href="<?php //echo URL . 'Evaluation/' . $value->eval_id; ?>">
										<?php //echo ucfirst($value->eval_nom); ?>
									<!--</a>
								</li-->
							<?php //}else{ ?>
								<li>
									<a class="external" href="<?php echo URL . 'Evaluation/Probation/' . $value->eval_id; ?>">
										<?php echo ucfirst($value->eval_nom); ?>
									</a>
								</li>
							<?php //} ?>
						<?php } ?>
					</ul>
				</li>
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
						Compétences management<span class="caret"></span>
					</a>
					<ul class="dropdown-menu" id="evaluation">
						<li class="dropdown-header dropdown-type">Général</li>
						<?php foreach ($this->generalCompetenceManagementDropdownList as $key => $value) { ?>
							<?php //if ($file==$value->eval_nom) { ?>
								<!--<li class="current-sub">
									<a class="external" href="<?php //echo URL . 'Evaluation/' . $value->eval_id; ?>">
										<?php //echo ucfirst($value->eval_nom); ?>
									<!--</a>
								</li-->
							<?php //}else{ ?>
								<li>
									<a class="external" href="<?php echo URL . 'Evaluation/Probation/' . $value->eval_id; ?>">
										<?php echo ucfirst($value->eval_nom); ?>
									</a>
								</li>
							<?php //} ?>
						<?php } ?>
						<li class="dropdown-header dropdown-type">Par thème</li>
						<?php foreach ($this->themeCompetenceManagementDropdownList as $key => $value) { ?>
							<?php //if ($file==$value->eval_nom) { ?>
								<!--<li class="current-sub">
									<a class="external" href="<?php //echo URL . 'Evaluation/' . $value->eval_id; ?>">
										<?php //echo ucfirst($value->eval_nom); ?>
									<!--</a>
								</li-->
							<?php //}else{ ?>
								<li>
									<a class="external" href="<?php echo URL . 'Evaluation/Probation/' . $value->eval_id; ?>">
										<?php echo ucfirst($value->eval_nom); ?>
									</a>
								</li>
							<?php //} ?>
						<?php } ?>
					</ul>
				</li>
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
						Compétences techniques<span class="caret"></span>
					</a>
					<ul class="dropdown-menu" id="evaluation">
						<li class="dropdown-header dropdown-type">Général</li>
						<?php foreach ($this->generalCompetenceTechniqueDropdownList as $key => $value) { ?>
							<?php //if ($file==$value->eval_nom) { ?>
								<!--<li class="current-sub">
									<a class="external" href="<?php //echo URL . 'Evaluation/' . $value->eval_id; ?>">
										<?php //echo ucfirst($value->eval_nom); ?>
									<!--</a>
								</li-->
							<?php //}else{ ?>
								<li>
									<a class="external" href="<?php echo URL . 'Evaluation/Probation/' . $value->eval_id; ?>">
										<?php echo ucfirst($value->eval_nom); ?>
									</a>
								</li>
							<?php //} ?>
						<?php } ?>
						<li class="dropdown-header dropdown-type">Par thème</li>
						<?php foreach ($this->themeCompetenceTechniqueDropdownList as $key => $value) { ?>
							<?php //if ($file == $value->eval_nom) { ?>
								<!--<li class="current-sub">
									<a class="external" href="<?php //echo URL . 'Evaluation/' . $value->eval_id; ?>">
										<?php //echo ucfirst($value->eval_nom); ?>
									<!--</a>
								</li-->
							<?php //}else{ ?>
								<li>
									<a class="external" href="<?php echo URL . 'Evaluation/Probation/' . $value->eval_id; ?>">
										<?php echo ucfirst($value->eval_nom); ?>
									</a>
								</li>
							<?php //} ?>
						<?php } ?>
					</ul>
				</li>
				<?php endif; ?>
				<?php if (isset($_SESSION["user"])) : ?>
				<li <?php echo $current_class; ?>>
					<a class="external" href="<?php echo URL . 'Report'; ?>">Rapports</a>
				</li>
				<?php endif; ?>
				<li <?php echo $current_class; ?>>
					<a class="external" href="<?php echo URL . 'About'; ?>">À propos</a>
				</li>
				<?php if (isset($_SESSION["user"]) && ($this->profil==0 || $this->profil==1 || $this->profil==2)) : ?>
				<li <?php echo $current_class; ?>>
					<a class="external" href="<?php echo URL . 'Account/Logout'; ?>"><i class="fa fa-sign-out fa-lg"></i></a>
				</li>
				<?php elseif (!isset($_SESSION["user"])) : ?>
				<li <?php echo $current_class;?> >
					<a class="external" href="<?php echo URL . 'Account'; ?>">Connexion</a>
				</li>
				<?php endif; ?>
			</ul>
		</div>
		<div class="clear"></div>
	</div>
</nav>
</header>

<div class="container">
<?php //if($file !== "evaluation.php"): ?>
	<div class="row header">
		<div class="col-md-4 col-sm-4 col-xs-4 header-page-fluid">
			<a href="<?php echo URL . 'Home'; ?>"></a>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-4"></div>
		<div class="col-md-4 col-sm-4 col-xs-4 welcome">
		</div>
	</div>
<?php //endif; ?>
