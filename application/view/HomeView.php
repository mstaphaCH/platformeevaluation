<?php

/**
* 
*/
class HomeView
{
	private $model;
	private $controller;

	function __construct($controller, $model) {
		$this->model = $model;
		$this->controller = $controller;
	}

	public function output() {
		$data = "Home page";
		require_once('Home.php');
	}
}
