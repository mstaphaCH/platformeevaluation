<div class="row" style="margin-bottom:5%;">
	<h3 style="text-align:center;width:30%;margin:auto;color:#337ab7;"><span>Modifier un thème<hr/></span></h3>
	<div align="right" style="margin-right:100px;"><a href="theme_list.php#form">+ Ajouter un thème</a></div>
	<div class="col-md-2 col-sm-2 col-xs-2"></div>
	<div class="col-md-8 col-sm-8 col-xs-8" style="min-height:350px;">
		<form action="<?php echo URL . 'Theme/Update'; ?>" method="post" class="form-horizontal theme-form theme-form-edit">
			<div class="form-group">
				<label class="col-md-3 col-sm-3 col-xs-3 control-label">Nom du thème <span style="color:#f55;">*</span></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input type="text" class="theme_title" name="theme_title" id="theme_title" value="<?php echo $theme_title; ?>" autocomplete="off" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-sm-3 col-xs-3 control-label">Description</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<textarea class="theme_description" name="theme_description" id="theme_description"><?php echo $theme_description; ?></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-sm-3 col-xs-3 control-label">Pondération</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input type="text" class="theme_weighting" name="theme_weighting" id="theme_weighting" value="<?php echo $theme_weighting; ?>" autocomplete="off"/>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-12 col-sm-12 col-xs-12">				
					<input name="thisID" type="hidden" value="<?php echo $targetID; ?>" />
					<input class="btn btn-default theme-button-edit" type="submit" name="submit" value="Modifier">
				</div>
			</div>
			<div class="contact-loading alert alert-info form-alert">
				<span class="message">Chargement...</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
			<div class="contact-success alert alert-success form-alert">
				<span class="message">Succès!</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
			<div class="contact-error alert alert-danger form-alert">
				<span class="message">Erreur!</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
		</form>
	</div>
	<div class="col-md-2 col-sm-2 col-xs-2"></div>
</div>