<div class="row">
	<h3 style="text-align:center;width:30%;margin:auto;color:#337ab7;"><span>Gérer un thème<hr/></span></h3>
	<div align="right" style="margin-right:32px;"><a href="<?php echo URL . 'Theme#form'; ?>theme_list.php#form" style="color:#337ab7">+ Ajouter un thème</a></div>
	<br/>
	<div class="horizontal">
		<table class="table table-striped table-bordered table-hover table-condensed">
			<thead>
				<tr>
					<th style="color:#31b0d5; text-align:center;min-width:30px;" class='thn1'><strong style=""><a style='padding:0px;' onmousedown='return false;' class='btn btn-plus' role='button'><i class="ion-minus-round" style="margin:0% !important;color:#31b0d5;"></i></a></strong></th>
					<th style="color:#31b0d5; text-align:center;min-width:15%;"><strong>Thème</strong></th>
					<th style="color:#31b0d5; text-align:center;width:100%;"><strong>Description</strong></th>
					<th style="color:#31b0d5; text-align:center;width:100%;"><strong>Pondération</strong></th>
					<th style="color:#31b0d5; text-align:center;width:100%;"><strong>Nbr Questions</strong></th>
					<th style="color:#31b0d5; text-align:center;width:100%;"><strong>Opération</strong></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($themes as $key => $theme) { ?>
				<tr style='width:100%;'>
					<td class='first-td'></td>
					<td><?php echo ucfirst($theme->thm_titre); ?></td>
					<td><?php echo ucfirst($theme->thm_description); ?></td>
					<td style='text-align:center;padding-left:0% !important;'><?php echo $theme->thm_ponderation; ?></td>
					<td><?php echo $this->getQuestionNumbers($theme->thm_id)->count; ?></td>
					<td class='user-listb'><a class='btn' role='button' href="<?php echo URL . 'Theme/Edit/' . $theme->thm_id; ?>"><i class='ion-compose' style='color:green;'></i></a><a class='btn theme delete' id="<?php echo $theme->thm_id; ?>" role="button"><i class='ion-close-circled' style='color:red;'></i></a></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>
<div class="row" style="margin-bottom:150px;">
	<hr id="form"/>
	<div class="col-md-3 col-sm-3 col-xs-3"></div>
	<div class="col-md-6 col-sm-6 col-xs-6">
		<h3 style="padding-bottom:0%;margin-bottom:0%;text-align:center;color:#337ab7;">Ajouter un thème</h3>
		<hr/>
		<br/><br/>
		<form action="<?php echo URL .'Theme/Add' ?>" method="post" class="form-horizontal theme-form theme-form-list">
			<div class="form-group">
				<label class="col-md-3 col-sm-3 col-xs-3 control-label">Nom du thème <span style="color:#f55;">*</span></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input type="text" class="theme_title" name="theme_title" id="theme_title" autocomplete="off" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-sm-3 col-xs-3 control-label">Description</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<textarea class="theme_description" name="theme_description" id="theme_description"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-sm-3 col-xs-3 control-label">Pondération</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input type="text" class="theme_weighting" name="theme_weighting" id="theme_weighting" autocomplete="off"/>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<input class="btn btn-default theme-button-add" type="submit" name="submit" value="Ajouter">
				</div>
			</div>
			<div class="contact-loading alert alert-info form-alert">
				<span class="message">Chargement...</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
			<div class="contact-success alert alert-success form-alert">
				<span class="message">Succès!</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
			<div class="contact-error alert alert-danger form-alert">
				<span class="message">Erreur!</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
		</form>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-3"></div>
</div>
</div>