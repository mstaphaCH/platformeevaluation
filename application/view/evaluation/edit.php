<div class="row" style="margin-bottom:150px;">
	<div class="col-md-3 col-sm-3 col-xs-3"></div>
	<div class="col-md-6 col-sm-6 col-xs-6">
		<h3 style="padding-bottom:0%;margin-bottom:0%;text-align:center;color:#337ab7;">Modifier une évaluation</h3>
		<hr/>
		<br/><br/>
		<form action="<?php echo URL . 'Evaluation/Update'; ?>" method="post" class="form-horizontal evaluation-form evaluation-form-edit">
			<div class="form-group">
				<label class="col-md-4 col-sm-4 col-xs-4 control-label">Titre de l'évaluation <span style="color:#f55;">*</span></label>
				<div class="col-md-8 col-sm-8 col-xs-8">
					<input type="text" name="eval_titre" id="eval_titre" value="<?php echo $eval_nom; ?>" autocomplete="off" disabled/>
				</div>
			</div>
			<div class="form-group">	
				<label class="col-md-4 col-sm-4 col-xs-4 control-label">Catégorie évaluation <span style="color:#f55;">*</span></label>
				<div class="col-md-8 col-sm-8 col-xs-8">
					<select class="eval_cat" name="eval_cat" id="eval_cat" required>
						<option value="compétenceManagement"<?php if(isset($cat) && $cat==='compétence management') echo 'selected'; ?>>compétence management</option>
									<option value="aptitude"<?php if(isset($cat) && $cat==='aptitude') echo 'selected'; ?>>Aptitude</option>
									<option value="compétencetechnique"<?php if(isset($cat) && $cat==='compétence technique') echo 'selected'; ?>>compétence technique</option>
					</select>
				</div>
			</div>
			<div class="form-group">	
				<label class="col-md-4 col-sm-4 col-xs-4 control-label">Type <span style="color:#f55;">*</span></label>
				<div class="col-md-8 col-sm-8 col-xs-8">
					<select class="eval_tpe" name="eval_tpe" id="eval_tpe" required>
						<option value="général" <?php if($eval_type==='général') echo 'selected'; ?>>Evaluation générale</option>
						<option value="thème" <?php if($eval_type==='thème') echo 'selected'; ?>>Evaluation par thème</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-4 col-sm-4 col-xs-4 control-label">Durée de l'évaluation</label>
				<div class="col-md-2 col-sm-2 col-xs-2" style="text-align:center;">
					<input type="text" name="eval_tmp_h" id="eval_tmp_h" style="text-align:center;" value="<?php echo $eval_duration_h; ?>" autocomplete="off"/>
				</div>
				<div class="col-md-1 col-sm-1 col-xs-1">
					hr(s)
				</div>
				<div class="col-md-2 col-sm-2 col-xs-2">
					<input type="text" name="eval_tmp_m" id="eval_tmp_m" style="text-align:center;" value="<?php echo $eval_duration_m; ?>" autocomplete="off"/>
				</div>
				<div class="col-md-1 col-sm-1 col-xs-1">
					min(s)
				</div>
				<div class="col-md-2 col-sm-2 col-xs-2"></div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Evaluation démonstration</label>
				<div class="col-md-9">
					<div class="make-switch" data-on-label="libres" data-off-label="non_libres">
						<input type="checkbox" <?php echo ($eval_access == 'libres' ? 'checked' : ''); ?> name="eval_access"/>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-4 col-sm-4 col-xs-4 control-label">Valeur seuil évaluation</label>
				<div class="col-md-8 col-sm-8 col-xs-8">
					<input type="text" name="eval_seuil" id="eval_seuil" style="text-align:center;" value="<?php echo $eval_seuil; ?>" autocomplete="off"/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-4 col-sm-4 col-xs-4 control-label">Description de l'évaluation</label>
				<div class="col-md-8 col-sm-8 col-xs-8">
					<textarea type="text" name="eval_desc" id="eval_desc" rows="4"><?php echo $eval_description; ?></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-4 col-sm-4 col-xs-4 control-label">Principe de réponse à l'évaluation</label>
				<div class="col-md-8 col-sm-8 col-xs-8">
					<textarea type="text" name="eval_princ" id="eval_princ" rows="4"><?php echo $eval_princ; ?></textarea>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<input name="thisID" type="hidden" value="<?php echo $targetID; ?>" />
					<input class="btn btn-default evaluation-button-edit" type="submit" name="submit" value="Modifier">
				</div>
			</div>
			<div class="contact-loading alert alert-info form-alert">
				<span class="message">Chargement...</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
			<div class="contact-success alert alert-success form-alert">
				<span class="message">Succès!</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
			<div class="contact-error alert alert-danger form-alert">
				<span class="message">Erreur!</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
		</form>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-3"></div>
</div>
</div>