<div class="row">
	<h3 style="font-size:20px;text-align:center;width:30%;margin:auto;color:#337ab7;"><span>Gestion des évaluations<hr/></span></h3>
	<br/>
	<?php if($this->profil===0): ?>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12" style="overflow:auto;">
				<div align="center" style="overflow:auto;" class="row">
					<form action="" method="post" class="form-horizontal" style="width:1170px; margin: 30px auto 10px auto;font-size:13px;">
						<div style="float:right;"><a href="evaluation_list.php#form">+ Ajouter une evaluation</a></div>
						<div class="form-group row" style="margin-left:0px !important;">
							<label class="col-md-2 col-sm-2 col-xs-2 control-label" style="padding-top:0px;text-align:right;" for="search_categories">Catégorie évaluation</label>
							<div class="col-md-2 col-sm-2 col-xs-2">
								<select name="search_categories" id="search_categories">
									<option value="">Tout</option>
					<?php foreach ($evaluationcategories as $key => $value) { ?>
						<?php if (isset($search_categories) && $search_categories===$value->eval_cat) { ?>
							<option value="<?php echo $value->eval_cat; ?>" selected><?php echo ucfirst($value); ?></option>
						<?php }else{ ?>
							<option value="<?php echo $value->eval_cat; ?>"><?php echo ucfirst($value->eval_cat); ?></option>
						<?php  } ?>
					<?php  } ?>

								</select>
							</div>
							<label class="col-md-1 col-sm-1 col-xs-1 control-label" style="padding-top:0px;text-align:right;" for="state">Type évaluation</label>
							<div class="col-md-2 col-sm-2 col-xs-2">
								<select name="search_type" id="search_type">
									<option value="">Tout</option>
									<option value="thème"<?php if(isset($search_type) && $search_type==='thème') echo 'selected'; ?>>Thème</option>
									<option value="général"<?php if(isset($search_type) && $type==='général') echo 'selected'; ?>>Général</option>
								</select>
							</div>
							<label class="col-md-1 col-sm-1 col-xs-1 control-label" style="padding-top:0px;text-align:right;width:6%;" for="state">Etat</label>
							<div class="col-md-2 col-sm-2 col-xs-2">
								<select name="state" id="state">
									<option value="">Tout</option>
									<option value="1"<?php if(isset($state) && $state==='1') echo 'selected'; ?>>Active</option>
									<option value="0"<?php if(isset($state) && $state==='0') echo 'selected'; ?>>Non active</option>
								</select>
							</div>
							<div class="col-md-1 col-sm-1 col-xs-1">
								<input class="btn btn-default search-form" type="submit" name="search" id="search" value="Rechercher"/>
							</div>
						</div>
					</form>
				</div>	
			</div>
		</div>
	<?php endif; ?>
</div>
<div class="row">
	<div class="horizontal">
		<table class="table table-striped table-bordered table-condensed">
			<thead>
				<tr style="width:100%;">
					<?php if($this->profil===0): ?>
						<th style="color:#31b0d5; text-align:center;min-width:30px;" class='thn1'><strong style=""><a style='padding:0px;' onmousedown='return false;' class='btn btn-plus' role='button'><i class="ion-minus-round" style="margin:0% !important;color:#31b0d5;"></i></a></strong></th>
						<th style="color:#31b0d5; text-align:center;width:5%;"><strong>Numéro</strong></th>
						<th style="color:#31b0d5; text-align:center;width:50%;"><strong>Evaluation</strong></th>
						<th style="color:#31b0d5; text-align:center;width:20%;"><strong>Type</strong></th>
						<th style="color:#31b0d5; text-align:center;width:30%;"><strong>Description</strong></th>
						<th style="color:#31b0d5; text-align:center;width:20%;"><strong>Date de création</strong></th>
						<th style="color:#31b0d5; text-align:center;width:50%;"><strong>Durée</strong></th>
						<th style="color:#31b0d5; text-align:center;width:20%;"><strong>Nbr questions</strong></th>
						<th style="color:#31b0d5; text-align:center;width:20%;"><strong>Seuil</strong></th>
						<th style="color:#31b0d5; text-align:center;width:20%;"><strong>Catégorie évaluation</strong></th>
						<th style="color:#31b0d5; text-align:center;width:20%;"><strong>Opération</strong></th>
					<?php elseif ($this->profil===1): ?>
						<th style="color:#31b0d5; text-align:center;width:5%;"><strong>Numéro</strong></th>
						<th style="color:#31b0d5; text-align:center;width:30%;"><strong>Evaluation</strong></th>
						<th style="color:#31b0d5; text-align:center;width:65%;"><strong>Description</strong></th>
					<?php endif; ?>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($evaluations as $key => $evaluation) { ?>
					<tr style='width:100%;'>
					<?php if($this->profil === 0): ?>
						<?php if($evaluation->eval_active == '1') { ?>
							<td class='first-td' style='padding-left:10px !important;padding-top:0px;padding-bottom:0px;'>
								<a class='btn eval_status' role='button' id="eval<?php echo $evaluation->eval_id; ?>" name="<?php echo $evaluation->eval_id; ?>">
									<i class='ion-ios-checkmark-outline'></i>
								</a>
							</td>
						<?php }else{ ?>
							<td class='first-td' style='padding-left:10px !important;padding-top:0px;padding-bottom:0px;'>
								<a class='btn eval_status' role='button' id="eval<?php echo $evaluation->eval_id; ?>" name="<?php echo $evaluation->eval_id; ?>">
									<i class='ion-ios-circle-outline'></i>
								</a>
							</td>
						<?php } ?>
						<td><?php echo $evaluation->eval_id; ?></td>
						<td><?php echo ucfirst($evaluation->eval_nom); ?></td>
						<td style='text-align:center;padding-left:0% !important;'><?php echo ucfirst($evaluation->eval_type); ?></td>
						<td style='text-align:left;'><?php echo ucfirst($evaluation->eval_description); ?></td>
						<td style='text-align:center;padding-left:0% !important;'><?php echo strftime("%d %b %Y", strtotime($evaluation->eval_date_creation)); ?></td>
						<td style='text-align:center;padding-left:0% !important;'><?php echo gmdate("H:i", strtotime($evaluation->eval_duration)) ?></td>
						<td><?php echo $this->getQuestionNumberByEvaluation($evaluation->eval_type, $evaluation->eval_id); ?></td>
						<td style='text-align:center;padding-left:0% !important;'><?php echo $evaluation->eval_seuil; ?></td>
						<td style='text-align:center;padding-left:0% !important;'><?php echo $evaluation->eval_cat; ?></td>
						<td class='user-listb'><a class='btn' role='button' href="<?php echo URL . 'Evaluation/Edit/' . $evaluation->eval_id; ?>"><i class='ion-compose' style='color:green;'></i></a><a class='btn evaluation delete' id="<?php echo $evaluation->eval_id; ?>" role="button"><i class='ion-close-circled' style='color:red;'></i></a></td>
					<?php elseif($this->profil === 1): ?>
						<tr style='width:100%;'><td><?php echo $evaluation->eval_id; ?></td>
						
						<td> <a class="external" href="<?php URL . 'view/evaluation/questionResponce/'.$evaluation->eval_active; ?>"</a><?php echo ucfirst($evaluation->eval_nom); ?> </td>
                        <td style='text-align:left;'><?php echo ucfirst($evaluation->eval_description); ?></td>
						
					<?php endif; ?>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>
<div class="row" style="margin-bottom:150px;margin-top:50px;">
	<?php if($this->profil===0): ?>
		<hr id="form"/>
		<div class="col-md-3 col-sm-3 col-xs-3"></div>
		<div class="col-md-6 col-sm-6 col-xs-6">
			<h3 style="padding-bottom:0%;margin-bottom:0%;text-align:center;color:#337ab7;">Ajouter une évaluation</h3>
			<hr/>
			<br/><br/>
			<form action="<?php echo URL . 'Evaluation/Add'; ?>" method="post" class="form-horizontal evaluation-form evaluation-form-add">
				<div class="form-group">
					<label class="col-md-4 col-sm-4 col-xs-4 control-label">Titre de l'évaluation <span style="color:#f55;">*</span></label>
					<div class="col-md-8 col-sm-8 col-xs-8">
						<input type="text" name="eval_titre" id="eval_titre" autocomplete="off" required/>
					</div>
				</div>
				<div class="form-group">	
					<label class="col-md-4 col-sm-4 col-xs-4 control-label">Catégorie évaluation <span style="color:#f55;">*</span></label>
					<div class="col-md-8 col-sm-8 col-xs-8">
						<select class="eval_cat" name="eval_cat" id="eval_cat" required>
							<option value=""></option>
							<option value="compétence technique">Compétence technique</option>
							<option value="compétence management">Compétence management</option>
                            <option value="aptitude">Aptitude</option>
						</select>
					</div>
				</div>
				<div class="form-group">	
					<label class="col-md-4 col-sm-4 col-xs-4 control-label">Type <span style="color:#f55;">*</span></label>
					<div class="col-md-8 col-sm-8 col-xs-8">
						<select class="eval_tpe" name="eval_tpe" id="eval_tpe" required>
							<option value=""></option>
							<option value="général">Évaluation générale</option>
							<option value="thème">Évaluation par thème</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 col-sm-4 col-xs-4 control-label">Durée de l'évaluation</label>
					<div class="col-md-2 col-sm-2 col-xs-2" style="text-align:center;">
						<input type="text" name="eval_tmp_h" id="eval_tmp_h" style="text-align:center;" autocomplete="off"/>
					</div>
					<div class="col-md-1 col-sm-1 col-xs-1">
						hr(s)
					</div>
					<div class="col-md-2 col-sm-2 col-xs-2">
						<input type="text" name="eval_tmp_m" id="eval_tmp_m" style="text-align:center;" autocomplete="off"/>
					</div>
					<div class="col-md-1 col-sm-1 col-xs-1">
						min(s)
					</div>
					<div class="col-md-2 col-sm-2 col-xs-2"></div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3">Evaluation démonstration</label>
					<div class="col-md-9">
						<div class="make-switch" data-on-label="libres" data-off-label="non_libres">
							<input type="checkbox" name="eval_access"/>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 col-sm-4 col-xs-4 control-label">Valeur seuil évaluation</label>
					<div class="col-md-8 col-sm-8 col-xs-8">
						<input type="text" name="eval_seuil" id="eval_seuil" autocomplete="off" required/>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 col-sm-4 col-xs-4 control-label">Description de l'évaluation</label>
					<div class="col-md-8 col-sm-8 col-xs-8">
						<textarea type="text" name="eval_desc" id="eval_desc" rows="4"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 col-sm-4 col-xs-4 control-label">Principe de réponse à l'évaluation</label>
					<div class="col-md-8 col-sm-8 col-xs-8">
						<textarea type="text" name="eval_princ" id="eval_princ" rows="4"></textarea>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<input class="btn btn-default evaluation-button-add" type="submit" name="submit" value="Ajouter">
					</div>
				</div>
				<div class="contact-loading alert alert-info form-alert">
					<span class="message">Chargement...</span>
					<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
				</div>
				<div class="contact-success alert alert-success form-alert">
					<span class="message">Succès!</span>
					<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
				</div>
				<div class="contact-error alert alert-danger form-alert">
					<span class="message">Erreur!</span>
					<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
				</div>
			</form>
		</div>
		<div class="col-md-3 col-sm-3 col-xs-3"></div>
	<?php endif; ?>
</div>
