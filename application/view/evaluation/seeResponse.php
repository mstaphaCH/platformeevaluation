
<div class="row form-elements" >
  <div class="col-sm-2"></div>
     <div class="col-sm-8">
		<div class="form-box drop-shadow">
			<div class="form-bottom">
				<div class="row form-elements">
					<label >La bonne réponse est :</label>
					<?php if($explication!=null) echo 'Option '.$explication[0]->opt_valeur; ?>
					<p> <?php if($explication!=null) echo $explication[0]->opt_description; ?> </p>
				</div>
				<div class="row form-elements">
					<label>L'explication est :</label>
					<p> <?php if($explication!=null) echo $explication[0]->qst_explication; ?> </p>
				</div>
				<button type="button" class="btn btn-primary pull-right" onclick="history.back();"> Quitter</button>	
			</div>
		</div>
	</div>
</div>
