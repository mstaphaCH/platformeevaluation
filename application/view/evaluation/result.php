<section id="page4">
    <div class="row page-content1">
        <div class="container result">
            <hr>
            <div class="row">
                <div class="col-xs-12 col-sm-10 col-md-3">
                    <img class="img-responsive" alt="Responsive image" src="../img/2.jpg"/>
                </div>
                <div class="col-xs-11 col-sm-11 col-md-6">
					   <div id="result-box">
							<p>Le nombre de bonnes réponses : <span class="answer"><?php echo $right_answer;?></span></p>
							<p>Le nombre de fausses réponses : <span class="answer"><?php echo $wrong_answer;?></span></p>
							<p>Le nombre de questions non répondues : <span class="answer"><?php echo $unanswered;?></span></p>
					   </div>
                    <a href="<?php echo URL.'Evaluation/Probation/'.$evalId; ?>" class='btn btn-success'>Commencer une nouvelle évaluation</a>
                </div>
            </div>    
            <div class="row">
			</div>
        </div>
    </div>
</section>
