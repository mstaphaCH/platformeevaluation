
    <section id="page2">
    	<div class="container">
    		<div class="row form-elements1" >
    			<div class="col-md-2 col-sm-0"></div>
    			<div class="col-sm-12 col-md-8">
    				<div class="row form-box drop-shadow">
    					<div class="col-sm-12">
    						<h3 style="padding-top:10px;"><?php echo $evaluation[0]->eval_nom; ?></h3>
    						<div class="form-bottom">
    							<div class="row form-elements">
    								<label style="margin-bottom:0px; margin-right: 3px; color:#1FA055; font-size:14px;">Principe de réponse à l'évaluation :</label>
    								<p style="padding-left: 0px;"> <?php echo $evaluationDescr[0]->eval_princ; ?></p>
    							</div>

    							<div class="row form-elements">
    								<label style="margin-bottom:0px; margin-right:3px; color:#1FA055;">Description de l'évaluation :</label>
    								<p style="padding-left:0px;"><?php echo $evaluationDescr[0]->eval_description; ?></p>
    							</div>1
    							<div class="row form-elements">
    								<label style="margin-bottom: 0px; margin-right:3px; color:#1FA055;">La durée de l'évaluation :</label>
    								<p style="padding-left:0px;"> <?php echo $evaluationDescr[0]->edb_time; ?></p>
    							</div>
    						</div>
    					</div>

    					<div class="boutonsEvaluations">

    						<div class="col-lg-4 col-xs-12">
    							<?php if ($evaluation[0]->eval_type=="général" || $evaluation[0]->eval_type=="thème") { ?>

    							<button type="button" class="btn btn-primary" onclick="window.location='question_suiviSession.php?id=<?php echo $evaluation[0]->eval_id;?>'" id="bouton1"> Reprendre la session précédente</button>

    							<?php }else if ($evaluation[0]->eval_type=="aptitude") { ?>

    						<button type="button" class="btn btn-primary" onclick="window.location='question_aptitude.php?id=<?php echo $evaluation[0]->eval_id; ?>'" id="bouton1"> Reprendre la session précédente</button>
    						<?php } ?>
    				</div>

    				<div class="col-lg-4 col-xs-12">
    					<?php if ($evaluation[0]->eval_type=="général" || $evaluation[0]->eval_type=="thème") { ?>
    						<button type="button" class="btn btn-primary new-evaluation general" id="bouton2">Commencer une nouvelle évaluation</button>
    					<?php }elseif ($evaluation[0]->eval_type=="aptitude") { ?>
    						<button type="button" class="btn btn-primary new-evaluation aptitude" id="bouton2">Commencer une nouvelle évaluation</button>
    					<?php }	?>
    				</div>

    				<div class="col-lg-4 col-xs-12">
    					<button type="button" class="btn btn-danger" id="bouton3"> Quitter</button>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
</div>
</section>

<div class="col-md-2 col-sm-0"></div>
<div id="dialog" title="<?php echo $evaluation[0]->eval_nom; ?>">
	<div class="dialog_content"></div>
</div>

<script type="text/javascript">
	var evaluation_id = '<?php echo $eval_id; ?>';
</script>