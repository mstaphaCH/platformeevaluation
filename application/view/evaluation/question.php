<script type="text/javascript">
    var qst_filter = {};
    var all_quest = {};
</script>
<section id="section1">
    <div class="container">
        <div class="row">
            <div class="col-sm-5">
                <h4><?php echo $user->fullname; ?></h4>
            </div>
            <div class="col-sm-5"></div>
        </div>
    </div>
</section>

<section id="section2">
    <div class="container cont_sec2">

        <div class="row drop-box">
            <div class="col-sm-12 col-md-8  questions_container question_content">

                <form class="form-horizontal" role="form" id='result-form' method="post" action="<?php echo URL.'Evaluation/Result'; ?>">
                    <input type="hidden" id="eval_id" name="eval_id" value="<?php echo $eval_id;?>"/>
                    <?php 
                    $quest_count = 1;

                    for ($x = 0; $x < count($questions); $x++){  ?>
                    <script type="application/javascript">
                        all_quest[<?php echo $quest_count; ?>] = 'none';
                        qst_filter[<?php echo $quest_count; ?>] = 'none';
                    </script>

                    <input style="display:none;" checked type="radio" value="none" id='questions_<?php echo $questions[$x]->qst_id;?>' name='questions[<?php echo $questions[$x]->qst_id;?>]'/>

                    <div id='question<?php echo $quest_count;?>' class="<?php echo ($quest_count == 1 ? 'block' : 'hidden' );?>">
                        <input style="display:none;" class="qst_id" value="<?php echo $questions[$x]->qst_id;?>" />	

                        <div class="row questionbox">
                           <div class="row question1993">

                               <!--  Enonce du question -->
                               <div class='questions' id="qname<?php echo $quest_count; ?> ">
                                <b><?php echo $quest_count;?> -</b> <?php echo $questions[$x]->qst_id; ?><br/>		
                            </div>
                            <div class='questionsChoix' id="qname<?php echo $quest_count; ?> ">
                                <?php
                                $responses = $this->getQuestionOptions($questions[$x]->qst_id);
                                for($y=0; $y < count($responses); $y++){
                                    ?>

                                    <div class="radio radio-info row">
                                        <div class="col-sm-12">

                                            <input type="radio" class="accept btn btn-default pull-right trv<?php echo $quest_count;?>" onchange="setAnswered(<?php echo $quest_count; ?>);" value="<?php echo $responses[$y]->opt_id;?>" id='questions_<?php echo $questions[$x]->qst_id;?>' name='questions[<?php echo $questions[$x]->qst_id;?>]'/>
                                            <label onclick="setAnswered(<?php echo $quest_count; ?>); setChecked('trv<?php echo $quest_count;?>','<?php echo $responses[$y]->opt_id;?>')">
                                                <?php echo $responses[$y]->opt_description;?>
                                            </label>
                                        </div>
                                    </div>
                                    <hr>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>

                        <!-- les boutons suivant,precedent,quitter,valider -->
                        <div class="row boutons">
                            <?php if($quest_count==1 && count($questions) > 1) { ?>
                            <button type="button" class="btn btn-primary bouton1 disabled" disabled style="margin:0px;">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                                Précédent
                            </button>
                            <button type="button" class="btn btn-primary bouton4" name="suivantB" onclick="next_quest(<?php echo $quest_count; ?>)">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                                Suivant
                            </button>
                            <button id='<?php echo '';?>' class="btn btn-primary bouton2" type='submit' style="margin: 0px;">
                                <span class="glyphicon glyphicon-ok-sign"></span>
                                Valider
                            </button>
                            <button type="button" class="btn btn-primary bouton3" onclick="<?php echo 'window.location=\''.URL.'\''; ?>">
                                <span class="glyphicon glyphicon-remove-sign"></span>
                                Quitter
                            </button>
                            <?php }elseif($quest_count == 1 && count($questions) == 1) { ?>
                        <button type="button" class="btn btn-primary bouton1 disabled" disabled style="margin:0px;"><span class="glyphicon glyphicon-chevron-left"></span>Précédent</button><button type="button" class="btn btn-primary bouton4" name="suivantB" disabled><span class="glyphicon glyphicon-chevron-right"></span>Suivant</button><button id='<?php echo '';?>' class="btn btn-primary bouton2" type='submit' style="margin: 0px;"><span class="glyphicon glyphicon-ok-sign"></span> Valider</button><button type="button" class="btn btn-primary bouton3" onclick="<?php echo 'window.location=\''.URL.'\''; ?>"><span class="glyphicon glyphicon-remove-sign"></span> Quitter</button>
                        <?php
                    }
                    elseif($quest_count > 1 && $quest_count < count($questions))
                        {?>
                    <button type="button" class="btn btn-primary bouton1" style="margin:0px;" onclick="prev_quest(<?php echo $quest_count; ?>)"><span class="glyphicon glyphicon-chevron-left"></span>Précédent</button><button type="button" class="btn btn-primary bouton4" name="suivantB" onclick="next_quest(<?php echo $quest_count; ?>)"><span class="glyphicon glyphicon-chevron-right"></span>Suivant</button><button id='<?php echo '';?>' class="btn btn-primary bouton2" type='submit' style="margin: 0px;"><span class="glyphicon glyphicon-ok-sign"></span> Valider</button><button type="button" class="btn btn-primary bouton3" onclick="<?php echo 'window.location=\''.URL.'\''; ?>"><span class="glyphicon glyphicon-remove-sign"></span> Quitter</button>

                    <?php }else{ ?>
                    <button type="button" class="btn btn-primary bouton1" style="margin:0px;" onclick="prev_quest(<?php echo $quest_count; ?>)"><span class="glyphicon glyphicon-chevron-left"></span>Précédent</button><button type="button" class="btn btn-primary bouton4" name="suivantB" disabled><span class="glyphicon glyphicon-chevron-right"></span>Suivant</button>
                    <button id='<?php echo '';?>' class="btn btn-primary bouton2" type='submit' style="margin: 0px;"><span class="glyphicon glyphicon-ok-sign"></span> Valider</button><button type="button" class="btn btn-primary bouton3" onclick="<?php echo 'window.location=\''.URL.'\''; ?>"><span class="glyphicon glyphicon-remove-sign"></span> Quitter</button>

                    <?php } ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3  col-lg-offset-0 checkbox checkbox-primary" style="margin-left:10px;">	
                    <input type="checkbox" class="accept btn btn-default pull-right disabled trv<?php echo $quest_count;?>" disabled onclick="setToReview(<?php echo $quest_count;?>,this)" > 			 
                    <label style="color:blue;">Pour revue</label>
                </div>
            </div>
        </div>
        <?php $quest_count++;
    } ?>
    <div id="question" class="no_data hidden">
        <h4>Aucune question n'est marquée comme pour revue</h4>
    </div>
</form>
</div>
<div class="col-lg-1 col-md-0 col-sm-0"></div>
<div id='timer' class="col-xs-12 col-sm-12 col-md-4 col-lg-3" style="position: relative;">
<!-- Nom de l'evaluation a gauche -->
<div class="row nomEvaluation">
    <div class="col-md-1"></div>
    <div class="col-md-10 col-sm-12">
        <span> <?php echo $evaluation[0]->eval_nom; ?> </span>
    </div>
</div>
<div class="row">&nbsp;</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-6">
        <div id="counter" style="padding: 0px; margin-right: 0px; margin-bottom: 0px; font-size: 100%; vertical-align: top; outline: 0px; -webkit-user-select: none; cursor: default; transform-origin: 0px 0px 0px; left: 0px; top: 0px; height: 80px; width: 200px; box-shadow: none; position: relative; display: block; clear: both; zoom: 1; overflow: visible; background-image: none; background-attachment: initial; background-origin: initial; background-clip: padding-box;">
            <div id="counter_hours_value" style="position: absolute; overflow: visible; padding: 0px; margin-right: 0px; margin-bottom: 0px; font-size: 100%; vertical-align: baseline; outline: 0px; -webkit-user-select: none; cursor: inherit; transform-origin: 0px 0px 0px; left: 5px; top: 0px; height: 55px; width: 90px; border-radius: 4.95px; box-shadow: none; z-index: 0; background-image: -webkit-linear-gradient(top, rgb(0, 0, 0) 0%, rgb(104, 104, 104) 50%, rgb(0, 0, 0) 50%, rgb(83, 80, 80) 100%); background-attachment: initial; background-origin: initial; background-clip: padding-box;">
                <div id="counter_hours" style="position: absolute; overflow: visible; padding: 0px; margin-right: 0px; margin-bottom: 0px; font-size: 27px; vertical-align: baseline; outline: 0px; -webkit-user-select: none; cursor: inherit; transform-origin: 0px 0px 0px; left: 0px; top: 0px; height: 32px; width: 90px; box-shadow: none; white-space: nowrap; line-height: 1.25em; color: rgb(255, 255, 255); font-family: Arial, _sans; text-align: center; font-weight: normal; text-shadow: none; text-decoration: none; zoom: 1; z-index: 0; background-image: none; background-attachment: initial; background-origin: initial; background-clip: padding-box; background-color:black;">
                    <span style="font-family: Arial, _sans;">00</span>
                </div>
                <div id="Box_jbeeb_8" style="position: absolute; overflow: visible; padding: 0px; margin-right: 0px; margin-bottom: 0px; font-size: 100%; vertical-align: baseline; outline: 0px; -webkit-user-select: none; cursor: inherit; transform-origin: 0px 0px 0px; left: 0px; top: 26.3px; height: 2.4px; width: 90px; box-shadow: none; z-index: 1; background-image: none; background-attachment: initial; background-color: rgb(0, 0, 0); background-origin: initial; background-clip: padding-box;">
                </div>
            </div>
            <div id="counter_minutes_label" style="position: absolute; overflow: visible; padding: 0px; margin-right: 0px; margin-bottom: 0px; font-size: 10.5px; vertical-align: baseline; outline: 0px; -webkit-user-select: none; cursor: inherit; transform-origin: 0px 0px 0px; left: 0px; top: 42.5px; height: 17.5px; width: 100px; box-shadow: none; white-space: nowrap; line-height: 1em; color: rgb(48, 48, 48); font-family: Arial, _sans; text-align: center; font-weight: bold; text-shadow: none; text-decoration: none; zoom: 1; z-index: 1; background-image: none; background-attachment: initial; background-origin: initial;">
                <span style="font-family: Arial, _sans; color:#95A595; font-size: 14px;">Heures</span>
            </div>
            <div id="counter_minute_value" style="position: absolute; overflow: visible; padding: 0px; margin-right: 0px; margin-bottom: 0px; font-size: 100%; vertical-align: baseline; outline: 0px; -webkit-user-select: none; cursor: inherit; transform-origin: 0px 0px 0px; left: 105px; top: 0px; height: 55px; width: 90px; border-radius: 4.95px; box-shadow: none; z-index: 2; background-image: -webkit-linear-gradient(top, rgb(0, 0, 0) 0%, rgb(104, 104, 104) 50%, rgb(0, 0, 0) 50%, rgb(83, 80, 80) 100%); background-attachment: initial; background-origin: initial; background-clip: padding-box;">
                <div id="counter_minutes" style="position: absolute; overflow: visible; padding: 0px; margin-right: 0px; margin-bottom: 0px; font-size: 27px; vertical-align: baseline; outline: 0px; -webkit-user-select: none; cursor: inherit; transform-origin: 0px 0px 0px; left: 0px; top: 0px; height: 32px; width: 90px; box-shadow: none; white-space: nowrap; line-height: 1.25em; color: rgb(255, 255, 255); font-family: Arial, _sans; text-align: center; font-weight: normal; text-shadow: none; text-decoration: none; zoom: 1; z-index: 0; background-image: none; background-attachment: initial; background-origin: initial; background-clip: padding-box; background-color:black;">
                    <span style="font-family: Arial, _sans;">00</span>
                </div>
                <div id="Box_jbeeb_12" style="position: absolute; overflow: visible; padding: 0px; margin-right: 0px; margin-bottom: 0px; font-size: 100%; vertical-align: baseline; outline: 0px; -webkit-user-select: none; cursor: inherit; transform-origin: 0px 0px 0px; left: 0px; top: 26.3px; height: 2.4px; width: 90px; box-shadow: none; z-index: 1; background-image: none; background-attachment: initial; background-color: rgb(0, 0, 0); background-origin: initial; background-clip: padding-box;">
                </div>
            </div>
            <div id="counter_minute_label" style="position: absolute; overflow: visible; padding: 0px; margin-right: 0px; margin-bottom: 0px; font-size: 10.5px; vertical-align: baseline; outline: 0px; -webkit-user-select: none; cursor: inherit; transform-origin: 0px 0px 0px; left: 100px; top: 42.5px; height: 17.5px; width: 100px; box-shadow: none; white-space: nowrap; line-height: 1em; color: rgb(48, 48, 48); font-family: Arial, _sans; text-align: center; font-weight: bold; text-shadow: none; text-decoration: none; zoom: 1; z-index: 3; background-image: none; background-attachment: initial; background-origin: initial; background-clip: padding-box;">
                <span style="font-family: Arial, _sans;color:#95A595; font-size: 14px;">Minutes</span>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <button type="button" class="btn btn-warning btn-block pps">Pause</button>
</div>
<div class="row">
    <button type="button" class="btn btn-primary btn-block ppl" style="display:none;">Reprendre</button>
</div>
<div class="row">
    <select class="btn-primary" data-style="btn-primary"  onchange="setFilter(this.value);" required>
        <option value="" disabled selected hidden>Filtrer les questions</option>
        <option value="all">Toutes</option>
        <option value="review">Pour revue</option>
        <option value="none">Non traiées</option>
    </select>
</div>
<div class="row">&nbsp;</div>
<!-- Progress bar -->
<div class="row">
    <div class="col-xs-7">
        <div class="progress">
            <div id="progress" class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" .aria-valuemin="0" aria-valuemax="100" style="width:0%">
            </div>	
        </div>
    </div>
    <div class="col-xs-3">
        <div id="affichage" style="text-align:center"> 0/<?php echo $quest_count-1;?> </div>
    </div>
</div>
<div class="row">
    <div class="btn-group-vertical" style="width:100%;">
        <button type="button" class="btn btn-primary" onclick="javascript:open_EtatAvancement();" style="border-bottom: 1px solid black; width:100%;">Etat d'avancement</button>
        <!-- Etat des reponses -->
        <?php
        if($this->profil == 2){
                //Cas du candidat
            $stat = GET_StatusCandidat($cdt_id);
            foreach($stat as $tab)
            {
                $status = $tab[0];
            }
            if($status == "Formation" || $status == "formation")
            {

                ?>

                <button type="button" class="btn btn-primary btn-block" onclick="javascript:etat_rep();" style="border-bottom: 1px solid black; widyh:100%;">Etat des réponses</button>
                <button type="button" class="btn btn-primary" onclick="window.location=current_qst()" style="border-bottom: 1px solid black; width:100%;">Voir la réponse</button>
                <?php
            }
        }else{
                // Cas d'administrateur, formateur, evaluateur
            ?>
            <button type="button" class="btn btn-primary btn-block" onclick="javascript:etat_rep();" style="border-bottom: 1px solid black; width:100%;">Etat des réponses</button>
            <button type="button" class="btn btn-primary" onclick="window.location=current_qst()" style="border-bottom: 1px solid black; width:100%;">Voir la réponse</button>
            <?php
        }
        ?>
        <!-- Bouton reset -->
        <button type="button" class="btn btn-danger btn-block" onclick="javascript:reinitialiser();" style="width:100%;">Reset</button>
    </div>
</div>
<div id="dialog" title="<?php echo $evaluation[0]->eval_nom; ?>">
    <div class="dialog_content"></div>
</div>
<div class="row lg-1 buttonBox"></div>
</div>
</div>
</div>
</section>
<script type="text/javascript">
    var eval_id = '<?php echo $eval_id; ?>';
    var quest_count = '<?php echo $quest_count; ?>';
    var evaluationDescr_timer = '<?php echo $timer; ?>';
</script>
