<?php foreach ($activeUsers as $key => $activeUser) { ?>
	<tr>
		<td><?php echo ucfirst($activeUser->util_nom); ?></td>
		<td><?php echo ucfirst($activeUser->util_prenom); ?></td>
		<td style='text-align:center;padding-left:0% !important;'><?php echo ucfirst($activeUser->prf_description); ?></td>
		<td style='text-align:center;padding-left:0% !important;'><?php ucfirst($activeUser->util_login); ?></td>
		<td style='text-align:center;padding-left:0% !important;'><?php echo $activeUser->util_email ?></td>
		<td style='text-align:center;padding-left:0% !important;'><?php echo $activeUser->util_siteweb; ?></td>
		<td style='text-align:center;padding-left:0% !important;'><?php echo ucfirst($activeUser->str_nom); ?></td>
		<td style='text-align:center;padding-left:0% !important;'><?php echo strftime("%d %b %Y", strtotime($activeUser->util_date_creation)); ?></td>
		<?php if($this->profil === 0) { ?>
			<td class='button-operation-3'><a class='btn' role='button' href="<?php echo URL . 'User/Edit/' . $activeUser->util_id; ?>"><i class='ion-compose' style='color:green;'></i></a><a class='btn' role='button' href="<?php echo URL . 'User/Inactive/' . $activeUser->util_id; ?>"><i class='ion-log-out' style='color:blue;'></i></a><a class='btn' role='button' href="<?php echo URL . 'User/Delete/' . $activeUser->util_id; ?>"><i class='ion-close-circled' style='color:red;'></i></a></td>
		<?php }elseif($this->profil === 1) { ?>
			<td class='button-operation-3'><a class='btn' role='button' href="<?php echo URL . 'User/Edit/' . $activeUser->util_id; ?>"><i class='ion-compose' style='color:green;'></i></a><a class='btn' role='button' href="<?php echo URL . 'User/Delete/' . $activeUser->util_id; ?>"><i class='ion-close-circled' style='color:red;'></i></a></td>
		<?php } ?>
	</tr>
<?php } ?>