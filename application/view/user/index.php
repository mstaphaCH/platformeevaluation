<div class="row">
	<h3 style="text-align:center;width:30%;margin:auto;color:#337ab7;"><span>Gestion des intervenants<hr/></span></h3>
	<div align="right" style="margin-right:32px;"><a href="<?php echo URL .'User#form'; ?>" style="color:#337ab7">+ Ajouter un intervenant</a></div>
	<h2 style="text-align:left;font-size:18px;margin-bottom:25px;">Intervenants actifs</h2>
	<form action="javascript:;" method="post" accept-charset="utf-8" class="form-horizontal active-user list filter" style="border-bottom: none;">
		<div class="form-group row">
			<label class="col-md-1 col-sm-1 col-xs-1 control-label" style="text-align:right;width:6%;" for="user_structure">Structure</label>
			<div class="col-md-2 col-sm-2 col-xs-2">
				<select name="user_structure" id="user_structure">
					<option value="">Tout</option>
					<?php foreach ($structureNames as $key => $structureName): ?>
						<?php if (isset($user_structure) && $user_structure === $structureName->str_nom): ?>
							<option value="<?php echo $structureName->str_nom; ?>" selected><?php echo ucfirst($structureName->str_nom); ?></option>
						<?php else: ?>
							<option value="<?php echo $structureName->str_nom; ?>"><?php echo ucfirst($structureName->str_nom); ?></option>
						<?php endif ?>
					<?php endforeach ?>
				</select>
			</div>
			<?php if($this->profil===0): ?>
			<label class="col-md-1 col-sm-1 col-xs-1 control-label" style="text-align:right;width:4%;" for="theme">Profil</label>
			<div class="col-md-2 col-sm-2 col-xs-2">
				<select name="type" id="type">
					<option value="">Tout</option>
					<?php foreach ($profilTitles as $key => $profilTitle): ?>
						<?php if (isset($type) && $type === $profilTitle->prf_nom): ?>
							<option value="<?php echo $profilTitle->prf_nom; ?>" selected>
								<?php echo ucfirst($profilTitle->prf_description); ?>
							</option>
						<?php else: ?>
							<option value="<?php echo $profilTitle->prf_nom; ?>">
								<?php echo ucfirst($profilTitle->prf_description); ?>
							</option>
						<?php endif ?>
					<?php endforeach; ?>
				</select>
			</div>
			<?php endif; ?>
		<div class="col-md-1 col-sm-1 col-xs-1">
			<input type="hidden" name="user" value="active">
			<input class="btn btn-default search-form" type="submit" name="search1" id="search" value="Rechercher"/>
		</div>
	</div>
</form>
<br/>
</div>
<div class="row" style="margin-bottom:5%;">
	<div class="horizontal">
		<table id="activeTable" class="table table-striped table-bordered table-condensed">
			<thead>
				<tr style="width:100%;">
					<th style="color:#31b0d5; text-align:center;width:10%;"><strong>Nom</strong></th>
					<th style="color:#31b0d5; text-align:center;width:10%;"><strong>Prénom</strong></th>
					<th style="color:#31b0d5; text-align:center;width:15%;"><strong>Profil</strong></th>
					<th style="color:#31b0d5; text-align:center;width:10%;"><strong>Login</strong></th>
					<th style="color:#31b0d5; text-align:center;width:15%;"><strong>Email</strong></th>
					<th style="color:#31b0d5; text-align:center;width:10%;"><strong>Site web</strong></th>
					<th style="color:#31b0d5; text-align:center;width:17%;"><strong>Structure</strong></th>
					<th style="color:#31b0d5; text-align:center;width:12%;"><strong>Date création</strong></th>
					<th style="color:#31b0d5; text-align:center;width:20%;"><strong>Opération</strong></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($activeUsers as $key => $activeUser) { ?>
					<tr>
						<td><?php echo ucfirst($activeUser->util_nom); ?></td>
						<td><?php echo ucfirst($activeUser->util_prenom); ?></td>
						<td style='text-align:center;padding-left:0% !important;'><?php echo ucfirst($activeUser->prf_description); ?></td>
						<td style='text-align:center;padding-left:0% !important;'><?php ucfirst($activeUser->util_login); ?></td>
						<td style='text-align:center;padding-left:0% !important;'><?php echo $activeUser->util_email ?></td>
						<td style='text-align:center;padding-left:0% !important;'><?php echo $activeUser->util_siteweb; ?></td>
						<td style='text-align:center;padding-left:0% !important;'><?php echo ucfirst($activeUser->str_nom); ?></td>
						<td style='text-align:center;padding-left:0% !important;'><?php echo strftime("%d %b %Y", strtotime($activeUser->util_date_creation)); ?></td>
						<?php if($this->profil === 0) { ?>
							<td class='button-operation-3'><a class='btn' role='button' href="<?php echo URL . 'User/Edit/' . $activeUser->util_id; ?>"><i class='ion-compose' style='color:green;'></i></a><a class='btn' role='button' href="<?php echo URL . 'User/Inactive/' . $activeUser->util_id; ?>"><i class='ion-log-out' style='color:blue;'></i></a><a class='btn user delete' id="<?php echo $activeUser->util_id; ?>" role="button"><i class='ion-close-circled' style='color:red;'></i></a></td>
						<?php }elseif($this->profil === 1) { ?>
							<td class='button-operation-3'><a class='btn' role='button' href="<?php echo URL . 'User/Edit/' . $activeUser->util_id; ?>"><i class='ion-compose' style='color:green;'></i></a><a class='btn user delete' id="<?php echo $activeUser->util_id; ?>" role="button"><i class='ion-close-circled' style='color:red;'></i></a></td>
						<?php } ?>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>
<div class="row">
	<h2 style="text-align:left;font-size:18px;margin-bottom:25px;">Intervenants non actifs</h2>
	<form action="javascript:;" method="post" accept-charset="utf-8" class="form-horizontal inactive-user list filter">
		<div class="form-group row">
			<label class="col-md-1 col-sm-1 col-xs-1 control-label" style="text-align:right;width:6%;" for="user_structure">Structure</label>
			<div class="col-md-2 col-sm-2 col-xs-2">
				<select name="user_structure" id="user_structure">
					<option value="">Tout</option>
					<?php foreach ($structureNames as $key => $structureName): ?>
						<?php if (isset($user_structure) && $user_structure === $structureName->str_nom): ?>
							<option value="<?php echo $structureName->str_nom; ?>" selected>
								<?php echo ucfirst($structureName->str_nom); ?>
							</option>
						<?php else: ?>
							<option value="<?php echo $structureName->str_nom; ?>">
								<?php echo ucfirst($structureName->str_nom); ?>
							</option>
						<?php endif ?>
					<?php endforeach ?>
				</select>
			</div>
			<?php if($this->profil === 0): ?>
			<label class="col-md-1 col-sm-1 col-xs-1 control-label" style="text-align:right;width:4%;" for="theme">Profil</label>
			<div class="col-md-2 col-sm-2 col-xs-2">
				<select name="type" id="type">
					<option value="">Tout</option>
					<?php foreach ($profilTitles as $key => $profilTitle): ?>
						<?php if (isset($type) && $type === $profilTitle->prf_nom): ?>
							<option value="<?php echo $profilTitle->prf_nom; ?>" selected>
								<?php echo ucfirst($profilTitle->prf_description); ?>
							</option>
						<?php else: ?>
							<option value="<?php echo $profilTitle->prf_nom; ?>">
								<?php echo ucfirst($profilTitle->prf_description); ?>
							</option>
						<?php endif ?>
					<?php endforeach; ?>
				</select>
			</div>
		<?php endif; ?>
		<div class="col-md-1 col-sm-1 col-xs-1">
			<input type="hidden" name="user" value="inactive">
			<input class="btn btn-default search-form" type="submit" name="search2" id="search" value="Rechercher"/>
		</div>
	</div>
</form>
<br/>
</div>
<div class="row" style="margin-bottom:5%;">
	<div class="horizontal">
		<table id="inactiveTable" class="table table-striped table-bordered table-condensed">
			<thead>
				<tr style="width:100%;">
					<th style="color:#31b0d5; text-align:center;width:10%;"><strong>Nom</strong></th>
					<th style="color:#31b0d5; text-align:center;width:10%;"><strong>Prénom</strong></th>
					<th style="color:#31b0d5; text-align:center;width:15%;"><strong>Profil</strong></th>
					<th style="color:#31b0d5; text-align:center;width:15%;"><strong>Login</strong></th>
					<th style="color:#31b0d5; text-align:center;width:10%;"><strong>Email</strong></th>
					<th style="color:#31b0d5; text-align:center;width:15%;"><strong>Site web</strong></th>
					<th style="color:#31b0d5; text-align:center;width:17%;"><strong>Structure</strong></th>
					<th style="color:#31b0d5; text-align:center;width:12%;"><strong>Date création</strong></th>
					<th style="color:#31b0d5; text-align:center;width:20%;"><strong>Opération</strong></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($inactiveUsers as $key => $inactiveUser): ?>
					<tr>
						<td><?php echo ucfirst($inactiveUser->util_nom); ?></td>
						<td><?php echo ucfirst($inactiveUser->util_prenom); ?></td>
						<td style='text-align:center;padding-left:0% !important;'><?php echo ucfirst($inactiveUser->prf_description); ?></td>
						<td style='text-align:center;padding-left:0% !important;'><?php ucfirst($inactiveUser->util_login); ?></td>
						<td style='text-align:center;padding-left:0% !important;'><?php echo $inactiveUser->util_email ?></td>
						<td style='text-align:center;padding-left:0% !important;'><?php echo $inactiveUser->util_siteweb; ?></td>
						<td style='text-align:center;padding-left:0% !important;'><?php echo ucfirst($inactiveUser->str_nom); ?></td>
						<td style='text-align:center;padding-left:0% !important;'><?php echo strftime("%d %b %Y", strtotime($inactiveUser->util_date_creation)); ?></td>
						<?php if ($this->profil == 0): ?>
							<td class='button-operation-3'><a class='btn' role='button' href="<?php echo URL . 'User/Edit/' . $inactiveUser->util_id; ?>"><i class='ion-compose' style='color:green;'></i></a><a class='btn' role='button' href="<?php echo URL . 'User/Active/' . $inactiveUser->util_id; ?>"><i class='ion-log-in' style='color:blue;'></i></a><a class='btn user delete' id="<?php echo $activeUser->util_id; ?>" role="button"><i class='ion-close-circled' style='color:red;'></i></a></td>
						<?php elseif($this->profil == 1): ?>
							<td class='button-operation-3'><a class='btn' role='button' href="<?php echo URL . 'User/Edit/' . $inactiveUser->util_id; ?>"><i class='ion-compose' style='color:green;'></i></a><a class='btn user delete' id="<?php echo $activeUser->util_id; ?>" role="button"><i class='ion-close-circled' style='color:red;'></i></a></td>
						<?php endif ?>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>
<div class="row" style="margin-bottom:150px;">
	<hr id="form"/>
	<div class="col-md-3 col-sm-3 col-xs-3"></div>
	<div class="col-md-6 col-sm-6 col-xs-6">
		<h3 style="padding-bottom:0%;margin-bottom:0%;text-align:center;color:#337ab7;">Ajouter un intervenant</h3>
		<hr/>
		<br/><br/>
		<form action="<?php echo URL.'User/Add'; ?>" method="post" class="form-horizontal user-list-form">
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Nom <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input type="text" class="user_firstname" name="user_firstname" id="user_firstname" autocomplete="off" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Prénom <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input type="text" class="user_lastname" name="user_lastname" id="user_lastname" autocomplete="off" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Profil <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<select class="form-control" data-style="btn-primary" name="user_profile" id="user_profile" required>
						<option value="" id="none"></option>
						<?php foreach ($profils as $key => $profil): ?>
							<option class="user_type" value="<?php echo $profil->prf_nom; ?>"><?php echo ucfirst($profil->prf_description); ?></option>
						<?php endforeach ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Login <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input type="text" class="user_login" name="user_login" id="user_login" autocomplete="off" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Mot de passe <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input type="password" class="user_password" name="user_password" id="user_password" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Mot de passe (Resaisir) <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input type="password" class="user_repassword" name="user_repassword" id="user_repassword" required/>
				</div>
			</div>
			<div class="form-group user-code">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Code <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input type="password" class="user_code" name="user_code" id="user_code"/>
				</div>
			</div>
			<div class="form-group user-code">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Code (Resaisir) <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input type="password" class="user_recode" name="user_recode" id="user_recode"/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Email <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input type="email" class="user_email" name="user_email" id="user_email" autocomplete="off" required/>
				</div>
			</div>
<!-- 			<div class="form-group">
				<label class="col-md-2 col-sm-2 col-xs-2 control-label">Numéro <span style="color:#f55;">*</span></label>
				<div class="col-md-2 col-sm-2 col-xs-2">
					<input type="text" class="user_address_number" name="user_address_number" id="user_address_number" autocomplete="off" required/>
				</div>
				<label class="col-md-1 col-sm-1 col-xs-1 control-label" style="text-align:center;">Rue <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input type="text" class="user_street" name="user_street" id="user_street" autocomplete="off" required/>
				</div>
			</div> -->
			<!-- <div class="form-group">
				<label class="col-md-2 col-sm-2 col-xs-2 control-label">Ville <span style="color:#f55;">*</span></label>
				<div class="col-md-3 col-sm-3 col-xs-3">
					<input type="text" class="user_city" name="user_city" id="user_city" autocomplete="off" required/>
				</div>
				<label class="col-md-2 col-sm-2 col-xs-2 control-label" style="text-align:center;">Pays <span style="color:#f55;">*</span></label>
				<div class="col-md-5 col-sm-5 col-xs-5">
					<input type="text" class="user_country" name="user_country" id="user_country" autocomplete="off" required/>
				</div>
			</div> -->
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Site Web</label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input type="text" class="user_website" name="user_website" id="user_website" autocomplete="off"/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Structure <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<select class="form-control" data-style="btn-primary" name="user_structure" id="user_structure" required>
						<option value="" id="none"></option>
						<?php foreach ($this->getStructures($strutil_id) as $key => $structure): ?>
							<option class="op2" value="<?php echo $structure->str_nom; ?>"><?php echo ucfirst($structure->str_nom); ?></option>
						<?php endforeach ?>
						<option value="excellium consulting" id="op1">Excellium consulting</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<input class="btn btn-default user-button-add" type="submit" name="submit" value="Ajouter">
				</div>
			</div>
			<div class="contact-loading alert alert-info form-alert">
				<span class="message">Chargement...</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
			<div class="contact-success alert alert-success form-alert">
				<span class="message">Succès!</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
			<div class="contact-error alert alert-danger form-alert">
				<span class="message">Erreur!</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
		</form>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-3"></div>
</div>
</div>
