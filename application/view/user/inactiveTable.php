<?php foreach ($inactiveUsers as $key => $inactiveUser): ?>
	<tr>
		<td><?php echo ucfirst($inactiveUser->util_nom); ?></td>
		<td><?php echo ucfirst($inactiveUser->util_prenom); ?></td>
		<td style='text-align:center;padding-left:0% !important;'><?php echo ucfirst($inactiveUser->prf_description); ?></td>
		<td style='text-align:center;padding-left:0% !important;'><?php ucfirst($inactiveUser->util_login); ?></td>
		<td style='text-align:center;padding-left:0% !important;'><?php echo $inactiveUser->util_email ?></td>
		<td style='text-align:center;padding-left:0% !important;'><?php echo $inactiveUser->util_siteweb; ?></td>
		<td style='text-align:center;padding-left:0% !important;'><?php echo ucfirst($inactiveUser->str_nom); ?></td>
		<td style='text-align:center;padding-left:0% !important;'><?php echo strftime("%d %b %Y", strtotime($inactiveUser->util_date_creation)); ?></td>
		<?php if ($this->profil == 0): ?>
			<td class='button-operation-3'><a class='btn' role='button' href="<?php echo URL . 'User/Edit/' . $inactiveUser->util_id; ?>"><i class='ion-compose' style='color:green;'></i></a><a class='btn' role='button' href="<?php echo URL . 'User/Active/' . $inactiveUser->util_id; ?>"><i class='ion-log-in' style='color:blue;'></i></a><a class='btn' role='button' href="<?php echo URL . 'User/Delete/' . $inactiveUser->util_id; ?>"><i class='ion-close-circled' style='color:red;'></i></a></td>
		<?php elseif($this->profil == 1): ?>
			<td class='button-operation-3'><a class='btn' role='button' href="<?php echo URL . 'User/Edit/' . $inactiveUser->util_id; ?>"><i class='ion-compose' style='color:green;'></i></a><a class='btn' role='button' href="<?php echo URL . 'User/Delete/' . $inactiveUser->util_id; ?>"><i class='ion-close-circled' style='color:red;'></i></a></td>
		<?php endif ?>
	</tr>
<?php endforeach ?>