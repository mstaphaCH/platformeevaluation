<div class="row" style="margin-bottom:5%;">
	<h3 style="text-align:center;width:30%;margin:auto;color:#337ab7;"><span>Modifier un intervenant<hr/></span></h3>
	<div align="right" style="margin-right:100px;"><a href="<?php echo URL . 'User#form'; ?>">+ Ajouter un intervenant</a></div>
	<div class="col-md-3 col-sm-3 col-xs-3"></div>
	<div class="col-md-6 col-sm-6 col-xs-6">
		<br/>
		<form action="<?php echo URL . 'User/Update' ?>" method="post" class="form-horizontal user-list-form">
			<div class="form-group">
				<label class="col-md-3 col-sm-3 col-xs-3 control-label">Nom <span style="color:#f55;">*</span></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input type="text" class="user_firstname" name="user_firstname" id="user_firstname" value="<?php echo $user_firstname; ?>" autocomplete="off" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-sm-3 col-xs-3 control-label">Prénom <span style="color:#f55;">*</span></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input type="text" class="user_lastname" name="user_lastname" id="user_lastname" value="<?php echo $user_lastname; ?>" autocomplete="off" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-sm-3 col-xs-3 control-label">Login <span style="color:#f55;">*</span></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input type="text" class="user_login" name="user_login" id="user_login" value="<?php echo $user_login; ?>" autocomplete="off"/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-sm-3 col-xs-3 control-label">Email <span style="color:#f55;">*</span></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input type="email" class="user_email" name="user_email" id="user_email" value="<?php echo $user_email; ?>" autocomplete="off" disabled/>
				</div>
			</div>
<!-- 			<div class="form-group">
				<label class="col-md-2 col-sm-2 col-xs-2 control-label">Numéro <span style="color:#f55;">*</span></label>
				<div class="col-md-2 col-sm-2 col-xs-2">
					<input type="text" class="user_address_number" name="user_address_number" id="user_address_number" value="<?php //echo $user_address_number; ?>" autocomplete="off" required/>
				</div>
				<label class="col-md-2 col-sm-2 col-xs-2 control-label" style="text-align:center;">Rue <span style="color:#f55;">*</span></label>
				<div class="col-md-6 col-sm-6 col-xs-6">
					<input type="text" class="user_street" name="user_street" id="user_street" value="<?php //echo $user_street; ?>" autocomplete="off"/>
				</div>
			</div> -->
			<!-- <div class="form-group">
				<label class="col-md-2 col-sm-2 col-xs-2 control-label">Ville <span style="color:#f55;">*</span></label>
				<div class="col-md-3 col-sm-3 col-xs-3">
					<input type="text" class="user_city" name="user_city" id="user_city" value="<?php //echo $user_city; ?>" autocomplete="off"/>
				</div>
				<label class="col-md-2 col-sm-2 col-xs-2 control-label" style="text-align:center;">Pays <span style="color:#f55;">*</span></label>
				<div class="col-md-5 col-sm-5 col-xs-5">
					<input type="text" class="user_country" name="user_country" id="user_country" value="<?php //echo $user_country; ?>" autocomplete="off"/>
				</div>
			</div> -->
			<div class="form-group">
				<label class="col-md-3 col-sm-3 col-xs-3 control-label">Site Web</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input type="text" class="user_website" name="user_website" id="user_website" value="<?php echo $user_website; ?>" autocomplete="off"/>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<input name="thisID" type="hidden" value="<?php echo $targetID; ?>" />
					<input class="btn btn-default structure-button-edit" type="submit" name="submit" value="Modifier">
				</div>
			</div>
			<div class="contact-loading alert alert-info form-alert">
				<span class="message">Chargement...</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
			<div class="contact-success alert alert-success form-alert">
				<span class="message">Succès!</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
			<div class="contact-error alert alert-danger form-alert">
				<span class="message">Erreur!</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
		</form>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-3"></div>
</div>