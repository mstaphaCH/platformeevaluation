<div class="row page-content login_page">
	<div class="col-md-2 col-sm-2 col-xs-12"></div>
	<div class="col-md-8 col-sm-8 col-xs-12">
		<h4 class="widget-header"><i class="ion-locked margin-right-icon"></i>CONNECTER</h4>
		<div class="widget-body">
			<form action="<?php echo URL . 'Account/userLogin' ?>" method="post" class="form-horizontal form-signin-signup login-form" id="login_form">
				<p><input type="text" name="login-email" id="login-email" autocomplete="off" placeholder="Adresse E-mail"/></p>
				<p><input type="password" name="login-passwd" id="login-passwd" autocomplete="off" placeholder="Mot de passe"/></p>
				<p class="login-code"><input type="password" name="login-code" id="login-code" autocomplete="off" placeholder="Code"/></p>
				<br/>
				<p>
					<button type="submit" class="button solid-button blue login-btn" name="submit">Connecter</button>
					<a class="button solid-button blue" role="button" href='sign_up.php'>Inscrire</a>
					<div class="clear"></div>
				</p>
				<div class="login-error alert alert-danger form-alert">
					<span class="message">Erreur!</span>
					<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
				</div>
				<div class="login-loading alert alert-info form-alert">
					<span class="message">Chargement...</span>
					<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
				</div>
				<div class="clear"></div>
			</form>
		</div>
	</div>
	<div class="col-md-2 col-sm-2 col-xs-12"></div>
</div>
