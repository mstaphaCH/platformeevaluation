<div class="row">
	<h3 style="font-size:18px;text-align:center;width:30%;padding-top:20px;margin:auto;color:#337ab7;">Gérer questions d'aptitude<hr style="margin-top:10px;margin-bottom:10px;" /></h3>
	<div class="fixed-top">
		<form action="" method="post" class="form-horizontal" style="width:1170px; margin: 30px auto 10px auto;font-size:13px;">
			<div style="float:right;"><a href="question_apt_list.php#form">+ Ajouter une question</a></div>
			<div class="form-group row">
				<label class="col-md-1 col-sm-1 col-xs-1 control-label" style="padding-top:0px;text-align:right;width:6%;" for="evaluation">Evaluation</label>
				<div class="col-md-2 col-sm-2 col-xs-2">
					<select name="evaluation" id="evaluation">
						<option value="">Tout</option>
						<?php foreach ($aptitudeEvaluationNames as $key => $aptitudeEvaluationName) { ?>
							<?php if(isset($evaluation) && $evaluation === $aptitudeEvaluationName->eval_nom) { ?>
								<option value="<?php echo $aptitudeEvaluationName->eval_nom; ?>" selected>
									<?php echo ucfirst($aptitudeEvaluationName->eval_nom); ?>
								</option>
							<?php }else{ ?>
								<option value="<?php echo $aptitudeEvaluationName->eval_nom; ?>">
									<?php echo ucfirst($aptitudeEvaluationName->eval_nom); ?>
								</option>
							<?php } ?>
						<?php } ?>
					</select>
				</div>
				<div class="col-md-1 col-sm-1 col-xs-1">
					<input class="btn btn-default search-form" type="submit" name="search" id="search" value="Rechercher"/>
				</div>
			</div>
		</form>
		<div class="fixed-horizontal">
			<div class="container" style="padding-left:0px;padding-right:0px;">
				<table class="table table-striped table-bordered table-hover table-condensed" style="margin-bottom:0px;width:100% !important;">
					<thead style="">
						<tr style="width:100%;">
							<th style="color:#31b0d5; text-align:center;" class="th1"><strong style=""><a style='padding:0px;' onmousedown='return false;' class='btn btn-plus' role='button'><i class="ion-minus-round" style="margin:0% !important;color:#31b0d5;"></i></a></strong></th>
							<th style="color:#31b0d5; text-align:center;" class="th2"><strong>Numéro</strong></th>
							<th style="color:#31b0d5; text-align:center;" class="th3"><strong>Question</strong></th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="horizontal">
		<table class="table table-striped table-bordered table-condensed">
			<thead>
				<tr style="width:100%;">
					<th style="color:#31b0d5; text-align:center;min-width:30px;" class='thn1'><strong style=""><a style='padding:0px;' onmousedown='return false;' class='btn btn-plus' role='button'><i class="ion-minus-round" style="margin:0% !important;color:#31b0d5;"></i></a></strong></th>
					<th style="color:#31b0d5; text-align:center;width:5%;" class='thn2'><strong>Numéro</strong></th>
					<th style="color:#31b0d5; text-align:center;width:100%;" class='thn3'><strong>Question</strong></th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($aptitudeQuestions as $key => $aptitudeQuestion) { ?>
				<tr style='width:100%;'>
					<td class='first-td'>
						<div class='cell-block'>
							<button class='button solid-button blue info-button question aptitude' id="<?php echo $aptitudeQuestion->qst_id; ?>">
								<i class='ion-ios-information-outline'></i>
							</button>
							<button class='button solid-button blue toggle-button' onclick="toggleNavPanel(<?php echo $aptitudeQuestion->qst_id; ?>,<?php echo $aptitudeQuestion->qst_id; ?>)">
								<span id="n<?php echo $aptitudeQuestion->qst_id; ?>">&#9662;</span>
							</button>
						</div>
					</td>
					<td style='width:10%;'><?php echo $aptitudeQuestion->qst_ref; ?></td>
					<td style='width:100%;'><?php echo $aptitudeQuestion->qst_description; ?></td>
					<tr><td colspan='12' style='padding:0%;'><div class='row sections_panel' id="h<?php echo $aptitudeQuestion->qst_id; ?>" style='height:0px;'><?php foreach ($aptitudeEvaluations as $key => $aptitudeEvaluation) { ?><?php if($this->evalQuesByEvalId($aptitudeQuestion->qst_id, $aptitudeEvaluation->eval_id) != null) { ?><?php foreach ($this->evalQuesByEvalId($aptitudeQuestion->qst_id, $aptitudeEvaluation->eval_id) as $key => $evalQues) { ?><?php if($evalQues->eval_id == $aptitudeEvaluation->eval_id) { ?><div style="margin-left:10px;text-align:left;" class="col-md-2 col-sm-2 col-xs-2"><input type="checkbox" style="width:14px;margin-right:5px;margin-left:10px;" id="<?php echo $aptitudeQuestion->qst_id; ?>" name="en<?php echo $aptitudeQuestion->qst_id; ?>" class="<?php echo $aptitudeQuestion->qst_id; ?>" value="<?php echo $aptitudeEvaluation->eval_nom; ?>" checked/><?php echo ucfirst($aptitudeEvaluation->eval_nom); ?></div><?php }else{ ?><div style="margin-left:10px;text-align:left;" class="col-md-2 col-sm-2 col-xs-2"><input type="checkbox" style="width:14px;margin-right:5px;margin-left:10px;" id="<?php echo $aptitudeQuestion->qst_id; ?>" name="en<?php echo $aptitudeQuestion->qst_id; ?>" class="<?php echo $aptitudeQuestion->qst_id; ?>" value="<?php echo $aptitudeEvaluation->eval_nom; ?>"/><?php echo ucfirst($aptitudeEvaluation->eval_nom); ?></div><?php } ?><?php } ?><?php }else{ ?><div style="margin-left:10px;text-align:left;" class="col-md-2 col-sm-2 col-xs-2"><input type="checkbox" style="width:14px;margin-right:5px;margin-left:10px;" id="<?php echo $aptitudeQuestion->qst_id; ?>" name="en<?php echo $aptitudeQuestion->qst_id; ?>" class="<?php echo $aptitudeQuestion->qst_id; ?>" value="<?php echo $aptitudeEvaluation->eval_nom; ?>"/><?php echo ucfirst($aptitudeEvaluation->eval_nom); ?></div><?php } ?><?php } ?><div class='clear'></div><div style='margin-top:15px;'><a class='btn btn-info' role='button' href="<?php echo URL.'AptitudeQuestion/Delete/'.$aptitudeQuestion->qst_id; ?>">Supprimer</a><a class='btn btn-info' role='button' href="<?php echo URL.'AptitudeQuestion/Edit/'.$aptitudeQuestion->qst_id; ?>">Modifier</a><a class='btn btn-info save' role='button' id="en<?php echo $aptitudeQuestion->qst_id; ?>">Enregistrer</a></div><div class='clear'></div></div></div></td></tr>
				<?php } ?>
				</tbody>
		</table>
	</div>
</div>
<div class="row" style="margin-bottom:5%;">
	<br/>
	<hr id="form"/>
	<div class="col-md-1 col-sm-1 col-xs-1"></div>
	<div class="col-md-10 col-sm-10 col-xs-10" style="padding:0%;">
		<h3 style="font-size:18px;padding-bottom:0%;margin-bottom:0%;text-align:center;color:#337ab7;">Ajouter une question</h3>
		<hr/>
		<form action="<?php echo URL . 'AptitudeQuestion/Add'; ?>" method="post" class="form-horizontal question-form-apt question-form-add-apt">
			<div class="form-group">
				<div class="col-md-2 col-sm-2 col-xs-2">
					<label class="control-label" style="text-align:right;">Numéro <span style="color:#f55;">*</span></label>
					<input type="text" class="qst_num" name="qst_num" id="qst_num" required value="<?php echo $qst_idd; ?>" autocomplete="off"/>
				</div>
				<div class="col-md-10 col-sm-10 col-xs-10">
					<label class="control-label" style="text-align:right;">Question <span style="color:#f55;">*</span></label>
					<textarea class="qst_desc" name="qst_desc" id="qst_desc" rows="4" required></textarea>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-4 col-sm-4 col-xs-4">
					<label class="control-label" style="text-align:right;">Type <span style="color:#f55;">*</span></label>
					<select name="qst_tpe" id="qst_tpe" required>
						<option value=""></option>
						<?php foreach ($typeNames as $key => $typeName) { ?>
							<option value="<?php echo $typeName->type_nom; ?>"><?php echo ucfirst($typeName->type_nom); ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<br><hr><br>
			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-6">
					<div class="option-form thumbnail">
						<fieldset class="scheduler-border">
							<legend class="scheduler-border">Option A <span style="color:#f55;">*</span></legend>
							<textarea class="qst_op1" name="qst_op1" id="qst_op1" rows="5" required></textarea>
							<div class="row">
								<div class="col-md-2 col-sm-2 col-xs-2 option-value">
									<label class="control-label">Valeur <span style="color:#f55;">*</span></label>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-4 input-value-adjust">
									<input type="text" class="qst_val_op1" name="qst_val_op1" id="qst_val_op1" autocomplete="off" required/>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-4">
									<label class="control-label" style="float:right;">Code option <span style="color:#f55;">*</span></label>
								</div>
								<div class="col-md-2 col-sm-2 col-xs-2 input-value-adjust">
									<select class="code-option" name="code_option_a">
										<option></option>
										<option value="j">J</option>
										<option value="v">V</option>
										<option value="b">B</option>
										<option value="r">R</option>
									</select>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6">
					<div class="option-form thumbnail">
						<fieldset class="scheduler-border">
							<legend class="scheduler-border">Option B <span style="color:#f55;">*</span></legend>
							<textarea class="qst_op2" name="qst_op2" id="qst_op2" rows="5" required></textarea>
							<div class="row">
								<div class="col-md-2 col-sm-2 col-xs-2 option-value">
									<label class="control-label">Valeur <span style="color:#f55;">*</span></label>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-4 input-value-adjust">
									<input type="text" class="qst_val_op2" name="qst_val_op2" id="qst_val_op2" autocomplete="off" required/>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-4">
									<label class="control-label" style="float:right;">Code option <span style="color:#f55;">*</span></label>
								</div>
								<div class="col-md-2 col-sm-2 col-xs-2 input-value-adjust">
									<select class="code-option" name="code_option_b">
										<option></option>
										<option value="j">J</option>
										<option value="v">V</option>
										<option value="b">B</option>
										<option value="r">R</option>
									</select>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-6">
					<div class="option-form thumbnail">
						<fieldset class="scheduler-border">
							<legend class="scheduler-border">Option C <span style="color:#f55;">*</span></legend>
							<textarea class="qst_op3" name="qst_op3" id="qst_op3" rows="4" required></textarea>
							<div class="row">
								<div class="col-md-2 col-sm-2 col-xs-2 option-value">
									<label class="control-label">Valeur <span style="color:#f55;">*</span></label>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-4 input-value-adjust">
									<input type="text" class="qst_val_op3" name="qst_val_op3" id="qst_val_op3" autocomplete="off" required/>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-4">
									<label class="control-label" style="float:right;">Code option <span style="color:#f55;">*</span></label>
								</div>
								<div class="col-md-2 col-sm-2 col-xs-2 input-value-adjust">
									<select class="code-option" name="code_option_c">
										<option></option>
										<option value="j">J</option>
										<option value="v">V</option>
										<option value="b">B</option>
										<option value="r">R</option>
									</select>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6">
					<div class="option-form thumbnail">
						<fieldset class="scheduler-border">
							<legend class="scheduler-border">Option D <span style="color:#f55;">*</span></legend>
							<textarea class="qst_op4" name="qst_op4" id="qst_op4" rows="4" required></textarea>
							<div class="row">
								<div class="col-md-2 col-sm-2 col-xs-2 option-value">
									<label class="control-label">Valeur <span style="color:#f55;">*</span></label>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-4 input-value-adjust">
									<input type="text" class="qst_val_op4" name="qst_val_op4" id="qst_val_op4" autocomplete="off" required/>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-4">
									<label class="control-label" style="float:right;">Code option <span style="color:#f55;">*</span></label>
								</div>
								<div class="col-md-2 col-sm-2 col-xs-2 input-value-adjust">
									<select class="code-option" name="code_option_d">
										<option></option>
										<option value="j">J</option>
										<option value="v">V</option>
										<option value="b">B</option>
										<option value="r">R</option>
									</select>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-6">
					<label class="control-label" style="text-align:right;">Importance</label>
					<textarea class="qst_imp" name="qst_imp" id="qst_imp" rows="4"></textarea>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6">
					<label class="control-label" style="text-align:right;">Explication</label>
					<textarea class="qst_expl" name="qst_expl" id="qst_expl" rows="4"></textarea>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<!-- <button class="button solid-button question-button-add-apt" type="button">Ajouter</button> -->
					<input type="submit" class="button solid-button question-button-add aptitude" type="button" value="Ajouter">	
				</div>
			</div>
			<div class="form-group">
				<div class="contact-loading alert alert-info form-alert">
					<span class="message">Chargement...</span>
					<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
				</div>
				<div class="contact-success alert alert-success form-alert">
					<span class="message">Succès!</span>
					<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
				</div>
				<div class="contact-error alert alert-danger form-alert">
					<span class="message">Erreur !</span>
					<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
				</div>
			</div>
		</form>
	</div>
	<div class="col-md-1 col-sm-1 col-xs-1"></div>
</div>