<div class="row" style="margin-bottom:5%;">
	<br/><br/>
	<div class="col-md-12 col-sm-12 col-xs-12" style="padding:0%;">
		<h3 style="text-align:center;width:30%;margin:auto;color:#337ab7;"><span>Détails de la question<hr/></span></h3>
		<div align="right" style="margin-right:50px;"><a href="question_apt_list.php#form">+ Ajouter une question</a></div>
		<br/>
		<form action="javascript:void(0)" method="post" class="form-horizontal question-form-apt">
			<div class="form-group">
				<div class="col-md-10 col-sm-10 col-xs-10">
					<label class="control-label" style="text-align:right;">Question <span style="color:#f55;">*</span></label>
					<textarea class="qst_desc" name="qst_desc" id="qst_desc" rows="4" required disabled><?php echo $question->qst_description; ?></textarea>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-4 col-sm-4 col-xs-4">
					<label class="control-label" style="text-align:right;">Type <span style="color:#f55;">*</span></label>
					<select name="qst_tpe" id="qst_tpe" required disabled>
						<?php foreach ($types as $key => $type) { ?>
							<?php if ($questionType===$type->type_nom) { ?>
								<option value="<?php echo $type->type_nom; ?>" selected><?php echo ucfirst($type->type_nom); ?></option>
							<?php }else{ ?>
								<option value="<?php echo $type->type_nom; ?>"><?php echo ucfirst($type->type_nom); ?></option>
							<?php } ?>
						<?php } ?>
					</select>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-6">
					<div class="option-form thumbnail">
						<fieldset class="scheduler-border">
							<legend class="scheduler-border">Option A <span style="color:#f55;">*</span></legend>
							<textarea class="qst_op1" name="qst_op1" id="qst_op1" rows="5" required disabled><?php echo $op[0]; ?></textarea>
							<div class="row">
								<div class="col-md-2 col-sm-2 col-xs-2 option-value">
									<label class="control-label">Valeur <span style="color:#f55;">*</span></label>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-4 input-value-adjust">
									<input type="text" class="qst_val_op1" name="qst_val_op1" id="qst_val_op1" required value="<?php echo $op_val[0]; ?>"  autocomplete="off" disabled/>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-4">
									<label class="control-label" style="float:right;">Code option <span style="color:#f55;">*</span></label>
								</div>
								<div class="col-md-2 col-sm-2 col-xs-2 input-value-adjust">
									<select class="code-option" name="code_option_a" disabled>
										<option></option>
										<option value="j" <?php if($cd_option[0]=='j') { echo 'selected';} ?>>J</option>
										<option value="v" <?php if($cd_option[0]=='v') { echo 'selected';} ?>>V</option>
										<option value="b" <?php if($cd_option[0]=='b') { echo 'selected';} ?>>B</option>
										<option value="r" <?php if($cd_option[0]=='r') { echo 'selected';} ?>>R</option>
									</select>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6">
					<div class="option-form thumbnail">
						<fieldset class="scheduler-border">
							<legend class="scheduler-border">Option B <span style="color:#f55;">*</span></legend>
							<textarea class="qst_op2" name="qst_op2" id="qst_op2" rows="5" required disabled><?php echo $op[1]; ?></textarea>
							<div class="row">
								<div class="col-md-2 col-sm-2 col-xs-2 option-value">
									<label class="control-label">Valeur <span style="color:#f55;">*</span></label>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-4 input-value-adjust">
									<input type="text" class="qst_val_op2" name="qst_val_op2" id="qst_val_op2" required value="<?php echo $op_val[1]; ?>"  autocomplete="off" disabled/>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-4">
									<label class="control-label" style="float:right;">Code option <span style="color:#f55;">*</span></label>
								</div>
								<div class="col-md-2 col-sm-2 col-xs-2 input-value-adjust">
									<select class="code-option" name="code_option_b" disabled>
										<option></option>
										<option value="j" <?php if($cd_option[1]=='j') { echo 'selected';} ?>>J</option>
										<option value="v" <?php if($cd_option[1]=='v') { echo 'selected';} ?>>V</option>
										<option value="b" <?php if($cd_option[1]=='b') { echo 'selected';} ?>>B</option>
										<option value="r" <?php if($cd_option[1]=='r') { echo 'selected';} ?>>R</option>
									</select>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-6">
					<div class="option-form thumbnail">
						<fieldset class="scheduler-border">
							<legend class="scheduler-border">Option C <span style="color:#f55;">*</span></legend>
							<textarea class="qst_op3" name="qst_op3" id="qst_op3" rows="4" required disabled><?php echo $op[2]; ?></textarea>
							<div class="row">
								<div class="col-md-2 col-sm-2 col-xs-2 option-value">
									<label class="control-label">Valeur <span style="color:#f55;">*</span></label>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-4 input-value-adjust">
									<input type="text" class="qst_val_op3" name="qst_val_op3" id="qst_val_op3" required value="<?php echo $op_val[2]; ?>"  autocomplete="off" disabled/>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-4">
									<label class="control-label" style="float:right;">Code option <span style="color:#f55;">*</span></label>
								</div>
								<div class="col-md-2 col-sm-2 col-xs-2 input-value-adjust">
									<select class="code-option" name="code_option_c" disabled>
										<option></option>
										<option value="j" <?php if($cd_option[2]=='j') { echo 'selected';} ?>>J</option>
										<option value="v" <?php if($cd_option[2]=='v') { echo 'selected';} ?>>V</option>
										<option value="b" <?php if($cd_option[2]=='b') { echo 'selected';} ?>>B</option>
										<option value="r" <?php if($cd_option[2]=='r') { echo 'selected';} ?>>R</option>
									</select>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6">
					<div class="option-form thumbnail">
						<fieldset class="scheduler-border">
							<legend class="scheduler-border">Option D <span style="color:#f55;">*</span></legend>
							<textarea class="qst_op4" name="qst_op4" id="qst_op4" rows="4" required disabled><?php echo $op[3]; ?></textarea>
							<div class="row">
								<div class="col-md-2 col-sm-2 col-xs-2 option-value">
									<label class="control-label">Valeur <span style="color:#f55;">*</span></label>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-4 input-value-adjust">
									<input type="text" class="qst_val_op4" name="qst_val_op4" id="qst_val_op4" required value="<?php echo $op_val[3]; ?>"  autocomplete="off" disabled/>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-4">
									<label class="control-label" style="float:right;">Code option <span style="color:#f55;">*</span></label>
								</div>
								<div class="col-md-2 col-sm-2 col-xs-2 input-value-adjust">
									<select class="code-option" name="code_option_d" disabled>
										<option></option>
										<option value="j" <?php if($cd_option[3]=='j') { echo 'selected';} ?>>J</option>
										<option value="v" <?php if($cd_option[3]=='v') { echo 'selected';} ?>>V</option>
										<option value="b" <?php if($cd_option[3]=='b') { echo 'selected';} ?>>B</option>
										<option value="r" <?php if($cd_option[3]=='r') { echo 'selected';} ?>>R</option>
									</select>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-6">
					<label class="control-label" style="text-align:right;">Importance</label>
					<textarea class="qst_imp" name="qst_imp" id="qst_imp" rows="4" disabled><?php echo $qst_importance; ?></textarea>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6">
					<label class="control-label" style="text-align:right;">Explication</label>
					<textarea class="qst_expl" name="qst_expl" id="qst_expl" rows="4" disabled><?php echo $qst_explication; ?></textarea>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<input name="thisID" type="hidden" value="<?php echo $targetID; ?>" />
					<input class="btn btn-default question-button aptitude details" type="submit" name="submit" value="Retour" />
				</div>
			</div>
			<div class="contact-loading alert alert-info form-alert">
				<span class="message">Chargement...</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
            <div class="contact-success alert alert-success form-alert">
				<span class="message">Succès!</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
			<div class="contact-error alert alert-danger form-alert">
				<span class="message">Erreur!</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
		</form>
	</div>
</div>