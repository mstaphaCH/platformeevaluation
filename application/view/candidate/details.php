<div class="row cdt-edit" style="margin-bottom:5%;">
	<h3 style="text-align:center;width:30%;margin:auto;color:#337ab7;"><span>Détails du candidat<hr/></span></h3>
	<div align="right" style="margin-right:100px;"><a href="candidat_list.php#form">+ Ajouter un candidat</a></div>
	<div class="col-md-3 col-sm-3 col-xs-3"></div>
	<div class="col-md-6 col-sm-6 col-xs-6">
		<form action="javascript:void(0)" method="post" class="form-horizontal candidate-form edit" style="margin:2% 10%;">
			<div class="form-group">
				<label class="col-md-3 col-sm-3 col-xs-3 control-label">Nom <span style="color:#f55;">*</span></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input type="text" class="candidat_firstname" name="candidat_firstname" id="candidat_firstname" value="<?php echo $candidat_firstname; ?>" required disabled/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-sm-3 col-xs-3 control-label">Prénom <span style="color:#f55;">*</span></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input type="text" class="candidat_lastname" name="candidat_lastname" id="candidat_lastname" value="<?php echo $candidat_lastname; ?>" required disabled/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-sm-3 col-xs-3 control-label">Login <span style="color:#f55;">*</span></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input type="text" class="candidat_login" name="candidat_login" id="candidat_login" value="<?php echo $candidat_login; ?>" required disabled/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-sm-3 col-xs-3 control-label">Email <span style="color:#f55;">*</span></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input type="email" class="candidat_email" name="candidat_email" id="candidat_email" value="<?php echo $candidat_email; ?>" required disabled/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 col-sm-2 col-xs-2 control-label">Numéro <span style="color:#f55;">*</span></label>
				<div class="col-md-2 col-sm-2 col-xs-2">
					<input type="text" class="an" name="an" id="an" title="Numéro" value="<?php echo $an; ?>" required disabled/>
				</div>
				<label class="col-md-2 col-sm-2 col-xs-2 control-label" style="text-align:center;">Rue <span style="color:#f55;">*</span></label>
				<div class="col-md-6 col-sm-6 col-xs-6">
					<input type="text" class="candidat_rue" name="candidat_rue" id="candidat_rue" value="<?php echo $candidat_rue; ?>" required disabled/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 col-sm-2 col-xs-2 control-label">Ville <span style="color:#f55;">*</span></label>
				<div class="col-md-3 col-sm-3 col-xs-3">
					<input type="text" class="candidat_ville" name="candidat_ville" id="candidat_ville" value="<?php echo $candidat_ville; ?>" required disabled/>
				</div>
				<label class="col-md-2 col-sm-2 col-xs-2 control-label" style="text-align:center;">Pays <span style="color:#f55;">*</span></label>
				<div class="col-md-5 col-sm-5 col-xs-5">
					<input type="text" class="candidat_pays" name="candidat_pays" id="candidat_pays" value="<?php echo $candidat_pays; ?>" required disabled/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-sm-3 col-xs-3 control-label">Téléphone</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input type="text" class="candidat_tel" name="candidat_tel" id="candidat_tel" value="<?php echo $candidat_tel; ?>" disabled/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Structure <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<select class="form-control candidat_structure" name="candidat_structure" data-style="btn-primary" disabled>
						<?php if ($this->profil === 0) { ?>
							<option value="candidats libres" selected>Candidats libres</option>
						<?php }elseif($this->profil === 1 || $this->profil === 3 || $this->profil === 4) { ?>
							<?php if ($structure != null) { ?>
								<?php if ($str_nom == $structure->str_nom) { ?>
									<option value="<?php echo $structure->str_nom; ?>" selected><?php echo ucfirst($structure->str_nom); ?></option>
								<?php }else{ ?>
									<option value="<?php echo $structure->str_nom; ?>"><?php echo ucfirst($structure->str_nom); ?></option>
								<?php } ?>
							<?php } ?>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Option d'accés</label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<select class="form-control acces" data-style="btn-primary" name="acces" disabled>
						<option value="evaluation" <?php if(isset($acs) && $acs==='evaluation') echo 'selected'; ?>>Evaluation</option>
						<option value="formation" <?php if(isset($acs) && $acs==='formation') echo 'selected'; ?>>Formation</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Session de travail <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<div class="input-daterange input-group" id="datepicker" style="margin-bottom:2px !important;">
					    <input type="text" class="input-sm form-control session_job_start edit" name="session_job_start" disabled/>
					    <span class="input-group-addon">à</span>
					    <input type="text" class="input-sm form-control session_job_end edit" name="session_job_end" disabled/>
					</div>
				</div>
				<div class="col-md-5 col-sm-5 col-xs-5"></div>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<!-- <select class="form-control session-job-edit" data-style="btn-primary" name="session_job_edit" id="session-job-edit" required>
					</select> -->
				</div>
			</div>
			<br/>
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Date de session</label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<div class="input-daterange input-group session-date" id="datepicker">
					    <input type="text" class="input-sm form-control session_start" name="session_start" value="<?php if(isset($session_start)) echo $session_start; ?>" disabled/>
					    <span class="input-group-addon">à</span>
					    <input type="text" class="input-sm form-control session_end" name="session_end" value="<?php if(isset($session_end)) echo $session_end; ?>" disabled/>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<input name="thisID" class="thisID" type="hidden" value="<?php echo $targetID; ?>" />
					<button class="btn btn-default candidate-button details" name="submit">Retour</button>
				</div>
			</div>
			<div class="contact-loading alert alert-info form-alert">
				<span class="message">Chargement...</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
			<div class="contact-success alert alert-success form-alert">
				<span class="message">Succès!</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
			<div class="contact-error alert alert-danger form-alert">
				<span class="message">Erreur!</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
		</form>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-3"></div>
</div>
