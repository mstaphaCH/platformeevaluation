<?php foreach ($active_candidate as $key => $value) { ?>
	<tr style='width:100%;'>
		<td class='first-td'>
			<div>
				<button class='button solid-button blue info-button candidat_info' id='<?php echo $value->cdt_id; ?>'>
					<i class='ion-ios-information-outline'></i>
				</button>
			</div>
		</td>
		<td><?php echo ucfirst($value->cdt_nom); ?></td>
		<td><?php echo ucfirst($value->cdt_prenom); ?></td>
		<td style='text-align:center;padding-left:0% !important;'><?php echo ucfirst($value->cdt_login); ?></td>
		<td style='text-align:center;padding-left:0% !important;'><?php echo $value->cdt_email; ?></td>
		<td style='text-align:center;padding-left:0% !important;'><?php echo $value->cdt_tel; ?></td>
		<td style='text-align:center;padding-left:0% !important;'><?php echo $this->StructureById($value->str_id)[0]->str_nom; ?></td>
		<td style='text-align:center;padding-left:0% !important;'><?php echo strftime("%d %b %Y",strtotime($value->cdt_date_creation)); ?></td>
	<?php if($this->profil === 0) { ?>
		<td class='button-operation-3'><a class='btn' role='button' href='<?php echo URL . 'Candidate/Edit/' . $value->cdt_id; ?>'><i class='ion-compose' style='color:green;'></i></a><a class='btn' role='button' href='<?php echo URL . 'Candidate/Desactive/' . $value->cdt_id; ?>'><i class='ion-log-out' style='color:blue;'></i></a><a class='btn' role='button' href='<?php echo URL . 'Candidate/Delete/' . $value->cdt_id; ?>'><i class='ion-close-circled' style='color:red;'></i></a></td>
	<?php }elseif($this->profil===1 || $this->profil===3 || $this->profil===4) { ?>
		<td class='button-operation-3'><a class='btn btn-info' role='button' href='#'>Compétences</a><a class='btn' role='button' href='<?php echo URL . 'Candidate/Edit/' . $value->cdt_id; ?>'><i class='ion-compose' style='color:green;'></i></a><a class='btn' role='button' href='<?php echo URL . 'Candidate/Delete/' . $value->cdt_id; ?>'><i class='ion-close-circled' style='color:red;'></i></a></td>
	<?php } ?>
	</tr>
<?php } ?>