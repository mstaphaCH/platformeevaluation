<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Gestion des candidats
		</h1>
		<ol class="breadcrumb pull-left">
			<li><a href="#"><i class="fa fa-users"></i> Administration</a></li>
			<li class="active">Changer le mot de passe</li>
		</ol>
	</section>
	<br><br>
	<section class="content">
		<div class="row">
			<section class="col-lg-12">
				<div class="box box-success">
					<div class="box-header with-border">
						<h3 class="box-title">Changer le mot de passe</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
						</div>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-2"></div>
								<form action="scripts/candidate/cdt_change_passwd.php" method="post" class="form-horizontal user-profil passwd">
									<div class="col-md-8">
										<div class="form-group field-input">
											<label>Nouveau mot de passe</label>
											<input type="password" class="form-control new_passwd label_better" name="new_passwd" placeholder="Nouveau mot de passe"/>
										</div>
										<div class="form-group field-input">
											<label>Nouveau mot de passe (Resaisir)</label>
											<input type="password" class="form-control new_repasswd label_better" name="new_repasswd" placeholder="Resaisir nouveau mot de passe"/>
										</div>
										<div class="form-group field-button">
											<input name="thisID" class="thisID" type="hidden" value="<?php echo $targetID; ?>" />
											<button class="btn btn-success pull-right col-md-4 cdt-passwd" type="button">Enregistrer</button>
											<button class="btn btn-primary col-md-4 cdt-passwd return" type="button">Retour</button>
										</div>
									</div>
									<div class="contact-loading alert alert-info form-alert">
										<span class="message">Chargement...</span>
										<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
									</div>
									<div class="contact-success alert alert-success form-alert">
										<span class="message">Succès!</span>
										<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
									</div>
									<div class="contact-error alert alert-danger form-alert">
										<span class="message">Erreur!</span>
										<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
									</div>
								</form>
								<div class="col-md-2"></div>
							</div>
						</div>
					</div>
					<div class="box-footer">
					</div>
				</div>
			</section>
		</div>
	</section>
</div>
