<!-- <div class="row page-content"> -->
<div class="row">
	<h3 style="font-size:20px;text-align:center;width:30%;margin:auto;color:#337ab7;"><span>Gérer un candidat<hr/></span></h3>
	<div align="right" style="margin-right:32px;"><a href="candidat_list.php#form">+ Ajouter un candidat</a></div>
	<h2 style="text-align:left;font-size:18px;margin-bottom:25px;">Candidats actifs</h2>
	<!-- Search form -->
	<form action="javascript:;" method="post" class="form-horizontal active-candidate list filter" style="font-size:12px;">
		<div class="form-group row">
			<label class="col-md-1 col-sm-1 col-xs-1 control-label" style="text-align:right;width:6%;" for="a-search-firstname">Nom</label>
			<div class="col-md-2 col-sm-2 col-xs-2">
				<input type="text" name="a_search_firstname" id="a_search_firstname">
<!-- 				<select name="a_search_firstname" id="a-search-firstname">
					<option value="">Tout</option>
					<?php foreach ($candidate_firstnames as $key => $value) { ?>
						<?php if (isset($a_search_firstname) && $a_search_firstname===$value->cdt_nom) { ?>
							<option value="<?php echo $value->cdt_nom; ?>" selected><?php echo ucfirst($value->cdt_nom); ?></option>
						<?php }else{ ?>
							<option value="<?php echo $value->cdt_nom; ?>"><?php echo ucfirst($value->cdt_nom); ?></option>
						<?php } ?>
					<?php } ?>
				</select> -->
			</div>
			<label class="col-md-1 col-sm-1 col-xs-1 control-label" style="text-align:right;width:6%;" for="a-search-lastname">Prénom</label>
			<div class="col-md-2 col-sm-2 col-xs-2">
				<input type="text" name="a_search_lastname" id="a_search_lastname">
<!-- 				<select name="a_search_lastname" id="a-search-lastname">
					<option value="">Tout</option>
					<?php foreach ($candidate_lastnames as $key => $value) { ?>
						<?php if (isset($a_search_lastname) && $a_search_lastname===$value->cdt_prenom) { ?>
							<option value="<?php echo $value->cdt_prenom; ?>" selected><?php echo ucfirst($value->cdt_prenom); ?></option>
						<?php }else{ ?>
							<option value="<?php echo $value->cdt_prenom; ?>"><?php echo ucfirst($value->cdt_prenom); ?></option>
						<?php } ?>
					<?php } ?>
				</select> -->
			</div>
			<label class="col-md-1 col-sm-1 col-xs-1 control-label" style="text-align:right;width:6%;" for="a-search-email">Email</label>
			<div class="col-md-2 col-sm-2 col-xs-2">
				<input type="text" name="a_search_email" id="a_search_email">
<!-- 				<select name="a_search_email" id="a-search-email">
					<option value="">Tout</option>
					<?php foreach ($candidate_emails as $key => $value) { ?>
						<?php if (isset($a_search_email) && $a_search_email===$value->cdt_email) { ?>
							<option value="<?php echo $value->cdt_email; ?>" selected><?php echo ucfirst($value->cdt_prenom); ?></option>
						<?php }else{ ?>
							<option value="<?php echo $value->cdt_email; ?>"><?php echo ucfirst($value->cdt_email); ?></option>
						<?php } ?>
					<?php } ?>
				</select> -->
			</div>
			<label class="col-md-1 col-sm-1 col-xs-1 control-label" style="text-align:right;width:6%;" for="a-search-structure">Structure</label>
			<div class="col-md-2 col-sm-2 col-xs-2">
				<select name="a_search_structure" id="a_search_structure">
					<option value="">Tout</option>
					<?php foreach ($candidate_structures as $key => $value) { ?>
						<?php if (isset($a_search_structure) && $a_search_structure===$value->str_nom) { ?>
							<option value="<?php echo $value->str_nom; ?>" selected><?php echo ucfirst($value->str_nom); ?></option>
						<?php }else{ ?>
							<option value="<?php echo $value->str_nom; ?>"><?php echo ucfirst($value->str_nom); ?></option>
						<?php } ?>
					<?php } ?>
				</select>
			</div>
			<div class="col-md-1 col-sm-1 col-xs-1">
				<input type="hidden" name="candidate" value="active">
				 <input class="btn btn-default search-form" type="submit" name="active_search" id="search" value="Rechercher"/> 
			</div>
		</div>
	</form><!-- End search form -->
</div>
<div class="row">
	<div class="horizontal">
		<table id="activeTable" class="table table-striped table-bordered table-hover table-condensed table-hide-1">
			<thead>
				<tr style="width:100%;">
					<th style="color:#31b0d5; text-align:center;min-width:30px;"><strong><a style='padding:0px;' onmousedown='return false;' class='btn btn-plus' role='button'><i class="ion-minus-round" style="margin:0% !important;color:#31b0d5;"></i></a></strong></th>
					<th style="color:#31b0d5; text-align:center;width:13%;"><strong>Nom</strong></th>
					<th style="color:#31b0d5; text-align:center;width:13%;"><strong>Prénom</strong></th>
					<th style="color:#31b0d5; text-align:center;width:13%;"><strong>Login</strong></th>
					<th style="color:#31b0d5; text-align:center;width:13%;"><strong>Email</strong></th>
					<th style="color:#31b0d5; text-align:center;width:13%;"><strong>Téléphone</strong></th>
					<th style="color:#31b0d5; text-align:center;width:13%;"><strong>Structure</strong></th>
					<th style="color:#31b0d5; text-align:center;width:13%;"><strong>Date de création</strong></th>
					<th style="color:#31b0d5; text-align:center;width:2%;"><strong>Opération</strong></th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($active_candidate as $key => $value) { ?>
				<tr style='width:100%;'>
					<td class='first-td'>
						<div>
							<button class='button solid-button blue info-button candidat_info' id='<?php echo $value->cdt_id; ?>'>
								<i class='ion-ios-information-outline'></i>
							</button>
						</div>
					</td>
					<td><?php echo ucfirst($value->cdt_nom); ?></td>
					<td><?php echo ucfirst($value->cdt_prenom); ?></td>
					<td style='text-align:center;padding-left:0% !important;'><?php echo ucfirst($value->cdt_login); ?></td>
					<td style='text-align:center;padding-left:0% !important;'><?php echo $value->cdt_email; ?></td>
					<td style='text-align:center;padding-left:0% !important;'><?php echo $value->cdt_tel; ?></td>
					<td style='text-align:center;padding-left:0% !important;'><?php echo $this->StructureById($value->str_id)[0]->str_nom; ?></td>
					<td style='text-align:center;padding-left:0% !important;'><?php echo strftime("%d %b %Y",strtotime($value->cdt_date_creation)); ?></td>
				<?php if($this->profil === 0) { ?>
					<td class='button-operation-3'><a class='btn' role='button' href='<?php echo URL . 'Candidate/Edit/' . $value->cdt_id; ?>'><i class='ion-compose' style='color:green;'></i></a><a class='btn' role='button' href='<?php echo URL . 'Candidate/Desactive/' . $value->cdt_id; ?>'><i class='ion-log-out' style='color:blue;'></i></a><a class='btn candidate delete' id="<?php echo $value->cdt_id; ?>" role="button"><i class='ion-close-circled' style='color:red;'></i></a></td>
				<?php }elseif($this->profil===1 || $this->profil===3 || $this->profil===4) { ?>
					<td class='button-operation-3'><a class='btn btn-info' role='button' href='#'>Compétences</a><a class='btn' role='button' href='<?php echo URL . 'Candidate/Edit/' . $value->cdt_id; ?>'><i class='ion-compose' style='color:green;'></i></a><a class='btn candidate delete' id="<?php echo $value->cdt_id; ?>" role="button"><i class='ion-close-circled' style='color:red;'></i></a></td>
				<?php } ?>
				</tr>
			<?php } ?>
			</tbody>
		</table>
	</div>
</div>

<div class="row">
	<br/>
	<h2 style="text-align:left;font-size:18px;margin-bottom:25px;">Candidats non actifs</h2>
	<!-- Search form -->
	<form action="javascript:;" method="post" class="form-horizontal inactive-candidate list filter" style="font-size:12px;">
		<div class="form-group row">
			<label class="col-md-1 col-sm-1 col-xs-1 control-label" style="text-align:right;width:6%;" for="na-search-firstname">Nom</label>
			<div class="col-md-2 col-sm-2 col-xs-2">
				<input type="text" name="na_search_firstname" id="na_search_firstname">
<!-- 				<select name="na_search_firstname" id="na-search-firstname">
					<option value="">Tout</option>
					<?php foreach ($candidate_firstnames as $key => $value) { ?>
						<?php if (isset($na_search_firstname) && $na_search_firstname===$value->cdt_nom) { ?>
							<option value="<?php echo $value->cdt_nom; ?>" selected><?php echo ucfirst($value->cdt_nom); ?></option>
						<?php }else{ ?>
							<option value="<?php echo $value->cdt_nom; ?>"><?php echo ucfirst($value->cdt_nom); ?></option>
						<?php } ?>
					<?php } ?>
				</select> -->
			</div>
			<label class="col-md-1 col-sm-1 col-xs-1 control-label" style="text-align:right;width:6%;" for="na-search-lastname">Prénom</label>
			<div class="col-md-2 col-sm-2 col-xs-2">
				<input type="text" name="na_search_lastname" id="na_search_lastname">
<!-- 				<select name="na_search_lastname" id="na-search-lastname">
					<option value="">Tout</option>
					<?php foreach ($candidate_lastnames as $key => $value) { ?>
						<?php if (isset($na_search_lastname) && $na_search_lastname===$value->cdt_prenom) { ?>
							<option value="<?php echo $value->cdt_prenom; ?>" selected><?php echo ucfirst($value->cdt_prenom); ?></option>
						<?php }else{ ?>
							<option value="<?php echo $value->cdt_prenom; ?>"><?php echo ucfirst($value->cdt_prenom); ?></option>
						<?php } ?>
					<?php } ?>
				</select> -->
			</div>
			<label class="col-md-1 col-sm-1 col-xs-1 control-label" style="text-align:right;width:6%;" for="na-search-email">Email</label>
			<div class="col-md-2 col-sm-2 col-xs-2">
				<input type="text" name="na_search_email" id="na_search_email">
<!-- 				<select name="na_search_email" id="na-search-email">
					<option value="">Tout</option>
					<?php foreach ($candidate_emails as $key => $value) { ?>
						<?php if (isset($na_search_email) && $na_search_email===$value->cdt_email) { ?>
							<option value="<?php echo $value->cdt_email; ?>" selected><?php echo ucfirst($value->cdt_prenom); ?></option>
						<?php }else{ ?>
							<option value="<?php echo $value->cdt_email; ?>"><?php echo ucfirst($value->cdt_email); ?></option>
						<?php } ?>
					<?php } ?>
				</select> -->
			</div>
			<label class="col-md-1 col-sm-1 col-xs-1 control-label" style="text-align:right;width:6%;" for="na-search-structure">Structure</label>
			<div class="col-md-2 col-sm-2 col-xs-2">
				<select name="na_search_structure" id="na_search_structure">
					<option value="">Tout</option>
					<?php foreach ($candidate_structures as $key => $value) { ?>
						<?php if (isset($na_search_structure) && $na_search_structure===$value->str_nom) { ?>
							<option value="<?php echo $value->str_nom; ?>" selected><?php echo ucfirst($value->str_nom); ?></option>
						<?php }else{ ?>
							<option value="<?php echo $value->str_nom; ?>"><?php echo ucfirst($value->str_nom); ?></option>
						<?php } ?>
					<?php } ?>
				</select>
			</div>
			<div class="col-md-1 col-sm-1 col-xs-1">
				<input type="hidden" name="candidate" value="inactive">
				<!-- <input class="btn btn-default search-form" type="submit" name="non_active_search" id="search" value="Rechercher"/> -->
			</div>
		</div>
	</form><!-- End search form -->
</div>
<div class="row">
	<div class="horizontal">
		<table id="inactiveTable" class="table table-striped table-bordered table-hover table-condensed table-hide-2">
			<thead>
				<tr style="width:100%;">
					<th style="color:#31b0d5; text-align:center;min-width:30px;"><strong><a style='padding:0px;' onmousedown='return false;' class='btn btn-plus' role='button'><i class="ion-minus-round" style="margin:0% !important;color:#31b0d5;"></i></a></strong></th>
					<th style="color:#31b0d5; text-align:center;width:13%;"><strong>Nom</strong></th>
					<th style="color:#31b0d5; text-align:center;width:13%;"><strong>Prénom</strong></th>
					<th style="color:#31b0d5; text-align:center;width:13%;"><strong>Login</strong></th>
					<th style="color:#31b0d5; text-align:center;width:13%;"><strong>Email</strong></th>
					<th style="color:#31b0d5; text-align:center;width:13%;"><strong>Téléphone</strong></th>
					<th style="color:#31b0d5; text-align:center;width:13%;"><strong>Structure</strong></th>
					<th style="color:#31b0d5; text-align:center;width:13%;"><strong>Date de création</strong></th>
					<th style="color:#31b0d5; text-align:center;width:2%;"><strong>Opération</strong></th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($inactive_candidate as $key => $value) { ?>
				<tr style='width:100%;'>
					<td class='first-td'>
						<div>
							<button class='button solid-button blue info-button candidat_info' id='<?php echo $value->cdt_id; ?>'>
								<i class='ion-ios-information-outline'></i>
							</button>
						</div>
					</td>
					<td><?php echo ucfirst($value->cdt_nom); ?></td>
					<td><?php echo ucfirst($value->cdt_prenom); ?></td>
					<td style='text-align:center;padding-left:0% !important;'><?php echo ucfirst($value->cdt_login); ?></td>
					<td style='text-align:center;padding-left:0% !important;'><?php echo $value->cdt_email; ?></td>
					<td style='text-align:center;padding-left:0% !important;'><?php echo $value->cdt_tel; ?></td>
					<td style='text-align:center;padding-left:0% !important;'><?php echo $this->StructureById($value->str_id)[0]->str_nom; ?></td>
					<td style='text-align:center;padding-left:0% !important;'><?php echo strftime("%d %b %Y",strtotime($value->cdt_date_creation)); ?></td>
				<?php if($this->profil === 0) { ?>
					<td class='button-operation-3'><a class='btn' role='button' href='<?php echo URL . 'Candidate/Edit/' . $value->cdt_id; ?>'><i class='ion-compose' style='color:green;'></i></a><a class='btn' role='button' href='<?php echo URL . 'Candidate/Active/' . $value->cdt_id; ?>'><i class='ion-log-in' style='color:blue;'></i></a><a class='btn candidate delete' id="<?php echo $value->cdt_id; ?>" role="button"><i class='ion-close-circled' style='color:red;'></i></a></td>
				<?php }elseif($this->profil===1 || $this->profil===3 || $this->profil===4) { ?>
					<td class='button-operation-3'><a class='btn btn-info' role='button' href='#'>Compétences</a><a class='btn' role='button' href='<?php echo URL . 'Candidate/Edit/' . $value->cdt_id; ?>'><i class='ion-compose' style='color:green;'></i></a><a class='btn candidate delete' id="<?php echo $value->cdt_id; ?>" role="button"><i class='ion-close-circled' style='color:red;'></i></a></td>
				<?php } ?>
				</tr>
			<?php } ?>
			</tbody>
		</table>
	</div>
</div>
<div class="row" style="margin-bottom:5%;">
	<hr id="form"/>
	<div class="col-md-6 col-sm-6 col-xs-6 col-md-offset-3" style="padding:0%;">
		<h3 style="padding-bottom:0%;margin-bottom:0%;text-align:center;color:#337ab7;">Ajouter un candidat</h3>
		<hr/>
		<br/><br/>
		<form action="<?php echo URL . 'Candidate/Add'; ?>" method="post" class="form-horizontal candidate-form add">
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Nom <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input type="text" class="candidat_firstname" name="candidat_firstname" required/>
					<div id="cdt-livesearch"></div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Prénom <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input type="text" class="candidat_lastname" name="candidat_lastname" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Login <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input type="text" class="candidat_login" name="candidat_login" autocomplete="off" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Mot de passe <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input type="password" class="candidat_passwd" name="candidat_passwd" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Mot de passe (Resaisir) <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input type="password" class="candidat_repasswd" name="candidat_repasswd" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Email <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input type="email" class="candidat_email" name="candidat_email" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 col-sm-2 col-xs-2 control-label">Numéro <span style="color:#f55;">*</span></label>
				<div class="col-md-2 col-sm-2 col-xs-2">
					<input type="text" class="an" name="an" title="Numéro" autocomplete="off" required/>
				</div>
				<label class="col-md-1 col-sm-1 col-xs-1 control-label" style="text-align:center;">Rue <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input type="text" class="candidat_rue" name="candidat_rue" autocomplete="off" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 col-sm-2 col-xs-2 control-label">Ville <span style="color:#f55;">*</span></label>
				<div class="col-md-3 col-sm-3 col-xs-3">
					<input type="text" class="candidat_ville" name="candidat_ville" autocomplete="off" required/>
				</div>
				<label class="col-md-2 col-sm-2 col-xs-2 control-label" style="text-align:center;">Pays <span style="color:#f55;">*</span></label>
				<div class="col-md-5 col-sm-5 col-xs-5">
					<input type="text" class="candidat_pays" name="candidat_pays" autocomplete="off" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Téléphone</label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input type="text" class="candidat_tel" name="candidat_tel" autocomplete="off"/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Structure <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<select class="form-control candidat_structure" name="candidat_structure" data-style="btn-primary">
						<?php if ($this->profil===0) { ?>
							<option value="candidats libres" selected>Candidats libres</option>
						<?php }elseif($this->profil===1 || $this->profil===3 || $this->profil===4) { ?>
							<option value="<?php $CandidateController->StructureById($strutil_id)[0]['str_nom']; ?>">
								<?php echo ucfirst($CandidateController->StructureById($strutil_id)[0]['str_nom']); ?>
							</option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Option d'accès <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<select class="form-control acces" data-style="btn-primary" name="acces" required>
						<option value=""></option>
						<option value="evaluation">Evaluation</option>
						<option value="formation">Formation</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Session de travail <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<div class="input-daterange input-group" id="datepicker" style="margin-bottom:2px !important;">
					    <input type="text" class="input-sm form-control session_job_start add" name="session_job_start" />
					    <span class="input-group-addon">à</span>
					    <input type="text" class="input-sm form-control session_job_end add" name="session_job_end" />
					</div>
				</div>
				<div class="col-md-5 col-sm-5 col-xs-5"></div>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<select class="form-control session-job-add" data-style="btn-primary" name="session_job_add" id="session-job-add" required>
					</select>
				</div>
			</div>
			<br/>
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Date de session <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<div class="input-daterange session-date input-group" id="datepicker">
					    <input type="text" class="input-sm form-control session_start" name="session_start" />
					    <span class="input-group-addon">à</span>
					    <input type="text" class="input-sm form-control session_end" name="session_end" />
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-12 col-sm-12 col-xs-12">
						<input class="btn btn-default evaluation-button-add" type="submit" name="submit" value="Ajouter">
				</div>
			</div>
			<div class="contact-loading alert alert-info form-alert">
				<span class="message">Chargement...</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
			<div class="contact-success alert alert-success form-alert">
				<span class="message">Succès!</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
			<div class="contact-error alert alert-danger form-alert">
				<span class="message">Erreur!</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
		</form>
	</div>
</div>
<!-- </div> -->