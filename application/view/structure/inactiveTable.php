<?php foreach ($inactive_structures as $key => $value) { ?>
<tr style='width:100%;'>
	<td class='first-td'><div><button class='button solid-button blue info-button structure_info' id="<?php echo $value->str_id; ?>"><i class='ion-ios-information-outline'></i></button></div></td>
	<?php if($value->str_status ==1) { ?>
		<td style='padding-left: 10px !important;padding-top:0px;padding-bottom:0px;'><a class='btn struct_status' role='button' id='str<?php echo $value->str_id; ?>' name='<?php echo $value->str_id; ?>'><i class='ion-ios-checkmark-outline'></i></a><?php echo ucfirst($value->str_nom); ?></td>
	<?php }elseif($value->str_status==0) { ?>
		<td style='padding-left: 10px !important;padding-top:0px;padding-bottom:0px;'><a class='btn struct_status' role='button' id='str<?php echo $value->str_id; ?>' name='<?php echo $value->str_id; ?>'><i class='ion-ios-circle-outline'></i></a><?php echo ucfirst($value->str_nom); ?></td>
	<?php } ?>
	<td><?php echo $value->str_tel; ?></td>
	<td style='text-align:center;padding-left:0% !important;'><?php echo $value->str_siteweb; ?></td>
	<td style='text-align:center;padding-left:0% !important;'><?php echo $this->getCity($value->adr_id)->adr_ville; ?></td>
	<td style='text-align:center;padding-left:0% !important;'><?php echo $this->getCountry($value->adr_id)->adr_pays; ?></td>
	<td class='user-listb'><a class='btn' role='button' href="<?php echo URL . 'Structure/Edit/' . $value->str_id; ?>"><i class='ion-compose' style='color:green;'></i></a><a class='btn active-structure' id='<?php echo $value->str_id; ?>' role='button'><i class='ion-log-in' style='color:blue;'></i></a><a class='btn structure delete' id="<?php echo $value->str_id; ?>" role="button"><i class='ion-close-circled' style='color:red;'></i></a></td>
</tr>
<?php } ?>
