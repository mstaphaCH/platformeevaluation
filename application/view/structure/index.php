<div class="row">
	<h3 style="text-align:center;width:30%;margin:auto;color:#337ab7;"><span>Gérer une structure<hr/></span></h3>
	<div align="right" style="margin-right:32px;"><a href="#form" style="color:#337ab7;">+ Ajouter une structure</a></div>
	<br/>
	<h2 style="text-align:left;font-size:18px;margin-bottom:25px;">Structures actifs</h2>	
	<!-- Search form -->
	<form action="" method="post" class="form-horizontal structure-filter active" style="font-size:12px;">
		<div class="form-group row">
			<label class="col-md-1 col-sm-1 col-xs-1 control-label" style="text-align:right;width:6%;" for="search-name">Nom</label>
			<div class="col-md-2 col-sm-2 col-xs-2">
				<select name="search_name" id="search-name">
					<option value="">Tout</option>
					<?php foreach ($structure_titles as $key => $value) { ?>
						<?php if (isset($search_name) && $search_name===$value->str_nom) { ?>
							<option value="<?php echo $value->str_nom; ?>" selected><?php echo ucfirst($value->str_nom); ?></option>
						<?php }else{ ?>
							<option value="<?php echo $value->str_nom; ?>"><?php echo ucfirst($value->str_nom); ?></option>
						<?php  } ?>
					<?php  } ?>
				</select>
			</div>
			<label class="col-md-1 col-sm-1 col-xs-1 control-label" style="text-align:right;width:6%;" for="search-country">Pays</label>
			<div class="col-md-2 col-sm-2 col-xs-2">
				<select name="search_country" id="search-country">
					<option value="">Tout</option>
					<?php foreach ($countries as $key => $value) { ?>
						<?php if (!empty($value->adr_pays)) { ?>
							<?php if (isset($search_country) && $search_country===$value->adr_pays) { ?>
								<option value="<?php echo $value->adr_pays; ?>" selected><?php echo ucfirst($value->adr_pays); ?></option>
							<?php }else{ ?>
								<option value="<?php echo $value->adr_pays; ?>"><?php echo ucfirst($value->adr_pays); ?></option>
							<?php } ?>
						<?php } ?>
					<?php } ?>
				</select>
			</div>
			<label class="col-md-1 col-sm-1 col-xs-1 control-label" style="text-align:right;width:6%;" for="search-city">Ville</label>
			<div class="col-md-2 col-sm-2 col-xs-2">
				<select name="search_city" id="search-city">
					<option value="">Tout</option>
					<?php foreach ($cities as $key => $value) { ?>
						<?php if (!empty($value->adr_ville)) { ?>
							<?php if (isset($search_city) && $search_city===$value->adr_ville) { ?>
								<option value="<?php echo $value->adr_ville; ?>" selected><?php echo ucfirst($value->adr_ville); ?></option>
							<?php }else{ ?>
								<option value="<?php echo $value->adr_ville; ?>"><?php echo ucfirst($value->adr_ville); ?></option>
							<?php } ?>
						<?php } ?>
					<?php } ?>
				</select>
			</div>
			<div class="col-md-1 col-sm-1 col-xs-1">
				<input type="hidden" name="structure" value="active">
				<input class="btn btn-default structure search" type="submit" name="search" id="search" value="Rechercher"/>
			</div>
		</div>
	</form>
</div>

<div class="row">
	<div class="horizontal">
		<table class="table table-striped table-bordered table-condensed table-hide" id="active-table">
			<thead>
				<tr style="width:100%;">
					<th style="color:#31b0d5; text-align:center; min-width:30px;"><strong><a style='padding:0px;' onmousedown='return false;' class='btn btn-plus' role='button'><i class="ion-minus-round" style="margin:0% !important;color:#31b0d5;"></i></a></strong></th>
					<th class="col-md-3" style="color:#31b0d5; text-align:center;"><strong>Structure</strong></th>
					<th class="col-md-2" style="color:#31b0d5; text-align:center;"><strong>Téléphone</strong></th>
					<th class="col-md-2" style="color:#31b0d5; text-align:center;"><strong>Site Web</strong></th>
					<th class="col-md-2" style="color:#31b0d5; text-align:center;"><strong>Ville</strong></th>
					<th class="col-md-2" style="color:#31b0d5; text-align:center;"><strong>Pays</strong></th>
					<th class="col-md-1" style="color:#31b0d5; text-align:center;"><strong>Opération</strong></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($active_structures as $key => $value) { ?>
				<tr style='width:100%;'>
					<td class='first-td'><div><button class='button solid-button blue info-button structure_info' id="<?php echo $value->str_id; ?>"><i class='ion-ios-information-outline'></i></button></div></td>
					<?php if($value->str_status ==1) { ?>
						<td style='padding-left: 10px !important;padding-top:0px;padding-bottom:0px;'><a class='btn struct_status' role='button' id='str<?php echo $value->str_id; ?>' name='<?php echo $value->str_id; ?>'><i class='ion-ios-checkmark-outline'></i></a><?php echo ucfirst($value->str_nom); ?></td>
					<?php }elseif($value->str_status==0) { ?>
						<td style='padding-left: 10px !important;padding-top:0px;padding-bottom:0px;'><a class='btn struct_status' role='button' id='str<?php echo $value->str_id; ?>' name='<?php echo $value->str_id; ?>'><i class='ion-ios-circle-outline'></i></a><?php echo ucfirst($value->str_nom); ?></td>
					<?php } ?>
					<td><?php echo $value->str_tel; ?></td>
					<td style='text-align:center;padding-left:0% !important;'><?php echo $value->str_siteweb; ?></td>
					<td style='text-align:center;padding-left:0% !important;'><?php echo $this->getCity($value->adr_id)->adr_ville; ?></td>
					<td style='text-align:center;padding-left:0% !important;'><?php echo $this->getCountry($value->adr_id)->adr_pays; ?></td>
					<td class='user-listb'><a class='btn' role='button' href="<?php echo URL . 'Structure/Edit/' . $value->str_id; ?>"><i class='ion-compose' style='color:green;'></i></a><a class='btn active-structure' id='<?php echo $value->str_id; ?>' role='button'><i class='ion-log-out' style='color:blue;'></i></a><a class='btn structure delete' id="<?php echo $value->str_id; ?>" role="button"><i class='ion-close-circled' style='color:red;'></i></a></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>

<div class="row">
	<h2 style="text-align:left;font-size:18px;margin-bottom:25px;">Structures non actifs</h2>	
	
	<form action="" method="post" class="form-horizontal structure-filter inactive" style="font-size:12px;">
		<div class="form-group row">
			<label class="col-md-1 col-sm-1 col-xs-1 control-label" style="text-align:right;width:6%;" for="search-name">Nom</label>
			<div class="col-md-2 col-sm-2 col-xs-2">
				<select name="search_name" id="search-name">
					<option value="">Tout</option>
					<?php foreach ($structure_titles as $key => $value) { ?>
						<?php if (isset($search_name) && $search_name===$value->str_nom) { ?>
							<option value="<?php echo $value->str_nom; ?>" selected><?php echo ucfirst($value->str_nom); ?></option>
						<?php }else{ ?>
							<option value="<?php echo $value->str_nom; ?>"><?php echo ucfirst($value->str_nom); ?></option>
						<?php  } ?>
					<?php  } ?>
				</select>
			</div>
			<label class="col-md-1 col-sm-1 col-xs-1 control-label" style="text-align:right;width:6%;" for="search-country">Pays</label>
			<div class="col-md-2 col-sm-2 col-xs-2">
				<select name="search_country" id="search-country">
					<option value="">Tout</option>
					<?php foreach ($countries as $key => $value) { ?>
						<?php if (!empty($value->adr_pays)) { ?>
							<?php if (isset($search_country) && $search_country===$value->adr_pays) { ?>
								<option value="<?php echo $value->adr_pays; ?>" selected><?php echo ucfirst($value->adr_pays); ?></option>
							<?php }else{ ?>
								<option value="<?php echo $value->adr_pays; ?>"><?php echo ucfirst($value->adr_pays); ?></option>
							<?php } ?>
						<?php } ?>
					<?php } ?>
				</select>
			</div>
			<label class="col-md-1 col-sm-1 col-xs-1 control-label" style="text-align:right;width:6%;" for="search-city">Ville</label>
			<div class="col-md-2 col-sm-2 col-xs-2">
				<select name="search_city" id="search-city">
					<option value="">Tout</option>
					<?php foreach ($cities as $key => $value) { ?>
						<?php if (!empty($value->adr_ville)) { ?>
							<?php if (isset($search_city) && $search_city===$value->adr_ville) { ?>
								<option value="<?php echo $value->adr_ville; ?>" selected><?php echo ucfirst($value->adr_ville); ?></option>
							<?php }else{ ?>
								<option value="<?php echo $value->adr_ville; ?>"><?php echo ucfirst($value->adr_ville); ?></option>
							<?php } ?>
						<?php } ?>
					<?php } ?>
				</select>
			</div>
			<div class="col-md-1 col-sm-1 col-xs-1">
				<input type="hidden" name="structure" value="inactive">
				<input class="btn btn-default structure search" type="submit" name="search" id="search" value="Rechercher"/>
			</div>
		</div>
	</form>
</div>

<div class="row">
	<div class="horizontal">
		<table class="table table-striped table-bordered table-condensed table-hide" id="inactive-table">
			<thead>
				<tr style="width:100%;">
					<th style="color:#31b0d5; text-align:center; min-width:30px;"><strong><a style='padding:0px;' onmousedown='return false;' class='btn btn-plus' role='button'><i class="ion-minus-round" style="margin:0% !important;color:#31b0d5;"></i></a></strong></th>
					<th class="col-md-3" style="color:#31b0d5; text-align:center;"><strong>Structure</strong></th>
					<th class="col-md-2" style="color:#31b0d5; text-align:center;"><strong>Téléphone</strong></th>
					<th class="col-md-2" style="color:#31b0d5; text-align:center;"><strong>Site Web</strong></th>
					<th class="col-md-2" style="color:#31b0d5; text-align:center;"><strong>Ville</strong></th>
					<th class="col-md-2" style="color:#31b0d5; text-align:center;"><strong>Pays</strong></th>
					<th class="col-md-1" style="color:#31b0d5; text-align:center;"><strong>Opération</strong></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($inactive_structures as $key => $value) { ?>
				<tr style='width:100%;'>
					<td class='first-td'><div><button class='button solid-button blue info-button structure_info' id="<?php echo $value->str_id; ?>"><i class='ion-ios-information-outline'></i></button></div></td>
					<?php if($value->str_status ==1) { ?>
						<td style='padding-left: 10px !important;padding-top:0px;padding-bottom:0px;'><a class='btn struct_status' role='button' id='str<?php echo $value->str_id; ?>' name='<?php echo $value->str_id; ?>'><i class='ion-ios-checkmark-outline'></i></a><?php echo ucfirst($value->str_nom); ?></td>
					<?php }elseif($value->str_status==0) { ?>
						<td style='padding-left: 10px !important;padding-top:0px;padding-bottom:0px;'><a class='btn struct_status' role='button' id='str<?php echo $value->str_id; ?>' name='<?php echo $value->str_id; ?>'><i class='ion-ios-circle-outline'></i></a><?php echo ucfirst($value->str_nom); ?></td>
					<?php } ?>
					<td><?php echo $value->str_tel; ?></td>
					<td style='text-align:center;padding-left:0% !important;'><?php echo $value->str_siteweb; ?></td>
					<td style='text-align:center;padding-left:0% !important;'><?php echo $this->getCity($value->adr_id)->adr_ville; ?></td>
					<td style='text-align:center;padding-left:0% !important;'><?php echo $this->getCountry($value->adr_id)->adr_pays; ?></td>
					<td class='user-listb'><a class='btn' role='button' href="<?php echo URL . 'Structure/Edit/' . $value->str_id; ?>"><i class='ion-compose' style='color:green;'></i></a><a class='btn inactive-structure' id='<?php echo $value->str_id; ?>' role='button'><i class='ion-log-in' style='color:blue;'></i></a><a class='btn structure delete' id="<?php echo $value->str_id; ?>" role="button"><i class='ion-close-circled' style='color:red;'></i></a></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>



<div class="row" style="margin-bottom:150px;">
	<hr id="form"/>
	<div class="col-md-3 col-sm-3 col-xs-3"></div>
	<div class="col-md-6 col-sm-6 col-xs-6">
		<h3 style="padding-bottom:0%;margin-bottom:0%;text-align:center;color:#337ab7;">Ajouter une structure</h3>
		<hr/>
		<br/><br/>
		<form action="<?php echo URL . 'Structure/Add'; ?>" method="post" class="form-horizontal str-form-validation">
			<div class="form-group">
				<label class="col-md-3 col-sm-3 col-xs-3 control-label">Nom <span style="color:#f55;">*</span></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input type="text" class="str_nom" name="str_nom" id="str_nom" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-sm-3 col-xs-3 control-label">Description</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<textarea class="str_description" name="str_description" id="str_description"></textarea>
				</div>
			</div>
			<br/>
			<div class="form-group">
				<label class="col-md-3 col-sm-3 col-xs-3 control-label">Nom contact <span style="color:#f55;">*</span></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input type="text" class="str_nom_contact" name="str_nom_contact" id="str_nom_contact" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-sm-3 col-xs-3 control-label">Prénom contact <span style="color:#f55;">*</span></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input type="text" class="str_prenom_contact" name="str_prenom_contact" id="str_prenom_contact" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-4 col-sm-4 col-xs-4 control-label">Adresse email contact <span style="color:#f55;">*</span></label>
				<div class="col-md-8 col-sm-8 col-xs-8">
					<input type="text" class="str_email_contact" name="str_email_contact" id="str_email_contact" required/>
				</div>
			</div>
			<br/>
			<div class="form-group">
				<label class="col-md-2 col-sm-2 col-xs-2 control-label">Numéro <span style="color:#f55;">*</span></label>
				<div class="col-md-2 col-sm-2 col-xs-2">
					<input type="text" class="an" name="an" id="an" title="Numéro" maxlength="5" required/>
				</div>
				<label class="col-md-2 col-sm-2 col-xs-2 control-label" style="text-align:center;">Rue <span style="color:#f55;">*</span></label>
				<div class="col-md-6 col-sm-6 col-xs-6">
					<input type="text" class="adr_rue" name="adr_rue" id="adr_rue" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-sm-3 col-xs-3 control-label">Code postal <span style="color:#f55;">*</span></label>
				<div class="col-md-4 col-sm-4 col-xs-4">
					<input type="text" class="adr_code_postal" name="adr_code_postal" id="adr_code_postal" required/>
				</div>
				<label class="col-md-1 col-sm-1 col-xs-1 control-label" style="text-align:center;">Ville <span style="color:#f55;">*</span></label>
				<div class="col-md-4 col-sm-4 col-xs-4">
					<input type="text" class="adr_ville" name="adr_ville" id="adr_ville" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 col-sm-2 col-xs-2 control-label">Pays <span style="color:#f55;">*</span></label>
				<div class="col-md-5 col-sm-5 col-xs-5">
					<input type="text" class="adr_pays" name="adr_pays" id="adr_pays" value="France" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-sm-3 col-xs-3 control-label">Téléphone <span style="color:#f55;">*</span></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input type="tel" class="str_tel" name="str_tel" id="str_tel" autocomplete="off" placeholder="" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-sm-3 col-xs-3 control-label">Site Web</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input type="text" class="str_siteweb" name="str_siteweb" id="str_siteweb" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<input class="btn btn-default structure-button-add" type="submit" name="submit" value="Ajouter">
				</div>
			</div>
			<div class="contact-loading alert alert-info form-alert">
				<span class="message">Chargement...</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
			<div class="contact-success alert alert-success form-alert">
				<span class="message">Succès!</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
			<div class="contact-error alert alert-danger form-alert">
				<span class="message">Erreur!</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
		</form>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-3"></div>
</div>