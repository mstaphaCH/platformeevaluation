<div class="row" style="margin-bottom:5%;">
	<h3 style="text-align:center;width:30%;margin:auto;color:#337ab7;"><span>Modifier une structure<hr/></span></h3>
	<div align="right" style="margin-right:100px;"><a href="<?php echo URL . 'Structure/#form' ?>" style="color:#337ab7;">+ Ajouter une structure</a></div>
	<div class="col-md-2 col-sm-2 col-xs-2"></div>
	<div class="col-md-8 col-sm-8 col-xs-8">
		<form action="<?php echo URL . 'Structure/Update' ?>" method="post" class="form-horizontal str-form-validation" style="margin:0% 20%;">
			<div class="form-group">
				<label class="col-md-3 col-sm-3 col-xs-3 control-label">Nom <span style="color:#f55;">*</span></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input type="text" class="str_nom" id="str_nom" <?php if($this->structure->str_nom !== 'candidats libres' && $this->structure->str_nom!=='excellium consulting') echo 'name="str_nom"'; ?> value="<?php echo $this->structure->str_nom; ?>" required <?php if($this->structure->str_nom==='candidats libres' || $this->structure->str_nom==='excellium consulting') echo 'disabled '; ?>/>
					<?php 
					if($this->structure->str_nom==='candidats libres' || $this->structure->str_nom==='excellium consulting')
						echo '<input type="hidden" name="str_nom" value="'.$this->structure->str_nom.'"/>';
					?>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-sm-3 col-xs-3 control-label">Description</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<textarea class="str_description" name="str_description" id="str_description" ><?php echo $this->structure->str_description; ?></textarea>
				</div>
			</div>
			<br/>
			<div class="form-group">
				<label class="col-md-3 col-sm-3 col-xs-3 control-label">Nom contact <span style="color:#f55;">*</span></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input type="text" class="str_nom_contact" name="str_nom_contact" id="str_nom_contact" value="<?php echo $this->structure->str_nom_contact; ?>" required />
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-sm-3 col-xs-3 control-label">Prénom contact <span style="color:#f55;">*</span></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input type="text" class="str_prenom_contact" name="str_prenom_contact" id="str_prenom_contact" value="<?php echo $this->structure->str_prenom_contact; ?>" required />
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-4 col-sm-4 col-xs-4 control-label">Adresse email contact <span style="color:#f55;">*</span></label>
				<div class="col-md-8 col-sm-8 col-xs-8">
					<input type="text" class="str_email_contact" name="str_email_contact" id="str_email_contact" value="<?php echo $this->structure->str_email_contact; ?>" required />
				</div>
			</div>
			<br/>
			<div class="form-group">
				<label class="col-md-2 col-sm-2 col-xs-2 control-label">Numéro <span style="color:#f55;">*</span></label>
				<div class="col-md-2 col-sm-2 col-xs-2">
					<input type="text" class="an" name="an" id="an" title="Numéro" value="<?php echo $this->address->adr_num; ?>" maxlength="5" required />
				</div>
				<label class="col-md-2 col-sm-2 col-xs-2 control-label" style="text-align:center;">Rue <span style="color:#f55;">*</span></label>
				<div class="col-md-6 col-sm-6 col-xs-6">
					<input type="text" class="adr_rue" name="adr_rue" id="adr_rue" value="<?php echo $this->address->adr_rue; ?>" required />
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-sm-3 col-xs-3 control-label">Code postal <span style="color:#f55;">*</span></label>
				<div class="col-md-3 col-sm-3 col-xs-3">
					<input type="text" class="adr_code_postal" name="adr_code_postal" id="adr_code_postal" value="<?php echo $this->address->adr_code_postal; ?>" required />
				</div>
				<label class="col-md-2 col-sm-2 col-xs-2 control-label" style="text-align:center;">Ville <span style="color:#f55;">*</span></label>
				<div class="col-md-4 col-sm-4 col-xs-4">
					<input type="text" class="adr_ville" name="adr_ville" id="adr_ville" value="<?php echo $this->address->adr_ville; ?>" required />
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 col-sm-2 col-xs-2 control-label">Pays <span style="color:#f55;">*</span></label>
				<div class="col-md-5 col-sm-5 col-xs-5">
					<input type="text" class="adr_pays" name="adr_pays" id="adr_pays" value="<?php echo $this->address->adr_pays; ?>" required />
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-sm-3 col-xs-3 control-label">Téléphone</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input type="tel" class="str_tel" name="str_tel" id="str_tel" value="<?php echo $this->structure->str_tel; ?>" autocomplete="off" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-sm-3 col-xs-3 control-label">Site web</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input type="text" class="str_siteweb" name="str_siteweb" id="str_siteweb" value="<?php echo $this->structure->str_siteweb; ?>" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<input name="thisID" type="hidden" value="<?php echo $this->structure->str_id; ?>" />
					<input class="btn btn-default structure-button-edit" type="submit" name="submit" value="<?php if(isset($_GET['op']) && $_GET['op']==='inf') echo 'retour'; else echo 'Modifier';?>">
				</div>
			</div>
			<div class="contact-loading alert alert-info form-alert">
				<span class="message">Chargement...</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
			<div class="contact-success alert alert-success form-alert">
				<span class="message">Succès!</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
			<div class="contact-error alert alert-danger form-alert">
				<span class="message">Erreur!</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
		</form>
	</div>
	<div class="col-md-2 col-sm-2 col-xs-2"></div>
</div>
