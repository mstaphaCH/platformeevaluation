<div class="row" style="margin-bottom:5%;">
	<br/><br/>
	<div class="col-md-12 col-sm-12 col-xs-12" style="padding:0%;">
		<h3 style="text-align:center;width:30%;margin:auto;color:#337ab7;"><span>Modifier une question<hr/></span></h3>
		<div align="right" style="margin-right:50px;"><a href="question_list.php#form">+ Ajouter une question</a></div>
		<br/>
		<form action="<?php echo URL.'CompetenceQuestion/Update'; ?>" method="post" enctype="multipart/form-data" accept-charset="utf-8" class="form-horizontal question-form edit">
			<div class="form-group">
				<div class="col-md-10 col-sm-10 col-xs-10">
					<label class="control-label" style="text-align:right;">Question <span style="color:#f55;">*</span></label>
					<textarea class="qst_desc" name="qst_desc" id="qst_desc" rows="4" required ><?php echo $qst_description; ?></textarea>
				</div>
				<div class="col-md-2 col-sm-2 col-xs-2">
					<label class="control-label" style="text-align:right;">Image <span style="color:#f55;">*</span></label>
					<img src="<?php echo URL.'public/img/questions/'.$qst_image; ?>" style="width:150px !important;height:100px !important" class="question_image" name="question_image" id="question_image"/>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-4 col-sm-4 col-xs-4">
					<label class="control-label" style="text-align:right;">Type <span style="color:#f55;">*</span></label>
					<select name="qst_tpe" id="qst_tpe" required >
						<?php foreach ($types as $key => $type) { ?>
							<?php if($type_title === $type->type_nom) { ?>
								<option value="<?php echo $type->type_nom; ?>" selected>
									<?php echo ucfirst($type->type_nom); ?>
								</option>
							<?php }else{ ?>
								<option value="<?php echo $type->type_nom; ?>">
									<?php echo ucfirst($type->type_nom); ?>
								</option>
							<?php } ?>
						<?php } ?>
					</select>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-3">
					<label class="control-label" style="text-align:right;">Thème <span style="color:#f55;">*</span></label>
					<select name="qst_thm" id="qst_thm" required >
						<option value=""></option>
						<?php foreach ($themes as $key => $theme) { ?>
							<?php if($theme_title === $theme->thm_titre) { ?>
								<option value="<?php echo $theme->thm_titre; ?>" selected>
									<?php echo ucfirst($theme->thm_titre); ?>
								</option>
							<?php }else{ ?>
								<option value="<?php echo $theme->thm_titre; ?>">
									<?php echo ucfirst($theme->thm_titre); ?>
								</option>
							<?php } ?>
						<?php } ?>
					</select>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-3">
					<label class="control-label" style="text-align:right;">Typologie <span style="color:#f55;">*</span></label>
					<select name="question_typologie" id="question_typologie" class="question_typologie" >
						<option value=""></option>
						<?php foreach ($typologies as $key => $typology) { ?>
							<?php if($typology_title === $typology->typ_title) { ?>
								<option value="<?php echo $typology->typ_title; ?>" selected>
									<?php echo ucfirst($typology->typ_title); ?>
								</option>
							<?php }else{ ?>
								<option value="<?php echo $typology->typ_title; ?>">
									<?php echo ucfirst($typology->typ_title); ?>
								</option>
							<?php } ?>
						<?php } ?>
					</select>
				</div>
				<div class="col-md-2 col-sm-2 col-xs-2">
					<label class="control-label" style="text-align:right;">Complexité <span style="color:#f55;">*</span></label>
					<select name="qst_comp" id="qst_comp" class="qst_comp" >
						<option value=""></option>
						<?php foreach ($complexities as $key => $complexity) { ?>
							<?php if($complexity_title === $complexity->comp_title) { ?>
								<option value="<?php echo $complexity->comp_title; ?>" selected>
									<?php echo ucfirst($complexity->comp_title); ?>
								</option>
							<?php }else{ ?>
								<option value="<?php echo $complexity->comp_title; ?>">
									<?php echo ucfirst($complexity->comp_title); ?>
								</option>
							<?php } ?>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-6">
						<div class="option-form thumbnail">
							<fieldset class="scheduler-border">
								<legend class="scheduler-border">Option A <span style="color:#f55;">*</span></legend>
								<textarea class="qst_op1" name="qst_op1" id="qst_op1" rows="5" required ><?php echo $op[0]; ?></textarea>
								<div class="row">
									<div class="col-md-6"></div>
									<div class="col-md-2 col-sm-2 col-xs-2 option-value">
										<label class="control-label">Valeur <span style="color:#f55;">*</span></label>
									</div>
									<div class="col-md-4 col-sm-4 col-xs-4 input-value-adjust">
										<input type="text" class="qst_val_op1" name="qst_val_op1" id="qst_val_op1" autocomplete="off" required value="<?php echo $op_val[0]; ?>" />
									</div>
								</div>
							</fieldset>
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<div class="option-form thumbnail">
							<fieldset class="scheduler-border">
								<legend class="scheduler-border">Option B <span style="color:#f55;">*</span></legend>
								<textarea class="qst_op2" name="qst_op2" id="qst_op2" rows="5" required ><?php echo $op[1]; ?></textarea>
								<div class="row">
									<div class="col-md-6"></div>
									<div class="col-md-2 col-sm-2 col-xs-2 option-value">
										<label class="control-label">Valeur <span style="color:#f55;">*</span></label>
									</div>
									<div class="col-md-4 col-sm-4 col-xs-4 input-value-adjust">
										<input type="text" class="qst_val_op2" name="qst_val_op2" id="qst_val_op2" autocomplete="off" required value="<?php echo $op_val[1]; ?>"  />
									</div>
								</div>
							</fieldset>
						</div>
					</div>
			</div>
			<div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-6">
						<div class="option-form thumbnail">
							<fieldset class="scheduler-border">
								<legend class="scheduler-border">Option C <span style="color:#f55;">*</span></legend>
								<textarea class="qst_op3" name="qst_op3" id="qst_op3" rows="5" required ><?php echo $op[2]; ?></textarea>
								<div class="row">
									<div class="col-md-6"></div>
									<div class="col-md-2 col-sm-2 col-xs-2 option-value">
										<label class="control-label">Valeur <span style="color:#f55;">*</span></label>
									</div>
									<div class="col-md-4 col-sm-4 col-xs-4 input-value-adjust">
										<input type="text" class="qst_val_op3" name="qst_val_op3" id="qst_val_op3" autocomplete="off" required value="<?php echo $op_val[2]; ?>" />
									</div>
								</div>
							</fieldset>
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<div class="option-form thumbnail">
							<fieldset class="scheduler-border">
								<legend class="scheduler-border">Option D <span style="color:#f55;">*</span></legend>
								<textarea class="qst_op4" name="qst_op4" id="qst_op4" rows="5" required ><?php echo $op[3]; ?></textarea>
								<div class="row">
									<div class="col-md-6"></div>
									<div class="col-md-2 col-sm-2 col-xs-2 option-value">
										<label class="control-label">Valeur <span style="color:#f55;">*</span></label>
									</div>
									<div class="col-md-4 col-sm-4 col-xs-4 input-value-adjust">
										<input type="text" class="qst_val_op4" name="qst_val_op4" id="qst_val_op4" autocomplete="off" required value="<?php echo $op_val[3]; ?>" />
									</div>
								</div>
							</fieldset>
						</div>
					</div>
			</div>
			<div class="form-group">
				<div class="col-md-4 col-sm-4 col-xs-4">
					<label class="control-label" style="text-align:right;">Réponse correcte  <span style="color:#f55;">*</span></label>
					<select class="qst_rep_cor" name="qst_rep_cor" id="qst_rep_cor" required >
						<option value="qst_op1" <?php if($correct==='qst_op1') echo 'selected'; ?>>Option A</option>
						<option value="qst_op2" <?php if($correct==='qst_op2') echo 'selected'; ?>>Option B</option>
						<option value="qst_op3" <?php if($correct==='qst_op3') echo 'selected'; ?>>Option C</option>
						<option value="qst_op4" <?php if($correct==='qst_op4') echo 'selected'; ?>>Option D</option>
					</select>
				</div>
				<div class="col-md-8 col-sm-8 col-xs-8">
					<label class="control-label" style="text-align:right;">Explication</label>
					<textarea class="qst_expl" name="qst_expl" id="qst_expl" rows="4" ><?php echo $qst_explication; ?></textarea>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-6">
					<label class="control-label" style="text-align:right;">Importance</label>
					<textarea class="qst_imp" name="qst_imp" id="qst_imp" rows="4" ><?php echo $qst_importance; ?></textarea>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6">
					<label class="control-label" style="text-align:right;">Objectif</label>
					<textarea class="qst_obj" name="qst_obj" id="qst_obj" rows="4" ><?php echo $qst_objectif; ?></textarea>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-6">
					<label class="control-label" style="text-align:right;">Image</label>
					<input type="file" class="question_image" name="question_image" id="question_image"/>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<input name="thisID" type="hidden" value="<?php echo $targetID; ?>" />
					<input class="btn btn-default question-button-edit-apt" type="submit" name="submit" value="<?php if(isset($_GET['op']) && $_GET['op']==='inf') echo 'retour'; else echo 'Modifier';?>" />
				</div>
			</div>
			<div class="contact-loading alert alert-info form-alert">
				<span class="message">Chargement...</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
			<div class="contact-success alert alert-success form-alert">
				<span class="message">Succès!</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
			<div class="contact-error alert alert-danger form-alert">
				<span class="message">Erreur!</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
		</form>
	</div>
</div>