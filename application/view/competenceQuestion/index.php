
<div class="row">
	<h3 style="font-size:18px;text-align:center;width:30%;padding-top:20px;margin:auto;color:#337ab7;">Gestion des questions de compétence<hr style="margin-top:10px;margin-bottom:10px;" /></h3>
	<div class="fixed-top">
		<form action="" method="post" class="form-horizontal" style="width:1170px; margin: 30px auto 10px auto;font-size:13px;">
			<div style="float:right;"><a href="question_list.php#form">+ Ajouter une question</a></div>
			<div class="form-group row">
				<label class="col-md-1 col-sm-1 col-xs-1 control-label" style="padding-top:0px;text-align:right;" for="complexite">Complexité</label>
				<div class="col-md-2 col-sm-2 col-xs-2">
					<select name="complexite" id="complexite">
						<option value="">Tout</option>
						<option value="facile"<?php if(isset($complexite) && $complexite==='facile') echo 'selected'; ?>>Facile</option>
						<option value="moyen"<?php if(isset($complexite) && $complexite==='moyen') echo 'selected'; ?>>Moyen</option>
						<option value="difficile"<?php if(isset($complexite) && $complexite==='difficile') echo 'selected'; ?>>Difficile</option>
					</select>
				</div>
				<label class="col-md-1 col-sm-1 col-xs-1 control-label" style="padding-top:0px;text-align:right;width:6%;" for="evaluation">Evaluation</label>
				<div class="col-md-2 col-sm-2 col-xs-2">
					<select name="evaluation" id="evaluation">
						<option value="">Tout</option>
						<?php foreach ($competenceEvaluationNames as $key => $competenceEvaluationName) { ?>
						<?php if(isset($evaluation) && $evaluation === $competenceEvaluationName->eval_nom) { ?>
						<option value="<?php echo $competenceEvaluationName->eval_nom; ?>" selected>
							<?php echo ucfirst($competenceEvaluationName->eval_nom); ?>
						</option>
						<?php }else{ ?>
						<option value="<?php echo $competenceEvaluationName->eval_nom; ?>">
							<?php echo ucfirst($competenceEvaluationName->eval_nom); ?>
						</option>
						<?php } ?>
						<?php } ?>
					</select>
				</div>
				<label class="col-md-1 col-sm-1 col-xs-1 control-label" style="padding-top:0px;text-align:right;width:4%;" for="theme">Thème</label>
				<div class="col-md-2 col-sm-2 col-xs-2">
					<select name="theme" id="theme">
						<option value="">Tout</option>
						<?php foreach ($themeTitles as $key => $themeTitle) { ?>
						<?php if(isset($theme) && $theme === $themeTitle->thm_titre) { ?>
						<option value="<?php echo $themeTitle->thm_titre; ?>" selected>
							<?php echo ucfirst($themeTitle->thm_titre); ?>
						</option>
						<?php }else{ ?>
						<option value="<?php echo $themeTitle->thm_titre; ?>">
							<?php echo ucfirst($themeTitle->thm_titre); ?>
						</option>
						<?php } ?>
						<?php } ?>
					</select>
				</div>
				<div class="col-md-1 col-sm-1 col-xs-1">
					<input class="btn btn-default search-form" type="submit" name="search_competence" id="search_competence" value="Rechercher"/>
				</div>
			</div>
		</form>
		<div class="fixed-horizontal">
			<div class="container" style="padding-left:0px;padding-right:0px;">
				<table class="table table-striped table-bordered table-hover table-condensed" style="margin-bottom:0px;width:100% !important;">
					<thead style="">
						<tr style="width:100%;">
							<th style="color:#31b0d5; text-align:center;" class="th1"><strong style=""><a style='padding:0px;' onmousedown='return false;' class='btn btn-plus' role='button'><i class="ion-minus-round" style="margin:0% !important;color:#31b0d5;"></i></a></strong></th>
							<th style="color:#31b0d5; text-align:center;" class="th2"><strong>Numéro</strong></th>
							<th style="color:#31b0d5; text-align:center;" class="th3"><strong>Question</strong></th>
							<th style="color:#31b0d5; text-align:center;" class="th4"><strong>Thème</strong></th>
							<th style="color:#31b0d5; text-align:center;" class="th5"><strong>Complexité</strong></th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="horizontal">
		<table class="table table-striped table-bordered table-condensed">
			<thead>
				<tr style="width:100%;">
					<th style="color:#31b0d5; text-align:center;min-width:30px;" class='thn1'><strong style=""><a style='padding:0px;' onmousedown='return false;' class='btn btn-plus' role='button'><i class="ion-minus-round" style="margin:0% !important;color:#31b0d5;"></i></a></strong></th>
					<th style="color:#31b0d5; text-align:center;width:5%;" class='thn2'><strong>Numéro</strong></th>
					<th style="color:#31b0d5; text-align:center;width:100%;" class='thn3'><strong>Question</strong></th>
					<th style="color:#31b0d5; text-align:center;width:100%;" class='thn4'><strong>Thème</strong></th>
					<th style="color:#31b0d5; text-align:center;width:100%;" class='thn5'><strong>Complexité</strong></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ((array)$active_questions as $key => $active_question): ?>
					<tr style='width:100%;'>
						<td class='first-td'>
							<div class='cell-block'>
								<button class='button solid-button blue info-button question competence' id="<?php echo $active_question->qst_id; ?>">
									<i class='ion-ios-information-outline'></i>
								</button>
								<button class='button solid-button blue toggle-button' onclick="toggleNavPanel(<?php echo $active_question->qst_id; ?>,<?php echo $active_question->qst_id; ?>)">
									<span id="n<?php echo $active_question->qst_id; ?>">&#9662;</span>
								</button>
							</div>
						</td>
						<td style='width:10%;'><?php echo $active_question->qst_ref; ?></td>
						<td style='width:100%;'><?php echo $active_question->qst_description; ?></td>
						<?php if (!empty($active_question->thm_titre)): ?>
							<td style='width:20%;'><?php echo ucfirst($active_question->thm_titre); ?></td>
						<?php else: ?>
							<td style='width:20%;'>n'est pas affecter</td>
						<?php endif ?>
						<td style='width:20%;'></td></tr>
						<tr><td colspan='12' style='padding:0%;'><div class='row sections_panel' id="h<?php echo $active_question->qst_id; ?>" style='height:0px;'><?php if ($competenceEvaluations != null): ?><?php foreach ($competenceEvaluations as $key => $competenceEvaluation): ?><?php if ($this->evaluationQuestions($active_question->qst_id, $competenceEvaluation->eval_id) != null): ?><?php foreach ($this->evaluationQuestions($active_question->qst_id, $competenceEvaluation->eval_id) as $key => $evaluationQuestion): ?><?php if ($evaluationQuestion->eval_id == $competenceEvaluation->eval_id): ?><div style="margin-left:10px;text-align:left;" class="col-md-2 col-sm-2 col-xs-2"><input type="checkbox" style="width:14px;margin-right:5px;margin-left:10px;" id="<?php echo $active_question->qst_id; ?>" name="en<?php echo $active_question->qst_id; ?>" class="<?php echo $active_question->qst_id; ?>" value="<?php echo $competenceEvaluation->eval_nom; ?>" checked/><?php echo ucfirst($competenceEvaluation->eval_nom); ?></div><?php else: ?><div style="margin-left:10px;text-align:left;" class="col-md-2 col-sm-2 col-xs-2"><input type="checkbox" style="width:14px;margin-right:5px;margin-left:10px;" id="<?php echo $active_question->qst_id; ?>" name="en<?php echo $active_question->qst_id; ?>" class="<?php echo $active_question->qst_id; ?>" value="<?php echo $competenceEvaluation->eval_nom; ?>"/><?php echo ucfirst($competenceEvaluation->eval_nom); ?></div><?php endif ?><?php endforeach ?><?php else: ?><div style="margin-left:10px;text-align:left;" class="col-md-2 col-sm-2 col-xs-2"><input type="checkbox" style="width:14px;margin-right:5px;margin-left:10px;" id="<?php echo $active_question->qst_id; ?>" name="en<?php echo $active_question->qst_id; ?>" class="<?php echo $active_question->qst_id; ?>" value="<?php echo $competenceEvaluation->eval_nom; ?>"/><?php echo ucfirst($competenceEvaluation->eval_nom); ?></div><?php endif; ?><?php endforeach ?><?php endif ?><div class='clear'></div><div style='margin-top:15px;'><a class='btn btn-info' role='button' href="<?php echo URL.'CompetenceQuestion/Delete/' . $active_question->qst_id; ?>">Supprimer</a><a class='btn btn-info' role='button' href="<?php echo URL.'CompetenceQuestion/Edit/'.$active_question->qst_id; ?>">Modifier</a><a class='btn btn-info save' role='button' id="en<?php echo $active_question->qst_id; ?>">Enregistrer</a></div><div class='clear'></div></div></div></td></tr>
				<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>
<div class="row" style="margin-bottom:5%;">
	<br/>
	<hr id="form"/>
	<div class="col-md-1 col-sm-1 col-xs-1"></div>
	<div class="col-md-10 col-sm-10 col-xs-10" style="padding:0%;">
		<h3 style="font-size:18px;padding-bottom:0%;margin-bottom:0%;text-align:center;color:#337ab7;">Ajouter une question</h3>
		<hr/>
		<form id="form-add-competence" action="<?php echo URL.'CompetenceQuestion/Add'; ?>" method="post" class="form-horizontal question-form question-form-add competence" enctype="multipart/form-data" accept-charset="utf-8">
			<div class="form-group">
				<div class="col-md-2 col-sm-2 col-xs-2">
					<label class="control-label" style="text-align:right;">Numéro <span style="color:#f55;">*</span></label>
					<input type="text" class="qst_num" name="qst_num" id="qst_num" required value="<?php echo $qst_idd; ?>" autocomplete="off"/>
				</div>
				<div class="col-md-10 col-sm-10 col-xs-10">
					<label class="control-label" style="text-align:right;">Question <span style="color:#f55;">*</span></label>
					<textarea class="qst_desc" name="qst_desc" id="qst_desc" rows="4" required></textarea>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-4 col-sm-4 col-xs-4">
					<label class="control-label" style="text-align:right;">Type <span style="color:#f55;">*</span></label>
					<select name="qst_tpe" id="qst_tpe" required>
						<option value=""></option>
						<?php foreach ($typeNames as $key => $typeName): ?>
							<option value="<?php echo $typeName->type_nom; ?>"><?php echo ucfirst($typeName->type_nom); ?></option>
						<?php endforeach ?>
					</select>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-3">
					<label class="control-label" style="text-align:right;">Thème <span style="color:#f55;">*</span></label>
					<select name="qst_thm" id="qst_thm" required>
						<option value=""></option>
						<?php foreach ($themeTitles as $key => $themeTitle): ?>
							<option value="<?php echo $themeTitle->thm_titre; ?>"><?php echo ucfirst($themeTitle->thm_titre); ?></option>
						<?php endforeach ?>
					</select>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-3">
					<label class="control-label" style="text-align:right;">Typologie <span style="color:#f55;">*</span></label>
					<select name="question_typologie" id="question_typologie" class="question_typologie" required>
						<option value=""></option>
						<?php foreach ($typologyTitles as $key => $typologyTitle): ?>
							<option value="<?php echo $typologyTitle->typ_title; ?>"><?php echo ucfirst($typologyTitle->typ_title); ?></option>
						<?php endforeach ?>
					</select>
				</div>
				<div class="col-md-2 col-sm-2 col-xs-2">
					<label class="control-label" style="text-align:right;">Complexité <span style="color:#f55;">*</span></label>
					<select name="qst_comp" id="qst_comp" class="qst_comp" required>
						<option value=""></option>
						<?php foreach ($complexityTitles as $key => $complexityTitle): ?>
							<option value="<?php echo $complexityTitle->comp_title; ?>"><?php echo ucfirst($complexityTitle->comp_title); ?></option>
						<?php endforeach ?>
					</select>
				</div>
			</div>
			<br><hr><br>
			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-6">
					<div class="option-form thumbnail">
						<fieldset class="scheduler-border">
							<legend class="scheduler-border">Option A <span style="color:#f55;">*</span></legend>
							<textarea class="qst_op1" name="qst_op1" id="qst_op1" rows="5" required></textarea>
							<div class="row">
								<div class="col-md-6"></div>
								<div class="col-md-2 col-sm-2 col-xs-2 option-value">
									<label class="control-label">Valeur <span style="color:#f55;">*</span></label>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-4 input-value-adjust">
									<input type="text" class="qst_val_op1" name="qst_val_op1" id="qst_val_op1" autocomplete="off" required/>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6">
					<div class="option-form thumbnail">
						<fieldset class="scheduler-border">
							<legend class="scheduler-border">Option B <span style="color:#f55;">*</span></legend>
							<textarea class="qst_op2" name="qst_op2" id="qst_op2" rows="5" required></textarea>
							<div class="row">
								<div class="col-md-6"></div>
								<div class="col-md-2 col-sm-2 col-xs-2 option-value">
									<label class="control-label">Valeur <span style="color:#f55;">*</span></label>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-4 input-value-adjust">
									<input type="text" class="qst_val_op2" name="qst_val_op2" id="qst_val_op2" autocomplete="off" required/>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-6">
					<div class="option-form thumbnail">
						<fieldset class="scheduler-border">
							<legend class="scheduler-border">Option C <span style="color:#f55;">*</span></legend>
							<textarea class="qst_op3" name="qst_op3" id="qst_op3" rows="5" required></textarea>
							<div class="row">
								<div class="col-md-6"></div>
								<div class="col-md-2 col-sm-2 col-xs-2 option-value">
									<label class="control-label">Valeur <span style="color:#f55;">*</span></label>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-4 input-value-adjust">
									<input type="text" class="qst_val_op3" name="qst_val_op3" id="qst_val_op3" autocomplete="off" required/>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6">
					<div class="option-form thumbnail">
						<fieldset class="scheduler-border">
							<legend class="scheduler-border">Option D <span style="color:#f55;">*</span></legend>
							<textarea class="qst_op4" name="qst_op4" id="qst_op4" rows="5" required></textarea>
							<div class="row">
								<div class="col-md-6"></div>
								<div class="col-md-2 col-sm-2 col-xs-2 option-value">
									<label class="control-label">Valeur <span style="color:#f55;">*</span></label>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-4 input-value-adjust">
									<input type="text" class="qst_val_op4" name="qst_val_op4" id="qst_val_op4" autocomplete="off" required/>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
			</div>
			<br><hr><br>
			<div class="form-group">
				<div class="col-md-4 col-sm-4 col-xs-4">
					<label class="control-label" style="text-align:right;">Réponse correcte  <span style="color:#f55;">*</span></label>
					<select class="qst_rep_cor" name="qst_rep_cor" id="qst_rep_cor" required>
						<option value=""></option>
						<option value="qst_op1">Option A</option>
						<option value="qst_op2">Option B</option>
						<option value="qst_op3">Option C</option>
						<option value="qst_op4">Option D</option>
					</select>
				</div>
				<div class="col-md-8 col-sm-8 col-xs-8">
					<label class="control-label" style="text-align:right;">Explication</label>
					<textarea class="qst_expl" name="qst_expl" id="qst_expl" rows="4"></textarea>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-6">
					<label class="control-label" style="text-align:right;">Importance</label>
					<textarea class="qst_imp" name="qst_imp" id="qst_imp" rows="4"></textarea>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6">
					<label class="control-label" style="text-align:right;">Objectif</label>
					<textarea class="qst_obj" name="qst_obj" id="qst_obj" rows="4"></textarea>
				</div>
			</div>
			<div class="form-group">
				<div class="form-group">
	                <label class="control-label" style="text-align:right;">Image</label>
				    <input type="file" class="form-control question_image" name="question_image" id="question_image"/>
			       </div>
			
			</div>
			<div class="form-group">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<!-- <button class="button solid-button question-button-add" type="button">Ajouter</button> -->
					<input type="submit" class="button solid-button question-button-add competence" value="Ajouter" />
				</div>
			</div>
			<div class="form-group">
				<div class="contact-loading alert alert-info form-alert">
					<span class="message">Chargement...</span>
					<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
				</div>
				<div class="contact-success alert alert-success form-alert">
					<span class="message">Succès!</span>
					<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
				</div>
				<div class="contact-error alert alert-danger form-alert">
					<span class="message">Erreur !</span>
					<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
				</div>
			</div>
		</form>
	</div>
	<div class="col-md-1 col-sm-1 col-xs-1"></div>
</div>