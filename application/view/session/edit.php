
<div class="row" style="margin-bottom:5%;">
	<div class="col-md-6 col-md-offset-3" style="padding:0%;">
		<div class="pull-right"><a href="<?php echo URL . 'Session#form' ?>">+ Ajouter une session</a></div>
		<div class="clear"><br><br></div>
		<h3 style="font-size:18px;padding-bottom:0%;margin-bottom:0%;text-align:center;color:#337ab7;">Modifier une session</h3>
		<br><br>
		<form action="<?php echo URL . 'Session/Update'; ?>" method="post" class="form-horizontal session-form">
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Référence <span style="color:#f55;">*</span></label>
				<div class="input-group">
					<span class="input-group-addon session-addon"><?php echo $plf_reff; ?></span>
					<input type="text" class="plf_ref" name="plf_ref disabled/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Titre <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input type="text" class="plf_title" name="plf_title" value="<?php echo $plf_title; ?>" required <?php if(isset($_GET['op']) && $_GET['op']==='inf') echo 'disabled'; ?> required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Nom responsable  <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input type="text" class="plf_resp_firstname" name="plf_resp_firstname" autocomplete="off" value="<?php echo $plf_resp_firstname; ?>" required <?php if(isset($_GET['op']) && $_GET['op']==='inf') echo 'disabled'; ?> required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Prénom responsable  <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input type="text" class="plf_resp_lastname" name="plf_resp_lastname" autocomplete="off" value="<?php echo $plf_resp_lastname; ?>" required <?php if(isset($_GET['op']) && $_GET['op']==='inf') echo 'disabled'; ?> required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Type</label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<select class="form-control plf_type" data-style="btn-primary" name="plf_type" <?php if(isset($_GET['op']) && $_GET['op']==='inf') echo 'disabled'; ?>>
						<option value="evaluation" <?php if(isset($plf_type) && $plf_type==='evaluation') echo 'selected'; ?>>Evaluation</option>
						<option value="formation" <?php if(isset($plf_type) && $plf_type==='formation') echo 'selected'; ?>>Formation</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Durée session <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<div class="input-daterange input-group" id="datepicker">
					    <input type="text" class="input-sm form-control session_start" name="session_start" value="<?php if(isset($session_start)) echo $session_start; ?>" <?php if(isset($_GET['op']) && $_GET['op']==='inf') echo 'disabled'; ?>/>
					    <span class="input-group-addon">à</span>
					    <input type="text" class="input-sm form-control session_end" name="session_end" value="<?php if(isset($session_end)) echo $session_end; ?>" <?php if(isset($_GET['op']) && $_GET['op']==='inf') echo 'disabled'; ?>/>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<input class="thisID" name="thisID" type="hidden" value="<?php echo $targetID; ?>" />
					<button class="button solid-button pull-right session-button-add" type="button"><?php if(isset($_GET['op']) && $_GET['op']==='inf') echo 'retour'; else echo 'Modifier';?></button>
				</div>
			</div>
			<div class="contact-loading alert alert-info form-alert">
				<span class="message">Chargement...</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
			<div class="contact-success alert alert-success form-alert">
				<span class="message">Succès!</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
			<div class="contact-error alert alert-danger form-alert">
				<span class="message">Erreur!</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
		</form>
	</div>
</div>