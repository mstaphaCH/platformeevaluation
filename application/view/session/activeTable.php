<?php foreach ($active_sessions as $key => $active_session) { ?>
				<tr style='width:100%;'>
					<td class='first-td'>
						<div class='cell-block'>
							<button class='button solid-button blue info-button session_info' id="<?php echo $active_session->plf_id; ?>">
								<i class='ion-ios-information-outline'></i>
							</button>
							<button class='button solid-button blue toggle-button' onclick="toggleNavPanel(<?php echo $active_session->plf_id; ?>,<?php echo $active_session->plf_id; ?>)">
								<span id="n<?php echo $active_session->plf_id; ?>">&#9662;</span>
							</button>
						</div>
					</td>
					<td style='width:5%;'><?php echo $active_session->plf_year . '-' . $active_session->str_id . '-' . $active_session->plf_ref; ?></td>
					<td style='width:20%;'><?php echo ucfirst($active_session->plf_title); ?></td>
					<td style='width:10%;'><?php echo strftime("%d/%m/%Y", strtotime($active_session->plf_start_date)); ?></td>
					<td style='width:10%;'><?php echo strftime("%d/%m/%Y", strtotime($active_session->plf_end_date)); ?></td>
					<td style='width:20%;'><?php echo ucfirst($active_session->plf_resp_firstname); ?></td>
					<td style='width:20%;'><?php echo ucfirst($active_session->plf_resp_lastname); ?></td>
					<td style='width:20%;text-align:center;'><?php echo ucfirst($active_session->plf_type); ?></td>
					<td class='button-operation-3'><a class='btn' role='button' href="<?php echo URL . 'Session/Edit/' . $active_session->plf_id; ?>"><i class='ion-compose' style='color:green;'></i></a><a class='btn' role='button' href="<?php echo URL . 'Session/Inactive/' . $active_session->plf_id; ?>"><i class='ion-log-out' style='color:blue;'></i></a><a class='btn session delete' id="<?php echo $active_session->plf_id; ?>" role="button"><i class='ion-close-circled' style='color:red;'></i></a></td>
				</tr>
				<tr>
					<td colspan='12' style='padding:0%;'><div class='row sections_panel' id="h<?php echo $active_session->plf_id; ?>" style='height:0px;'><?php foreach ($active_evaluations as $key => $active_evaluation) { ?><?php if($this->evalPlanif($active_session->plf_id, $active_evaluation->eval_id) != null) { ?><?php foreach ($this->evalPlanif($active_session->plf_id, $active_evaluation->eval_id) as $key => $evalPlan) { ?><?php if ($evalPlan->eval_id == $active_evaluation->eval_id) { ?><div style="margin-left:10px;text-align:left;" class="col-md-2 col-sm-2 col-xs-2"><input type="checkbox" style="width:14px;margin-right:5px;margin-left:10px;" id="<?php echo $active_session->plf_id; ?>" name="en<?php echo $active_session->plf_id; ?>" class="<?php echo $active_session->plf_id; ?>" value="<?php echo $active_evaluation->eval_nom; ?>" checked/><?php echo ucfirst($active_evaluation->eval_nom); ?></div><?php }else{ ?><div style="margin-left:10px;text-align:left;" class="col-md-2 col-sm-2 col-xs-2"><input type="checkbox" style="width:14px;margin-right:5px;margin-left:10px;" id="<?php echo $active_session->plf_id; ?>" name="en<?php echo $active_session->plf_id; ?>" class="<?php echo $active_session->plf_id; ?>" value="<?php echo $active_evaluation->eval_nom; ?>"/><?php echo ucfirst($active_evaluation->eval_nom); ?></div><?php } ?><?php } ?><?php }else{ ?><div style="margin-left:10px;text-align:left;" class="col-md-2 col-sm-2 col-xs-2"><input type="checkbox" style="width:14px;margin-right:5px;margin-left:10px;" id="<?php echo $active_session->plf_id; ?>" name="en<?php echo $active_session->plf_id; ?>" class="<?php echo $active_session->plf_id; ?>" value="<?php echo $active_evaluation->eval_nom; ?>"/><?php echo ucfirst($active_evaluation->eval_nom); ?></div><?php } ?><?php } ?><div class='clear'></div><div style='margin-top:15px;'><a class='btn btn-info savee' role='button' id="en<?php echo $active_session->plf_id; ?>">Enregistrer</a></div><div class='clear'></div></div></div></td>
				</tr>
			<?php } ?>