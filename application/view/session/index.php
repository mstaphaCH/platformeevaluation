<div class="row">
	<h3 style="font-size:20px;text-align:center;width:30%;margin:auto;color:#337ab7;"><span>planification des sessions du travail<hr/></span></h3>
	<div style="float:right;"><a href="session_list.php#form">+ Ajouter une session</a></div>	
</div>
<div class="row">
	<h2 style="text-align:left;font-size:18px;margin-bottom:25px;">Sessions actifs</h2>
	<form action="" method="post" class="form-inline structure-filter active" style="font-size:12px;">
		<div class="form-group row">

			<label class="col-md-1 col-sm-1 col-xs-1 control-label" style="text-align:right;width:6%;" for="search-name">Type</label>
			<div class="col-md-2 col-sm-2 col-xs-2">
				<select name="search_type" id="search_type">
				    <option value="">Tout</option>
					<option value="evaluation">Evaluation</option>
					<option value="formation">Formation</option>
				</select>
			</div>

				<label class="col-md-2 col-sm-5 col-xs-5 control-label">Date debut session <span style="color:#f55;">*</span></label>
				<div class="col-md-2 col-sm-5 col-xs-5">
					<div class="input-daterange input-group" id="date" name="date">
					    <input type="text" class="input-sm form-control session_start" name="search_debut_session" id="search_debut_session"  />
                    </div>
                </div>
      
				<label class="col-md-2 col-sm-5 col-xs-5 control-label">Date fin session <span style="color:#f55;">*</span></label>
				<div class="col-md-2 col-sm-5 col-xs-5">
					<div class="input-daterange input-group" id="date" name="date">
					    <input type="text" class="input-sm form-control session_start" name="search_fin_session" id="search_fin_session"  />
                    </div>
                </div>
         
			
			<div class="col-md-1 col-sm-1 col-xs-1">
				<input type="hidden" name="structure" value="active">
				<input class="btn btn-default structure search" type="submit" name="search3" id="search" value="Rechercher"/>
			</div>
		</div>
	</form> 

</div><div class="clear"></div>
<div class="row ">
	<div class="horizontal">
		<table class="table table-striped table-bordered table-condensed table-hide-1">
			<thead>
				<tr style="width:100%;">
					<th style="color:#31b0d5; text-align:center;min-width:30px;"><strong><a style='padding:0px;' onmousedown='return false;' class='btn btn-plus' role='button'><i class="ion-minus-round" style="margin:0% !important;color:#31b0d5;"></i></a></strong></th>
					<th style="color:#31b0d5; text-align:center;width:5%;" class='thn2'><strong>Référence</strong></th>
					<th style="color:#31b0d5; text-align:center;width:20%;" class='thn3'><strong>Titre</strong></th>
					<th style="color:#31b0d5; text-align:center;width:10%;" class='thn4'><strong>Date de début</strong></th>
					<th style="color:#31b0d5; text-align:center;width:10%;" class='thn5'><strong>Date de fin</strong></th>
					<th style="color:#31b0d5; text-align:center;width:20%;" class='thn5'><strong>Nom responsable</strong></th>
					<th style="color:#31b0d5; text-align:center;width:20%;" class='thn5'><strong>Prénom responsable</strong></th>
					<th style="color:#31b0d5; text-align:center;width:20%;" class='thn5'><strong>Type</strong></th>
					<th style="color:#31b0d5; text-align:center;width:20%;" class='thn5'><strong>Opération</strong></th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($active_sessions as $key => $active_session) { ?>
				<tr style='width:100%;'>
					<td class='first-td'>
						<div class='cell-block'>
							<button class='button solid-button blue info-button session_info' id="<?php echo $active_session->plf_id; ?>">
								<i class='ion-ios-information-outline'></i>
							</button>
							<button class='button solid-button blue toggle-button' onclick="toggleNavPanel(<?php echo $active_session->plf_id; ?>,<?php echo $active_session->plf_id; ?>)">
								<span id="n<?php echo $active_session->plf_id; ?>">&#9662;</span>
							</button>
						</div>
					</td>
					<td style='width:5%;'><?php echo $active_session->plf_year . '-' . $active_session->str_id . '-' . $active_session->plf_ref; ?></td>
					<td style='width:20%;'><?php echo ucfirst($active_session->plf_title); ?></td>
					<td style='width:10%;'><?php echo strftime("%d/%m/%Y", strtotime($active_session->plf_start_date)); ?></td>
					<td style='width:10%;'><?php echo strftime("%d/%m/%Y", strtotime($active_session->plf_end_date)); ?></td>
					<td style='width:20%;'><?php echo ucfirst($active_session->plf_resp_firstname); ?></td>
					<td style='width:20%;'><?php echo ucfirst($active_session->plf_resp_lastname); ?></td>
					<td style='width:20%;text-align:center;'><?php echo ucfirst($active_session->plf_type); ?></td>
					<td class='button-operation-3'><a class='btn' role='button' href="<?php echo URL . 'Session/Edit/' . $active_session->plf_id; ?>"><i class='ion-compose' style='color:green;'></i></a><a class='btn' role='button' href="<?php echo URL . 'Session/Inactive/' . $active_session->plf_id; ?>"><i class='ion-log-out' style='color:blue;'></i></a><a class='btn session delete' id="<?php echo $active_session->plf_id; ?>" role="button"><i class='ion-close-circled' style='color:red;'></i></a></td>
				</tr>
				<tr>
					<td colspan='12' style='padding:0%;'><div class='row sections_panel' id="h<?php echo $active_session->plf_id; ?>" style='height:0px;'><?php foreach ($active_evaluations as $key => $active_evaluation) { ?><?php if($this->evalPlanif($active_session->plf_id, $active_evaluation->eval_id) != null) { ?><?php foreach ($this->evalPlanif($active_session->plf_id, $active_evaluation->eval_id) as $key => $evalPlan) { ?><?php if ($evalPlan->eval_id == $active_evaluation->eval_id) { ?><div style="margin-left:10px;text-align:left;" class="col-md-2 col-sm-2 col-xs-2"><input type="checkbox" style="width:14px;margin-right:5px;margin-left:10px;" id="<?php echo $active_session->plf_id; ?>" name="en<?php echo $active_session->plf_id; ?>" class="<?php echo $active_session->plf_id; ?>" value="<?php echo $active_evaluation->eval_id; ?>" checked/><?php echo ucfirst($active_evaluation->eval_nom); ?></div><?php }else{ ?><div style="margin-left:10px;text-align:left;" class="col-md-2 col-sm-2 col-xs-2"><input type="checkbox" style="width:14px;margin-right:5px;margin-left:10px;" id="<?php echo $active_session->plf_id; ?>" name="en<?php echo $active_session->plf_id; ?>" class="<?php echo $active_session->plf_id; ?>" value="<?php echo $active_evaluation->eval_id; ?>"/><?php echo ucfirst($active_evaluation->eval_nom); ?></div><?php } ?><?php } ?><?php }else{ ?><div style="margin-left:10px;text-align:left;" class="col-md-2 col-sm-2 col-xs-2"><input type="checkbox" style="width:14px;margin-right:5px;margin-left:10px;" id="<?php echo $active_session->plf_id; ?>" name="en<?php echo $active_session->plf_id; ?>" class="<?php echo $active_session->plf_id; ?>" value="<?php echo $active_evaluation->eval_id; ?>"/><?php echo ucfirst($active_evaluation->eval_nom); ?></div><?php } ?><?php } ?><div class='clear'></div><div style='margin-top:15px;'><a class='btn btn-info savee' role='button' id="en<?php echo $active_session->plf_id; ?>">Enregistrer</a></div><div class='clear'></div></div></div></td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
	</div>
</div>
<div class="row">
	<h2 style="text-align:left;font-size:18px;margin-bottom:25px;">Sessions non actifs</h2>
	<div class="horizontal">
		<table class="table table-striped table-bordered table-condensed table-hide-2">
			<thead>
				<tr style="width:100%;">
					<th style="color:#31b0d5; text-align:center;min-width:30px;"><strong><a style='padding:0px;' onmousedown='return false;' class='btn btn-plus' role='button'><i class="ion-minus-round" style="margin:0% !important;color:#31b0d5;"></i></a></strong></th>
					<th style="color:#31b0d5; text-align:center;width:5%;" class='thn2'><strong>Référence</strong></th>
					<th style="color:#31b0d5; text-align:center;width:20%;" class='thn3'><strong>Titre</strong></th>
					<th style="color:#31b0d5; text-align:center;width:10%;" class='thn4'><strong>Date de début</strong></th>
					<th style="color:#31b0d5; text-align:center;width:10%;" class='thn5'><strong>Date de fin</strong></th>
					<th style="color:#31b0d5; text-align:center;width:20%;" class='thn5'><strong>Nom responsable</strong></th>
					<th style="color:#31b0d5; text-align:center;width:20%;" class='thn5'><strong>Prénom responsable</strong></th>
					<th style="color:#31b0d5; text-align:center;width:20%;" class='thn5'><strong>Type</strong></th>
					<th style="color:#31b0d5; text-align:center;width:20%;" class='thn5'><strong>Opération</strong></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($inactive_sessions as $key => $inactive_session) { ?>
				<tr style='width:100%;'>
					<td class='first-td'>
						<div class='cell-block'>
							<button class='button solid-button blue info-button session_info' id="<?php echo $inactive_session->plf_id; ?>">
								<i class='ion-ios-information-outline'></i>
							</button>
							<button class='button solid-button blue toggle-button' onclick="toggleNavPanel(<?php echo $inactive_session->plf_id; ?>,<?php echo $inactive_session->plf_id; ?>)">
								<span id="n<?php echo $inactive_session->plf_id; ?>">&#9662;</span>
							</button>
						</div>
					</td>
					<td style='width:5%;'><?php echo $inactive_session->plf_year . '-' . $inactive_session->str_id . '-' . $inactive_session->plf_ref; ?></td>
					<td style='width:20%;'><?php echo ucfirst($inactive_session->plf_title); ?></td>
					<td style='width:10%;'><?php echo strftime("%d/%m/%Y", strtotime($inactive_session->plf_start_date)); ?></td>
					<td style='width:10%;'><?php echo strftime("%d/%m/%Y", strtotime($inactive_session->plf_end_date)); ?></td>
					<td style='width:20%;'><?php echo ucfirst($inactive_session->plf_resp_firstname); ?></td>
					<td style='width:20%;'><?php echo ucfirst($inactive_session->plf_resp_lastname); ?></td>
					<td style='width:20%;text-align:center;'><?php echo ucfirst($inactive_session->plf_type); ?></td>
					<td class='button-operation-3'><a class='btn' role='button' href="<?php echo URL . 'Session/Edit/' . $inactive_session->plf_id; ?>"><i class='ion-compose' style='color:green;'></i></a><a class='btn' role='button' href="<?php echo URL . 'Session/Active/' . $inactive_session->plf_id; ?>"><i class='ion-log-in' style='color:blue;'></i></a><a class='btn session delete' id="<?php echo $inactive_session->plf_id; ?>" role="button"><i class='ion-close-circled' style='color:red;'></i></a></td>
				</tr>
				<tr>
					<td colspan='12' style='padding:0%;'><form action="<?php echo URL . 'Session/eval_planif'; ?>" method="post"><div class='row sections_panel' id="h<?php echo $inactive_session->plf_id; ?>" style='height:0px;'><?php foreach ($inactive_evaluations as $key => $inactive_evaluation) { ?><?php if($this->evalPlanif($inactive_session->plf_id, $inactive_evaluation->eval_id) != null) { ?><?php foreach ($this->evalPlanif($inactive_session->plf_id, $inactive_evaluation->eval_id) as $key => $evalPlan) { ?><?php if ($evalPlan->eval_id == $inactive_evaluation->eval_id) { ?><div style="margin-left:10px;text-align:left;" class="col-md-2 col-sm-2 col-xs-2"><input type="checkbox" style="width:14px;margin-right:5px;margin-left:10px;" id="<?php echo $inactive_session->plf_id; ?>" name="en<?php echo $inactive_session->plf_id; ?>" class="<?php echo $inactive_session->plf_id; ?>" value="<?php echo $inactive_evaluation->eval_nom; ?>" checked/><?php echo ucfirst($inactive_evaluation->eval_nom); ?></div><?php }else{ ?><div style="margin-left:10px;text-align:left;" class="col-md-2 col-sm-2 col-xs-2"><input type="checkbox" style="width:14px;margin-right:5px;margin-left:10px;" id="<?php echo $inactive_session->plf_id; ?>" name="en<?php echo $inactive_session->plf_id; ?>" class="<?php echo $inactive_session->plf_id; ?>" value="<?php echo $inactive_evaluation->eval_nom; ?>"/><?php echo ucfirst($inactive_evaluation->eval_nom); ?></div><?php } ?><?php } ?><?php }else{ ?><div style="margin-left:10px;text-align:left;" class="col-md-2 col-sm-2 col-xs-2"><input type="checkbox" style="width:14px;margin-right:5px;margin-left:10px;" id="<?php echo $inactive_session->plf_id; ?>" name="en<?php echo $inactive_session->plf_id; ?>" class="<?php echo $inactive_session->plf_id; ?>" value="<?php echo $inactive_evaluation->eval_nom; ?>"/><?php echo ucfirst($inactive_evaluation->eval_nom); ?></div><?php } ?><?php } ?><div class='clear'></div><div style='margin-top:15px;'><a class='btn btn-info savee' role='button' id="en<?php echo $inactive_session->plf_id; ?>">Enregistrer</a></div><div class='clear'></div></div></div></form></td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
	</div>
</div>
<div id="add" class="row" style="margin:100px 0px;">
	<div class="col-md-6 col-md-offset-3" style="padding:0px;border: 1px solid rgba(0, 0, 0, 0.34);">
	<h3 style="font-size:20px;text-align:center;width:30%;margin:auto;color:#337ab7;"><span>Ajouter une session<hr/></span></h3>
		<br><br>

		<form action="<?php echo URL.'Session/Add'; ?>" method="post" class="form-horizontal session-form session-form-add">
			
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Référence <span style="color:#f55;">*</span></label>
				<div class="input-group">
						
				<input type="text" class="plf_ref" id="plf_ref" name="plf_ref" <?php if( isset($request['plf_ref']) ) { echo 'value="'.$request['plf_ref'].'"'; } else { echo "value=''" ;} ?> />
				<?php if( isset($request['plf_ref']) ) { echo '<span class="error">cette référence est déja existe</span>' ;} ?>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Titre <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input type="text" class="plf_title" name="plf_title" <?php if( isset($request['plf_title']) ) { echo 'value="'.$request['plf_title'].'"'; } ?>  required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Nom responsable  <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input type="text" class="plf_resp_firstname" name="plf_resp_firstname" <?php if( isset($request['plf_resp_firstname']) ) { echo 'value="'.$request['plf_resp_firstname'].'"'; } ?> autocomplete="off" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Prénom responsable  <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input type="text" class="plf_resp_lastname" name="plf_resp_lastname" <?php if( isset($request['plf_resp_lastname']) ) { echo 'value="'.$request['plf_resp_lastname'].'"'; } ?> autocomplete="off" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Type <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<select class="form-control plf_type" data-style="btn-primary" name="plf_type" required>
						<option value=""></option>
						<option value="evaluation">Evaluation</option>
						<option value="formation">Formation</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-5 col-sm-5 col-xs-5 control-label">Durée session <span style="color:#f55;">*</span></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<div class="input-daterange input-group" id="datepicker">
					    <input type="text" class="input-sm form-control session_start" name="session_start" />
					    <span class="input-group-addon">à</span>
					    <input type="text" class="input-sm form-control session_end" name="session_end" />
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-12 col-sm-12 col-xs-12">
						<input class="btn btn-default session-button-add" type="submit" name="add" value="Ajouter">
				</div>
			</div>
			<div class="contact-loading alert alert-info form-alert">
				<span class="message">Chargement...</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
			<div class="contact-success alert alert-success form-alert">
				<span class="message">Succès!</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
			<div class="contact-error alert alert-danger form-alert">
				<span class="message">Erreur!</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
		</form>
	</div>
</div>