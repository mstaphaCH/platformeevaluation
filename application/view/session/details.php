<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Planification session travail
			<small>Details d'une session</small>
		</h1>
		<ol class="breadcrumb pull-left" style="position: static !important;">
			<li><a href="#"><i class="fa fa-dashboard"></i> Planification session travail</a></li>
			<li class="active">Details d'une session</li>
		</ol>
	</section>
	<br><br>
	<section class="content">

		<div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title">Details de session</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
				</div>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Référence <span style="color:#f55;">*</span></label><br><br>
								<div class="input-group">
									<span class="input-group-addon session-addon"><?php echo $plf_reff; ?></span>
									<input type="text" class="form-control plf_ref" name="plf_ref" required value="<?php echo $plf_ref; ?>" disabled/>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label">Titre <span style="color:#f55;">*</span></label>
								<input type="text" class="form-control plf_title" name="plf_title" value="<?php echo $plf_title; ?>" required disabled/>
							</div>
							<div class="form-group">
								<label class="control-label">Nom responsable  <span style="color:#f55;">*</span></label>
								<input type="text" class="form-control plf_resp_firstname" name="plf_resp_firstname" autocomplete="off" value="<?php echo $plf_resp_firstname; ?>" disabled/>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Prénom responsable  <span style="color:#f55;">*</span></label>
								<input type="text" class="form-control plf_resp_lastname" name="plf_resp_lastname" autocomplete="off" value="<?php echo $plf_resp_lastname; ?>" required disabled/>
							</div>
							<div class="form-group">
								<label class="control-label">Type</label>
								<select class="form-control plf_type" data-style="btn-primary" name="plf_type" disabled>
									<option value="evaluation" <?php if(isset($plf_type) && $plf_type==='evaluation') echo 'selected'; ?>>Evaluation</option>
									<option value="formation" <?php if(isset($plf_type) && $plf_type==='formation') echo 'selected'; ?>>Formation</option>
								</select>
							</div>
							<div class="form-group">
								<label class="control-label">Durée session <span style="color:#f55;">*</span></label>
								<div class="input-daterange input-group" id="datepicker">
									<input type="text" class="input-sm form-control session_start" name="session_start" value="<?php if(isset($session_start)) echo $session_start; ?>" disabled/>
									<span class="input-group-addon">à</span>
									<input type="text" class="input-sm form-control session_end" name="session_end" value="<?php if(isset($session_end)) echo $session_end; ?>" disabled/>
								</div>
							</div>
							<div class="form-group">
								<input class="thisID" name="thisID" type="hidden" value="<?php echo $targetID; ?>" />
								<button class="btn btn-primary pull-right col-md-4 session-button-details" type="button">Retour</button>
							</div>
							<div class="contact-loading alert alert-info form-alert">
								<span class="message">Chargement...</span>
								<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
							</div>
							<div class="contact-success alert alert-success form-alert">
								<span class="message">Succès!</span>
								<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
							</div>
							<div class="contact-error alert alert-danger form-alert">
								<span class="message">Erreur!</span>
								<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
							</div>
						</div>
					</div>
				</div>
				<div class="box-footer">
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	var url = '<?php echo URL; ?>';
</script>