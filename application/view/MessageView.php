<?php

/**
* 
*/
class MessageView
{
	private $model;
	private $controller;

	function __construct($controller, $model) {
		$this->model = $model;
		$this->controller = $controller;
	}

	public function output() {
		$data = "Message page";
		require_once('Message.php');
	}
}
