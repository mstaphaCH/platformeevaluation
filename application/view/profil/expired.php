<div class="row user-list-profil">
	<div class="container">
      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0" >
   	    <div class="panel panel-info">
            <div class="panel-heading">
              	<h3 class="panel-title">
              		<?php echo $pageTitle; ?>
              	</h3>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-3 col-lg-3 " align="center">
                	<img alt="User Pic" src="<?php echo URL.'public/img/'.$this->picture; ?>" style="width:200px !important" class="img-circle img-responsive">
                </div>
                <div class=" col-md-9 col-lg-9"> 
					<table class="table table-striped table-bordered table-hover table-condensed user-profile" style="width:100% !important;">
						<tr><td><strong>Nom :</strong></td><td><?php echo $this->firstname; ?></td></tr>
						<tr><td><strong>Prénom :</strong></td><td><?php echo $this->lastname; ?></td></tr>
						<tr><td><strong>Login :</strong></td><td><?php echo $this->login; ?></td></tr>
						<tr><td><strong>Email :</strong></td><td><?php echo $this->email; ?></td></tr>
						<?php if ($this->profil === 2) { ?>
							<tr><td><strong>Telephone :</strong></td><td><?php echo $this->phone; ?></td></tr>	
						<?php } else { ?>
							<tr><td><strong>Site web :</strong></td><td><?php echo $this->website; ?></td></tr>
						<?php } ?>
						<tr><td><strong>Numéro:</strong></td><td><?php echo $this->address_number; ?></td></tr>
						<tr><td><strong>Rue :</strong></td><td><?php echo $this->address_street; ?></td></tr>
						<tr><td><strong>Ville :</strong></td><td><?php echo $this->address_city; ?></td></tr>
						<tr><td><strong>Pays :</strong></td><td><?php echo $this->address_country; ?></td></tr>
						<tr><td><strong>Date de création :</strong></td><td><?php echo $this->util_date_creation; ?></td></tr>
					</table>
                </div>
              </div>
            </div>
                <div class="panel-footer">
                	<a href="<?php echo URL . 'Profil/Inactive/' . $this->model->id; ?>" class="btn btn-primary">Désactiver</a>
                	<a role="button" class="btn btn-primary modprof">Modifier</a>
                	<?php if ($this->profil===0 || $this->profil===1 || $this->profil===2 || $this->profil===3 || $this->profil===4 || $this->profil===5): ?>
                		<a role="button" class="btn btn-primary paswd">Changer le mot de passe</a>
                	<?php endif; ?>
                	<br/><br/>
                	<?php if ($this->profil===0): ?>
                		<a role="button" class="btn btn-primary paswd_2">Ajouter/modifier un deuxième mot de passe</a>
                	<?php endif; ?>
                	<br/><br/><input type="file" class="btn-primary image-upload" size="60" id="image" value="Ajouter votre image" />
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0" >
   	    <div class="panel panel-info">
            <div class="panel-heading">
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-3 col-lg-3 " align="center"></div>
                <div class=" col-md-9 col-lg-9"> 
					<table class="table table-striped table-bordered table-hover table-condensed" style="width:100% !important;">
			
					</table>
                </div>
              </div>
            </div>
                <div class="panel-footer">
                </div>
            </div>
        </div>
      </div>
    </div>
</div>

<div class="row modprofform" style="margin-bottom:150px;">
	<div class="col-sm-6 col-sm-offset-3">
		<form action="<?php echo URL . 'Profil/Update'; ?>" method="post" class="form-horizontal">
			<div class="form-group">
				<label class="col-md-2 col-sm-2 col-xs-2 control-label">Nom</label>
				<div class="col-md-10 col-sm-10 col-xs-10">
					<input type="text" class="pfn" name="pfn" id="pfn" value="<?php echo $this->firstname; ?>" size="20" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 col-sm-2 col-xs-2 control-label">Prénom</label>
				<div class="col-md-10 col-sm-10 col-xs-10">
					<input type="text" class="pln" name="pln" id="pln" value="<?php echo $this->lastname; ?>" size="20" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 col-sm-2 col-xs-2 control-label">Login <span style="color:#f55;">*</span></label>
				<div class="col-md-10 col-sm-10 col-xs-10">
					<input type="text" class="pl" name="pl" id="pl" value="<?php echo $this->login; ?>" size="20" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 col-sm-2 col-xs-2 control-label">Email <span style="color:#f55;">*</span></label>
				<div class="col-md-10 col-sm-10 col-xs-10">
					<input type="email" class="pe" name="pe" id="pe" value="<?php echo $this->email; ?>" size="20" required disabled/>
				</div>
			</div>
			<?php if($this->profil===0 || $this->profil===1) { ?>
			<div class="form-group">
				<label class="col-md-2 col-sm-2 col-xs-2 control-label">Site Web</label>
				<div class="col-md-10 col-sm-10 col-xs-10">
					<input type="text" class="ps" name="ps" id="ps" value="<?php echo $this->website; ?>" size="20" />
				</div>
			</div>
			<?php }elseif($this->profil===2) { ?>
			<div class="form-group">
				<label class="col-md-2 col-sm-2 col-xs-2 control-label">Telephone</label>
				<div class="col-md-10 col-sm-10 col-xs-10">
					<input type="text" class="pt" name="pt" id="pt" value="<?php echo $this->phone; ?>" size="20" />
				</div>
			</div>
			<?php } ?>
			<div class="form-group">
				<label class="col-md-2 col-sm-2 col-xs-2 control-label">Numéro <span style="color:#f55;">*</span></label>
				<div class="col-md-2 col-sm-2 col-xs-2">
					<input type="text" class="an" name="an" id="an" onkeyup="clean(this)" onkeydown="clean(this)" oninvalid="check(this)" oninput="setCustomValidity('')" title="Numéro" value="<?php echo $this->address_number; ?>" maxlength="5" required/>
				</div>
				<label class="col-md-2 col-sm-2 col-xs-2 control-label" style="text-align:center;">Rue <span style="color:#f55;">*</span></label>
				<div class="col-md-6 col-sm-6 col-xs-6">
					<input type="text" class="pr" name="pr" id="pr" value="<?php echo $this->address_street; ?>" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 col-sm-2 col-xs-2 control-label">Ville <span style="color:#f55;">*</span></label>
				<div class="col-md-3 col-sm-3 col-xs-3">
					<input type="text" class="pv" name="pv" id="pv" value="<?php echo $this->address_city; ?>" required/>
				</div>
				<label class="col-md-2 col-sm-2 col-xs-2 control-label" style="text-align:center;">Pays <span style="color:#f55;">*</span></label>
				<div class="col-md-5 col-sm-5 col-xs-5">
					<input type="text" class="pp" name="pp" id="pp" value="<?php echo $this->address_country; ?>" required/>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<input name="thisID" type="hidden" value="<?php echo $this->model->id; ?>" />
					<input class="btn btn-default user-form" type="submit" name="submit" value="Modifier">
				</div>
			</div>
			<div class="contact-loading alert alert-info form-alert">
				<span class="message">Chargement...</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
			<div class="contact-success alert alert-success form-alert">
				<span class="message">Succès!</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
			<div class="contact-error alert alert-danger form-alert">
				<span class="message">Erreur!</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
		</form>
	</div>
</div>
<?php if ($this->profil === 0 || $this->profil === 1 || $this->profil === 2 || $this->profil === 3 || $this->profil === 4 || $this->profil === 5): ?>
<div class="row chgpaswd" style="margin-bottom:150px;">
	<div class="col-xs-6 col-xs-offset-3 passwd-form">
		<h1>Changer votre mot de passe</h1>
		<form action="<?php echo URL . 'Profil/changePassword'; ?>" method="post" class="form-horizontal user-profil passwd">
			<div class="form-group field-input">
				<input type="password" class="new_passwd label_better" name="new_passwd" placeholder="Nouveau mot de passe"/>
			</div>
			<div class="form-group field-input">
				<input type="password" class="new_repasswd label_better" name="new_repasswd" placeholder="Resaisir nouveau mot de passe"/>
			</div>
			<div class="form-group field-button">
					<input name="thisID" class="thisID" type="hidden" value="<?php echo $this->model->id; ?>" />
					<button class="button pull-right solid-button add 1st-passwd" type="button">Enregistrer</button>
					<button class="button pull-right solid-button add 1st-passwd return" type="button">Retour</button>
			</div>
			<div class="contact-loading alert alert-info form-alert">
				<span class="message">Chargement...</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
			<div class="contact-success alert alert-success form-alert">
				<span class="message">Succès!</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
			<div class="contact-error alert alert-danger form-alert">
				<span class="message">Erreur!</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
		</form>
	</div>
</div>	
<?php endif; ?>

<?php if ($this->profil === 0): ?>
<div class="row chgpaswd_2" style="margin-bottom:150px;">
	<div class="col-xs-6 col-xs-offset-3 passwd-form">
		<h1>Ajouter un deuxième mot de passe</h1>
		<form action="<?php echo URL . 'Profil/changePassword'; ?>" method="post" class="form-horizontal user-profil passwd_2">
			<div class="form-group field-input">
				<input type="password" class="new_passwd_2 label_better" name="new_passwd_2" placeholder="Nouveau mot de passe"/>
			</div>
			<div class="form-group field-input">
				<input type="password" class="new_repasswd_2 label_better" name="new_repasswd_2" placeholder="Resaisir nouveau mot de passe"/>
			</div>
			<div class="form-group field-button">
					<input name="thisID" class="thisID" type="hidden" value="<?php echo $this->model->id; ?>" />
					<button class="button pull-right solid-button add 2nd-passwd" type="button">Ajouter</button>
					<button class="button pull-right solid-button add 2nd-passwd return" type="button">Retour</button>
			</div>
			<div class="contact-loading alert alert-info form-alert">
				<span class="message">Chargement...</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
			<div class="contact-success alert alert-success form-alert">
				<span class="message">Succès!</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
			<div class="contact-error alert alert-danger form-alert">
				<span class="message">Erreur!</span>
				<button type="button" class="close" data-hide="alert" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
		</form>
	</div>
</div>	
<?php endif; ?>
